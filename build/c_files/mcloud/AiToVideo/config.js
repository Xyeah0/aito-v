window.AppConfig = {
    modulePath:'MODULE_PATH',
    themeColor:'#THEME_COLOR',
    appType:'APP_TYPE',
	containerBg:'Bcak_Ground',
    helpUrl:'YUMI_HELP_URL',//帮助文档地址
	canUpload:true,
    uploadLimit:100,//M 文件大小
    uploadSplit:10,//分片文件大小
    favicon:'FAVICON_URL',
    microEditUrl:'MICROEDIT_URL',
    showProofreadBtn:SHOW_PROOFREAD_BTN,//是否显示文本校对按钮
    showExamineBtn:SHOW_EXMAINE_BTN,//是否显示审核按钮
    showHighlightBtn:SHOW_HIGHLIGHT_BTN,//是否显示高亮按钮
    showExpandBtn:SHOW_EXPAND_BTN,//是否显示扩写按钮
    showAudioDubBtn:SHOW_AUDIODUB_BTN,//是否显示语音合成按钮
    autoSaveTime:AUTO_SAVE_TIME,//自动保存时间
    projectName:'PROJECT_NAME',//项目名称
}

let pathname = window.location.pathname;
if(pathname.indexOf('/yumi') == 0){
    window.AppConfig.modulePath = pathname.split('/').slice(0,2).join('/');
}
