##################begin 配置区#####################
# 记录日志
DIR=/docker-entrypoint.d
# 配置文件
AUDIOEDIT_CONFIG_FILE=/mcloud/AiToVideo/config.js
AiToVideo_NGINX_FILE=/etc/nginx/conf.d/AiToVideo.conf
##################end 配置区#######################

###################################################
# AUDIOEDIT_CONFIG_FILE
function AUDIOEDIT_CONFIG_FILE_exsit()
{
env | grep "THEME_COLOR" || export THEME_COLOR="on"
    sed -i "s#THEME_COLOR#$THEME_COLOR#g" $AUDIOEDIT_CONFIG_FILE
    echo "THEME_COLOR replace $THEME_COLOR -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log
	
env | grep "APP_TYPE" || export APP_TYPE="on"
    sed -i "s#APP_TYPE#$APP_TYPE#g" $AUDIOEDIT_CONFIG_FILE
    echo "APP_TYPE replace $APP_TYPE -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log
	
env | grep "Bcak_Ground" || export Bcak_Ground="on"
    sed -i "s#Bcak_Ground#$Bcak_Ground#g" $AUDIOEDIT_CONFIG_FILE
    echo "Bcak_Ground replace $Bcak_Ground -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "MODULE_PATH" || export MODULE_PATH=""
    sed -i "s#MODULE_PATH#$MODULE_PATH#g" $AUDIOEDIT_CONFIG_FILE
    echo "MODULE_PATH replace $MODULE_PATH -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log	

env | grep "YUMI_HELP_URL" || export YUMI_HELP_URL="http://showdoc.chinamcloud.com/web/#/p/c9b5fe1c97f98c4514bf57a4622597de"
    sed -i "s+YUMI_HELP_URL+$YUMI_HELP_URL+g" $AUDIOEDIT_CONFIG_FILE
    echo "YUMI_HELP_URL replace $YUMI_HELP_URL -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "FAVICON_URL" || export FAVICON_URL=""
    sed -i "s+FAVICON_URL+$FAVICON_URL+g" $AUDIOEDIT_CONFIG_FILE
    echo "FAVICON_URL replace $FAVICON_URL -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "MICROEDIT_URL" || export MICROEDIT_URL=""
    sed -i "s+MICROEDIT_URL+$MICROEDIT_URL+g" $AUDIOEDIT_CONFIG_FILE
    echo "MICROEDIT_URL replace $MICROEDIT_URL -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "SHOW_PROOFREAD_BTN" || export SHOW_PROOFREAD_BTN="true"
    sed -i "s+SHOW_PROOFREAD_BTN+$SHOW_PROOFREAD_BTN+g" $AUDIOEDIT_CONFIG_FILE
    echo "SHOW_PROOFREAD_BTN replace $SHOW_PROOFREAD_BTN -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "SHOW_EXMAINE_BTN" || export SHOW_EXMAINE_BTN="true"
    sed -i "s+SHOW_EXMAINE_BTN+$SHOW_EXMAINE_BTN+g" $AUDIOEDIT_CONFIG_FILE
    echo "SHOW_EXMAINE_BTN replace $SHOW_EXMAINE_BTN -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "SHOW_HIGHLIGHT_BTN" || export SHOW_HIGHLIGHT_BTN="true"
    sed -i "s+SHOW_HIGHLIGHT_BTN+$SHOW_HIGHLIGHT_BTN+g" $AUDIOEDIT_CONFIG_FILE
    echo "SHOW_HIGHLIGHT_BTN replace $SHOW_HIGHLIGHT_BTN -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "SHOW_EXPAND_BTN" || export SHOW_EXPAND_BTN="true"
    sed -i "s+SHOW_EXPAND_BTN+$SHOW_EXPAND_BTN+g" $AUDIOEDIT_CONFIG_FILE
    echo "SHOW_EXPAND_BTN replace $SHOW_EXPAND_BTN -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "SHOW_AUDIODUB_BTN" || export SHOW_AUDIODUB_BTN="true"
    sed -i "s+SHOW_AUDIODUB_BTN+$SHOW_AUDIODUB_BTN+g" $AUDIOEDIT_CONFIG_FILE
    echo "SHOW_AUDIODUB_BTN replace $SHOW_AUDIODUB_BTN -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "AUTO_SAVE_TIME" || export AUTO_SAVE_TIME="60000"
    sed -i "s+AUTO_SAVE_TIME+$AUTO_SAVE_TIME+g" $AUDIOEDIT_CONFIG_FILE
    echo "AUTO_SAVE_TIME replace $AUTO_SAVE_TIME -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

env | grep "PROJECT_NAME" || export PROJECT_NAME=""
    sed -i "s+PROJECT_NAME+$PROJECT_NAME+g" $AUDIOEDIT_CONFIG_FILE
    echo "PROJECT_NAME replace $PROJECT_NAME -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

# env | grep "AiToVideo_SVIP" || export AiToVideo_SVIP=""
#     sed -i "s+AiToVideo_SVIP+$AiToVideo_SVIP+g" $AUDIOEDIT_CONFIG_FILE
#     echo "AiToVideo_SVIP replace $AiToVideo_SVIP -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

# env | grep "AiToVideo_AIGC" || export AiToVideo_AIGC=""
#     sed -i "s+AiToVideo_AIGC+$AiToVideo_AIGC+g" $AUDIOEDIT_CONFIG_FILE
#     echo "AiToVideo_AIGC replace $AiToVideo_AIGC -> $AUDIOEDIT_CONFIG_FILE" >> $DIR/change.log

}
function AiToVideo_NGINX_FILE_exsit()
{
env | grep "AiToVideo_SVIP" || export AiToVideo_SVIP=""
    sed -i "s#AiToVideo_SVIP#$AiToVideo_SVIP#g" $AiToVideo_NGINX_FILE
    echo "AiToVideo_SVIP replace $AiToVideo_SVIP -> $AiToVideo_NGINX_FILE" >> $DIR/change.log
	
env | grep "Appsocket_SVIP" || export Appsocket_SVIP=""
    sed -i "s#Appsocket_SVIP#$Appsocket_SVIP#g" $AiToVideo_NGINX_FILE
    echo "Appsocket_SVIP replace $Appsocket_SVIP -> $AiToVideo_NGINX_FILE" >> $DIR/change.log

env | grep "Appsocket_AIGC" || export Appsocket_AIGC=""
    sed -i "s#Appsocket_AIGC#$Appsocket_AIGC#g" $AiToVideo_NGINX_FILE
    echo "Appsocket_AIGC replace $Appsocket_AIGC -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	
	
env | grep "AiToVideo_AIGC" || export AiToVideo_AIGC="AiToVideo_AIGC"
    sed -i "s#AiToVideo_AIGC#$AiToVideo_AIGC#g" $AiToVideo_NGINX_FILE
    echo "AiToVideo_AIGC replace $AiToVideo_AIGC -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	

env | grep "AiDrawImage_PIC" || export AiDrawImage_PIC="AiDrawImage_PIC"
    sed -i "s#AiDrawImage_PIC#$AiDrawImage_PIC#g" $AiToVideo_NGINX_FILE
    echo "AiDrawImage_PIC replace $AiDrawImage_PIC -> $AiToVideo_NGINX_FILE" >> $DIR/change.log

env | grep "TLSOURCE_URL" || export TLSOURCE_URL=""
    sed -i "s#TLSOURCE_URL#$TLSOURCE_URL#g" $AiToVideo_NGINX_FILE
    echo "TLSOURCE_URL replace $TLSOURCE_URL -> $AiToVideo_NGINX_FILE" >> $DIR/change.log
env | grep "MediaCut_URL" || export MediaCut_URL=""
    sed -i "s#MediaCut_URL#$MediaCut_URL#g" $AiToVideo_NGINX_FILE
    echo "MediaCut_URL replace $MediaCut_URL -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	
	
    
env | grep "NODECONVERT_URL" || export NODECONVERT_URL=""
    sed -i "s#NODECONVERT_URL#$NODECONVERT_URL#g" $AiToVideo_NGINX_FILE
    echo "NODECONVERT_URL replace $NODECONVERT_URL -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	
	
env | grep "APP_SPIDERUPLOAD" || export APP_SPIDERUPLOAD=""
    sed -i "s#APP_SPIDERUPLOAD#$APP_SPIDERUPLOAD#g" $AiToVideo_NGINX_FILE
    echo "APP_SPIDERUPLOAD replace $APP_SPIDERUPLOAD -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	

env | grep "SPIDER_MATERIAL" || export SPIDER_MATERIAL=""
    sed -i "s#SPIDER_MATERIAL#$SPIDER_MATERIAL#g" $AiToVideo_NGINX_FILE
    echo "SPIDER_MATERIAL replace $SPIDER_MATERIAL -> $AiToVideo_NGINX_FILE" >> $DIR/change.log	
	
}
############begin 执行#################
# 判断文件是否存在
ls $AUDIOEDIT_CONFIG_FILE >/dev/null 2>&1 && AUDIOEDIT_CONFIG_FILE_exsit || echo "$AUDIOEDIT_CONFIG_FILE 不存在" >> $DIR/change.log
ls $AiToVideo_NGINX_FILE >/dev/null 2>&1 && AiToVideo_NGINX_FILE_exsit || echo "$AiToVideo_NGINX_FILE 不存在" >> $DIR/change.log
##############end 执行#################
