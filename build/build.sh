#!/bin/bash

AIDISPATCH_GIT_DESC=$(git describe --abbrev=0 --tags) 
AIDISPATCH_GIT_COMMIT=$(git log -1 --format=%h)  



DOCKER_IAMGE=cmc-tcr.tencentcloudcr.com/cmc/aidispatch:${AIDISPATCH_GIT_DESC}-${AIDISPATCH_GIT_COMMIT}


# 开始构建时钉钉通知 
dingOnStart(){
    if test -n "$DINGDING_EP";
    then
        curl -v -X POST "$DINGDING_EP" -H 'Content-Type: application/json' -d '{
            "msgtype": "markdown", 
            "markdown": {
                "title" : "Start", 
                    "text"  : "![](https://gitlab.chinamcloud.com/dailindong/imgs/raw/master/ci-build/start_build.jpg)\n\n Start building...\n\n\n > `'${DOCKER_IAMGE}'`"
            },
            "at": {
                "atMobiles": [],
                "isAtAll": false 
            }
        }'
    fi
}


# 构建成功时候通知钉钉 
dingOnSuccess(){
    if test -n "$DINGDING_EP";
    then
        curl -v -X POST "$DINGDING_EP" -H 'Content-Type: application/json' -d '{
            "msgtype": "markdown", 
            "markdown": {
                "title" : "Success", 
                    "text"  : "![](https://gitlab.chinamcloud.com/dailindong/imgs/raw/master/ci-build/success.jpg)\n\n Succeeded in building...\n\n\n > `'${DOCKER_IAMGE}'`"
            },
            "at": {
                "atMobiles": [],
                "isAtAll": false 
            }
        }'
    fi
}

# 构建失败的时候通知钉钉 
dingOnFailure(){
    if test -n "$DINGDING_EP";
    then
        curl -v -X POST "$DINGDING_EP" -H 'Content-Type: application/json' -d '{
            "msgtype": "markdown", 
            "markdown": {
                "title" : "Failed", 
                    "text"  : "![](https://gitlab.chinamcloud.com/dailindong/imgs/raw/master/ci-build/failure.jpg)\n\n Failed to build...\n\n\n > `'${DOCKER_IMAGE}'`"
            },
            "at": {
                "atMobiles": [],
                "isAtAll": false 
            }
        }'
    fi
}

# confirm function from https://stackoverflow.com/questions/3231804/in-bash-how-to-add-are-you-sure-y-n-to-any-command-or-alias
# 
confirm() {
    if test -n "$IN_JENKINS";
    then
        return 0;
    fi
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}



dingOnStart;

docker build  -f Dockerfile -t  ${DOCKER_IAMGE} .

if [ $? -eq 0 ]; then
    echo "pushing  ${DOCKER_IAMGE} ?"  && 
    confirm && docker push   ${DOCKER_IAMGE} &&  
    dingOnSuccess
else
    dingOnFailure
    exit 255;
fi

