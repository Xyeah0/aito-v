# postervideoedit

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# 版本记录

##  2021.09.02 升级说明
### 1. 基本信息
    version: 1.0.0
### 2. 修改Bug
### 3. 新增功能
    1.初始发版