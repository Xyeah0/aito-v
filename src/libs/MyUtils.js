

class MyUtils {
    constructor() {

    }
    /**防抖函数 */
    debounce(fn, delay) {
        let timer = null;
        return function (...args) {
            let context = this;
            if (timer) clearTimeout(timer);
            timer = setTimeout(() => {
                fn.apply(context, args);
            }, delay)

        }
    }

    /**节流函数 */
    throttle(fn, delay) {
        let startTime = 0;
        return function (...args) {
            let context = this;
            let nowTime = Date.now();
            if (nowTime - startTime > delay) {
                fn.apply(context, args);
                startTime = nowTime;
            }
        }
    }

    /**普通字符转为转意符 */
    html2Escape(sHtml) {
        return sHtml.replace(/[<>&"]/g, function (c) { return { '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;' }[c]; });
    }

    /**转意符换成普通字符 */
    escape2Html(str) {
        var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
        return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) { return arrEntities[t]; });
    }

    /**文稿排列方式切换 */
    arrangementChange(arrangementType, contents) {
        let arrangementData = [];
        let texts = [];
        let remainImages = [];
        if (arrangementType === 0) {
            //文字在图片上方
            for (let i = 0; i < contents.length; i++) {
                if (contents[i].type === "image") {
                    if (texts.length === 0) {
                        remainImages.push(contents[i]);
                    } else {
                        arrangementData.push({ imgDesc: contents[i].imgDesc, imageUrl: contents[i].url, texts: texts });
                        texts = [];
                    }
                } else if (contents[i].type === "text") {
                    texts.push(contents[i]);
                    if (i === contents.length - 1) {
                        let pushObj = { imageUrl: contents[i].url || "", texts: texts };
                        arrangementData.push(pushObj);
                        texts = [];
                    }
                }
            }
        } else if (arrangementType === 1) {
            let lastImageUrl = "";
            let lastAnalysisValue = null;
            //文字在图片下方
            for (let i = 0; i < contents.length; i++) {
                if (contents[i].type === "image") {
                    if (texts.length > 0) {
                        let obj = { imageUrl: lastImageUrl, texts: texts };
                        if (lastAnalysisValue) obj.imgDesc = lastAnalysisValue;
                        arrangementData.push(obj);
                        texts = [];
                        if (i === contents.length - 1) {
                            remainImages.push(contents[i].url);
                        }
                    } else {
                        if (lastImageUrl) {
                            remainImages.push(lastImageUrl);
                        }
                    }

                    lastImageUrl = contents[i].url;
                    lastAnalysisValue = contents[i].imgDesc;


                } else if (contents[i].type === "text") {
                    texts.push(contents[i]);
                    if (i === contents.length - 1) {
                        let obj = { imageUrl: lastImageUrl, texts: texts };
                        if (lastAnalysisValue) obj.imgDesc = lastAnalysisValue;
                        arrangementData.push(obj);
                        texts = [];
                    }

                }
            }

        }
        // console.log("arrangementData", arrangementData);
        return arrangementData;
    }
    proxyImageUrl(url) {
        return window.AppConfig.modulePath + "/nodeconvert/proxyImg?url=" + url;
    }
    /**每个段落保证50字以上,无标点加句号 */
    combiningContent(contents) {
        for (let i = 0; i < contents.length; i++) {
            if (contents[i].type === "text") {
                let textContent = contents[i].content;
                let textContentLen = textContent.length;
                if (textContentLen < 50) {
                    for (let j = i + 1; j < contents.length; j++) {
                        if (contents[j].type === "text") {
                            if (!(textContent.slice(-1).match(/[\!！\?？\.。,，]/))) {
                                contents[i].content += "。";
                            }
                            contents[i].content += contents[j].content;
                            contents.splice(j, 1);
                            if (contents[i].content.length >= 50) {
                                break;
                            }

                        }
                    }

                }

            }
        }
        // console.log(contents)
        return contents;

    }
}

export default new MyUtils;