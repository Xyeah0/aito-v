import MD5 from 'js-md5';
// import MediaInfo from './MediaInfo.js';
class SelectFile{
    constructor(){
        this.inputfile = document.createElement('input');
        this.inputfile.type = 'file';

        // this.mediaInfo = new MediaInfo();

        this.video = document.createElement('video');
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.mediainfo = null;

        MediaInfo({}, (mediainfo) => {
            this.mediainfo = mediainfo;
        })
    }

    /**选择 */
    select(obj){
        this.inputfile.accept = obj.accept;
        this.inputfile.value = '';
        this.inputfile.click();
        console.log('this.inputfile.accept',this.inputfile.accept)
        return new Promise((resolve,reject) =>{
            this.inputfile.onchange = (event)=>{
                let files = event.target.files;
                if(files.length>0){
                    let file = files[0];
                    resolve(file);
                }else{
                    reject(null);
                }
            }
        })
    }

    /** 读取文件信息 同一时间只能读取一个文件*/
    readFileInfo(file){
        return new Promise((resolve,reject) =>{
            // console.log('Working…',this.mediainfo)
        
            const getSize = () => file.size
        
            const readChunk = (chunkSize, offset) =>
                new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = (event) => {
                    if (event.target.error) {
                        reject(event.target.error)
                    }
                    resolve(new Uint8Array(event.target.result))
                }
                reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize))
            })
        
            this.mediainfo.analyzeData(getSize, readChunk)
                .then((result) => {
                    // output.value = result
                    // console.log('result',result)
                    let track = result.media.track;
                    let info = {
                        rawInfo:{}
                    };
                    for(let i=0;i<track.length;i++){
                        let item = track[i];
                        if(item['@type'] == 'General'){
                            Object.assign(info.rawInfo,item);
                            info.type = item.Format;
                            if(info.type == 'MPEG-4'){
                                info.type = 'MP4';
                                info.duration = parseFloat(item.Duration);
                            }else if(info.type == 'MPEG Audio'){
                                info.type = 'MP3';
                            }
                            info.type = info.type.toLocaleLowerCase();
                        }else if(item['@type'] == 'Image'){
                            if(!info.mediaType){
                                info.mediaType = 'Image';
                            }
                            Object.assign(info.rawInfo,item);
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            
                        }else if(item['@type'] == 'Video'){
                            if(!info.mediaType){
                                info.mediaType = 'Video';
                            }
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            if(!info.duration){
                                info.duration = parseFloat(item.Duration);
                            }
                            info.frameRate = parseInt(item.FrameRate);
                            info.frameCount = parseInt(item.FrameCount);
                            info.videoFormat = item.Format;
                            info.videoBitRate = parseInt(item.BitRate);
                            info.mediaType = 'Video';
                            info.rawInfo.video = item;
                        }else if(item['@type'] == 'Audio'){
                            if(!info.mediaType){
                                info.mediaType = 'Audio';
                            }
                            info.channel = parseInt(item.Channels);
                            info.audioBitRate = parseInt(item.BitRate);
                            info.audioSamplingRate = parseInt(item.SamplingRate);
                            info.rawInfo.audio = item;
                        }
                    }
                    resolve({file:file,info:info});
                })
                .catch((error) => {
                    resolve({file:file,info:null});
                    console.log(`An error occured:\n${error.stack}`)
                })
        })
    }

    // /** 读取文件信息 */
    // readFileInfo(file){
    //     return new Promise((resolve,reject) =>{
    //         //读取文件信息
    //         let reader = new FileReader();
    //         reader.onload = (result) => {
    //             let info = this.mediaInfo.getInfo(reader.result);
    //             console.log('info...',info)
    //             if(!info.info){
    //                 let arr = file.name.split('.');
    //                 let typeName = arr[arr.length-1];
    //                 console.log('重试强类型',typeName)
    //                 info = this.mediaInfo.getInfo(reader.result,typeName);
    //             }
    //             if(info){
    //                 resolve({file:file,info:info});
    //             }else{
    //                 reject(info);
    //             }
    //         }
    //         reader.readAsArrayBuffer(file);
    //     })
    // }

    /** 获得video缩略图 */
    getThumbImg(objUrl){
        return new Promise((resolve,reject) =>{

            this.video.onseeked = ()=>{
                // this.canvas.width = 256;
                this.canvas.width = parseInt(144 * (this.video.videoWidth / this.video.videoHeight));
                this.canvas.height = 144;
                //这种是默认拉伸铺满
                this.ctx.drawImage(this.video,0,0,this.video.videoWidth,this.video.videoHeight,0,0,this.canvas.width,this.canvas.height);
                resolve(this.canvas.toDataURL('image/png'));
            }
            this.video.onerror = ()=>{
                reject(null);
            }
            this.video.src = objUrl;
            this.video.currentTime = 0.01;
        })
        
    }
}

export default SelectFile;