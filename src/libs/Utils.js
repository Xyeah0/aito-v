/**
 * Created by tangshanghai on 2017/3/1.
 */
import MyMediaInfo from './MediaInfo';//自己开发的，有BUG

class Utils{
    constructor(){
        this.dateObj = new Date();
        this.myMediaInfo = new MyMediaInfo();
        this.mediainfo = null;
        MediaInfo({}, (mediainfo) => {
            this.mediainfo = mediainfo;
        })
    }

   
    /**
     * 传入秒数或毫秒，取得22：22格式
     */
    getFormatSec(sec,isMs){
        if(isMs){
            sec /= 1000;
        }
        let m = parseInt(sec / 60);
        let s = parseInt(sec % 60);
        let mStr = String(m);
        let sStr = String(s);
        if (mStr.length == 1) {
            mStr = "0" + mStr;
        }
        if (sStr.length == 1) {
            sStr = "0" + sStr;
        }
        return mStr + ":" + sStr;
    }
    /**
     * 传入秒，取得22：22：22:22格式最后一位代表帧数
     */
    getFormatSecond(sec){
        
        let h = parseInt(sec/3600);
        let m = parseInt(sec/60%60);
        let s = parseInt(sec%60);
        let ms = ~~(sec*1000%1000/40);
        
        let result = (h<10?"0"+h:h)+":"+(m<10?"0"+m:m)+":"+(s<10?"0"+s:s)+":"+this.PrefixInteger(ms,2);
        return result;
    }
    /**
     * 传入毫秒，取得22：22：22格式
     */
    getFormatHMS(_ms){
        let sec = _ms/1000;
        
        let h = Math.floor(sec/3600);
        let m = parseInt(sec/60%60);
        let s = parseInt(sec%60);
        //let ms = parseInt(_ms%1000);
        
        let result = (h<10?"0"+h:h)+":"+(m<10?"0"+m:m)+":"+(s<10?"0"+s:s);
        return result;
    }

    //获得年月日
	getFormatYYMMDD(gmt){
		this.dateObj.setTime(gmt);
		let y = this.dateObj.getFullYear();
		let m = this.dateObj.getMonth()+1;
		let d = this.dateObj.getDate();
		let str = "";
		str += y;
		str += (m < 10 ? "0" + m : m);
		str += (d < 10 ? "0" + d : d);
		return str;
	}
	
	//获得yyyy-mm-dd hh:mm:ss
	getFormatYYMMDD2(gmt){
		this.dateObj.setTime(gmt);
		let y = this.dateObj.getFullYear();
		let m = this.dateObj.getMonth()+1;
		let d = this.dateObj.getDate();
		let str = y+"-"+(m < 10 ? "0" + m : m)+"-"+(d < 10 ? "0" + d : d);
		let timeStr = this.dateObj.toTimeString();
        timeStr = timeStr.substr(0,8);
		return str+" "+timeStr;
    }
    
    //获得yyyy-mm-dd hh:mm:ss:ff
	getFormatYYMMDD3(gmt){
		this.dateObj.setTime(gmt);
		let y = this.dateObj.getFullYear();
		let m = this.dateObj.getMonth()+1;
		let d = this.dateObj.getDate();
		let str = y+"-"+(m < 10 ? "0" + m : m)+"-"+(d < 10 ? "0" + d : d);
		let timeStr = this.dateObj.toTimeString();
        timeStr = timeStr.substr(0,8);
        let msStr = Math.floor(this.dateObj.getMilliseconds()/40);
        if(msStr < 10) msStr = '0'+msStr;
		return str+" "+timeStr+':'+msStr;
    }
    
    //获得hh:mm:ss:ff
	getFormatYYMMDD4(gmt){
		this.dateObj.setTime(gmt);
		// let y = this.dateObj.getFullYear();
		// let m = this.dateObj.getMonth()+1;
		// let d = this.dateObj.getDate();
		// let str = y+"-"+(m < 10 ? "0" + m : m)+"-"+(d < 10 ? "0" + d : d);
		let timeStr = this.dateObj.toTimeString();
        timeStr = timeStr.substr(0,8);
        let msStr = Math.floor(this.dateObj.getMilliseconds()/40);
        if(msStr < 10) msStr = '0'+msStr;
		return timeStr+':'+msStr;
	}


    // 为数字前位补0，返回字符串
    PrefixInteger(num, length) {
        return (Array(length).join('0') + num).slice(-length);
    }

    /**
     * 获取字符的byte长度
     */
    getByteLen (str) {  
        let len = 0;  
        for (let i=0; i<str.length; i++) {
            if ((str.charCodeAt(i) & 0xff00) != 0)
                len ++;
            len ++;  
        }  
        return len;  
    }

    GUID(){
        // let time = new Date();
        // time = String(time.getTime());
        // let str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        // let len = str.length;
        // let len2 = 32-time.length;
        // let uid = "";
        // let spliceint = Math.floor(Math.random()*len2);
        // for(let i=0;i<len2;i++){
        //     //在uid 随机位置插入时间值 进一步增加随机性
        //     if(i == spliceint)
        //     {
        //         uid += time;
        //     }
        //     uid += str.charAt(Math.floor(Math.random()*len));
        // }
        // return uid;
        let t = (new Date).getTime();
        return "xxxxxxxx-xxxx-yxxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(e){
            let n=(t+16*Math.random())%16|0;
            t=Math.floor(t/16);
            return ("x"==e?n:3&n|8).toString(16);
        });
    }
    GUID32(){
        let t = (new Date).getTime();
        return "xxxxxyxxxxxyxxxxxyxxxxxyxxxxxyxx".replace(/[xy]/g,function(e){
            let n=(t+16*Math.random())%16|0;
            t=Math.floor(t/16);
            return ("x"==e?n:3&n|8).toString(16);
        });
    }
    GUIDTOGUID(id,random=''){
        if(!id) return '';
        let rawfeature = btoa(id);//原特征码
        rawfeature = rawfeature.substr(rawfeature.length-12,12);
        let salt = random || id;//随机因子
        salt = salt.substr(14,9);
        return id.substr(0,14)+salt+'-'+rawfeature;
        // let newstr = random || btoa(id);
        // return id.substr(0,id.length-12)+newstr.substr(newstr.length-12,12);
    }

    //返回body最大的z-index
    getMaxZindex() {
        let elementObj = document.body;
        //取得容器节点下第一层所有节点
        let childNodes = elementObj.children || elementObj.childNodes;
        let zIndex = 0;
        for (let i = 0; i < childNodes.length; i++) {
            let node = childNodes[i];
            let ti1 = parseInt(this.getClass(node, "z-index"));
            let ti2 = parseInt(node.style.zIndex);
            let ti = ti2 || ti1;
            if (isNaN(ti)) continue;
            if (ti > zIndex) zIndex = ti;
        }
        zIndex += 10;
        return zIndex;
    }

    getClass(obj, name) {
        if (obj.currentStyle) {
            return obj.currentStyle[name]; //IE下获取非行间样式
        } else {
            return getComputedStyle(obj, false)[name]; //FF、Chorme下获取非行间样式
        }
    }

    dataURLtoBlob(dataurl) {
        let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
    }

    /**对象转URL参数 */
    objectToURLParam (obj,isAsk=true){
        let str = '';
        for(let info in obj){
            str += '&'+info+'='+obj[info];
        }
        if(isAsk){
            str = str.replace('&','?');
        }
        return str;
    }

    /** 传入高宽 返回码率标识 */
    getBitRateMark(w=0,h=0){
        let mark = 'SD';
        // (w === 3840 && h === 2160) || (w === 4096 && h === 2160) || (w === 4096 && h === 2304)
        if(w >= 3840 || h >= 2000){
            mark = '4K';
        }else if(w === 2048 && h === 1080){
            mark = '2K';
        }else if((w === 1920 && h === 1080) || (w === 1280 && h === 720)|| (w === 1440 && h === 1080)){
            mark = 'HD';
        }else if(h===144 || h===288 || h===360 || h===480||h==576){
            mark = 'SD';
        }else{
            if((w/h).toFixed(2) === '1.77'){
                if(h >= 2160){
                    mark = '4K';
                }else if(h >= 1080){
                    mark = 'HD';
                }else{
                    mark = 'SD';
                }
            }else{
                mark = 'SD';
            }
        }
        return mark;
    }

    /** 传入地址进行拼接 */
    getURLSplicing(arr){
        const pathReg = new RegExp(/^[a-zA-Z]:/);
        const httpReg = new RegExp(/^http:|^https:/);
        let strEnd = arr[arr.length - 1];
        if (pathReg.test(strEnd) || httpReg.test(strEnd) || strEnd.indexOf('data:image/png') === 0) {
            return strEnd;
        }
        
        let str = arr.join('/');
        str = str.replace(/\\+/g,'/');
        let index = str.indexOf(':/')+3;
        str = str.slice(0,index)+str.slice(index).replace(/\/+/g,'/');
        return str;
    }

    /** 传入url地址，获得文件mediaInfo */
    getMediaInfoOfUrl(url,isMy = true){
        return new Promise((resolve,reject)=>{

            let xhr = new XMLHttpRequest(); //建立一个请求
            xhr.open('GET', url, true); //配置好请求类型，文件路径等
            xhr.responseType = 'arraybuffer'; //配置数据返回类型
            // 一旦获取完成，对音频进行进一步操作，比如解码
            xhr.onreadystatechange = () => {
                // let audioContext = new AudioContext() || new WebkitAudioContext() || new mozAudioContext() || new msAudioContext();
                if(xhr.readyState === 4){
                    if((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304){
                        if(isMy){
                            resolve(this.myMediaInfo.getInfo(xhr.response));
                        }else{
                            resolve(xhr.response);
                        }
                    }else{
                        reject(null);
                    }
                }
            }
            
            xhr.send();
        })
    }

    /** 读取二进制文件信息*/
    readArrayBufferInfo(arrayBuffer){
        return new Promise((resolve,reject) =>{
            // console.log('Working…',this.mediainfo)
        
            const getSize = () => arrayBuffer.byteLength
        
            const readChunk = (chunkSize, offset) =>
                new Promise((resolve, reject) => {
                    resolve(new Uint8Array(arrayBuffer.slice(offset, offset + chunkSize)))
                // const reader = new FileReader()
                // reader.onload = (event) => {
                //     if (event.target.error) {
                //         reject(event.target.error)
                //     }
                //     resolve(new Uint8Array(event.target.result))
                // }
                // reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize))
            })
        
            this.mediainfo.analyzeData(getSize, readChunk)
                .then((result) => {
                    // output.value = result
                    // console.log('result',result)
                    let track = result.media.track;
                    let info = {
                        rawInfo:{}
                    };
                    for(let i=0;i<track.length;i++){
                        let item = track[i];
                        if(item['@type'] == 'General'){
                            Object.assign(info.rawInfo,item);
                            info.type = item.Format;
                            if(info.type == 'MPEG-4'){
                                info.type = 'MP4';
                                info.duration = parseFloat(item.Duration);
                            }else if(info.type == 'MPEG Audio'){
                                info.type = 'MP3';
                            }
                            info.type = info.type.toLocaleLowerCase();
                        }else if(item['@type'] == 'Image'){
                            if(!info.mediaType){
                                info.mediaType = 'Image';
                            }
                            Object.assign(info.rawInfo,item);
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            
                        }else if(item['@type'] == 'Video'){
                            if(!info.mediaType){
                                info.mediaType = 'Video';
                            }
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            if(!info.duration){
                                info.duration = parseFloat(item.Duration);
                            }
                            info.frameRate = parseInt(item.FrameRate);
                            info.frameCount = parseInt(item.FrameCount);
                            info.videoFormat = item.Format;
                            info.videoBitRate = parseInt(item.BitRate);
                            info.mediaType = 'Video';
                            info.rawInfo.video = item;
                        }else if(item['@type'] == 'Audio'){
                            if(!info.mediaType){
                                info.mediaType = 'Audio';
                            }
                            info.channel = parseInt(item.Channels);
                            info.audioBitRate = parseInt(item.BitRate);
                            info.audioSamplingRate = parseInt(item.SamplingRate);
                            info.rawInfo.audio = item;
                        }
                    }
                    resolve({file:arrayBuffer,info:info});
                })
                .catch((error) => {
                    resolve({file:arrayBuffer,info:null});
                    console.log(`An error occured:\n${error.stack}`)
                })
        })
    }

    /** 读取文件信息 同一时间只能读取一个文件*/
    readFileInfo(file){
        return new Promise((resolve,reject) =>{
            // console.log('Working…',this.mediainfo)
        
            const getSize = () => file.size
        
            const readChunk = (chunkSize, offset) =>
                new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = (event) => {
                    if (event.target.error) {
                        reject(event.target.error)
                    }
                    resolve(new Uint8Array(event.target.result))
                }
                reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize))
            })
        
            this.mediainfo.analyzeData(getSize, readChunk)
                .then((result) => {
                    // output.value = result
                    // console.log('result',result)
                    let track = result.media.track;
                    let info = {
                        rawInfo:{}
                    };
                    for(let i=0;i<track.length;i++){
                        let item = track[i];
                        if(item['@type'] == 'General'){
                            Object.assign(info.rawInfo,item);
                            info.type = item.Format;
                            if(info.type == 'MPEG-4'){
                                info.type = 'MP4';
                                info.duration = parseFloat(item.Duration);
                            }else if(info.type == 'MPEG Audio'){
                                info.type = 'MP3';
                            }
                            info.type = info.type.toLocaleLowerCase();
                        }else if(item['@type'] == 'Image'){
                            if(!info.mediaType){
                                info.mediaType = 'Image';
                            }
                            Object.assign(info.rawInfo,item);
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            
                        }else if(item['@type'] == 'Video'){
                            if(!info.mediaType){
                                info.mediaType = 'Video';
                            }
                            info.width = parseInt(item.Width);
                            info.height = parseInt(item.Height);
                            if(!info.duration){
                                info.duration = parseFloat(item.Duration);
                            }
                            info.frameRate = parseInt(item.FrameRate);
                            info.frameCount = parseInt(item.FrameCount);
                            info.videoFormat = item.Format;
                            info.videoBitRate = parseInt(item.BitRate);
                            info.mediaType = 'Video';
                            info.rawInfo.video = item;
                        }else if(item['@type'] == 'Audio'){
                            if(!info.mediaType){
                                info.mediaType = 'Audio';
                            }
                            info.channel = parseInt(item.Channels);
                            info.audioBitRate = parseInt(item.BitRate);
                            info.audioSamplingRate = parseInt(item.SamplingRate);
                            info.rawInfo.audio = item;
                        }
                    }
                    resolve({file:file,info:info});
                })
                .catch((error) => {
                    resolve({file:file,info:null});
                    console.log(`An error occured:\n${error.stack}`)
                })
        })
    }

    /** 求高宽公约后的比值 */
    getRatioWH(width,height){
        let a = width,b=height;
        let temp;
        while(b != 0){
            temp = a % b;
            a = b;
            b = temp;
        }
        return width/a+':'+height/a;
    }

    loadFont(fontName){
		
		
		return new Promise((resolve,reject)=>{
			// let fontFace = new FontFace(obj.cssValue, `local('${obj.cssValue}'),url('${obj.url}') format('ttf'),url('${obj.url}')`);
			let fontUrl = window.globalFonts.fontLibs[fontName];
			if(!fontUrl){
				reject('not find font url');
				return;
			}
			let fontFace = new FontFace(fontName,`url(${fontUrl})`);
			fontFace.load().then(newFontFace => {
				resolve(newFontFace);
			}).catch(err=>{
				reject(err);
			});
		})
		
    }
	
	checkFont(name,_doc){
		//不在自定义字体列表中的，直接返回true
		if(!window.globalFonts.fontLibs[name]) return true;
		let values=_doc.fonts.values();
		// let isHave=false;
		// console.log('checkFont',values)
		// for(let fontFace of values){
		// 	if(fontFace.family == name){
		// 		isHave = true;
		// 		break;
		// 	}
		// }
		// return isHave;
        let isHave=false;
        let item=values.next();
        while(!item.done&&!isHave){
            let fontFace=item.value;
            if(fontFace.family==name){
                isHave=true;
            }
            item=values.next();
        }
        return isHave;
    }

    /** 获得某个比例下 1080的基本高宽 */
    getWHof1080(w=16,h=9){
        let sw = w;
        let sh = h;
        let willW = 0;
        let willH = 0;
        if(sw>=sh){
            willH = 1080;
            willW = willH*sw/sh;
        }else{
            willW = 1080;
            willH = willW*sh/sw;
        }
        return {width:willW,height:willH};
    }

    /** 对文本进行分段 0为粗分，1为细分*/
    splitText(str,type=0){
        const juArr = [];
        let startIndex = 0;
        const libs = ['。','！','？','；',',']
        for(let i=0;i<str.length;i++){
            if(libs.indexOf(str[i])>=0){
                juArr.push(str.slice(startIndex,i+1));
                startIndex = i+1;
            }
        }
        if(startIndex < str.length-1){
            juArr.push(str.slice(startIndex));
        }
        if(type == 0){
            return juArr;
        }else if(type == 1){
            const newLists = [];
            // 12289
            
            for(let i=0;i<juArr.length;i++){
                let curStr = juArr[i];
                let startIndex = 0;
                let dunhaoCount = 0;
                for(let j=0;j<curStr.length;j++){
                    if(curStr.charCodeAt(j) == 12289){
                        dunhaoCount++;
                    }
                    if(dunhaoCount >= 4){
                        dunhaoCount = 0;
                        newLists.push(curStr.slice(startIndex,j+1));
                        startIndex = j+1;
                        continue;
                    }
                    if(curStr.charCodeAt(j) == 65292 && j-startIndex >=7){
                        dunhaoCount = 0;
                        newLists.push(curStr.slice(startIndex,j+1));
                        startIndex = j+1;
                        continue;
                    }
                    //空格需要大于10个字
                    if(curStr.charCodeAt(j) == 32 && j-startIndex >=10){
                        dunhaoCount = 0;
                        newLists.push(curStr.slice(startIndex,j+1));
                        startIndex = j+1;
                        continue;
                    }
                }

                if(startIndex < curStr.length-1){
                    newLists.push(curStr.slice(startIndex));
                }
            }
            return newLists;
        }
        // const segmenter_sentence = new Intl.Segmenter('zh', { granularity: 'sentence' });
        // const segmenter_word = new Intl.Segmenter('zh', { granularity: 'word' });

        // const firstArr = Array.from(segmenter_sentence.segment(str),s=>{
        //     return s.segment;
        // })
        // if(type == 0){
        //     return firstArr;
        // }else if(type == 1){
        //     const secondArr = [];
        //     for(let i=0;i<firstArr.length;i++){
        //         const wordArr = Array.from(segmenter_word.segment(firstArr[i]),s=>{
        //             return s.segment;
        //         })
        //         console.log('wordArr',wordArr)
        //     }
            
        //     return firstArr;
        // }

        // const explorer = window.navigator.userAgent;
        // if(explorer.indexOf('Firefox')>=0){
        //     const juArr = [];
        //     let startIndex = 0;
        //     const libs = ['。','！','？','；']
        //     for(let i=0;i<str.length;i++){
        //         if(libs.indexOf(str[i])>=0){
                    
        //         }
        //     }
        // }
    }

    /**
     * 获得解压后的图片素材
     */
    getBitmapOfZip(url){
		return new Promise((resolve,reject)=>{
			const canvasList = [];
			fetch(url)
			.then(response => {
				return response.blob();
			})
			.then(res=>{
				return JSZip.loadAsync(res);
			})
			.then(zipfile=>{
				const promise_list = [];
				for(let key in zipfile.files){
					let temp = zipfile.file(key).async('blob');
					promise_list.push(temp);
					canvasList.push({
						keyName:key
					})
				}
				return Promise.all(promise_list);
			})
			.then(blobList=>{
				const promise_list = [];
				for(let i=0;i<blobList.length;i++){
					promise_list.push(createImageBitmap(blobList[i]));
				}
				return Promise.all(promise_list);
			})
			.then(imglist=>{
				for(let i=0;i<imglist.length;i++){
					let imgBitmap = imglist[i];
					let canvas = document.createElement('canvas');
					let ctx = canvas.getContext('2d');
					canvas.width = imgBitmap.width;
					canvas.height = imgBitmap.height;
					ctx.drawImage(imgBitmap,0,0);
					canvasList[i].canvas = canvas;
				}
				resolve(canvasList);
			})
			.catch(err=>{
				reject(err);
			})
		})
		
	}
}

export default new Utils;