import MD5 from 'js-md5';
import * as Interface from '../store/interfaceUrl';
// import global from "../plugins/global"

class UploadFile {
    constructor(global) {
        this.global = global;
        this.callback = null;
        console.log('window', window.AppConfig.uploadSplit)
        let configSplit = window.AppConfig.uploadSplit || 10;
        this.splitSize = configSplit * 1024 * 1024;//每片10M
    }

    /**获得md5 */
    getMD5(file) {
        //         lastModified: 1435630686484
        // lastModifiedDate: Tue Jun 30 2015 10:18:06 GMT+0800 (中国标准时间) {}
        // name: "等一个晴天.mp3"
        // size: 4273331
        return MD5(file.name + file.size + file.lastModified)
    }

    /** 上传文件 */
    uploadFile(file, callback) {
        this.callback = callback;
        let md5 = this.getMD5(file);
        let formData = new FormData();
        formData.append('fileMd5', md5);
        let xhr = new XMLHttpRequest();
        xhr.open('POST', Interface.uploadFile + this.global.search, true);
        xhr.responseType = 'json';
        xhr.onload = () => {
            if (xhr.readyState === 4) {
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
                    if (xhr.response.returnCode == 0) {
                        if (this.callback) {
                            this.callback({ cmd: 'success', data: xhr.response })
                        }
                    } else {
                        this.uploadSplit(file);
                    }
                }
            }
        }
        xhr.send(formData);
    }


    /**上传文件 */
    upload(file) {
        let md5 = this.getMD5(file);
        let formData = new FormData();
        // let file = new File([_blob],'画板图片.png',{type:'image/png'})
        // console.log(file);
        formData.append('file', file);
        formData.append('fileMd5', md5);

        let url = Interface.uploadFile + this.global.search;
        if (url === '') {
            // document.body.removeChild(this.MainRoot);
            if (this.callback) {
                this.callback({ cmd: 'end', data: null })
            }
            return;
        }
        let self = this;
        // url += Utils.objectToURLParam(Configuration.interface.upLoadImgParam,true,'queryParam');
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        // xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
        // xhr.setRequestHeader("Content-Type","multipart/form-data;");
        // xhr.setRequestHeader("Content-Type","multipart/form-data;boundary=----WebKitFormBoundaryR3jrzTlX5ycOyYN5");
        xhr.responseType = 'json';

        xhr.upload.onprogress = function (evt) {
            if (self.callback) {
                self.callback({ cmd: 'progress', data: evt.loaded / evt.total })
            }
        }
        xhr.onerror = function (error) {
            console.log('上传出错', error)
            if (self.callback) {
                self.callback({ cmd: 'error', data: error })
            }
        }
        xhr.onabort = function (evt) {
            console.log('上传被取消或断开', evt)
            if (self.callback) {
                self.callback({ cmd: 'abort', data: evt })
            }
        }
        xhr.onload = function () {
            if (xhr.readyState === 4) {
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
                    console.log('上传完成', xhr.response)
                    // imgUploadProgress.style.width = '0%';
                    // imgmsg.isUploaded = true;
                    // imgmsg.sourceUrl = xhr.response.returnData || xhr.response.data;
                    // recordImgUpload[file.lastModified+file.name] = imgmsg;
                    // self.startEditImg(imgmsg);
                    if (self.callback) {
                        self.callback({ cmd: 'success', data: xhr.response })
                    }
                } else {
                    console.log('上传完成出错', xhr.response)
                    self.callback({ cmd: 'error', data: xhr.response })
                    // Toast.show('上传出错！');
                }
                if (self.callback) {
                    self.callback({ cmd: 'end', data: null })
                }
            }
        }
        xhr.send(formData);
    }

    /**分片上传文件 */
    uploadSplit(file) {
        let md5 = this.getMD5(file);
        // let formData = new FormData();
        // formData.append('fileMd5', md5);

        let url = Interface.uploadFile + this.global.search;
        if (url === '') {
            // document.body.removeChild(this.MainRoot);
            if (this.callback) {
                this.callback({ cmd: 'end', data: null })
            }
            return;
        }
        // url = 'http://172.29.3.215:8080/svip/resources/upload'
        let self = this;
        //文件分片
        const splitFiles = [];
        const splitSend = 5;
        let totalSplitNum = Math.ceil(file.size / this.splitSize);
        for (let i = 0; i < totalSplitNum; i++) {
            let start = i * this.splitSize;
            let end = start + this.splitSize;
            let split_file = file.slice(start, end);
            let formData = new FormData();
            formData.append('file', split_file);
            formData.append('fileMd5', md5);
            formData.append('fileName', file.name);
            formData.append('fileType', file.type);
            formData.append('chunk', i);
            formData.append('chunks', totalSplitNum);
            formData.append('fileSize', file.size);

            let xhr = new XMLHttpRequest();
            xhr.order = i;
            xhr.responseType = 'json';
            xhr.upload.onprogress = function (evt) {
                statusManager({ type: 'progress', order: xhr.order, data: evt.loaded / evt.total });
            }
            xhr.onerror = function (error) {
                statusManager({ type: 'error', order: xhr.order, data: error });
            }
            xhr.onabort = function (evt) {
                statusManager({ type: 'abort', order: xhr.order, data: evt });
            }
            xhr.onload = function () {
                if (xhr.readyState === 4) {
                    console.log('xhr.status', xhr.status, xhr.response)
                    if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
                        statusManager({ type: 'success', order: xhr.order, data: xhr.response });
                    } else {
                        statusManager({ type: 'error', order: xhr.order, data: xhr.response });
                        // Toast.show('上传出错！');
                    }
                }
            }
            if (i < splitSend) {
                xhr.open('POST', url, true);
                xhr.send(formData);
            }
            splitFiles.push({
                formData: formData,
                xhr: xhr,
                order: i,
                progress: 0,
                complete: false,
                isSend: i < splitSend ? true : false,
                url: url
            })
        }

        let isreceive = true;
        function statusManager(obj) {
            if (!isreceive) return;
            if (obj.type == 'progress') {
                splitFiles[obj.order].progress = obj.data;
                let sumProgress = 0;
                splitFiles.forEach(ele => {
                    sumProgress += ele.progress / splitFiles.length;
                })
                if (self.callback) {
                    self.callback({ cmd: 'progress', data: sumProgress })
                }
            } else if (obj.type == 'error' || obj.type == 'abort') {
                if (self.callback) {
                    self.callback({ cmd: 'error', data: obj.data })
                }
                // splitFiles.forEach(ele=>{
                //     ele.xhr.abort();
                // })
                isreceive = false;
            } else if (obj.type == 'success') {
                splitFiles[obj.order].complete = true;
                // splitFiles[obj.order].isSend = true;
                for (let i = 0; i < splitFiles.length; i++) {
                    let item = splitFiles[i];
                    if (!item.isSend) {
                        item.xhr.open('POST', item.url, true);
                        item.xhr.send(item.formData);
                        item.isSend = true;
                        break;
                    }
                }
                if (obj.data.returnCode == 0) {
                    if (self.callback) {
                        self.callback({ cmd: 'success', data: obj.data })
                    }
                } else {
                    let isComplete = true;
                    for (let i = 0; i < splitFiles.length; i++) {
                        let item = splitFiles[i];
                        if (!item.complete) {
                            isComplete = false;
                            break;
                        }
                    }
                    if (isComplete) {
                        if (self.callback) {
                            self.callback({ cmd: 'error', data: obj.data })
                        }
                    }
                }
            }
        }
    }
}

export default UploadFile;