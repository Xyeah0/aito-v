class EasyDefTool{
	constructor(transformCfg,changeBack,changedBack){

		this.changeBack = changeBack;
		this.changedBack = changedBack;
		this._showTool = false;
		this.isAutoHidden = true;
		this.isStopPropagation = false;

		this.transform = Object.assign({
			x:0,
			y:0,
			width:0,
			height:0,
			scale:1,
			rotation:0
		},transformCfg);
		this.isUseWidthHeight = false;//使用高宽参数代替scale,scale始终为1
		this.lockRatio = 0;//0为自由，否则为高/宽的比值
		this.isRotate = true;

		this.adsorbentCoefficient = 1/100;
		this.root = document.createElement('div');
		this.rootDrag = document.createElement('div');
		this.minLeftTop = this.getMinCircle();
		this.minRightTop = this.getMinCircle();
		this.minLeftBottom = this.getMinCircle();
		this.minRightBottom = this.getMinCircle();
		this.minRotation = document.createElement('div');

		this.root.appendChild(this.rootDrag);
		this.root.appendChild(this.minLeftTop);
		this.root.appendChild(this.minRightTop);
		this.root.appendChild(this.minLeftBottom);
		this.root.appendChild(this.minRightBottom);
		this.root.appendChild(this.minRotation);


		this.root.style.position = 'absolute';
		this.root.style.border = '1px solid #ffffff';
		this.root.style.pointerEvents = 'all';

		this.rootDrag.style.position = 'absolute';
		this.rootDrag.style.width = '100%';
		this.rootDrag.style.height = '100%';
		// this.rootDrag.style.background = 'rgba(0,0,0,0.5)';
		// this.rootDrag.style.cursor = 'move';

		this.minLeftTop.style.left = '0px';
		this.minLeftTop.style.top = '0px';

		this.minRightTop.style.right = '0px';
		this.minRightTop.style.top = '0px';
		this.minRightTop.style.transform = 'translate(50%,-50%)';

		this.minLeftBottom.style.left = '0px';
		this.minLeftBottom.style.bottom = '0px';
		this.minLeftBottom.style.transform = 'translate(-50%,50%)';

		this.minRightBottom.style.right = '0px';
		this.minRightBottom.style.bottom = '0px';
		this.minRightBottom.style.transform = 'translate(50%,50%)';
		// this.rootBox.style.position = 'absolute';
		// this.rootBox.style.left = -this.transform.width*0.5*100+'%';
		// this.rootBox.style.top = -this.transform.height*0.5*100+'%';
		// this.rootBox.style.width = this.transform.width*100+'%';
		// this.rootBox.style.height = this.transform.height*100+'%';
		// this.rootBox.style.border = '1px solid #ffffff';
		this.minRotation.style.position = 'absolute';
		this.minRotation.style.left = '50%';
		this.minRotation.style.bottom = '0px';
		this.minRotation.style.transform = 'translate(-50%,130%)';
		this.minRotation.style.width = '20px';
		this.minRotation.style.height = '20px';
		this.minRotation.style.background = '#ffffff';
		this.minRotation.style.backgroundImage = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAF8WlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDggNzkuMTY0MDM2LCAyMDE5LzA4LzEzLTAxOjA2OjU3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIyLTAyLTA5VDE1OjE3OjQ4KzA4OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIyLTAyLTA5VDE1OjE3OjQ4KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMi0wMi0wOVQxNToxNzo0OCswODowMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpkZGVmMjA2ZC0wY2U0LWY2NDUtYmJjYi04NmY1M2QyMzI1N2EiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpjZjAwNDY2OS00MmJhLTA1NDEtYTc3Yy00ZjU3NjJmYjBkMzEiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0NWY3ZjhkZi01MTU2LWMyNDAtOWFlYS01NjI5MmIyMTUwMDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBwaG90b3Nob3A6SUNDUHJvZmlsZT0ic1JHQiBJRUM2MTk2Ni0yLjEiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1ZjdmOGRmLTUxNTYtYzI0MC05YWVhLTU2MjkyYjIxNTAwMCIgc3RFdnQ6d2hlbj0iMjAyMi0wMi0wOVQxNToxNzo0OCswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpkZGVmMjA2ZC0wY2U0LWY2NDUtYmJjYi04NmY1M2QyMzI1N2EiIHN0RXZ0OndoZW49IjIwMjItMDItMDlUMTU6MTc6NDgrMDg6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMS4wIChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4o51tKAAABRElEQVQ4y83USytFURjG8ZVyv+SuXAZuyZnJyAADfAL5OiYMZaAYKGWmyMRtanROyoQQCUkGUucLKG3/tx61BnvtvRRl1W9w2ms9Z73r5pIkcb/J/VlgRqvFFI5wiJa0TnmBNmgdkxjCs42RK6ygJjawG0UNXkUl5nCGMt71zX735wXWq2Oi8ka8P2rAMFqxrz6XmkAwcFkdj1Gdsa4V2FHf3VBgI+7xhnaX3zq1JLOhwAN8KHDQxbcBqyYt8FQlXKMjMmxMmzSfFjiDV0xHhjWhpEkU0wJtoQs/KLUPD/i06kK7PI5F9EQE1uERL+gKBW6qhD3NOKttq+9G1jm0mV14oW06Hv6Z7PXCbtGcd/XsOp1rwJNKKmmzqrCkbzcYjX0c7Aqu4c57FMpa4wlsfV+52ED/oTjRY7Gge+yinq9/+2J/AbQ58qEf+KP4AAAAAElFTkSuQmCC)';
		this.minRotation.style.backgroundSize = 'cover';
		this.minRotation.style.borderRadius = '10px';

		// this.pointer = this.getMinCircle();
		// this.root.appendChild(this.pointer);
		// this.pointer.style.left = '50%';
		// this.pointer.style.top = '50%';
		this.adsorbentRowLine = document.createElement('div');
		this.adsorbentRowLine.style.position = 'absolute';
		// this.adsorbentRowLine.style.transform = 'translate(-50%,-50%)';
		this.adsorbentRowLine.style.pointerEvents = 'none';
		this.adsorbentRowLine.style.backgroundColor = '#00c1cd';

		this.adsorbentColLine = document.createElement('div');
		this.adsorbentColLine.style.position = 'absolute';
		// this.adsorbentColLine.style.transform = 'translate(-50%,-50%)';
		this.adsorbentColLine.style.pointerEvents = 'none';
		this.adsorbentColLine.style.backgroundColor = '#00c1cd';

		addFun(this.adsorbentRowLine,this.root,'row');
		addFun(this.adsorbentColLine,this.root,'col');
		// console.log('this.adsorbentRowLine',this.adsorbentRowLine)
		function addFun(dom,root,dir='row'){
			dom.showDom = function (type){
				if(!dom.parentNode){
					root.parentNode.appendChild(dom);
				}
				dom.style.visibility = 'visible';
				if(dir=='row'){
					dom.style.width = '1px';
					dom.style.height = '100%';
					dom.style.top = '0%';
					if(type==0){
						dom.style.left = '0%';
					}else if(type == 1){
						dom.style.left = '50%';
					}else if(type == 2){
						dom.style.left = 'initial';
						dom.style.right = '0%';
					}
				}else if(dir=='col'){
					dom.style.width = '100%';
					dom.style.height = '1px';
					if(type==0){
						dom.style.top = '0%';
					}else if(type == 1){
						dom.style.top = '50%';
					}else if(type == 2){
						dom.style.top = 'initial';
						dom.style.bottom = '0%';
					}
				}
			}

			dom.hiddenDom = function (){
				dom.style.visibility = 'hidden';
			}
		}

		this.initToolsEvent();
		this.flushTransform();
		this.showTool = false;
	}

	initToolsEvent () {
		this.defminBtns = [this.rootDrag,this.minLeftTop,this.minRightTop,this.minLeftBottom,this.minRightBottom,this.minRotation];
		let self = this;
		this.defminBtns.forEach((item,index) => {
			item.moveType = index;
			item.addEventListener('mousedown',btnsMouseDownHandler);
		});


		function btnsMouseDownHandler(event) {
			// if(!self.showTool) return;
			let e = event ? event : window.event;
			e.preventDefault();
			if(self.isStopPropagation){
				e.stopPropagation();
			}
			let moveType = e.target.moveType;//0为移动
			
			let mouseX = e.clientX,mouseY = e.clientY;
			let beforePos = Object.assign({},self.transform);
			
			document.addEventListener('mousemove',mousemoveHandler);
			document.addEventListener('mouseup',mouseupHandler);


			function mousemoveHandler(event){
				// console.log('self',self.moveType,self.showTool)
				event.preventDefault();
				event.stopPropagation();
				
				self.computerPos(moveType,beforePos,event.clientX-mouseX,event.clientY-mouseY,event.clientX,event.clientY);
				if(self.changeBack){
					self.changeBack(self.transform);
				}
			}

			function mouseupHandler(event){
				event.preventDefault();
				event.stopPropagation();

				document.removeEventListener('mousemove',mousemoveHandler);
				document.removeEventListener('mouseup',mouseupHandler);

				self.adsorbentRowLine.hiddenDom();
				self.adsorbentColLine.hiddenDom();
				// self.flushDefPos(self.transform,true);
				if(self.changedBack){
					let isSame = true;
					for(let info in self.transform){
						if(self.transform[info] != beforePos[info]){
							isSame = false;
							break;
						}
					}
					self.changedBack(self.transform,isSame);
				}
			}
			// console.log(e.target.classList,e.target === btnLT,moveType)
		}

		document.addEventListener('mousedown',rootMouseDownHandler);

		this.rootDrag.addEventListener('mouseover',dragOverHandler);
		this.rootDrag.addEventListener('mouseout',dragOutHandler);
		this.rootDrag.addEventListener('click',dragClickHandler);
		this.rootDrag.addEventListener('dblclick',dragdblclickHandler);

		function rootMouseDownHandler(event){
			if(self.isAutoHidden){
				let e = event ? event : window.event;
				if(e.target == self.root || self.root.contains(e.target)){
					return;
				}else{
					self.showTool = false;
				}
			}
		}

		function dragClickHandler(event){
			self.showTool = true;
			self.rootDrag.style.border = 'none';
			if(self.clickBack){
				self.clickBack(event);
			}
		}

		function dragOverHandler(event){
			if(self.showTool) return;
			self.rootDrag.style.border = '1px dashed #ffffff';
		}

		function dragOutHandler(event){
			self.rootDrag.style.border = 'none';
		}
		function dragdblclickHandler(event){
			if(self.dblClickBack){
				self.dblClickBack(event);
			}
		}
		//释放
		this.removeEvent = function (){
			self.defminBtns.forEach((item,index) => {
				item.removeEventListener('mousedown',btnsMouseDownHandler);
			});
			document.removeEventListener('mousedown',rootMouseDownHandler);
			self.rootDrag.removeEventListener('click',dragClickHandler);
			self.rootDrag.removeEventListener('mouseover',dragOverHandler);
			self.rootDrag.removeEventListener('mouseout',dragOutHandler);
			self.rootDrag.removeEventListener('dblclick',dragdblclickHandler);
		}
	}

	/** 显示变形工具 */
	set showTool(b){
		this._showTool = !!b;
		if(b){
			this.minLeftTop.style.visibility = 'visible';
			this.minRightTop.style.visibility = 'visible';
			this.minLeftBottom.style.visibility = 'visible';
			this.minRightBottom.style.visibility = 'visible';
			this.minRotation.style.visibility = this.isRotate?'visible':'hidden';
			this.root.style.border = '1px solid #ffffff';
		}else{
			this.minLeftTop.style.visibility = 'hidden';
			this.minRightTop.style.visibility = 'hidden';
			this.minLeftBottom.style.visibility = 'hidden';
			this.minRightBottom.style.visibility = 'hidden';
			this.minRotation.style.visibility = 'hidden';
			this.root.style.border = 'none';
			this.adsorbentRowLine.hiddenDom();
			this.adsorbentColLine.hiddenDom();
		}
	}

	get showTool(){
		return this._showTool;
	}

	/**
	 * 计算最终位置
	 */
	computerPos (moveType,beforePos,diffX,diffY,clientX,clientY){
		const parentNode = this.root.parentNode;
		if(!parentNode) return;
		
		const rect = parentNode.getBoundingClientRect();
		const parentW = rect.width;//parentNode.offsetWidth;
		const parentH = rect.height;//parentNode.offsetHeight;
		//仅移动
		if(moveType === 0){
			this.transform.x = beforePos.x + diffX/parentW;
			this.transform.y = beforePos.y + diffY/parentH;
			if(this.transform.rotation == 0 || Math.abs(this.transform.rotation) == 180){
				let leftSide = this.transform.x - this.transform.width*this.transform.scale*0.5;
				let rightSide = this.transform.x + this.transform.width*this.transform.scale*0.5;
				let topSide = this.transform.y - this.transform.height*this.transform.scale*0.5;
				let bottomSide = this.transform.y + this.transform.height*this.transform.scale*0.5;
				if(this.transform.x < 0.5+this.adsorbentCoefficient && this.transform.x > 0.5-this.adsorbentCoefficient){
					this.transform.x = 0.5;
					this.adsorbentRowLine.showDom(1);
				}else if(leftSide > -this.adsorbentCoefficient && leftSide < this.adsorbentCoefficient){
					this.transform.x = this.transform.width*this.transform.scale*0.5;
					this.adsorbentRowLine.showDom(0);
				}else if(rightSide > 1-this.adsorbentCoefficient && rightSide < 1+this.adsorbentCoefficient){
					this.transform.x = 1-this.transform.width*this.transform.scale*0.5;
					this.adsorbentRowLine.showDom(2);
				}else{
					this.adsorbentRowLine.hiddenDom();
				}

				if(this.transform.y < 0.5+this.adsorbentCoefficient && this.transform.y > 0.5-this.adsorbentCoefficient){
					this.transform.y = 0.5;
					this.adsorbentColLine.showDom(1);
				}else if(topSide > -this.adsorbentCoefficient && topSide < this.adsorbentCoefficient){
					this.transform.y = this.transform.height*this.transform.scale*0.5;
					this.adsorbentColLine.showDom(0);
				}else if(bottomSide > 1-this.adsorbentCoefficient && bottomSide < 1+this.adsorbentCoefficient){
					this.transform.y = 1-this.transform.height*this.transform.scale*0.5;
					this.adsorbentColLine.showDom(2);
				}else{
					this.adsorbentColLine.hiddenDom();
				}
			}else if(Math.abs(this.transform.rotation) == 90){
				let willW = this.transform.height/parentW*parentH;
				let willH = this.transform.width/parentH*parentW;
				let leftSide = this.transform.x - willW*this.transform.scale*0.5;
				let rightSide = this.transform.x + willW*this.transform.scale*0.5;
				let topSide = this.transform.y - willH*this.transform.scale*0.5;
				let bottomSide = this.transform.y + willH*this.transform.scale*0.5;
				if(this.transform.x < 0.5+this.adsorbentCoefficient && this.transform.x > 0.5-this.adsorbentCoefficient){
					this.transform.x = 0.5;
					this.adsorbentRowLine.showDom(1);
				}else if(leftSide > -this.adsorbentCoefficient && leftSide < this.adsorbentCoefficient){
					this.transform.x = willW*this.transform.scale*0.5;
					this.adsorbentRowLine.showDom(0);
				}else if(rightSide > 1-this.adsorbentCoefficient && rightSide < 1+this.adsorbentCoefficient){
					this.transform.x = 1-willW*this.transform.scale*0.5;
					this.adsorbentRowLine.showDom(2);
				}else{
					this.adsorbentRowLine.hiddenDom();
				}

				if(this.transform.y < 0.5+this.adsorbentCoefficient && this.transform.y > 0.5-this.adsorbentCoefficient){
					this.transform.y = 0.5;
					this.adsorbentColLine.showDom(1);
				}else if(topSide > -this.adsorbentCoefficient && topSide < this.adsorbentCoefficient){
					this.transform.y = willH*this.transform.scale*0.5;
					this.adsorbentColLine.showDom(0);
				}else if(bottomSide > 1-this.adsorbentCoefficient && bottomSide < 1+this.adsorbentCoefficient){
					this.transform.y = 1-willH*this.transform.scale*0.5;
					this.adsorbentColLine.showDom(2);
				}else{
					this.adsorbentColLine.hiddenDom();
				}
			}
		}else if(moveType === 1 || moveType === 2 || moveType === 3 || moveType === 4){
			if(this.isUseWidthHeight){
				if(moveType == 1){
					diffX = -diffX;
					diffY = -diffY;
				}else if(moveType == 2){
					diffY = -diffY;
				}else if(moveType == 3){
					diffX = -diffX;
				}
				this.transform.width = beforePos.width + diffX/parentW*2;
				this.transform.height = beforePos.height + diffY/parentH*2;

				this.transform.width = Math.max(this.transform.width,0.02);
				this.transform.height = Math.max(this.transform.height,0.02);
				if(this.lockRatio > 0){
					this.transform.height = this.transform.width*(1/this.lockRatio)*(parentW/parentH);
				}
				

				let ratio = this.transform.width/this.transform.height;

				//吸附
				let leftSide = this.transform.x - this.transform.width*this.transform.scale*0.5;
				let rightSide = this.transform.x + this.transform.width*this.transform.scale*0.5;
				let topSide = this.transform.y - this.transform.height*this.transform.scale*0.5;
				let bottomSide = this.transform.y + this.transform.height*this.transform.scale*0.5;
				if(leftSide > -this.adsorbentCoefficient && leftSide < this.adsorbentCoefficient){
					this.transform.width = this.transform.x/(this.transform.scale*0.5);
					if(this.lockRatio > 0){
						this.transform.height = this.transform.width*(1/ratio);
					}
					this.adsorbentRowLine.showDom(0);
				}else if(rightSide > 1-this.adsorbentCoefficient && rightSide < 1+this.adsorbentCoefficient){
					this.transform.width = (1-this.transform.x)/(this.transform.scale*0.5);
					if(this.lockRatio > 0){
						this.transform.height = this.transform.width*(1/ratio);
					}
					this.adsorbentRowLine.showDom(2);
				}else if(topSide > -this.adsorbentCoefficient && topSide < this.adsorbentCoefficient){
					this.transform.height = this.transform.y/(this.transform.scale*0.5);
					if(this.lockRatio > 0){
						this.transform.width = this.transform.height*ratio;
					}
					this.adsorbentColLine.showDom(0);
				}else if(bottomSide > 1-this.adsorbentCoefficient && bottomSide < 1+this.adsorbentCoefficient){
					this.transform.height = (1-this.transform.y)/(this.transform.scale*0.5);
					if(this.lockRatio > 0){
						this.transform.width = this.transform.height*ratio;
					}
					this.adsorbentColLine.showDom(2);
				}else{
					this.adsorbentRowLine.hiddenDom();
					this.adsorbentColLine.hiddenDom();
				}

				
			}else{
				if(moveType == 1 || moveType == 3){
					diffX = -diffX;
				}
				let sx = (diffX/parentW*2+beforePos.width)/(beforePos.width);
				this.transform.scale = Math.max(beforePos.scale+(sx-1),0.1);


				//吸附
				let leftSide = this.transform.x - this.transform.width*this.transform.scale*0.5;
				let rightSide = this.transform.x + this.transform.width*this.transform.scale*0.5;
				let topSide = this.transform.y - this.transform.height*this.transform.scale*0.5;
				let bottomSide = this.transform.y + this.transform.height*this.transform.scale*0.5;
				if(leftSide > -this.adsorbentCoefficient && leftSide < this.adsorbentCoefficient){
					this.transform.scale = this.transform.x/(this.transform.width*0.5);
					this.adsorbentRowLine.showDom(0);
				}else if(rightSide > 1-this.adsorbentCoefficient && rightSide < 1+this.adsorbentCoefficient){
					this.transform.scale = (1-this.transform.x)/(this.transform.width*0.5);
					this.adsorbentRowLine.showDom(2);
				}else if(topSide > -this.adsorbentCoefficient && topSide < this.adsorbentCoefficient){
					this.transform.scale = this.transform.y/(this.transform.height*0.5);
					this.adsorbentColLine.showDom(0);
				}else if(bottomSide > 1-this.adsorbentCoefficient && bottomSide < 1+this.adsorbentCoefficient){
					this.transform.scale = (1-this.transform.y)/(this.transform.height*0.5);
					this.adsorbentColLine.showDom(2);
				}else{
					this.adsorbentRowLine.hiddenDom();
					this.adsorbentColLine.hiddenDom();
				}
			}

		}else if(moveType === 5){
			let tx = (clientX-rect.x);
			let ty = (clientY-rect.y);
			let rotation = -Math.atan2(tx-this.transform.x*parentW,ty-this.transform.y*parentH)*(180/Math.PI);
			let ys = rotation%45;
			if(ys < 5 && ys > -5){
				rotation = rotation - ys;
			}else if(ys > -45 && ys < -40){
				rotation = rotation - (ys+45);
			}else if(ys > 40 && ys < 45) {
				rotation = rotation + (45-ys);
			}
			this.transform.rotation = rotation;
		}

		this.flushTransform();
	}
	
	/** 刷新位置 */
	flushTransform(transformCfg){
		Object.assign(this.transform,transformCfg);
		let tw = this.transform.width*this.transform.scale;
		let th = this.transform.height*this.transform.scale;
		// if(this.isFreeDef){
		// 	th = this.transform.height*this.transform.scaleY;
		// }

		this.root.style.left = (this.transform.x-tw*0.5)*100+'%';
		this.root.style.top = (this.transform.y-th*0.5)*100+'%';
		this.root.style.width = tw*100+'%';
		this.root.style.height = th*100+'%';

		this.root.style.transform = 'rotate('+this.transform.rotation+'deg)';
		// console.log('flushTransform',this.transform)
	}

	getMinCircle(){
		const div = document.createElement('div');
		div.style.position = 'absolute';
		div.style.width = '10px';
		div.style.height = '10px';
		div.style.background = '#ffffff';
		div.style.borderRadius = '5px';
		div.style.cursor = 'pointer';
		div.style.transform = 'translate(-50%,-50%)';
		return div;
	}

	destroy(){
		this.removeEvent();
        if(this.root.parentNode){
            this.root.parentNode.removeChild(this.root);
        }
        this.changeBack = null;
		this.changedBack = null;
		this.clickBack = null;
		this.dblClickBack = null;
    }
}

export default EasyDefTool;