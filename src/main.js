import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import './theme/element-variables.scss';
import './style/index.css';
import './style/black.less';
import './plugins/axios'
import './style/globalstyle.scss'
import router from './router';
import store from './store';
import App from './App.vue';
import $App from './model/App.js';
import VueUeditorWrap from 'vue-ueditor-wrap'; // ES6 Module

$App.version = '2.0.87';
console.log("\n %c AiToVideo %c v" + $App.version + " \n", "color: #fadfa3; background: #18A689; font-width: 600; font-size: 20px; padding:5px 0;", "font-size: 16px;background: #fadfa3; padding:5px 0;color:red;");

Vue.use(ElementUI);
Vue.config.productionTip = false;
Vue.component('vue-ueditor-wrap', VueUeditorWrap);
Vue.prototype.$bus = new Vue();

if (process.env.NODE_ENV == 'development') {


    // document.cookie = "login_cmc_id=d723c989870936efedd056219c3f81df;path=/";
    // document.cookie = "login_cmc_tid=b0ed9eda12547a9e0f004f1c2c8df701;path=/";

    //演示环境
    document.cookie = "login_cmc_id=8c40d64b90ce833b4463655f8ab61f54;path=/";
    document.cookie = "login_cmc_tid=1ff3ddcaaa97db01ad1db5b45bea72b4;path=/";


    //成都环境
    // document.cookie = "login_cmc_id=e1eab3add6283b48a5d278841d2afb9a;path=/";
    // document.cookie = "login_cmc_tid=5bda00a24ea5a8904d69d6b921b07177;path=/";

}
if (window.AppConfig.favicon) {
    const link = document.getElementById('favicon');
    link.setAttribute('href', window.AppConfig.favicon);
}

//前置路由守卫
router.beforeEach((to, from, next) => {
    let isLogin = !!store.state.appStore.userInfo; // 是否登录
    console.log('to', to, from)
    // 未登录状态；跳转至Home

    if (!isLogin && to.name != 'Home') {

        router.push({ name: 'Home', query: Object.assign({ rawRoute: to.name }, to.query) });
    }
    next();



});

window.AppVue = new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')

window.App = $App;

//全局字体信息
window.globalFonts = {
    fontFamilys: [],
    fontLibs: {}
}
//加载字体信息
fetch(window.AppConfig.modulePath + '/tlSource/fonts/fonts.json')
    .then(res => {
        return res.json();
    }).then(fontDatas => {
        for (let i = 0; i < fontDatas.length; i++) {
            const item = fontDatas[i];
            if (item.isCustom) {
                window.globalFonts.fontLibs[item.value] = window.AppConfig.modulePath + item.fontUrl;
            }
            window.globalFonts.fontFamilys.push({
                label: item.label,
                value: item.value
            })
        }
    }).catch(error => {
        console.log('动态字幕模板字体加载错误' + error)
    })