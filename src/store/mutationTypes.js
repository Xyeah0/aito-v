
export const SET_USER_INFO = 'set_user_info';//设置用户信息
export const LOAD_CONFIG = 'load_config';
export const GET_ARTICLEAIDATA = 'get_articleAiData';
export const SET_TEMPLATEDATA = 'set_templateData';
export const SET_FILTERAIDATA = 'set_filterAiData';
export const INIT_TIMELINE = 'init_timeline';
export const INIT_TIMELINE2 = 'init_timeline2';
export const FUSE_TIMELINE = 'fuse_timeline';
export const TASK_LIST_MSG = 'task_list_msg';
export const BODY_WIDTH = 'body_width';

export const SET_CREATIVE_DATA = 'set_creative_data';//创意数据信息
export const INIT_CREATIVE_DETAIL = 'init_createtive_detail';//初始化文案详情数据
export const CREATE_SCENE = 'create_scene';//生成场景数据
export const SET_CREATIVE_TEXT_DATA = 'set_creative_text_data';//设置文案创意数据
export const RELOAD_CREATIVE = 'reload_creative';//重新加载创意页面
export const SET_ADVANCESET_DATA = 'set_advanceSet_data';//高级设置数据
export const SET_EDITOR_INSTANCE = 'set_editor_instance';//设置编辑器实例
export const SET_KEYWORD = 'set_keyword';//设置关键词数据
export const SET_HIGHLIGHT_DATA = 'set_highlight_data';//设置高亮数据
export const SET_PROOFREAD_LISTS = 'set_proofread_lists';//设置文本校对列表
export const SET_SHOW_ADJUST_AREA = 'set_show_adjust_area';//设置校对审核区域显示
export const SET_EXAMINE_DATA = 'set_examine_data';//设置文本审核信息
export const SET_COPYWRITING_CONFIG = 'set_copywriting_config';//高级设置
export const SEND_DUB = 'send_dub';//发起语音合成
export const BACK_DUB = 'back_dub';//语音合成返回
export const SEND_AIDRAW = 'send_aidraw';//发起Ai作画
export const BACK_AIDRAW = 'back_aidraw';//Ai作画返回
export const RECORD_SCENE_CLIP = 'record_scene_clip';//记录场景主素材
export const SET_AISPLIT = 'set_aiSplit';//设置缓存分段内容
export const GET_AISPLIT = 'get_aiSplit';//获取缓存分段内容


export const KEYWORD="_keyword";//关键词高亮
export const PROOFREAD="_proofread";//文本校对高亮
export const EXAMINE="_exmaine";//文本审核高亮

/**document */
export const SET_DOCUMENT_DATA="set_document_data";
export const SET_ARTICLE_DETAIL="set_article_detail";//获取的文章数据
export const CREATE_SCENE_DOCUMENT="create_scene_document";//获取的文章数据
export const SET_UNDO_DATA="set_undo_data";//设置撤销数据
export const SET_REDO_DATA="set_redo_data";//设置恢复数据
export const UNDO_HANDLER="undo_data";//撤销操作
export const REDO_HANDLER="redo_data";//恢复操作
export const CLEAR_REUN_DATA="clear_reun_data";//清空撤销恢复


export const SET_KEYWORDS="set_keywords";//设置关键字
export const SET_ARRANGEMENT_DATA="set_arrangement_data";//处理排列数据
export const INIT_DOCUMENT_DETAIL = 'init_document_detail';//初始化文稿详情数据
export const RELOAD_DOCUMENT = 'reload_document';//重新载入数据
export const COMBINING_CONTENT = 'combining_content';//按照规则合并段落
export const SET_SCENE_DATA = 'set_scene_data';//添加场景数据



















//模板例表页
export const TL_FILTER_TABS = 'tl_filter_tabs';
export const TL_LOADTEMPLATE = 'tl_loadTLList';
export const TL_LOADTEMPLATE_TT = 'tl_loadTemplate_tt';
export const TL_SELECTED_TT = 'tl_selected_tt';
export const TL_SET_CHECKED_NUM = 'tl_set_checked_num'; //设置模板选中数量
export const TL_SET_ACTIVE_NAME = 'tl_set_active_name'; //设置当前选中类型
export const TL_SET_SELECT_ITEMS = 'tl_set_select_items'; //设置当前选中模板数据
export const TL_SET_FILTER_PARAMS = 'tl_set_filter_params'; //设置当前过滤数据
