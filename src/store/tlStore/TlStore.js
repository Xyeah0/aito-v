import mutations from './TlMutations.js'
import actions from './TlActions.js'

const state = {
    filterTabs:[],
    filterTabs2:[],
    tlList:[],
    tlListTT:[],//片头片尾list
    tlTotal:0,
    selectItems:[],
}

const getters = {
    
}

export default {
    state,
    actions,
    getters,
    mutations
}
