import * as types from '../mutationTypes.js'
import Vue from 'vue';
export default {
    [types.TL_FILTER_TABS](state, data) {
        const arr = data || [];
        const arr2 = [];
        arr.forEach(item => {
            arr2.push({
                label: item.name,
                value: String(item.id),
                scope: item.scope
            })
        })
        state.filterTabs = arr2;
        state.filterTabs2 = arr2.slice();
    },
    [types.TL_LOADTEMPLATE](state, data) {
        let arr = [];
        let newTabs = [];
        console.log(data)
        for (let i = 0; i < state.filterTabs2.length; i++) {
            let ft_item = state.filterTabs2[i];
            let arr1 = [];
            for (let j = 0; j < data.length; j++) {
                let d_item = data[j];
                if (d_item.templateScope === 'preset') {
                    if (ft_item.value == d_item.parentId) {
                        arr1.push(d_item);
                    }
                } else if (d_item.templateScope === 'custom') {
                    if (ft_item.value == d_item.categoryId) {
                        arr1.push(d_item);
                    }
                }
            }
            arr.push(arr1);
            if (arr1.length) {
                newTabs.push(ft_item)
            }
        }
        let newArr = [];
        for (let i = 0; i < arr.length; i++) {
            let item = arr[i];
            if (item.length) {
                newArr.push(item);
            }
        }
        state.filterTabs = newTabs;
        state.tlList = newArr;
    },
    [types.TL_LOADTEMPLATE_TT](state, data) {
        const arr = data;
        arr.forEach(element => {
            element.isSelected = false;
        });
        state.tlListTT = arr;//Object.assign({...state.config},data);
    },
    [types.TL_SELECTED_TT](state, data) {
        const arr = state.tlListTT;
        state.tlListTT.forEach(element => {
            Vue.set(element, 'isSelected', element.templateId == data.templateId)
        })
    },
      //设置选中模板数据
      [types.TL_SET_SELECT_ITEMS](state, data) {
        state.selectItems = data;
    },
} 