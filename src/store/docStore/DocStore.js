import mutations from './DocMutations.js'
import actions from './DocActions.js'


function initParams(search) {
    let newSearch = '';
    let contentSourceId = '';
    let url_params = search.replace("?", "").split("&");
    // console.log('url_params =',url_params,window.location.search)
    const obj = {};
    for (let k = 0; k < url_params.length; k++) {
        if (url_params[k] === '') continue;
        let arr = url_params[k].split("=");
        let key = arr[0], value = decodeURIComponent(arr[1]) || "";
        if (key.toLocaleLowerCase() === 'contentsourceid' || key.toLocaleLowerCase() === 'contentids') {
            contentSourceId = value;
            obj.contentSourceId = value;
            continue;
        }
        if (key == 'rawRoute') continue;
        obj[key] = value;
        if (newSearch) {
            newSearch += '&' + key + '=' + value;
        } else {
            newSearch += '?' + key + '=' + value;
        }
    }
    return {
        rawSearch: search,
        search: newSearch,
        params: obj,
        contentSourceId: contentSourceId,
    };
}

const state = {
    global: initParams(window.location.search),
    reloadDocument: 0,
    documentData: {
        doc: "",//文稿字符
        title: "",
        contents: [],//文稿内容
        rawConent: "",
        sceneDatas: [],
        sceneType: 0,
        htmlArticleContent: [],
        listItemData: {},//详情
        historySceneDatas: [],//历史场景
        preference: {
            mediaRange: {
                findType: 'all',// catalog  mediaList
                isOnlyRange: true,//仅选  false优选
                catalogIds: [],//ids
                mediaIds: []//ids
            },
            audioCfg: 'onlyBg' //bgAndMain onlyDub
        },//用户偏好设置
        arrangementType: -1,//排列类型  0文字在图片上方 1文字在图片下方
        arrangementData: [],
        keyWords: [],//关键词
        submitStrategy:1,//检索方式
    },
    advancedSetData: {
        "dynamicLevel": "0.8",
        "duplicateLevel": "1.5",
        "sceneSeg": "sentence"
    },//高级设置
    redoData: [],//撤销
    undoData: [],//恢复



}


const getters = {

}

export default {
    state,
    actions,
    getters,
    mutations
}
