import * as Action from '../actions.js';
import * as types from '../mutationTypes.js';
import * as Interface from '../interfaceUrl.js';

import MySocket from '../../plugins/socket.js';

export default {

    getExtractHTML({ commit, state }, params) {

        return Action.getJsonData(Interface.getExtractHTML + state.global.search, params);
    },
    getBdrealtime({ commit, state }, params) {
        return Action.getJsonData(Interface.getBdrealtime + state.global.search, params);
    },
    getBdNewsContent({ commit, state }, params) {
        return Action.getJsonData(Interface.getBdNewsContent + state.global.search, params);
    },
    /**保存文稿 */
    doc_saveDocument({ commit, state }, params) {
        return Action.postJsonData(Interface.saveDocument + state.global.search, params);
    },
    /**修改文稿 */
    doc_updateDocument({ commit, state }, params) {
        return Action.postJsonData(Interface.updateDocument + state.global.search, params);
    },
    /**删除文稿 */
    doc_deleteDocument({ commit, state }, params) {
        return Action.deleteJsonData(Interface.deleteDocument + state.global.search, params);
    },
    /**查询文稿列表 */
    doc_getDocLists({ commit, state }, params) {
        return Action.postJsonData(Interface.getDocLists + state.global.search, params);
    },
    /**查询文稿详情 */
    doc_getDocDetail({ commit, state }, params) {
        return Action.getJsonData(Interface.getDocDetail + state.global.search, params);
    },

    /**自动保存 */
    doc_autoSave({ commit, state }, params) {

        params = {
            docId: state.documentData.docId || null,
            title: state.documentData.title, //标题
            doc: state.documentData.doc, //文稿字符
            sceneType: state.documentData.sceneType, //场景类型
            // submitStrategy: this.documentData.submitStrategy, //检索方式
            docData: state.documentData.sceneDatas, //场景数据
            preference: formatPreference(state.documentData.preference), //用户偏好设置
        };
        if (!params.title) return;
        // console.log(params);
        if (!params.docId) {
            this.dispatch("doc_saveDocument", params).then(res => {
                if (res.returnCode === 0) {
                    commit(types.SET_DOCUMENT_DATA, { docId: res.returnData });
                } else {
                    // this._vm.$message.error(res.returnDesc || "自动保存失败");
                }

            })
        } else {
            this.dispatch("doc_updateDocument", params).then(res => {
                if (res.returnCode === 0) {

                } else {
                    // this._vm.$message.error(res.returnDesc || "自动保存失败");
                }
            })
        }

        //格式化偏好数据
        function formatPreference(preference) {
            if (!preference) return null;
            let originPreference = JSON.parse(JSON.stringify(preference));
            let { findType, isOnlyRange, catalogIds, mediaIds } = originPreference.mediaRange;
            if (findType === "all") {
                originPreference.mediaRange = {
                    findType: findType,
                }
            } else if (findType === "catalog") {
                originPreference.mediaRange = {
                    findType: findType,
                    isOnlyRange: isOnlyRange,
                    catalogIds: catalogIds,
                }
            } else if (findType === "mediaList") {
                originPreference.mediaRange = {
                    findType: findType,
                    isOnlyRange: isOnlyRange,
                    mediaIds: mediaIds
                }
            }
            return originPreference;
        }

    },
    /**依稿成片提交智能处理 */
    doc_intelligentSubmit({ commit, state }, params) {
        return Action.postJsonData(Interface.docIntelligentSubmit + state.global.search, params);
    },

    /**图片语义分析 */
    doc_imageCaption({ commit, state }, params) {
        return Action.postJsonData(Interface.imageCaption + state.global.search, params);
    },
    /**段落浓缩 */
    doc_summary({ commit, state }, params) {
        return Action.postTextData(Interface.summary + state.global.search, params);
    },
}
