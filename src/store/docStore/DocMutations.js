import * as types from '../mutationTypes.js'
import App from 'AppCore';
import Utils from '../../libs/Utils.js';
import MyUtils from '../../libs/MyUtils.js';

import md5 from 'js-md5';


export default {
    [types.LOAD_CONFIG](state, data) {
        state.config = Object.assign({ ...state.config }, data);
        if (state.config.appType == 'aigc-creative') {
            state.config.appTitle = '依案成片';
        } else if (state.config.appType == 'aigc-document') {
            state.config.appTitle = '依稿成片';
        }
    },
    [types.SET_USER_INFO](state, data) {
        state.userInfo = data;
    },
    [types.RELOAD_DOCUMENT](state, data) {
        state.reloadDocument++;
    },
    [types.INIT_DOCUMENT_DETAIL](state, data) {
        let detail = data;
        let docId = detail.docId;
        let contents = detail.doc ? JSON.parse(detail.doc).contents : [];
        let saveData = {
            docId: docId,
            listItemData: detail,
            title: detail.title,//标题
            doc: detail.doc,//文搞纯文本
            contents: contents,//文搞内容
            sceneDatas: detail.docData || detail.docDatas || [],
            sceneType: detail.sceneType,//0 默认 1 按句号分 
            historySceneDatas: detail.docData || detail.docDatas || [],
        }
        if (detail.preference) {
            saveData.preference = detail.preference;
        }
        //将场景中的关键词提取出来
        let keyWords = [];
        keyWords = saveData.sceneDatas.map(item => item.keyWords).flat();
        keyWords = [...new Set(keyWords.map(item=>item.word))];
        saveData.keyWords = keyWords;
        this.commit(types.SET_DOCUMENT_DATA, saveData);
        this.commit(types.RELOAD_DOCUMENT);

    },
 
    [types.COMBINING_CONTENT](state, data) {
        //每个段落保证50字以上,无标点加句号
        let contents = data.contents;
        data.contents = MyUtils.combiningContent(contents);
        this.commit(types.SET_DOCUMENT_DATA, data);

    },
    [types.SET_DOCUMENT_DATA](state, data) {

        let beforeSceneType = state.documentData.sceneType;
        let beforeArrangemenType = state.documentData.arrangementType;
        let beforeContents = state.documentData.contents;
        state.documentData = Object.assign(state.documentData, data);//data;


        if (beforeSceneType != state.documentData.sceneType) {
            this.commit(types.CREATE_SCENE_DOCUMENT);
        }
        // console.log("beforeArrangemenType",beforeArrangemenType,"now",state.documentData.arrangementType)
        if (beforeArrangemenType != state.documentData.arrangementType) {
            this.commit(types.SET_ARRANGEMENT_DATA);
        }
        if (beforeContents != state.documentData.contents) {
            state.documentData.doc = JSON.stringify({ contents: state.documentData.contents });
            // console.log(state.documentData.doc)
            this.commit(types.SET_ARRANGEMENT_DATA);
        }
    },
    [types.SET_ARRANGEMENT_DATA](state, data) {
        let arrangementData = MyUtils.arrangementChange(
            state.documentData.arrangementType,
            state.documentData.contents
        );
        // console.log("切换排列方式=", arrangementData);
        state.documentData.arrangementData = arrangementData;
        this.commit(types.CREATE_SCENE_DOCUMENT);


    },
    [types.CREATE_SCENE_DOCUMENT](state, data) {
        let articleContents = state.documentData.contents;
        let arrangementData = state.documentData.arrangementData;
        let sceneType = state.documentData.sceneType;
        let keyWords = state.documentData.keyWords;
        keyWords = [...new Set(keyWords)];
        // console.log("keyWords=", keyWords);
        // console.log("articleContents", articleContents)
       
        const sceneDatas = [];
        const historySceneDatas = state.documentData.historySceneDatas;

        if (sceneType == '0') {
            splitByParagraph();
        } else if (sceneType == '1') {
            splitBySentence();
        }




        function splitByParagraph() {
            //按照段落分割
            for (let i = 0; i < arrangementData.length; i++) {
                let item = arrangementData[i];
                // console.log(item);
                item.texts.forEach(element => {
                    const scene = {
                        "keyWords": [],
                        "dub": {
                            "url": null,
                            "duration": 0,
                            "channels": 0
                        },
                        "sceneCfg": "",
                        // "estimateDuration": 0,
                        "type": "text",
                        "outerUrl": item.imageUrl,
                        "content": element.content,
                        "imgDesc":"",
                    }
                    let keywordData = getKeywordData(scene.content, keyWords)
                    scene.htmlText = keywordData.htmlText;
                    scene.keyWords = keywordData.keyWords;

                    if (item.imageUrl) {
                        scene.type = "image";
                        scene.imgDesc = item.imgDesc;
                    }
                    let randomStr = String(Math.floor(Math.random() * 1000)) + String(Math.floor(Math.random() * 1000));
                    scene.uuid = md5(scene.content + randomStr);
                    scene.estimateDuration = scene.content.length * 300;//ms
                    // 使用历史数据回写dub
                    for (let k = 0; k < historySceneDatas.length; k++) {
                        let b_item = historySceneDatas[k];
                        if (b_item.content == scene.content) {
                            scene.dub = b_item.dub;
                            scene.sceneCfg = b_item.sceneCfg || '';
                            break;
                        }
                    }
                    sceneDatas.push(scene)

                });

            }
            // console.log("sceneDatas=", sceneDatas)
            state.documentData.sceneDatas = sceneDatas;
        }
        function splitBySentence() {
            //语句分割
            let cloneData = JSON.parse(JSON.stringify(arrangementData))
            for (let i = 0; i < cloneData.length; i++) {
                let item = cloneData[i];
                let juArr = [];
                item.texts.forEach(element => {
                    let startIndex = 0;
                    const libs = ['。', '！', '？', '；']
                    let str = element.content;
                    for (let i = 0; i < str.length; i++) {
                        if (libs.indexOf(str[i]) >= 0) {
                            juArr.push(str.slice(startIndex, i + 1));
                            startIndex = i + 1;
                        }
                    }
                    if (startIndex < str.length - 1) {
                        juArr.push(str.slice(startIndex));
                    }
                });
                // item.splitArr=juArr;
                juArr.forEach(v => {
                    const scene = {
                        "keyWords": [],
                        "dub": {
                            "url": null,
                            "duration": 0,
                            "channels": 0
                        },
                        "sceneCfg": "",
                        // "estimateDuration": 0,
                        "type": "text",
                        "outerUrl": item.imageUrl,
                        "content": v
                    }
                    let keywordData = getKeywordData(scene.content, keyWords);
                    scene.htmlText = keywordData.htmlText;
                    scene.keyWords = keywordData.keyWords;


                    if (item.imageUrl) {
                        scene.type = "image";
                        scene.imgDesc = item.imgDesc;

                    }
                    let randomStr = String(Math.floor(Math.random() * 1000)) + String(Math.floor(Math.random() * 1000));
                    scene.uuid = md5(scene.content + randomStr);
                    scene.estimateDuration = scene.content.length * 300;//ms
                    // 使用历史数据回写dub
                    for (let k = 0; k < historySceneDatas.length; k++) {
                        let b_item = historySceneDatas[k];
                        if (b_item.content == scene.content) {
                            scene.dub = b_item.dub;
                            scene.sceneCfg = b_item.sceneCfg || '';
                            break;
                        }
                    }
                    sceneDatas.push(scene)

                });
            }
            state.documentData.sceneDatas = sceneDatas;
        }
        /**获取场景所需要的关键词相关信息 */
        function getKeywordData(content, keyWords) {
            let htmlText = content;
            let reText = "";
            for (let key of keyWords) {
                if (reText.indexOf(key) > -1) {
                    continue;
                }
                let label = `<span style='color:rgb(255, 114, 0)'>${key}</span>`
                htmlText = htmlText.replaceAll(key, label);
                reText += label;
            }

            // console.log(htmlText)
            let sceneKeyWords = getSceneKeyWords(htmlText, keyWords);
            // console.log("htmlText=",htmlText,"sceneKeyWords=",sceneKeyWords)
            return {
                htmlText,
                keyWords: sceneKeyWords,
            }
        }


        function getSceneKeyWords(content, keyWords, result = []) {
            let text = content;
            let spanStart = text.indexOf("<");
            if (spanStart == -1) return result;
            let spanEnd = text.indexOf("/span>");
            spanEnd += 6;
            let spanHtml = text.substring(spanStart, spanEnd);
            let keyword = spanHtml.match(/^<span.*>(.*)<\/span>$/i)[1];
            let start = 0, end = 0;
            start = spanStart;
            end = start + keyword.length;
            text = text.substring(0, spanStart) + keyword + text.slice(spanEnd)
            result.push({ start, end, word: keyword })
            return getSceneKeyWords(text, keyWords, result);
        }
    },


    [types.SET_SCENE_DATA](state, data) {
        state.documentData.sceneDatas = data.map(item => Object.assign({}, item));

    },

    [types.CLEAR_REUN_DATA](state, data) {
        state.undoData = [];
        state.redoData = [];
    },

    [types.SET_UNDO_DATA](state, data) {
        // console.log(data)
        //栈，先进后出
        if (state.undoData.length < 10) {
            state.undoData.push(data);
        } else {
            state.undoData.shift();
            state.undoData.push(data);
        }
        // state.undoData = data;
    },
    [types.SET_REDO_DATA](state, data) {

        if (state.redoData.length < 10) {
            state.redoData.push(data);
        } else {
            state.redoData.shift();
            state.redoData.push(data);
        }
        // state.redoData = data;
    },

    [types.UNDO_HANDLER](state, data) {
        if (state.undoData.length == 0) return;
        // console.log("undo", state.undoData)
        //撤销操作
        let undoItem = state.undoData.pop();
        this.commit(types.SET_DOCUMENT_DATA, { contents: undoItem.oldValue });
        this.commit(types.SET_REDO_DATA, undoItem);
        // console.log(undoItem)
    },
    [types.REDO_HANDLER](state, data) {
        if (state.redoData.length == 0) return;
        //恢复操作
        let redoItem = state.redoData.pop();
        // console.log(redoItem)
        this.commit(types.SET_DOCUMENT_DATA, { contents: redoItem.newValue });
        this.commit(types.SET_UNDO_DATA, redoItem);
    },
} 