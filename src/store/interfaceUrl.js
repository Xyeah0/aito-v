export const verify = window.AppConfig.modulePath + '/svip/login-auth/validate';//获得权限

export const titleTailList = window.AppConfig.modulePath + '/svip/template/list/head-tail'//获得片头片尾例表
export const titleTailSave = window.AppConfig.modulePath + '/svip/template/head-tail/save'//获得片头片尾例表
export const titleTailDel = window.AppConfig.modulePath + '/svip/template/head-tail/delete'//获得片头片尾例表
export const titleTailTop = window.AppConfig.modulePath + '/svip/template/head-tail/top'//获得片头片尾例表
// export const config = '/postervideo/config';//获得配置项'
export const uploadFile = window.AppConfig.modulePath + '/svip/resources/upload';//上传文件

export const submitRender = window.AppConfig.modulePath + '/svip/process/aigc/submit';//提交
export const taskList = window.AppConfig.modulePath + '/svip/process/task-list';//任务例表
export const taskDelete = window.AppConfig.modulePath + '/svip/process/task-delete';//删除任务
export const downLoadWorks = window.AppConfig.modulePath + '/svip/product/download';//下载模板

// export const saveDraft = window.AppConfig.modulePath + '/svip/interaction/save-draft';//保存草稿
// export const deleteDraft = window.AppConfig.modulePath + '/svip/interaction/delete-draft';//删除草稿
// export const listDraft = window.AppConfig.modulePath + '/svip/interaction/list-draft';//草稿列表
// export const detailDraft = window.AppConfig.modulePath + '/svip/interaction/query-draft-data';//草稿详情

// 全部模板页面
export const getTemplateCategory = window.AppConfig.modulePath + '/svip/template/category/categories';//获得模板分类
export const getTemplateList = window.AppConfig.modulePath + '/svip/template/list/poster-pack-v12';//获取海报/场景包装模板列表
export const getTemplateDetail = window.AppConfig.modulePath + '/svip/template/template-data/';//获得模板详情

//编辑页所用
export const getMediaListOfIds = window.AppConfig.modulePath + '/svip/resources/getByIds';//根据ids获得素材例表
export const getMediaList = window.AppConfig.modulePath + '/svip/resources/outer-resources';//获得素材例表
export const getCatalog = window.AppConfig.modulePath + '/svip/resources/outer-catalogs';//获得栏目
export const getTemplateList_dynamic = `${window.AppConfig.modulePath}/svip/dynamic-subtitles/presetList`;//动态字幕模板列表
export const getTemplateList_effects = `${window.AppConfig.modulePath}/aigc/effects/list`;//内置的贴图.音乐
// export const getTimeLineList = `${window.AppConfig.modulePath}/aigc/pipeline/list`;//获得时间线列表
// export const saveTimeLine = `${window.AppConfig.modulePath}/aigc/pipeline/save`;//保存/编辑时间线
// export const deleteTimeLine = `${window.AppConfig.modulePath}/aigc/pipeline/delete`;//删除时间线
// export const getTimeLineInfo = `${window.AppConfig.modulePath}/aigc/pipeline/pipeline-info`;//获得单个时间线详情
export const saveDraft = window.AppConfig.modulePath + '/svip/aigc-data/save-draft';//保存草稿
export const deleteDraft = window.AppConfig.modulePath + '/svip/aigc-data/delete-draft';//删除草稿
export const listDraft = window.AppConfig.modulePath + '/svip/aigc-data/list-draft';//草稿列表
export const detailDraft = window.AppConfig.modulePath + '/svip/aigc-data/query-draft-data';//草稿详情

export const pipelineSave = `${window.AppConfig.modulePath}/aigc/v2/pipeline/save`;//保存aigc所需要信息
export const txt2img = `${window.AppConfig.modulePath}/aigc-pic/txt2img`;//文本生成图像


//文案创意
export const expandWrite = window.AppConfig.modulePath + '/aigc/v2/paperwork/extend';//智能扩写
export const copyWritingList = window.AppConfig.modulePath + '/aigc/v2/paperwork/list';//文案列表
export const deletePaperwork = window.AppConfig.modulePath + '/aigc/v2/paperwork/delete';//删除文案
export const updatePaperwork = window.AppConfig.modulePath + '/aigc/v2/paperwork/update';//修改文案信息
export const highlight = window.AppConfig.modulePath + '/aigc/v2/paperwork/highlight';//关键词
export const textProofread = window.AppConfig.modulePath + '/aigc/v2/paperwork/check';//文本校对
export const textExamine = window.AppConfig.modulePath + '/aigc/v2/paperwork/examine';//文本审核
export const textAiSplit = window.AppConfig.modulePath + '/aigc/v2/paperwork/shot';//智能分段
// export const intelligentSubmit = window.AppConfig.modulePath + '/aigc/v2/paperwork/submit';//智能处理
export const intelligentSubmit = window.AppConfig.modulePath + '/aigc/v2/paperwork/submit2spi';//智能处理

export const intelligentProcess = window.AppConfig.modulePath + '/aigc/v2/paperwork/submitProcess';//智能进度
export const copyWritingConfig = window.AppConfig.modulePath + '/aigc/v2/paperwork/config';//文案创意设置
export const save = window.AppConfig.modulePath + '/aigc/v2/paperwork/save';//保存文案信息
export const paperworkDetail = window.AppConfig.modulePath + '/aigc/v2/paperwork/detail';//查询文案详情
export const lock = window.AppConfig.modulePath + '/aigc/v2/paperwork/lock';//文案锁定/解锁


//文案时间线
export const pipelineScene = window.AppConfig.modulePath + '/aigc/v2/pipeline/text';//智能场景数据
export const pipelineDub = window.AppConfig.modulePath + '/aigc/v2/pipeline/dub';//文本转语音



/**document */
export const getExtractHTML = window.AppConfig.modulePath + '/nodeconvert/getExtractHTML';//解析
export const getBdrealtime = window.AppConfig.modulePath + '/nodeconvert/getBdrealtime';//热搜
export const getBdNewsContent = window.AppConfig.modulePath + '/nodeconvert/getBdNewsContent';//热搜解析
export const saveDocument = window.AppConfig.modulePath + '/aigc/doc/save';//文稿保存
export const updateDocument = window.AppConfig.modulePath + '/aigc/doc/update';//文稿更新
export const deleteDocument = window.AppConfig.modulePath + '/aigc/doc/delete';//文稿删除
export const getDocLists = window.AppConfig.modulePath + '/aigc/doc/list';//文稿列表
export const getDocDetail = window.AppConfig.modulePath + '/aigc/doc/detail';//文稿详情
export const docIntelligentSubmit = window.AppConfig.modulePath + '/aigc/doc/submit2Spi';//智能处理
export const imageCaption = window.AppConfig.modulePath + '/aigc/doc/image-caption';//语义分析
export const summary = window.AppConfig.modulePath + '/aigc/doc/summary';//段落浓缩

//内容库
export const getAiTagAndExamine = window.AppConfig.modulePath + '/spider-material/web/aitask/async/getAiTagAndExamine';//获得标签素材例表
export const updateAi = window.AppConfig.modulePath + '/spider-material/web/aitask/async/updateAi';//更新标签素材例表
export const getProductMainResourceById = window.AppConfig.modulePath + '/spider-material/web/productMainResource/getProductMainResourceById';// 获取素材详情
//24/123标签接口
export const getAiTags = window.AppConfig.modulePath + '/spider-material/web/aiTag/getAiTags';// 标签-查
export const getAiTagsClasses = window.AppConfig.modulePath + '/spider-material/web/aiTag/classList';// 标签分类-查
export const updateAiTags = window.AppConfig.modulePath + '/spider-material/web/aiTag/update';// 标签-编辑
export const addAiTags = window.AppConfig.modulePath + '/spider-material/web/aiTag/add';// 标签-增加
export const deleAiTags = window.AppConfig.modulePath + '/spider-material/web/aiTag/delete';// 标签-删




