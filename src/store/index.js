import Vue from 'vue'
import Vuex from 'vuex'
import appStore from './appStore/AppStore.js'
import docStore from './docStore/DocStore.js'


// import tlStore from './tlStore/TlStore.js'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: { 
		appStore,
		docStore,
		// tlStore,
	}
})
