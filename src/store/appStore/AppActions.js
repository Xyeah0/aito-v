import * as Action from '../actions.js';
import * as types from '../mutationTypes.js';
import * as Interface from '../interfaceUrl.js';

import MySocket from '../../plugins/socket';
import AppStore from '../../store/appStore/AppStore.js';
import md5 from 'js-md5';

export default {
    app_verify({ commit, state }) {
        return Action.getJsonData(Interface.verify + state.global.search, {});
    },
    app_loadConfig({ commit }) {
        // Action.postJsonData(Interface.config,{}).then( data =>{
        //     commit(types.LOAD_CONFIG,data);
        // }).catch( err =>{
        //     console.log('配置项加载错误',err)
        // })
    },
    connectSocket({ commit, state }) {
        const onmessage = (event) => {
            console.log('ws message', event)
            if (event.data != 'pong') {
                let msg = JSON.parse(event.data);
                if (msg.type == 'taskStatus' || msg.type == 'templateTaskStatus') {
                    commit(types.TASK_LIST_MSG, JSON.parse(msg.message));
                }
            }
        }
        console.log('connectSocket', state.userInfo, state.config.appType)
        let mySocket = null;
        if (process.env.NODE_ENV == 'development') {


            mySocket = new MySocket('https://console.mtooltest.chinamcloud.cn/yumi-aitovideo' + '/appsocket/ws/' + state.config.appType + '/' + state.userInfo.token, onmessage);
            // mySocket = new MySocket('https://svip-mtool.mty.chinamcloud.com/svip' + '/ws/' + state.config.appType + '/' + state.userInfo.token, onmessage);
        } else {
            mySocket = new MySocket(window.AppConfig.modulePath + '/appsocket/ws/' + state.config.appType + '/' + state.userInfo.token, onmessage);
        }

        mySocket.connect();
    },
    connectSocket2({ commit, state }) {
        const onmessage = (event) => {
            console.log('aigc ws message', event)
            if (event.data != 'pong') {
                let msg = JSON.parse(event.data);
                console.log('msg', msg)
                if (msg.method == 'dubCallback') {
                    commit(types.BACK_DUB, msg.message);
                } else if (msg.method == 'hqy_plot_txt2img') {
                    commit(types.BACK_AIDRAW, msg.message);
                } else if (msg.method == 'onOpen') {
                    state.sessionId = msg.message;
                }
                // if (msg.type == 'taskStatus' || msg.type == 'templateTaskStatus') {
                //     commit(types.TASK_LIST_MSG, JSON.parse(msg.message));
                // }
            }
        }
        const onclose = (event) => {
            //发起任务查询
        }
        console.log('connectSocket aigc', window.AppConfig.AiToVideoAIGC)
        let mySocket = null;//state.config.appType
        if (process.env.NODE_ENV == 'development') {
            mySocket = new MySocket("https://console.mtooltest.chinamcloud.cn/yumi-aitovideo" + '/aigcsocket/websocket/' + state.config.appType + '/' + state.userInfo.token, onmessage);
            // mySocket = new MySocket('https://aigc.mty.chinamcloud.com' + '/aigc/websocket/' + 'aigc-creative' + '/' + state.userInfo.token, onmessage, null, null, onclose);
        } else {
            mySocket = new MySocket(window.AppConfig.modulePath + '/aigcsocket/websocket/' + state.config.appType + '/' + state.userInfo.token, onmessage);
        }
        // const mySocket = new MySocket("http://172.29.3.57:9092/websocket/aigc/eyJhZG1pbmlzdHJhdG9yIjp0cnVlLCJjbWNMb2dpbklkIjoiZDcyM2M5ODk4NzA5MzZlZmVkZDA1NjIxOWMzZjgxZGYiLCJjbWNMb2dpblRpZCI6IjYwYWU4OGYwMjlkYWY2YjQ1NzE3ZGE4MGIxMDJkZTU4IiwiY21jVXNlclRva2VuIjoiOGVmZTg4MGIzNDQzMzNjYzJiMDkyYzJiMmM5NmUyMzMiLCJuaWNrbmFtZSI6IuWqkuS9k-S6kSIsInByb2R1Y3RJZCI6ImE0MDg1M2M5Y2VmZTcyYzYxNzg0MGQ0Y2M5YWRhYzg5Iiwicm9sZXMiOltdLCJ0ZW5hbnRpZCI6IjIyNmViYmExNDM4NzhiYTFjZTlmMjU5MDMyODA0NWFkIiwidWlkIjoiMiIsInVzZXJuYW1lIjoiY21jZWYyMTIyNTYiLCJ1c2VybmljayI6IuWqkuS9k-S6kSJ9", onmessage);
        mySocket.connect();
    },
    // 全部页面获得模板分类
    app_loadTLFilterTabs({ commit, state }, params) {
        return Action.getJsonData(Interface.getTemplateCategory + state.global.search, params);
    },
    app_loadTLList({ commit, state }, params) {
        return Action.postJsonData(Interface.getTemplateList + state.global.search, params);
    },
    app_loadTemplateOfId({ commit, state }, params) {
        return Action.getJsonData(Interface.getTemplateDetail + params.templateId + state.global.search);
    },
    //获取动态字幕模板列表
    app_getTemplateListDynamic({ commit, state }, _params) {
        // const queryConfig = state.config.queryConfig;
        // const params = Object.assign({...queryConfig},_params);
        return Action.postJsonData(Interface.getTemplateList_dynamic + state.global.search, _params);
    },
    //获取内置贴图、音乐等
    app_getTemplateListEffects({ commit, state }, _params) {
        // const queryConfig = state.config.queryConfig;
        // const params = Object.assign({...queryConfig},_params);
        return Action.postJsonData(Interface.getTemplateList_effects + state.global.search, _params);
    },

    //素材
    app_loadMediaList({ commit, state }, params) {
        return Action.postJsonData(Interface.getMediaList + state.global.search, params);
    },
    app_loadMediaOfIds({ commit, state }, params) {
        return Action.getJsonData(Interface.getMediaListOfIds + state.global.search, params);
    },
    app_getCatalog({ commit, state }, params) {
        return Action.getJsonData(Interface.getCatalog + state.global.search, params);
    },
    //保存aigc所需要信息
    app_pipelineSave({ commit, state }, params) {
        return Action.postJsonData(Interface.pipelineSave + state.global.search, params);
    },
    //提交和任务
    app_submit({ commit, state }, params) {
        return Action.postJsonData(Interface.submitRender + state.global.search, params);
    },
    app_taskList({ commit, state }, params) {
        return Action.postJsonData(Interface.taskList + state.global.search, params);
    },
    app_taskDelete({ commit, state }, params) {
        return Action.postFormData(Interface.taskDelete + state.global.search, params);
    },
    //任务列表下载
    app_downLoad({ commit, state }, params) {
        let { taskId } = params;
        let url = Interface.downLoadWorks + state.global.search + '&taskId=' + taskId;
        return url;
    },
    //片头片尾
    app_loadTitleTailList({ commit, state }) {
        return Action.getJsonData(Interface.titleTailList + state.global.search, {})
    },
    app_saveTitleTail({ commit, state }, params) {
        return Action.postFormData(Interface.titleTailSave + state.global.search, params);
    },
    app_delTitleTail({ commit, state }, params) {
        return Action.postFormData(Interface.titleTailDel + state.global.search, params);
    },
    app_topTitleTail({ commit, state }, params) {
        return Action.getJsonData(Interface.titleTailTop + state.global.search, params)
    },
    //草稿(时间线)
    app_saveDraft({ commit, state }, params) {
        return Action.postJsonData(Interface.saveDraft + state.global.search, params)
    },
    app_deleteDraft({ commit, state }, params) {
        return Action.getJsonData(Interface.deleteDraft + state.global.search, params)
    },
    app_listDraft({ commit, state }, params) {
        return Action.postJsonData(Interface.listDraft + state.global.search, params)
    },
    app_getDraftDetail({ commit, state }, params) {
        return Action.getJsonData(Interface.detailDraft + state.global.search, params)
    },
    //时间线操作
    // app_getTimeLineList({ commit, state }, params) {
    //     // return Action.getJsonData(Interface.getTimeLineList + state.global.search, params);
    //     return new Promise((resolve,reject) =>{
    //         setTimeout(()=>{
    //             resolve({
    //                 "data": [
    //                     {
    //                         "pipelineId": "a740f916cc40490f847061a6b46d1f71",
    //                         "title": "太阳时间线",
    //                         "textId": "101",
    //                         "templateId": "84Y9AUDOA0R58G08G0AGSF0OAFH",
    //                         "createTime": "2023-02-13 11:50:32",
    //                         "updateTime": "2023-02-13 12:30:14",
    //                         "createUserId": "123",
    //                         "release": ""
    //                     },
    //                     {
    //                         "pipelineId": "7652f916ccf847061a6b125sfa42411",
    //                         "title": "月亮时间线",
    //                         "textId": "102",
    //                         "templateId": "GDIASUDA289012G10RGAIGDAOGF",
    //                         "createTime": "2023-02-13 11:55:32",
    //                         "updateTime": "2023-02-14 07:13:15",
    //                         "createUserId": "123",
    //                         "release": ""
    //                     }
    //                 ],
    //                 "code": 0,
    //                 "desc": "success",
    //                 "total": 2
    //             });
    //         },1000)
    //     })
    // },
    // app_saveTimeLine({ commit, state }, params) {
    //     return Action.getJsonData(Interface.saveTimeLine + state.global.search, params)
    // },
    // app_deleteTimeLine({ commit, state }, params) {
    //     return Action.getJsonData(Interface.deleteTimeLine + state.global.search, params)
    // },
    // app_getTimeLineInfo({ commit, state }, params) {
    //     return Action.getJsonData(Interface.getTimeLineInfo + state.global.search, params)
    // },
    // getResourceList({commit,state},[_params,callback]){
    //     const success = data => {
    //         commit(types.GET_SOURCELIST,data)
    //         if(callback){
    //             callback(true,data);
    //         }
    //     }
    //     const error = err => {
    //         commit(types.GET_SOURCELIST,err)
    //         if(callback){
    //             callback(false,err);
    //         }
    //     }
    //     const params = Object.assign({params:{
    //         currentpage: _params.currentpage,
    //         rownum: _params.rownum,
    //         catalogid:_params.catalogid || '1',
    //         title: _params.searchText || '',
    //         type: _params.type || '',
    //         range:{
    //             start: _params.startDate || '',
    //             end: _params.endDate || ''
    //         }
    //     }},Util.config.defaultParams);
    //     Action.postJsonData(Util.interfaceObj.getResources,params,success,error);
    // }
    //场景智能数据
    //获得智能场景数据
    // app_getAiScene({ commit,state}, params){
    //     const AiSceneData = {
    //         scenes:[
    //             {
    //                 content:'小学生的名字，代表了家长的期许',
    //                 duration:9966,
    //                 material:{
    //                     type:'video',
    //                     duration:9966,
    //                     cover:"https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/Pic/2023/02/09/06b0f13e088d454993d6224d70c6b1e0_20000k.jpg",
    //                     path: "X:/226ebba143878ba1ce9f2590328045ad/2023/02/09/06b0f13e088d454993d6224d70c6b1e0_4000k.mp4",
    //                     preUrl: "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/02/09/06b0f13e088d454993d6224d70c6b1e0_500k.mp4",
    //                     width:"1280",
    //                     height:"720",
    //                     frameRate:"25",
    //                     channels:"2",
    //                     inPoint:0,
    //                     outPoint:9966
    //                 }
    //             },
    //             {
    //                 content:'起名字是一件大事，现在很多年轻家长',
    //                 duration:5000,
    //                 material:{
    //                     "addUser": "cmcef212256",
    //                     "addUserNickname": "媒体云",
    //                     "aiType": "",
    //                     "asr": false,
    //                     "asrTrans": false,
    //                     "channels": "",
    //                     "contentId": "257f633ba16445edba5f93d1a8e9e77b",
    //                     "cover": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/257f633ba16445edba5f93d1a8e9e77b_s.png",
    //                     "duration": 0,
    //                     "frameRate": "",
    //                     "height": "427",
    //                     "id": 21454,
    //                     "ossFlag": 0,
    //                     "path": "X:\\\\226ebba143878ba1ce9f2590328045ad\\2023\\03\\01\\257f633ba16445edba5f93d1a8e9e77b.jpg",
    //                     "preUrl": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/257f633ba16445edba5f93d1a8e9e77b.jpg",
    //                     "storageId": 0,
    //                     "title": "名字1",
    //                     "type": "image",
    //                     "updateTime": "2023-03-01 15:43:41",
    //                     "width": "613"
    //                 }
    //             },
    //             {
    //                 content:'思想前卫，接受了高等教育',
    //                 duration:5000,
    //                 material:{
    //                     "addUser": "cmcef212256",
    //                     "addUserNickname": "媒体云",
    //                     "aiType": "",
    //                     "asr": false,
    //                     "asrTrans": false,
    //                     "channels": "",
    //                     "contentId": "93216912bf8c49199370f3784b50f6f9",
    //                     "cover": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/93216912bf8c49199370f3784b50f6f9_s.png",
    //                     "duration": 0,
    //                     "frameRate": "",
    //                     "height": "419",
    //                     "id": 21446,
    //                     "ossFlag": 0,
    //                     "path": "X:\\\\226ebba143878ba1ce9f2590328045ad\\2023\\03\\01\\93216912bf8c49199370f3784b50f6f9.jpg",
    //                     "preUrl": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/93216912bf8c49199370f3784b50f6f9.jpg",
    //                     "storageId": 0,
    //                     "title": "名字2",
    //                     "type": "image",
    //                     "updateTime": "2023-03-01 15:43:41",
    //                     "width": "646"
    //                 }
    //             },
    //             {
    //                 content:'深知名字不会对学生的命运产生什么影响，逐渐变得随意起来',
    //                 duration:5000,
    //                 material:{
    //                     "addUser": "cmcef212256",
    //                     "addUserNickname": "媒体云",
    //                     "aiType": "",
    //                     "asr": false,
    //                     "asrTrans": false,
    //                     "channels": "",
    //                     "contentId": "9c88226396f34b09ae83d68d94929747",
    //                     "cover": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/9c88226396f34b09ae83d68d94929747_s.png",
    //                     "duration": 0,
    //                     "frameRate": "",
    //                     "height": "844",
    //                     "id": 21450,
    //                     "ossFlag": 0,
    //                     "path": "X:\\\\226ebba143878ba1ce9f2590328045ad\\2023\\03\\01\\9c88226396f34b09ae83d68d94929747.jpg",
    //                     "preUrl": "https://mserver.mty.chinamcloud.com/0/226ebba143878ba1ce9f2590328045ad/2023/03/01/9c88226396f34b09ae83d68d94929747.jpg",
    //                     "storageId": 0,
    //                     "title": "名字3",
    //                     "type": "image",
    //                     "updateTime": "2023-03-01 15:43:41",
    //                     "width": "1228"
    //                 }
    //             }
    //         ]
    //     }
    //     return new Promise((resolve,reject) =>{
    //         setTimeout(()=>{
    //             resolve({
    //                 returnCode:0,
    //                 returnData:AiSceneData,
    //                 returnDesc:'返回成功'
    //             });
    //         },2000)
    //     })
    // },
    app_expandWrite({ commit, state }, params) {
        return Action.postJsonData(Interface.expandWrite + state.global.search, params);
    },
    app_getCopyWritingList({ commit, state }, params) {
        return Action.postJsonData(Interface.copyWritingList + state.global.search, params);
    },
    app_deletePaperwork({ commit, state }, params) {
        return Action.deleteJsonData(Interface.deletePaperwork + state.global.search, params);
    },
    app_textProofread({ commit, state }, params) {
        return Action.postJsonData(Interface.textProofread + state.global.search, params);
    },
    app_textExamine({ commit, state }, params) {
        return Action.postJsonData(Interface.textExamine + state.global.search, params);
    },
    app_textAiSplit({ commit, state }, params) {
        return Action.postTextData(Interface.textAiSplit + state.global.search, params);
    },
    //提交智能处理
    app_intelligentSubmit({ commit, state }, params) {
        return Action.postJsonData(Interface.intelligentSubmit + state.global.search, params);
    },
    //查询智能处理进度
    app_intelligentProcess({ commit, state }, params) {
        return Action.getJsonData(Interface.intelligentProcess + state.global.search, params);
    },
    app_copyWritingConfig({ commit, state }, params) {
        return Action.postJsonData(Interface.copyWritingConfig + state.global.search, params);
    },
    app_highlight({ commit, state }, params) {
        return Action.postJsonData(Interface.highlight + state.global.search, params);
    },
    //保存文案信息
    app_save({ commit, state }, params) {
        return Action.postJsonData(Interface.save + state.global.search, params);
    },
    //修改文案信息
    app_updatePaperwork({ commit, state }, params) {
        return Action.postJsonData(Interface.updatePaperwork + state.global.search, params);
    },
    //查询文案详情
    app_paperworkDetail({ commit, state }, params) {
        return Action.getJsonData(Interface.paperworkDetail + state.global.search, params);
    },
    //查询智能处理结果（场景数据）
    app_pipelineScene({ commit, state }, params) {
        return Action.getJsonData(Interface.pipelineScene + state.global.search, params);
    },
    //发起配音
    app_pipelineDub({ commit, state }, params) {
        // return Action.postTextData(Interface.pipelineDub + state.global.search, params.dubText);
        return Action.postJsonData(Interface.pipelineDub + state.global.search + '&sessionId=' + state.sessionId, params);
    },
    //文案锁定/解锁
    app_lock({ commit, state }, params) {
        return Action.getJsonData(Interface.lock + state.global.search, params);
    },
    //自动保存
    app_autoSave({ commit, state }, params) {
        params = {
            textId: state.creativeTextData.textId || null,
            title: state.creativeTextData.title, //标题
            text: state.creativeTextData.originality, //创意
            aiText: state.creativeTextData.copywriting,//扩写文本 this.creativeTextData.copywritingplainCopywriting,
            sceneType: state.creativeTextData.sceneType, //场景类型
            sceneDatas: state.creativeTextData.sceneDatas,//场景数据
            htmlCopywriting: state.creativeTextData.htmlCopywriting,//html数据
            preference: formatPreference(state.creativeTextData.preference),//用户偏好设置
        };
        if (!params.title) return;
        // console.log(params);
        if (!params.textId) {
            this.dispatch("app_save", params).then(res => {
                if (res.returnCode === 0) {
                    commit(types.SET_CREATIVE_TEXT_DATA, { textId: res.returnData });
                } else {
                    // this._vm.$message.error(res.returnDesc || "自动保存失败");
                }

            })
        } else {
            this.dispatch("app_updatePaperwork", params).then(res => {
                if (res.returnCode === 0) {

                } else {
                    // this._vm.$message.error(res.returnDesc || "自动保存失败");
                }
            })
        }

        //格式化偏好数据
        function formatPreference(preference) {
            if (!preference) return null;
            let originPreference = JSON.parse(JSON.stringify(preference));
            let { findType, isOnlyRange, catalogIds, mediaIds } = originPreference.mediaRange;
            if (findType === "all") {
                originPreference.mediaRange = {
                    findType: findType,
                }
            } else if (findType === "catalog") {
                originPreference.mediaRange = {
                    findType: findType,
                    isOnlyRange: isOnlyRange,
                    catalogIds: catalogIds,
                }
            } else if (findType === "mediaList") {
                originPreference.mediaRange = {
                    findType: findType,
                    isOnlyRange: isOnlyRange,
                    mediaIds: mediaIds
                }
            }
            return originPreference;
        }

    },
    //文生图
    app_txt2img({ commit, state }, params) {
        // console.log(window.AppConfig.appType)
        let appType = "aigc-creative";
        if (window.AppConfig.appType != "aigc-creative") {
            appType = "aigc-document";
        }
        const base = {
            "prompt": "一只猫",
            "negativePrompt": "",
            "restoreFaces": true,
            "samplingMethod": "Euler a",
            "samplingSteps": 30,
            "batchCount": 1,
            "width": 512,
            "height": 512,
            "cfgScale": 7,
            "style": "写实",
            "appType": appType,

        }
        return Action.postJsonData(Interface.txt2img + state.global.search + '&sessionId=' + state.sessionId, Object.assign(base, params));
    },

    get_aiSplit({ commit, state }, data) {
        let key = md5(data.content); console.log('key get', key, data)
        const cachesAiSplit = state.cachesAiSplit;
        // console.log('cachesAiSplit.hasOwnProperty(key)',cachesAiSplit.hasOwnProperty(key),cachesAiSplit[key])
        if (cachesAiSplit.hasOwnProperty(key)) {
            return cachesAiSplit[key];

        }
        return null;

    },
    //
    app_getAiTagAndExamine({ commit, state }, params) {
        return Action.getJsonData(Interface.getAiTagAndExamine + state.global.search, params);
    },
    //
    app_updateAi({ commit, state }, params) {
        return Action.postJsonData(Interface.updateAi + state.global.search, params);
    },
    //素材详情接口 getProductMainResourceById
    getProductMainResourceById({ commit, state }, params) {
        return Action.getJsonData(Interface.getProductMainResourceById + state.global.search, params);
    },
    //标签新-增删改查
    app_updateAiTags({ commit, state }, params) {
        return Action.postJsonData(Interface.updateAiTags + state.global.search, params);
    },
    app_addAiTags({ commit, state }, params) {
        return Action.postJsonData(Interface.addAiTags + state.global.search, params);
    },
    app_deleAiTags({ commit, state }, params) {
        return Action.postJsonData(Interface.deleAiTags + params);
    },
    ////查
    app_getAiTags({ commit, state }, params) {
        return Action.getJsonData(Interface.getAiTags + state.global.search, params);
    },
    app_getAiTagsClasses({ commit, state }, params) {
        return Action.getJsonData(Interface.getAiTagsClasses + state.global.search + '&' + params);
    },

}
