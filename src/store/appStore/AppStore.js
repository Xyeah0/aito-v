import mutations from './AppMutations.js'
import actions from './AppActions.js'


function initParams(search) {
    let newSearch = '';
    let contentSourceId = '';
    let url_params = search.replace("?", "").split("&");
    // console.log('url_params =',url_params,window.location.search)
    const obj = {};
    for (let k = 0; k < url_params.length; k++) {
        if (url_params[k] === '') continue;
        let arr = url_params[k].split("=");
        let key = arr[0], value = decodeURIComponent(arr[1]) || "";
        if (key.toLocaleLowerCase() === 'contentsourceid' || key.toLocaleLowerCase() === 'contentids') {
            contentSourceId = value;
            obj.contentSourceId = value;
            continue;
        }
        if (key == 'rawRoute') continue;
        obj[key] = value;
        if (newSearch) {
            newSearch += '&' + key + '=' + value;
        } else {
            newSearch += '?' + key + '=' + value;
        }
    }
    return {
        rawSearch: search,
        search: newSearch,
        params: obj,
        contentSourceId: contentSourceId,
    };
}

const state = {
    userInfo: null,//用户信息
    global: initParams(window.location.search),
    bodyWidth: 0,
    fullscreenLoading: true,//全网页加载中
    config: {
        themeColor: '#409EFF',
        appType: 'creative',
        appTitle: '依案成片',
        containerBg: '1',
        canUpload: false,
        uploadLimit: 100,//M 文件大小
    },
    bgConfig: {
        bgMode: 0,//0 默认模板，1 选择图片 2 纯色背景
    },
    taskListMsg: {},
    taskPromptDot: false,//新任务提示小圆点
    templateData: {},
    filterAiData:[],//智能筛选的素材列表
    articleAiData: {},
    cachesAiSplit:{},//智能分断缓存
    creativeTextData: {
        textId: "",//当前文案ID
        listItemData: {},//当前从文案列表打开时记录的对象信息
        title: "",//标题
        originality: "",//创意
        copywriting: "",//文案纯文本
        plainCopywriting:'',//带格式的纯文本
        htmlCopywriting:'',//html文本
        aiSplit:[],
        sceneType:'0',//0 默认 1 按句号分 2 精细分
        sceneDatas:[],//场景数据
        historySceneDatas:[],//历史场景
        keyWordColor: 'rgb(255, 114, 0)',//关键词颜色
        locked:0,
        preference: {
            mediaRange: {
                findType: 'all',// catalog  mediaList
                isOnlyRange: true,//仅选  false优选
                catalogIds: [],//ids
                mediaIds: []//ids
            },
            audioCfg: 'onlyBg' //bgAndMain onlyDub
        },//用户偏好设置
    },
    reloadCreative: 0,//重新加载创意页面
    editorInstance: null,//富文本剪辑器
    checkedKeyWord:false,//是否已经处理过关键词
    highlightData: {
        type: "_keyword",//类型
        value: ["文案", "hello"],//值
        style: {
            color: "rgb(255, 114, 0)",
            // background:"gray"
        },//样式
    },//高亮
    proofreadLists: [],//文本校对列表
    showAdjustArea: false,//显示校对审核区域
    examineData: [],//文本审核信息
    advancedSetData: {
        "dynamicLevel": "0.8",
        "duplicateLevel": "1.5",
        "sceneSeg": "sentence"
    },//高级设置
    sessionId:'',//aigc socketID
    dubTasks:[],//语音合成任务列表 {uuid:xxx,taskId:xxxx}
    aiDrawTasks:[],//ai作画任务列表 {sceneId:xxx,taskId:xxxx}
}


const getters = {

}

export default {
    state,
    actions,
    getters,
    mutations
}
