import * as types from '../mutationTypes.js'
import testJSON from './test.json';
import testJSON3 from './test3.json';
import testJSON4 from './test4.json';
import App from 'AppCore';
import Utils from '../../libs/Utils.js';
import md5 from 'js-md5';


export default {
    [types.LOAD_CONFIG](state, data) {
        state.config = Object.assign({ ...state.config }, data);
        if (state.config.appType == 'aigc-creative') {
            state.config.appTitle = '依案成片';
        } else if (state.config.appType == 'aigc-document') {
            state.config.appTitle = '依稿成片';
        }
    },
    [types.SET_USER_INFO](state, data) {
        state.userInfo = data;
    },
    [types.GET_ARTICLEAIDATA](state, data) {
        const summary = [];//已经挑选
        data.tagList.forEach((element, index) => {
            element.isSelect = false;
            if (index % 2 == 0) {
                element.isSelect = true;
                summary.push({
                    order: summary.length,
                    type: element.type,
                    source: element.source
                })
            }
        });
        data.summary = summary;
        state.articleAiData = data;
    },
    [types.SET_TEMPLATEDATA](state, data) {
        state.templateData = testJSON;//data;
    },
    [types.INIT_TIMELINE](state, data) {

        state.templateData = data.tlData;
        state.dubTasks = [];//清空配音任务
        // let reg = new RegExp(`<aikeyword.*?>(.*?)<\/aikeyword>`, "gm");
        let sceneDatas = state.creativeTextData.sceneDatas;

        if (window.AppConfig.appType === "aigc-document") {
            sceneDatas = this.state.docStore.documentData.sceneDatas;
        }
        for (let i = 0; i < data.sceneData.length; i++) {
            let sdata = data.sceneData[i];
            // sdata.kcontent = sdata.content;
            let order = i % sceneDatas.length;
            let sceneItem = sceneDatas[order];

            if (sdata.content == sceneItem.rawText) {
                sdata.keyWords = sceneItem.keyWords;
            } else {
                sdata.keyWords = [];
            }
            sdata.uuid = md5(sdata.content) + "_" + i;
            sdata.rawContent = sdata.content;
        }

        App.activeSequence.initTimeLineOfTemplate(data.tlData, data.sceneData);
        //查询原有配音文件
        const appSceneDatas = App.activeSequence.sceneDatas;
        for (let i = 0; i < appSceneDatas.length; i++) {
            let app_scene_item = appSceneDatas[i];
            for (let j = 0; j < sceneDatas.length; j++) {
                let item2 = sceneDatas[j];
                if (app_scene_item.content == item2.rawText) {
                    if (item2.dub && item2.dub.url) {
                        App.activeSequence.addDubSceneOfId(app_scene_item.sceneId, {
                            title: "",
                            type: "audio",
                            duration: item2.dub.duration,
                            path: item2.dub.url,
                            preUrl: item2.dub.url,
                            width: 0,
                            height: 0,
                            channels: Number(item2.dub.channels),
                            inPoint: 0,
                            outPoint: item2.dub.duration,
                        });
                    }

                    if (item2.sceneCfg) {
                        try {
                            const sceneCfg = JSON.parse(item2.sceneCfg);
                            if (sceneCfg.replaceClip) {
                                App.activeSequence.replaceSceneOfMain(sceneCfg.replaceClip, app_scene_item.sceneId);
                            }
                        } catch (err) {
                            console.log('sceneCfg转换错误', err)
                        }
                    }
                    break;
                }
            }
        }
        const audioCfg = state.creativeTextData.preference.audioCfg;
        if (audioCfg == 'onlyBg') {
            App.activeSequence.enableAudioBg(true);
            App.activeSequence.enableAudioMain(false);
            App.activeSequence.enableAudioDub(false);
        } else if (audioCfg == 'onlyDub') {
            App.activeSequence.enableAudioBg(false);
            App.activeSequence.enableAudioMain(false);
            App.activeSequence.enableAudioDub(true);
        }
        // audioCfg: 'onlyBg' //bgAndMain onlyDub
        // App.activeSequence.addDubSceneOfId(this.sceneItem.sceneId,svipdata);
    },
    [types.SET_FILTERAIDATA](state, data) {
        state.filterAiData = data;
    },
    [types.INIT_TIMELINE2](state, data) {
        state.templateData = data.templateData == 1 ? testJSON3 : testJSON4;//data;
        App.activeSequence.initTimeLineOfTemplate(state.templateData, data.sceneData);
    },
    [types.TASK_LIST_MSG](state, data) {
        state.taskListMsg = Object.assign({}, data)
    },
    [types.BODY_WIDTH](state, data) {
        // if(data != state.bodyWidth){
        state.bodyWidth = data;
        // }
    },
    [types.SET_AISPLIT](state, data) {
        let key = md5(data.content); console.log('key set', key, data)
        state.cachesAiSplit[key] = data.aiSplit;
    },
    [types.GET_AISPLIT](state, data) {
        // let key = md5(data.content);console.log('key get',key,data)
        // const cachesAiSplit = state.cachesAiSplit;
        // console.log('cachesAiSplit.hasOwnProperty(key)',cachesAiSplit.hasOwnProperty(key),cachesAiSplit[key])
        // if(cachesAiSplit.hasOwnProperty(key)){
        //     console.log("ss=",cachesAiSplit[key])
        //     return cachesAiSplit[key];
        // }
        // return null;

    },
    [types.INIT_CREATIVE_DETAIL](state, data) {
        let detail = data;
        let textId = detail.textId;
        // if (detail.locked === 1) {
        //     textId = null;
        // }

        let saveData = {
            textId: textId,
            listItemData: detail,
            title: detail.title,//标题
            originality: detail.text,//创意
            copywriting: detail.aiText,//文案纯文本
            // plainCopywriting: item.aiText,//带格式的纯文本
            sceneDatas: detail.sceneDatas || [],
            htmlCopywriting: detail.htmlCopywriting,//html文本
            sceneType: detail.sceneType,//0 默认 1 按句号分 2 精细分 10智能分割
            historySceneDatas: detail.sceneData || detail.sceneDatas || [],
            locked: detail.locked,
        }
        if (detail.preference) {
            saveData.preference = detail.preference;
        }
        // this.setCreativeTextData(saveData);
        // console.log(saveData.sceneType, saveData.sceneDatas.length)
        if (saveData.sceneType == 10 && saveData.sceneDatas.length > 0) {
            //如果是智能分割，将场景转为aiSplit
            saveData.aiSplit = sceneToAiSplit(saveData.sceneDatas)
            /** 场景数据转aiSplit */
            function sceneToAiSplit(sceneDatas) {
                let result = [];
                sceneDatas.forEach(item => {
                    let keyword = item.keyWords.map(k => k.word);
                    let aiSpiltItem = {
                        "source": item.rawText,
                        "shot": item.shot,
                        "type": item.chatType,
                        "keyword": keyword,
                        "des": item.des,
                        "bgm": item.bgm,
                        "length": ""
                    }
                    result.push(aiSpiltItem)

                });
                return result;
            }

        }
        this.commit(types.SET_CREATIVE_TEXT_DATA, saveData);

        this.commit(types.RELOAD_CREATIVE);

        // detail.highlight = ['中国','著名']
        //文案本身携带关键词
        if (detail.highlight && detail.highlight.length) {
            setTimeout(() => {
                this.commit(types.SET_HIGHLIGHT_DATA, {
                    type: types.KEYWORD,
                    value: detail.highlight,
                });
            }, 100)
        }
    },
    [types.SET_CREATIVE_TEXT_DATA](state, data) {
        let beforeSceneType = state.creativeTextData.sceneType;
        state.creativeTextData = Object.assign(state.creativeTextData, data);
        //场景类型切换时，重新计算场景
        if (beforeSceneType != state.creativeTextData.sceneType) {
            this.commit(types.CREATE_SCENE);
        }
        // console.log('arr',sceneDatas,keyWords)
    },
    [types.RELOAD_CREATIVE](state, data) {
        state.reloadCreative++;
    },
    [types.CREATE_SCENE](state, data) {
        let htmlCopywriting = state.creativeTextData.htmlCopywriting;
        let copywriting = state.creativeTextData.copywriting;
        let plainCopywriting = state.creativeTextData.plainCopywriting;
        let sceneType = state.creativeTextData.sceneType;
        let rgb = state.creativeTextData.keyWordColor;

        if (sceneType == 10) {
            let aiSplit = state.creativeTextData.aiSplit;
            const sceneArr = [];
            for (let i = 0; i < aiSplit.length; i++) {
                let ai_item = aiSplit[i];
                let rawText = ai_item.source;
                let keyword = ai_item.keyword || ai_item.keywords;
                if (typeof keyword == 'string') {
                    keyword = keyword.split(',');
                }
                let nowKeyWords = [];
                keyword.forEach(ele => {
                    nowKeyWords.push({
                        start: 0,
                        end: 0,
                        word: ele,
                    })
                })
                const scene = {
                    rawText: rawText, //原始文字
                    htmlText: rawText,
                    keyWords: nowKeyWords,
                    dub: {
                        url: '',
                        duration: 0,
                        channels: 0,
                    },
                    sceneCfg: '',
                    uuid: md5(rawText) + "_" + i,
                    estimateDuration: rawText.length * 300,
                    rawAiData: ai_item,//原始数据
                    shot: ai_item.shot,
                    chatType: ai_item.type,
                    des: ai_item.des,
                    bgm: ai_item.bgm,


                }
                sceneArr.push(scene);
            }
            state.creativeTextData.sceneDatas = sceneArr;

            return;
        }

        let keyIndex = 0;
        let startIndex = 0,
            endIndex = 0;
        let labelType = 0;
        let labelStatus = "";
        // console.log('htmlCopywriting111',htmlCopywriting)
        htmlCopywriting = htmlCopywriting.replaceAll('&nbsp;', ' ');
        htmlCopywriting = htmlCopywriting.replaceAll('&quot;', '"');
        htmlCopywriting = htmlCopywriting.replaceAll('&#39;', "'");
        htmlCopywriting = htmlCopywriting.replaceAll('&amp;', '&');
        // console.log('htmlCopywriting222',htmlCopywriting)
        const keyWords = [];
        for (let i = 0; i < htmlCopywriting.length; i++) {
            if (htmlCopywriting[i] == "<") {
                labelType = 1;
                continue;
            }
            if (htmlCopywriting[i] == ">") {
                labelType = 0;
                if (labelStatus == "span_start") {
                    startIndex = keyIndex;
                    labelStatus = "";
                } else if (labelStatus == "span_end") {
                    endIndex = keyIndex;
                    labelStatus = "";
                    if (endIndex > startIndex) {
                        keyWords.push({
                            startIndex: startIndex,
                            endIndex: endIndex,
                            word: htmlCopywriting.slice(
                                i - (endIndex - startIndex) - 6,
                                i - 6
                            ),
                        });
                    }
                }
                continue;
            }
            if (labelType == 0) {
                if (labelStatus == "span_start") {
                    startIndex = keyIndex;
                    labelStatus = "";
                } else if (labelStatus == "span_end") {
                    endIndex = keyIndex;
                    labelStatus = "";
                    if (endIndex > startIndex) {
                        keyWords.push({
                            startIndex: startIndex,
                            endIndex: endIndex,
                            word: htmlCopywriting.slice(
                                i - (endIndex - startIndex) - 7,
                                i - 7
                            ),
                        });
                    }
                }
                keyIndex++;
            } else if (labelType == 1) {
                if (htmlCopywriting.slice(i, i + 4) == "span" && labelStatus == "") {
                    labelStatus = "span_start";
                } else if (htmlCopywriting.slice(i, i + 5) == "/span" && labelStatus == "") {
                    labelStatus = "span_end";
                }
                // console.log('str.slice(i,i+5)',str.slice(i,i+5))
            }
        }
        // console.log('生成关键词信息',htmlCopywriting,copywriting,keyWords)
        let splitArr = [];
        if (sceneType == '0') {
            splitArr = plainCopywriting.split('\n');
        } else if (sceneType == '1') {
            splitArr = Utils.splitText(copywriting, 0);
        } else if (sceneType == '2') {
            splitArr = Utils.splitText(copywriting, 1);
        }

        let globalIndex = 0;
        const historySceneDatas = state.creativeTextData.historySceneDatas;
        // console.log('historySceneDatas',historySceneDatas)
        const sceneDatas = [];
        const libs = ['。', '，', '、', '；', ',']
        for (let i = 0; i < splitArr.length; i++) {
            let lineStr = splitArr[i];
            if (lineStr == '') {
                continue;
            }
            let isFirst = true;
            let labelStr = "";
            let slice_before = 0, slice_start = 0,
                slice_end = 0;
            let nowKeyWords = [];
            for (let j = 0; j < keyWords.length; j++) {
                let cur = keyWords[j];
                if (
                    cur.startIndex >= globalIndex &&
                    cur.endIndex <= globalIndex + lineStr.length
                ) {
                    slice_start = cur.startIndex - globalIndex;
                    slice_end = cur.endIndex - globalIndex;
                    if (slice_start > slice_before) {
                        labelStr += lineStr.slice(slice_before, slice_start);
                    }
                    labelStr += '<span style="color:' + rgb + '">' + lineStr.slice(slice_start, slice_end) + "</span>";
                    nowKeyWords.push({
                        start: slice_start,
                        end: slice_end,
                        word: cur.word,
                    });
                    slice_before = slice_end;
                }
            }
            if (slice_end < lineStr.length) {
                labelStr += lineStr.slice(slice_end);
            }
            // labelStr = "<p>" + labelStr + "</p>";
            const scene = {
                rawText: lineStr, //原始文字
                htmlText: labelStr,
                keyWords: nowKeyWords,
                dub: {
                    url: '',
                    duration: 0,
                    channels: 0,
                },
                sceneCfg: '',
            }
            //去掉最后一个符号
            let endStr = lineStr[lineStr.length - 1];
            if (libs.indexOf(endStr) > -1) {
                scene.rawText = lineStr.slice(0, lineStr.length - 1);
                scene.htmlText = labelStr.slice(0, labelStr.length - 1);
            }
            scene.uuid = md5(scene.rawText) + "_" + i;
            scene.estimateDuration = scene.rawText.length * 300;//ms
            // 使用历史数据回写dub
            for (let k = 0; k < historySceneDatas.length; k++) {
                let b_item = historySceneDatas[k];
                if (b_item.uuid == scene.uuid) {
                    scene.dub = b_item.dub;
                    scene.sceneCfg = b_item.sceneCfg || '';
                    break;
                }
            }

            sceneDatas.push(scene);
            globalIndex += lineStr.length;
        }

        state.creativeTextData.sceneDatas = sceneDatas;
    },
    [types.SET_ADVANCESET_DATA](state, data) {
        state.advancedSetData = data;
    },
    [types.SET_EDITOR_INSTANCE](state, data) {
        state.editorInstance = data;
    },

    [types.SET_HIGHLIGHT_DATA](state, data) {
        // console.log("SET_HIGHLIGHT_DATA", data);
        let obj = {

        }
        if (data.type === types.KEYWORD) {
            obj.style = {
                color: state.creativeTextData.keyWordColor
            };
        } else if (data.type === types.PROOFREAD) {
            obj.style = {
                color: "#000",
                background: "rgba(126, 251, 184, 0.6)",
            };

        } else if (data.type === types.EXAMINE) {
            obj.style = {
                color: "blue"
            };
        }
        state.checkedKeyWord = data.type == types.KEYWORD;

        state.highlightData = Object.assign(obj, data);
    },
    [types.SET_KEYWORD](state, data) {
        let plainCopywriting = state.creativeTextData.plainCopywriting;
        const style = {
            color: state.creativeTextData.keyWordColor
        }
        const type = '_keyword';
        let styleContent = "";
        for (let prop in style) {
            styleContent += `${prop}:${style[prop]};`;
        };
        state.curKeyWords = data;
        for (let v of state.curKeyWords) {
            if (plainCopywriting && plainCopywriting.includes(v.Word)) {
                let label = `<span  style='${styleContent}' class='${type}'>${v.Word}</span>`;
                plainCopywriting = plainCopywriting.replaceAll(v.Word, label);
            }
        }
        state.creativeTextData.keyWordContent = plainCopywriting;
        // console.log('SET_KEYWORD',state.creativeTextData,data,plainCopywriting)
    },
    [types.SET_PROOFREAD_LISTS](state, data) {
        state.proofreadLists = data;
    },
    [types.SET_SHOW_ADJUST_AREA](state, data) {
        state.showAdjustArea = data;
    },
    [types.SET_EXAMINE_DATA](state, data) {
        state.examineData = data;
    },
    [types.SET_COPYWRITING_CONFIG](state, data) {
        state.copywritingConfig = data;
    },
    [types.SET_COPYWRITING_CONFIG](state, data) {
        state.copywritingConfig = data;
    },
    [types.SEND_DUB](state, data) {
        // state.dubTasks = data;
        this.dispatch('app_pipelineDub', data)
            .then(res => {
                if (res.returnCode == 0) {
                    const taskList = res.returnData || [];
                    for (let i = 0; i < taskList.length; i++) {
                        let item = taskList[i];
                        state.dubTasks.push({
                            uuid: item.uuid,
                            taskId: item.taskId
                        })
                    }
                } else {
                    console.log('语音合成调用失败', res);
                }
            }).catch(err => {
                console.log('语音合成调用失败', err);
            })
    },
    [types.BACK_DUB](state, data) {
        // state.dubTasks = data;
        if (typeof data == 'string') {
            data = JSON.parse(data);
        }
        //这一步是为了取得配音时长
        Utils.getMediaInfoOfUrl(data.dubUrl, false).then(res => {
            // console.log('svipdata.preUrl',svipdata.preUrl,res)
            return Utils.readArrayBufferInfo(res);
        }).then(res => {
            const info = res.info.rawInfo.audio;
            const svipdata = {
                duration: Number(info.Duration) * 1000,
                channels: info.Channels,
                preUrl: data.dubUrl,
                path: data.dubUrl,
                type: 'audio',
            }
            svipdata.inPoint = 0;
            svipdata.outPoint = svipdata.duration;
            //从任务列表中删除
            let nowUUID = '';
            for (let i = 0; i < state.dubTasks.length; i++) {
                let item = state.dubTasks[i];
                if (item.taskId == data.taskId) {
                    nowUUID = item.uuid;
                    state.dubTasks.splice(i, 1);
                    break;
                }
            }
            //找到场景对象
            const appSceneDatas = App.activeSequence.sceneDatas;
            let sceneItem = null;
            for (let i = 0; i < appSceneDatas.length; i++) {
                let item = appSceneDatas[i];
                if (item.uuid == nowUUID) {
                    sceneItem = item;
                    break;
                }
            }
            // console.log('sceneItem',sceneItem,appSceneDatas,data)
            if (sceneItem) {
                App.activeSequence.addDubSceneOfId(sceneItem.sceneId, svipdata);

                const sceneDatas_before = state.creativeTextData.sceneDatas;
                let isFind = false;
                for (let i = 0; i < sceneDatas_before.length; i++) {
                    let item = sceneDatas_before[i];
                    if (item.uuid == sceneItem.uuid) {
                        item.dub = {
                            url: svipdata.preUrl,
                            duration: svipdata.duration,
                            channels: svipdata.channels
                        }
                        isFind = true;
                        break;
                    }
                }

                if (isFind) {
                    this.dispatch("app_autoSave", {});
                }
            }

            // console.log('svipdata.preUrl22222',res)

        })
            .catch(err => {
                console.log('svipdata.preUrl', err)
            })
    },
    [types.SEND_AIDRAW](state, data) {
        // state.dubTasks = data;
        this.dispatch('app_txt2img', data.params)
            .then(res => {
                if (res.returnCode == 0) {
                    const taskId = res.returnData || '';
                    if (taskId) {
                        state.aiDrawTasks.push({
                            sceneId: data.sceneId,
                            mainId: data.mainId,
                            taskId: res.returnData,
                        })
                    }

                } else {
                    console.log('AI作画调用失败', res);
                }
            }).catch(err => {
                console.log('AI作画调用失败', err);
            })
    },
    [types.BACK_AIDRAW](state, data) {
        // state.dubTasks = data;
        if (typeof data == 'string') {
            data = JSON.parse(data);
        }

        // {"result":["https://chengdu.mty.chinamcloud.com/ai/clip/aiplot/2023_7/21/AQC35wO4zH.png"],"taskId":"bb14cd7b-4572-4805-9fbb-4df13d006d13","status":"finished"}

        // 从任务列表中删除
        let nowTask = null;
        for (let i = 0; i < state.aiDrawTasks.length; i++) {
            let item = state.aiDrawTasks[i];
            if (item.taskId == data.taskId) {
                nowTask = item;
                state.aiDrawTasks.splice(i, 1);
                break;
            }
        }
        if (!nowTask) return;
        let imgUrl = '';
        if (Array.isArray(data.result) && data.result.length) {
            imgUrl = data.result[0]
        }
        let svipdata = {
            type: 'image',
            title: 'AI图片',
            width: nowTask.width,
            height: nowTask.height,
            cover: imgUrl,
            preUrl: imgUrl,
            path: imgUrl,
            duration: nowTask.duration,
        }
        // console.log('this.clip.modelId',this.clip.modelId)
        let msg = App.activeSequence.replaceSceneOfMain(svipdata, nowTask.sceneId);
        if (msg.cmd == 'success') {
            this.commit(types.RECORD_SCENE_CLIP, {
                sceneId: nowTask.sceneId,
                svipdata: svipdata
            })
        }

    },
    [types.RECORD_SCENE_CLIP](state, data) {
        //找到场景对象
        const appSceneDatas = App.activeSequence.sceneDatas;
        let sceneItem = null;
        for (let i = 0; i < appSceneDatas.length; i++) {
            let item = appSceneDatas[i];
            if (item.sceneId == data.sceneId) {
                sceneItem = item;
                break;
            }
        }
        console.log('sceneItem', sceneItem, appSceneDatas, data)
        if (sceneItem) {
            // App.activeSequence.addDubSceneOfId(sceneItem.sceneId,svipdata);

            const sceneDatas_before = state.creativeTextData.sceneDatas;
            let isFind = false;
            //通过uuid来记录可能会存在误差,后期需要优化
            for (let i = 0; i < sceneDatas_before.length; i++) {
                let item = sceneDatas_before[i];
                if (item.uuid == sceneItem.uuid) {
                    let sceneCfg = item.sceneCfg;
                    if (sceneCfg) {
                        sceneCfg = JSON.parse(sceneCfg);
                    } else {
                        sceneCfg = {};
                    }
                    sceneCfg.replaceClip = data.svipdata;
                    item.sceneCfg = JSON.stringify(sceneCfg);
                    isFind = true;
                    break;
                }
            }

            if (isFind) {
                this.dispatch("app_autoSave", {});
            }
        }
    }


} 