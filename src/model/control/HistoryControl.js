/**
 * 开发者：tangshanghai
 * 日期：2017/10/17
 * 说明：历史记录控制器+数据
 */
import Event from "../../libs/Event";
class HistoryControl extends Event{
    constructor(){
        super();
        this.HISTORY_UNDO = "history_undo";
        this.HISTORY_REDO = "history_redo";
        this.HISTORY_UNDOANDREDO_STATE = "history_undoandredo_state";
    }

    /**
     * 发出撤消 消息
     */
    undo(data){
        this.$emit(this.HISTORY_UNDO,data);
    }
    /**
     * 发出重做 消息
     */
    redo(data)
    {
        this.$emit(this.HISTORY_REDO,data);
    }
    /**
     * 发出重做撤消的状态消息
     */
    undoRedoState(event)
    {
        this.$emit(this.HISTORY_UNDOANDREDO_STATE,event);
    }
}
export default HistoryControl;