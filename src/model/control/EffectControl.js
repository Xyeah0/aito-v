import Event from "../../libs/Event";

class EffectControl extends Event{
    constructor(){
        super();

        this.EFFECT_ADD = 'effect_add';
        this.EFFECT_SELECT = 'effect_select';
        this.EFFECT_CHANGE = 'effect_change';
        this.EFFECT_REMOVE = 'effect_remove';
        this.EFFECT_SELECT_STATUS = 'effect_select_status';
        this.EFFECT_KEYFRAME_STATUS = 'effect_keyframe_status';

        this.EFFECT_TRANS_ADD = 'effect_trans_add';
        this.EFFECT_TRANS_CHANGE = 'effect_trans_change';
        this.EFFECT_TRANS_REMOVE = 'effect_trans_remove';
        this.EFFECT_TRANS_SELECT = 'effect_trans_select';
        this.EFFECT_TRANS_ADD_DRAGED = 'effect_trans_add_draged';

        //主要用于关键帧状态的变化
        this.EffectEmumType = {
            KEYFRAME_SELECT: 'keyFrame_select',
            KEYFRAME_CHANGE: 'keyFrame_change',
            KEYFRAME_ADD: 'keyFrame_add',
            KEYFRAME_REMOVE: 'keyFrame_remove',
        }
    }

    /**
     * 特技关键帧状态变化
     */
    effectKeyFrameStatus(obj)
    {
        this.$emit(this.EFFECT_KEYFRAME_STATUS,obj);
    }
}
export default EffectControl;