import Event from "../../libs/Event";

class CoreControl extends Event{
    constructor(){
        super();

        this.CLEAR_TIMELINE = 'clear_timeline';
        this.INIT_TIMELINE = 'init_timeline';
        this.PRESET_CHANGE = 'preset_change';
        this.CLIP_ADD = 'clip_add';
        this.CLIP_DELETE = 'clip_delete';
        this.CLIP_CHANGE = 'clip_change';
        this.CLIP_SELECT = 'clip_select';
        this.TITLETAIL_STATUS = 'titletail_status';
        this.STARTSECOND_CHANGE = 'startSecond_change';
        this.TIMELINE_SCALE = 'timeline_scale';
        this.SET_ZOOMBAR = 'set_zoombar';
        this.SYSTEM_CHANGE = 'system_change';
        
        this.TRACK_ADD = 'track_add';
        this.TRACK_CHANGE = 'track_change';
        this.TRACK_DELETE = 'track_delete';
        this.SEEK = 'seek';

        this.PREVIEW_STATUS = 'preview_status';
        this.CURRENTTIME_CHANGE = 'currentTime_change';
        this.SEQ_DURATION = 'seq_duration';
        this.PREVIEW_START = 'preview_start';
        this.PREVIEW_END =  'preview_end';
        this.PREVIEW_WAIT_CHANGE =  'preview_wait_change';
        this.CHECK_CURRENTTIME = 'check_currentTime';
        this.CLIP_VOLUME_CHANGE = 'clip_volume_change';
        this.CLIP_TEXT_CONTENT = 'clip_text_content';

        this.TGA_LOAD_START = 'tga_load_start';
        this.TGA_LOAD_PROGRESS = 'tga_load_progress';
        this.TGA_LOAD_COMPLETE = 'tga_load_complete';

        //场景类
        this.SCENE_INIT = 'scene_init';
        this.SCENE_CHANGE = 'scene_change';
        this.SCENE_SELECT = 'scene_select';
        

        //动态字幕事件传递
        this.DYNAMIC_TEXT_CHANGE = 'dynamic_text_change';
        this.UNDOORREDO_FINISH = 'undo_or_redo_finish';
        this.CHANGE_TAB = 'change_tab';
    }

    /**
     * 发出起始时间改变事件
     */
    tellStartSecondChange(sec)
    {
        this.$emit(this.STARTSECOND_CHANGE,sec);
    }

    /**
     * 缩放比值改变事件
     */
    timeScaleChange()
    {
        this.$emit(this.TIMELINE_SCALE);
    }
    /**
     * 设置zoomBar的值
     */
    setZoomBarValue(obj)
    {
        this.$emit(this.SET_ZOOMBAR,obj);
    }

    /**
     * 切换tab {type:'xxxx',mode:'to'} mode：to切换 hide 隐藏 show显示
     */
    changeTab(obj){
        this.$emit(this.CHANGE_TAB,obj);
    }
}
export default CoreControl;