/**
 * 开发者：tangshanghai
 * 日期：2017/10/17
 * 说明：历史记录数据
 */
const App = {};
class HistoryModel{
    constructor(app, seq){
        App.CONTROL = app.CONTROL;
        App.SEQ = seq;
        this.nameArray = new Array();
        this.dataArray = new Array();//下面记录的是一维的多个数组
        this.undoStep = 20;//默认只能撤消20步
        this.undoIndex = 0;//撤消索引
        this.isUndo = false;//是否能撤消
        this.isRedo = false;//是否能重做
        this.undoData = new Array();//当前撤销数据
        this.redoData = new Array();//当前重做数据
        this.nameData = null;

        this.undoIndex = 0;

        this.dealyTimer = 0;

    }

    /**
     * 初始化记录数组数据
     */
    initData(){
        this.undoIndex = 0;//撤消索引
        this.isUndo = false;//是否能撤消
        this.isRedo = false;//是否能重做
        this.undoStep = 20;


        this.nameArray = [{
            title: '初始数据',
            currentTime:0
        }];
        this.dataArray = [App.SEQ.toJSON(false)];
        App.CONTROL.HISTORY.undoRedoState({isUndo:this.isUndo,isRedo:this.isRedo});
    }
    /** 延时记录 */
    throttleRecord(_name,delay=1000){
        clearTimeout(this.dealyTimer);
        this.dealyTimer = setTimeout(this.record.bind(this),delay,_name);
    }
    /**
     * 记录历史记录
     */
    record(_name)
    {
        //撤销后再次重做将覆盖后面的数据
        if(this.undoIndex < this.dataArray.length-1){
            this.nameArray = this.nameArray.slice(0,this.undoIndex+1);
            this.dataArray = this.dataArray.slice(0,this.undoIndex+1);
        }
        this.nameArray.push({
            title: _name,
            currentTime:App.SEQ.currentTime
        });
        this.dataArray.push(App.SEQ.toJSON(false));

        if(this.nameArray.length > this.undoStep){
            this.nameArray.shift();
        }
        if(this.dataArray.length > this.undoStep){
            this.dataArray.shift();
        }
        this.undoIndex = this.dataArray.length - 1;
        this.checkUndoAndRedo();
        // console.log("数据3",this.nameArray,this.dataArray)
        App.SEQ.setTimeLineSaveStatus(false);
    }
    /**
     * 发出撤消 消息
     */
    undo(){
        this.undoIndex--;
        if(this.undoIndex < 0){
            this.undoIndex = 0;
        }
        //trace(undoIndex+"aaaaa");
        if(this.undoIndex >= 0 && this.undoIndex <= this.dataArray.length -1){
            this.undoData = this.dataArray[this.undoIndex];
            this.nameData = this.nameArray[this.undoIndex];
        }
        else{
            this.undoData = new Array();
            this.nameData = null;
        }
        this.checkUndoAndRedo();

        App.CONTROL.HISTORY.undo({nameData: this.nameData,data:this.undoData});
    }

    /**
     * 发出重做 消息
     */
    redo(){
        this.undoIndex++;
        //checkUndoAndRedo();
        if(this.undoIndex > this.undoStep-1){
            this.undoIndex = this.undoStep-1;
        }

        if(this.undoIndex >= 0 && this.undoIndex <= this.dataArray.length - 1){
            this.redoData = this.dataArray[this.undoIndex];
            this.nameData = this.nameArray[this.undoIndex];
        }
        else{
            this.redoData = new Array();
            this.nameData = null;
        }
        this.checkUndoAndRedo();
        App.CONTROL.HISTORY.undo({nameData: this.nameData,data:this.redoData});
    }
    /**
     * 是否能重做或撤消
     */
    checkUndoAndRedo(){
        if(this.dataArray.length > 1 && this.undoIndex > 0){
            this.isUndo = true;
        }else{
            this.isUndo = false;
        }

        if(this.dataArray.length > 1 && this.undoIndex < this.dataArray.length-1) {
            this.isRedo = true;
        }else{
            this.isRedo = false;
        }
        App.CONTROL.HISTORY.undoRedoState({isUndo:this.isUndo,isRedo:this.isRedo});
    }


    
}
export default HistoryModel;