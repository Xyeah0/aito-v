import DataType from "./DataType";
import Utils from '../libs/Utils';
class Pack {
    constructor() {

    }

    getTemplate(activeSeq) {
        let tracks = activeSeq.tracks;
        let tlTracks = [];
        let flag = 0;
        console.log(tracks)
        // tracks长度为5，1文字标题 2默认字幕 3图片 4视频 5音频
        function common(now_clip, old_clip) {
            old_clip.TrackIn = now_clip.trackIn;
            old_clip.TrackOut = now_clip.trackOut;
            old_clip.PreviewUrl = now_clip.previewUrl;
            old_clip.HighPath = now_clip.path;
            if(now_clip.fromType == 'local'){
                old_clip.PreviewUrl = now_clip.carryData.preUrl;
            }
        }

        for (let i = 0; i < tracks.length; i++) {
            let t_item = tracks[i];
            let tl_item = JSON.parse(JSON.stringify(t_item.rawTrackData));
            for (let j = 0; j < tl_item.Clips.length; j++) {
                //原始数据及保存数据
                let now_clip = t_item.clips[j];
                let old_clip = tl_item.Clips[j];

                //文字
                if (now_clip.dataType == DataType.CLIP_CG_TEXT) {
                    old_clip.Content = now_clip.content;//内容
                    old_clip.Pos = now_clip.pos;//位置
                    old_clip.Style = now_clip.styles;//样式
                }

                // 未替换或替换为图片
                if (now_clip.dataType == DataType.CLIP_VIDEO_IMAGE) {
                    old_clip.ClipType = "Image"
                    old_clip.Width = now_clip.imageWidth;
                    old_clip.Height = now_clip.imageHeight;
                    old_clip.KeyFrameUrl = now_clip.keyFrameUrl;
                    old_clip.FromType = now_clip.fromType
                    common(now_clip, old_clip)
                }

                // 替换为视频
                if (now_clip.dataType == DataType.CLIP_VIDEO) {
                    old_clip.ClipType = "Video";
                    old_clip.Width = now_clip.imageWidth;
                    old_clip.Height = now_clip.imageHeight;
                    old_clip.KeyFrameUrl = now_clip.keyFrameUrl;
                    old_clip.FromType = now_clip.fromType
                    common(now_clip, old_clip)
                    // 新增属性
                    old_clip.Duration = now_clip.duration;
                    old_clip.TrimIn = now_clip.inPoint;
                    old_clip.TrimOut = now_clip.outPoint;
                    old_clip.Mute = now_clip.isMute;
                }

                // 音频
                if (now_clip.dataType == DataType.CLIP_AUDIO) {
                    old_clip.KeyFrameUrl = "";
                    common(now_clip, old_clip)
                    old_clip.Duration = now_clip.duration;
                }

                // 判断是否进行替换
                if (now_clip.carryData != null) {
                    flag++
                }
            }
            tlTracks.push(tl_item);
        }
        if (flag != 0) {
            tlTracks.forEach(element => {
                element.Clips.splice(flag, element.Clips.length - flag)
            });
        }

        let rawtlData = activeSeq.rawtlData;
        return {
            Type: rawtlData.Type,
            Version: rawtlData.Version,
            TemplateId: rawtlData.TemplateId,
            InPoint: activeSeq.inPoint,
            OutPoint: activeSeq.outPoint,
            TLFormat: rawtlData.TLFormat,
            HostPath: rawtlData.HostPath,
            Materials: [],
            Tracks: tlTracks,
            //自定义模板标识
            TemplateMode:'custom'
        }
    }
}

export default new Pack;