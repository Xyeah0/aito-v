import DataFactory from "./track/DataFactory";
import EffectFactory from "./track/EffectFactory";
import Utils from "@/libs/Utils";
import DataType from "./DataType";
import Track from "./track/Track";
import EffectAgent from "./EffectAgent";
import HistoryModel from "./HistoryModel";

const App = {};
class Sequence {
    constructor(app) {
        App.CONTROL = app.CONTROL;

        this.rawtlData = null;

        this.name = '未命名';
        this.id = Utils.GUID();
        this.version = app.version;
        this.sequenceID = ''; //时间线id 后台保存文件时给的
        this.inPoint = -1;
        this.outPoint = -1;
        this.isPreview = false;
        this.isSaved = true;//是否已经保存时间线
        this.preset = {
            width: 1920,
            height: 1080,
            fps: 25
        }
        this.tracks = [];
        this.videoTracks = [];
        this.audioTracks = [];
        this.cgTracks = [];
        this.sceneDatas = [];//场景数据

        //特技方法代理
        // this.EffectAgent = new EffectAgent(app,this);
        this.EffectAgent = new EffectAgent(app, this);
        //历史记录对象
        this.HistoryModel = new HistoryModel(app,this);

        this.ttMaterials = null;//片头片尾原始数据
        this.systemConfig = {
            enabledAudioBg: true,//开启背景音乐
            enabledAudioMain: true,//开启主轨声音
            enabledAudioDub: true,//开启配音声音
            // fadeOutAudioBg: false,//背景音乐淡出
            // fadeOutCon: false,//海报内容淡出
            // isFixedDuration: false,//固定clip时长
            // isHasTextTab: true,//是否有文字设置Tab
        }


        this.seqDuration = 0;
        this.currentTime = 0; //当前播放头


        // this.sap_scale = 1;//秒数和像素的比值1-100   //0.1-51  //最大比值0.004S比1像素
        // this.show_sceonds = 600;//默认时间线展示的秒数
        // this.maxsap_scale = 1/0.004;
        // this.minsap_scale = 0.1;
        // this.startSeconds = 0;//当前开始时间
        // this.startSecPercentage = 0;//开始时间百分比
        // this.trackBodyWidth = 0;//当前轨道宽度

        this.sap_scale = 1000;//毫秒数和像素的比值1-100   //0.1-51  //最大比值4毫秒比1像素（1毫秒0.25像素）
        this.show_sceonds = 600;//默认时间线展示的秒数
        this.maxsap_scale = 1/4;
        this.minsap_scale = 0.1;
        this.startSeconds = 0;//当前开始时间
        this.startSecPercentage = 0;//开始时间百分比
        this.trackBodyWidth = 0;//当前轨道宽度
    }
    /** 将模板数据转换为私有时间线 */
    // templateToSeq(tlData){
    //     this.rawtlData = tlData;

    //     console.log('tlData',tlData)
    //     let vdata = DataFactory.getVideoData({

    //     })
    //     console.log('vdata',vdata);
    // }

    /*
    * 秒数转像素
    * */
    secondToPixel(second){
        // return second/this.sap_scale;
        return this.sap_scale * second;
    }
    /*
    * 像素转秒数
    * */
    pixelToSecond(pixel){
        // return pixel*this.sap_scale;
        return pixel/this.sap_scale;
    }

    /** 校正缩放比值 */
    correctScale(b){
        let trackBodyW = this.trackBodyWidth;//- this.Config.timeLine.trackBodyAllow;
        // let canShowSec = this.pixelToSecond(trackBodyW);
        let endTime = this.seqDuration;
        this.show_sceonds = Math.max(endTime,this.show_sceonds);
        if(endTime === 0){
            this.show_sceonds = 600000;
            this.startSeconds = 0;
            this.startSecPercentage = 0;
            if(this.currentTime < 0 || this.currentTime > this.show_sceonds){
                this.seek(0);
            }
        }
        // console.log('correctScale',endTime,this.seqDuration);
        if(endTime > 0 && b){
            this.show_sceonds = endTime;
            this.minsap_scale = Math.min(this.maxsap_scale,trackBodyW/this.show_sceonds);
            this.sap_scale = this.minsap_scale;
            this.startSeconds = 0;
            this.startSecPercentage = 0;
            this.zoomValue = 0;//1;
        }else{
            this.minsap_scale = trackBodyW/this.show_sceonds;
            this.sap_scale = Math.max(this.minsap_scale,this.sap_scale);
            this.sap_scale = Math.min(this.maxsap_scale,this.sap_scale);
            // this.zoomValue = 1-(this.sap_scale-this.minsap_scale)/(this.maxsap_scale-this.minsap_scale);
            let t1 = 1 - Math.pow((1-(this.sap_scale-this.minsap_scale)/(this.maxsap_scale-this.minsap_scale)),2);
            this.zoomValue = Math.pow(t1,1/3);
        }
        
        App.CONTROL.CORE.setZoomBarValue({value:this.zoomValue});
        App.CONTROL.CORE.timeScaleChange();
        App.CONTROL.CORE.tellStartSecondChange({startSec:this.startSeconds,v:this.startSecPercentage});
    }
    /**
     * 传入 0-1 设置缩放比值
     */
    setScaleValue(v,b){
        v = Math.max(0,v);
        v = Math.min(1,v);
        this.zoomValue = v;
        let v2 = 1-Math.sqrt(1-Math.pow(v,3));
        this.sap_scale = v2*(this.maxsap_scale-this.minsap_scale)+this.minsap_scale;
        App.CONTROL.CORE.timeScaleChange();
        if(b){
            App.CONTROL.CORE.setZoomBarValue({value:this.zoomValue});
        }

        
        //以time为中心点，计算开始时间位置
        let trackBodyW = this.trackBodyWidth;// - this.Config.timeLine.trackBodyAllow;
        let canShowSec = this.pixelToSecond(trackBodyW);
        let willSec = this.currentTime - canShowSec/2;
        let maxSec = Math.max(0,this.show_sceonds - canShowSec);
        this.startSeconds = Math.max(0,willSec);
        this.startSeconds = Math.min(maxSec,this.startSeconds);
        this.startSecPercentage = maxSec==0?0:this.startSeconds/maxSec;
        App.CONTROL.CORE.tellStartSecondChange({startSec:this.startSeconds,v:this.startSecPercentage});
        // //维持time位置
        // this.currentTime = Math.max(0,this.currentTime);
        // this.currentTime = Math.min(this.currentTime,this.show_sceonds);
        // let scale = 1;
        // //0.1-51
        // if(v >= 0.5){
        //     scale = (v - 0.5)*2;
        //     this.sap_scale = Math.floor(scale*50)+1;
        // }else{
        //     scale = v*2;
        //     this.sap_scale = (Math.floor(scale*90)+10)/100;
        // }
    }

    /** 设置开始时间 传入百分比 */
    setStartSecond(v) {
        v = Math.max(0,v);
        v = Math.min(1,v);
        let trackBodyW = this.trackBodyWidth;// - this.Config.timeLine.trackBodyAllow;
        let canShowSec = this.pixelToSecond(trackBodyW);
        this.startSeconds = Math.max(0,v * (this.show_sceonds - canShowSec));
        this.startSecPercentage = v;

        App.CONTROL.CORE.tellStartSecondChange({startSec:this.startSeconds,v:this.startSecPercentage});
    }

    /** 设置开始时间 传入开始时间 */
    setStartSecond2(sec){
        let trackBodyW = this.trackBodyWidth;// - this.Config.timeLine.trackBodyAllow;
        let canShowSec = this.pixelToSecond(trackBodyW);
        let maxSec = Math.max(0,this.show_sceonds - canShowSec);
        this.startSeconds = Math.max(0,sec);
        this.startSeconds = Math.min(maxSec,this.startSeconds);
        this.startSecPercentage = maxSec==0?0:this.startSeconds/maxSec;
        App.CONTROL.CORE.tellStartSecondChange({startSec:this.startSeconds,v:this.startSecPercentage});
    }

    /**
     * 更新预设高宽
     */
    changePreset(obj){
        this.preset.width = obj.width;
        this.preset.height = obj.height;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PRESET_CHANGE);
        //更新裁剪参数
        const clips = this.getMainTrack().clips;
        clips.forEach(clip=>{
            this.EffectAgent.updateEffect2D(clip,clip.fillPos)
        })
        this.seek();
    }
    /**
     * 根据传入数据 生成新的序列
     */
    overrideSeq(_seqData){
        this.clear();
        this.inPoint = _seqData.inPoint;
        this.outPoint = _seqData.outPoint;
        this.preset = _seqData.preset;
        this.ttMaterials = _seqData.ttMaterials || null;
        console.log('_seqData.tracks',_seqData.tracks)
        for(let i=0;i<_seqData.tracks.length;i++){
            let trackData = _seqData.tracks[i];
            let track = new Track(App,this,{});
            // track.dataType = trackData.dataType;
            // track.dataType2 = trackData.dataType2;
            // track.isMain = trackData.isMain;
            // track.trackIndex = trackData.trackIndex;
            // track.isAddClip = trackData.isAddClip;
            // track.clips = trackData.clips;
            Object.assign(track,trackData);
            
            if(track.dataType === DataType.TRACK_VIDEO){
                this.videoTracks.push(track);
            }else if(track.dataType === DataType.TRACK_AUDIO){
                this.audioTracks.push(track);
            }else if(track.dataType === DataType.TRACK_CG){
                this.cgTracks.push(track);
            }
            
        }
        this.sortTrack();
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PRESET_CHANGE);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.INIT_TIMELINE);

        this.tracks.forEach(track=>{
            track.clips.forEach(clip=>{
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,clip);
            })
        })
        // console.log('this.tracks',this.tracks)
        this.sceneDatas = _seqData.sceneDatas;
        this.systemConfig = _seqData.systemConfig;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_INIT,this.sceneDatas);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SYSTEM_CHANGE,this.systemConfig);

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek();
    }

    /**
     * 撤销或重做发来的新时间线
     */
    changeSeqData (_seqData) {

        this.inPoint = _seqData.inPoint;
        this.outPoint = _seqData.outPoint;
        this.preset = _seqData.preset;
        this.ttMaterials = _seqData.ttMaterials || null;

        // 将当前数据与现有数据作对比 多出的部分 删除，少于的部分 添加，其余全部更新

        //第一步，先向记录数据查询是否有本身，如果没查到，则删除
        for(let i=0;i<this.tracks.length;i++){
            let trackI = this.tracks[i];
            let isFind = false;
            for(let j=0;j<_seqData.tracks.length;j++){
                let trackI2 = _seqData.tracks[j];
                if(trackI.modelId === trackI2.modelId){
                    isFind = true;
                    break;
                }
            }
            if(!isFind){
                this.tracks.splice(i,1);
                this.deleteTrack2(trackI);
                i--;
            }
        }

        let updateTracks = [];
        let newTracks = [];
        //第二步，向本身数据查找是否有记录的数据，如果有，则更新，如果没有，则新建
        for(let j=0;j<_seqData.tracks.length;j++){
            let trackI2 = _seqData.tracks[j];
            let isFind = false;
            for(let i=0;i<this.tracks.length;i++){
                let trackI = this.tracks[i];
                if(trackI.modelId === trackI2.modelId){
                    isFind = true;
                    for(let key in trackI){
                        if(key != 'clips'){
                            trackI[key] = trackI2[key];
                        }
                    }
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_CHANGE, trackI);
                    updateTracks.push({
                        track:trackI,
                        clips:trackI2.clips
                    })
                    break;
                }
            }
            if(!isFind){
                newTracks.push(trackI2);
            }
        }
        //这里为了解决后执行的轨道有可能把前面新建的给删除了，因此，确保优先对所有轨道先执行clip的删除匹配，再执行更新或新建操作
        updateTracks.forEach(td=>{
            td.track.Contrastclips(td.clips);
        })
        updateTracks.forEach(td=>{
            td.track.Contrastclips2(td.clips);
        })
        newTracks.forEach(track =>{
            this.createTrack2(track);
            track.clips.forEach(clip=>{
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,clip);
            })
        })
        this.sortTrack();
        this.flushSeqDuration();
        this.EffectAgent.computerTrans();

        this.sceneDatas = _seqData.sceneDatas;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_INIT,this.sceneDatas);

        App.CONTROL.CORE.$emit(App.CONTROL.CORE.UNDOORREDO_FINISH);
    }
    /**
     * 创建一个轨道
    */
    createTrack(tTracks){
        let track = new Track(App,this,tTracks);
        if(track.dataType === DataType.TRACK_VIDEO){
            this.videoTracks.push(track);
        }else if(track.dataType === DataType.TRACK_AUDIO){
            this.audioTracks.push(track);
        }else if(track.dataType === DataType.TRACK_CG){
            this.cgTracks.push(track);
        }

        return track;
    }
    /**
     * 创建一个轨道2 传入已有数据
    */
    createTrack2(trackData){
        let track = new Track(App,this,{});
        Object.assign(track,trackData);

        if(track.dataType === DataType.TRACK_VIDEO){
            this.videoTracks.push(track);
        }else if(track.dataType === DataType.TRACK_AUDIO){
            this.audioTracks.push(track);
        }else if(track.dataType === DataType.TRACK_CG){
            this.cgTracks.push(track);
        }
        this.tracks.push(track);
        // console.log('createTrack2',td);
        // this.sortTrack();
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_ADD, track);
    }
    /**
     * 删除一个轨道
    */
    deleteTrack(track){
        let willD = null;
        for(let i=0;i<this.tracks.length;i++){
            if(track.modelId === this.tracks[i].modelId){
                willD = this.tracks[i];
                this.tracks.splice(i,1);
                //同时需要找到类型数据，将其清除
                this.videoTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.videoTracks.splice(index,1);
                    }
                })
                this.audioTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.audioTracks.splice(index,1);
                    }
                })
                this.cgTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.cgTracks.splice(index,1);
                    }
                })
                break;
            }
        }
        if(willD){
            willD.clear();
            this.sortTrack();

            App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_DELETE, willD);
            
        }
    }
    /**
     * 删除一个轨道2
    */
    deleteTrack2(track){
        this.videoTracks.forEach((item,index)=>{
            if(item.modelId === track.modelId){
                this.videoTracks.splice(index,1);
            }
        })
        this.audioTracks.forEach((item,index)=>{
            if(item.modelId === track.modelId){
                this.audioTracks.splice(index,1);
            }
        })
        this.cgTracks.forEach((item,index)=>{
            if(item.modelId === track.modelId){
                this.cgTracks.splice(index,1);
            }
        })
        track.clear();
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_DELETE, track);
    }
    /**
     * 更新一个轨道
    */
    updateTrack(track){
        let isFind = false;
        for(let i=0;i<this.tracks.length;i++){
            if(track.modelId === this.tracks[i].modelId){
                isFind = true;
                this.tracks[i] = track;
                //同时需要找到类型数据，将其更新
                this.videoTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.videoTracks[index] = track;
                    }
                })
                this.audioTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.audioTracks[index] = track;
                    }
                })
                this.cgTracks.forEach((item,index)=>{
                    if(item.modelId === track.modelId){
                        this.cgTracks[index] = track;
                    }
                })
                break;
            }
        }
        if(isFind){
            // this.sortTrack();
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_CHANGE, track);
        }
    }
    /**
     * 更新轨道2
     */
    updateTrack2(trackData){
        let td = this.getTrackOfID(trackData.modelId);
        td.trackName = trackData.trackName;
        td.volumeValue = trackData.volumeValue;
        td.displaySec = trackData.displaySec;
        td.trackIndex = trackData.trackIndex;
        td.isLock = trackData.isLock;
        td.isSelected = trackData.isSelected;
        td.isMain = trackData.isMain;
        td.trackHeight = trackData.trackHeight;
        td.weightCoeff = trackData.weightCoeff;
        
        App.CONTROL.CORE.updateSequence({
            cmd: App.CONTROL.CORE.SeqEmumType.TRACK_CHANGE,
            needStop: false,
            data: td
        });
        return {track:td,clips:trackData.clips};
        td.Contrastclips(trackData.clips);

    }
    /**
     * 清空时间线
     */
    clear() {
        
        this.tracks = [];
        this.videoTracks = [];
        this.audioTracks = [];
        this.cgTracks = [];
        this.isAddTT = false;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLEAR_TIMELINE);

        this.ttMaterials = null;//片头片尾原始数据
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.TITLETAIL_STATUS, this.ttMaterials);
    }
    /** 通过原始模板融合时间线 */
    initTimeLineOfTemplate(tlData,sceneData){
        this.clear();
        this.rawtlData = tlData;
        this.preset.width = tlData.TLFormat.Width;
        this.preset.height = tlData.TLFormat.Height;
        this.preset.fps = tlData.TLFormat.FPS;
        this.inPoint = tlData.InPoint;
        this.outPoint = tlData.OutPoint;
        this.isAddTT = false;
        // this.systemConfig.isFixedDuration = !!tlData.FixedDuration;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PRESET_CHANGE);
        // this.presetPreviewChange(360,360*(this.preset.height/this.preset.width));
        this.tracks = [];
        this.videoTracks = [];
        this.audioTracks = [];
        this.cgTracks = [];

        // let isSetMainTrack = false;
        // let isHasText = false;
        console.log('tlData',tlData)
        let trackMain = null;
        for (let i = 0; i < tlData.Tracks.length; i++) {
            let _track = tlData.Tracks[i];
            let track = new Track(App, this, _track);
            // track.trackIndex = 400-i;
            if (track.dataType === DataType.TRACK_VIDEO) {
                this.videoTracks.push(track);
            } else if (track.dataType === DataType.TRACK_AUDIO) {
                this.audioTracks.push(track);
            } else if (track.dataType === DataType.TRACK_CG) {
                this.cgTracks.push(track);
            }
            if (track.dataType === DataType.TRACK_VIDEO && track.dataType2 == "Main") {
                track.isMain = true;
                trackMain = track;
            }
        }
        

        let _trackWater = new Track(App,this,{
            TrackType:'CG',
            TrackType2:'Water',
            Clips:[]
        });
        //多层CG轨，可放多个同时间段的CG素材，合成时再生成多轨CG
        let _trackMultilayer = new Track(App,this,{
            TrackType:'CG',
            TrackType2:'Multilayer',
            Clips:[]
        });
        this.cgTracks.unshift(_trackMultilayer);
        this.cgTracks.unshift(_trackWater);
        //配音轨
        let _trackDub = new Track(App,this,{
            TrackType:'A',
            TrackType2:'Dub',
            Clips:[]
        });
        //音效轨
        let _trackSoundEffect = new Track(App,this,{
            TrackType:'A',
            TrackType2:'SoundEffect',
            Clips:[]
        });
        this.audioTracks.push(_trackDub);
        this.audioTracks.push(_trackSoundEffect);
        this.sortTrack();
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.INIT_TIMELINE);


        const groupClips = [];
        //分析组成员
        for(let i=0;i<trackMain.rawTrackData.Clips.length;i++){
            let tClipMain = trackMain.rawTrackData.Clips[i];
            const groupClip = {
                trackMain:trackMain,
                tClipMain:tClipMain,
                // clipDynamic:null,
                // trackDynamic:null,
                otherClips:[],
            }
            for(let j=0;j<this.tracks.length;j++){
                let track = this.tracks[j];
                if(track.dataType === DataType.TRACK_AUDIO || track.isMain){
                    continue;
                }
                for(let k=0;k<track.rawTrackData.Clips.length;k++){
                    let _tClip = track.rawTrackData.Clips[k];
                    //只要入点在主素材范围内，就与主素材成组
                    let end_trackOut = tClipMain.TrackOut;
                    if(tClipMain.TransFX){
                        end_trackOut -= tClipMain.TransFX.Duration;
                    }
                    let p1 = Math.max(tClipMain.TrackIn,_tClip.TrackIn);
                    let p2 = Math.min(end_trackOut,_tClip.TrackOut);
                    //相交超80%，则成组
                    if(p2-p1>0 && (p2-p1)/(end_trackOut-tClipMain.TrackIn) >= 0.8){
                        groupClip.otherClips.push({
                            tClip:_tClip,
                            track:track,
                        })
                    }
                    // if(_tClip.TrackIn>= tClipMain.TrackIn && _tClip.TrackIn < end_trackOut){
                    //     groupClip.otherClips.push({
                    //         tClip:_tClip,
                    //         track:track,
                    //     })
                    // }
                }
            }
            groupClips.push(groupClip);
        }

        // console.log('fuseTimeLineOfScene',sceneData,groupClips)
        const dataScences = sceneData;
        const newScences = [];
        let st = 0;
        for(let i=0;i<dataScences.length;i++){
            let scene_data = dataScences[i];
            let order = i%groupClips.length;
            let sceneId = Utils.GUID();
            const new_scene = {
                sceneId: sceneId,
                isSelected: false,
                dubClipId:'',
                clips:[]
            }
            Object.assign(new_scene,scene_data);
            const groupClip = groupClips[order];

            const svipData = scene_data.material; 
            let clipMain;
            if(svipData){
                if(svipData.type == 'image'){
                    svipData.duration = scene_data.duration;
                }
                clipMain = this.fuseToClip(groupClip.tClipMain,svipData, groupClip.trackMain,tlData);
            }else{
                clipMain = this.fuseToClip(groupClip.tClipMain,{
                    title: "未命名",
                    type: "image",
                    duration: scene_data.duration,
                    cover: './assets/scr_media_off_960.png',
                    path: './assets/scr_media_off_960.png',
                    preUrl: './assets/scr_media_off_960.png',
                    width: 960,
                    height: 540,
                    frameRate: 0,
                    channels: 0,
                    inPoint: 0,
                    outPoint: scene_data.duration,
                    noFind: true,//未找到
                }, groupClip.trackMain,tlData);
            }
            
            clipMain.sceneId = sceneId;
            clipMain.trackIn = st;
            clipMain.trackOut = st + scene_data.duration;
            //尝试使用前一个的转场
            if(!clipMain.effectTrans){
                let tempOrder = order-1;
                if(tempOrder>=0 && tempOrder<groupClips.length){
                    const temp_tClip = groupClips[tempOrder].tClipMain;
                    clipMain.effectTrans = this.EffectAgent.transTLToTrans(temp_tClip.TransFX);
                }
            }
            if(clipMain.effectTrans){
                //转场时长，最长为切段的一半
                clipMain.effectTrans.duration = Math.min(scene_data.duration/2,clipMain.effectTrans.duration);
            }

            groupClip.trackMain.insertClip(clipMain);
            new_scene.clips.push(clipMain.modelId);
            // console.log('clipMain',clipMain,st,clipMain.outPoint - clipMain.inPoint)
            let textDynamic = 0;
            for(let j=0;j<groupClip.otherClips.length;j++){
                let otherClip = groupClip.otherClips[j];
                let svipData = {type:'image'};
                if(otherClip.tClip.ClipType == 'Text_Dynamic'){
                    svipData.type = 'text_dynamic';
                    const contents = otherClip.tClip.Contents.slice();
                    svipData.contents = contents;
                    otherClip.tClip.Config.systems.splitMode = 'side';
                    textDynamic++;
                }
                let cgClip = this.fuseToClip(otherClip.tClip,svipData, otherClip.track,tlData);
                // console.log('cgClip',svipData,cgClip)
                cgClip.sceneId = sceneId;
                
                cgClip.trackIn = st ;
                cgClip.trackOut = cgClip.trackIn + (clipMain.trackOut-clipMain.trackIn);
                cgClip.duration = cgClip.trackOut - cgClip.trackIn;
                cgClip.canRecord = false;

                if(textDynamic == 1){
                    //第一个动态字幕进行替换
                    cgClip.contents[0] = scene_data.content;
                    new_scene.dynamicClipId = cgClip.modelId;
                }

                otherClip.track.insertClip(cgClip);
                new_scene.clips.push(cgClip.modelId);
            }
            st = clipMain.trackOut;
            newScences.push(new_scene);
        }
        this.sceneDatas = newScences;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_INIT,this.sceneDatas);
        //初始化音频轨
        for(let i=0;i<this.audioTracks.length;i++){
            let track = this.audioTracks[i];
            for (let j = 0; j < track.rawTrackData.Clips.length; j++) {
                let _clip = track.rawTrackData.Clips[j];
                let clip = this.tlClipToClip(track, _clip, tlData);
                if (clip) {
                    track.insertClip(clip);
                }
            }
        }

        // //根据主轨道素材，对其它轨道素材进行编组，并记录时间头差值
        // for(let i=0;i<trackMain.clips.length;i++){
        //     let sceneId = Utils.GUID();
        //     const clipMain = trackMain.clips[i];
        //     clipMain.sceneId = sceneId;
        //     for(let j=0;j<this.cgTracks.length;j++){
        //         const cgTrack = this.cgTracks[j];
        //         for(let m=0;m<cgTrack.clips.length;m++){
        //             const cgClip = cgTrack.clips[m];
        //             //只要入点在主素材范围内，就与主素材成组
        //             if(cgClip.trackIn>= clipMain.trackIn && cgClip.trackIn < clipMain.trackOut){
        //                 cgClip.sceneId = sceneId;
        //             }
        //         }
        //     }
        // }
        
        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek(0);
        this.HistoryModel.initData();
    }
    /**
     * 模板Clip转为内部clip
     */
    tlClipToClip(track, tlClip, tlData) {
        let clip;
        let hostPath = tlData.HostPath;
        // let isCustom = tlData.TemplateMode == 'custom';
        switch (tlClip.ClipType) {
            case 'Text':
                clip = DataFactory.getTextData({
                    carryData: null,
                    tlData: tlClip,
                    content: tlClip.Content,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    dataType2: track.dataType2,
                    pos: tlClip.Pos,
                    styles: tlClip.Style,
                    textType: tlClip.TextType
                })
                break;
            case 'Text_Dynamic':
                clip = DataFactory.getTextDynamicData({
                    carryData: null,
                    tlData: tlClip,
                    contents: tlClip.Contents,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    sourceUrl: Utils.getURLSplicing([hostPath.HttpHost, tlClip.PreviewUrl]),
                    width: tlClip.Width,
                    height: tlClip.Height,
                    duration: tlClip.Duration,
                    config: tlClip.Config,
                    path: Utils.getURLSplicing([hostPath.HttpHost, tlClip.HighPath])
                })

                clip.inAnimDur = tlClip.InAnimDur;
                clip.outAnimDur = tlClip.OutAnimDur;
                break;
            case 'Video':
                clip = DataFactory.getVideoData({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    inPoint: tlClip.TrimIn,
                    outPoint: tlClip.TrimOut,
                    duration: tlClip.Duration,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                    isMute: !!tlClip.Mute,
                    sameId: tlClip.SameId
                })
                if (clip.dataType2 != 'Main') clip.isMute = true;
                clip.effects = this.EffectAgent.transTLToEffect(tlClip, true);
                clip.effectTrans = this.EffectAgent.transTLToTrans(tlClip.TransFX);
                this.EffectAgent.initFillScale(clip);
                if ((~~tlClip.ImageMode) === -1) {
                    if (tlClip.Width / tlClip.Height <= this.preset.width / this.preset.height) {
                        clip.scaleType = 1;
                    } else {
                        clip.scaleType = 2;
                    }
                    // console.log(tlClip.Width/tlClip.Height , this.preset.width/this.preset.height)
                } else {
                    clip.scaleType = (~~tlClip.ImageMode);
                }
                // console.log('clip.scaleType',clip.scaleType)
                // if(clip.dataType2 === 'VideoBg'){
                //     clip.isMute = true;
                //     clip.scaleType = 0;
                // }
                break;
            case 'Audio':
                clip = DataFactory.getAudioData({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    inPoint: tlClip.TrimIn,
                    outPoint: tlClip.TrimOut,
                    duration: tlClip.Duration,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                })
                if (clip.dataType2 == 'AudioBg') {
                    if(tlClip.HighPath == ''){
                        clip.path = '';
                    }
                }
                break;
            case 'Image':
                if (track.dataType === DataType.TRACK_CG) {
                    clip = DataFactory.getImageData({
                        carryData: null,
                        tlData: tlClip,
                        dataType2: track.dataType2,
                        trackIn: tlClip.TrackIn,
                        trackOut: tlClip.TrackOut,
                        keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                        previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                        path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                        imageWidth: tlClip.Width,
                        imageHeight: tlClip.Height,
                        pos: tlClip.Pos
                    })
                } else if (track.dataType === DataType.TRACK_VIDEO) {
                    clip = DataFactory.getVideoImageData({
                        carryData: null,
                        tlData: tlClip,
                        dataType2: track.dataType2,
                        trackIn: tlClip.TrackIn,
                        trackOut: tlClip.TrackOut,
                        keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                        previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                        path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                        imageWidth: tlClip.Width,
                        imageHeight: tlClip.Height,
                        sameId: tlClip.SameId
                    });
                    clip.effects = this.EffectAgent.transTLToEffect(tlClip, true);
                    clip.effectTrans = this.EffectAgent.transTLToTrans(tlClip.TransFX);
                    this.EffectAgent.initFillScale(clip);
                    if ((~~tlClip.ImageMode) === -1) {
                        if (tlClip.Width / tlClip.Height <= this.preset.width / this.preset.height) {
                            clip.scaleType = 1;
                        } else {
                            clip.scaleType = 2;
                        }
                    } else {
                        clip.scaleType = (~~tlClip.ImageMode);
                    }
                    // if(clip.dataType2 === 'VideoBg'){
                    //     clip.scaleType = 0;
                    // }
                }

                break;
            case 'Image_Tga':
                clip = DataFactory.getTgaData({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                    totalFrames: tlClip.TotalFrames,
                    pos: tlClip.Pos
                })
                break;
            case 'Image_Webp':
                clip = DataFactory.getImageDataWebp({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                    totalFrames: tlClip.TotalFrames,
                    pos: tlClip.Pos
                })
                break;
            case 'Image_Gif':
                clip = DataFactory.getImageDataGIF({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                    totalFrames: tlClip.TotalFrames,
                    pos: tlClip.Pos
                })
                break;
        }

        clip.tlData = tlClip;
        return clip;
    }

    /**
     * 融合tlClip与svip数据
     */
    fuseToClip(tlClip, svipData,track,tlData) {
        let clip;
        let hostPath = tlData.HostPath;
        switch (svipData.type) {
            case 'text_dynamic':
                clip = DataFactory.getTextDynamicData({
                    dataType2: track.dataType2,
                    carryData: null,
                    tlData: tlClip,
                    contents: svipData.contents,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    sourceUrl: Utils.getURLSplicing([hostPath.HttpHost, tlClip.PreviewUrl]),
                    width: tlClip.Width,
                    height: tlClip.Height,
                    duration: tlClip.Duration,
                    config: JSON.parse(JSON.stringify(tlClip.Config)),
                    path: Utils.getURLSplicing([hostPath.HttpHost, tlClip.HighPath])
                })

                clip.inAnimDur = tlClip.InAnimDur;
                clip.outAnimDur = tlClip.OutAnimDur;
                break;
            case 'video':
                clip = DataFactory.getVideoData({
                    dataType2: track.dataType2,
                    carryData: svipData,
                    tlData: tlClip,
                    trackIn: 0,
                    trackOut: Number(svipData.duration),
                    inPoint: Number(svipData.inPoint),
                    outPoint: Number(svipData.outPoint),
                    duration: Number(svipData.duration),
                    keyFrameUrl: svipData.cover || Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: svipData.preUrl || Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: svipData.path || Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                    imageWidth: Number(svipData.width),
                    imageHeight: Number(svipData.height),
                    title: svipData.title,
                    isMute: false,
                })
                //兼容后端给的出点超出视频长度
                if(clip.outPoint > clip.duration){
                    let dur = clip.outPoint - clip.inPoint;
                    clip.outPoint = clip.duration;
                    clip.inPoint = clip.outPoint - dur;
                }
                if (clip.dataType2 != 'Main') clip.isMute = true;
                clip.effects = this.EffectAgent.transTLToEffect(tlClip, true);
                clip.effectTrans = this.EffectAgent.transTLToTrans(tlClip.TransFX);
                this.EffectAgent.initFillScale(clip);
                // if(clip.width / clip.height <= this.preset.width / this.preset.height){
                //     clip.scaleType = 1;
                // }else{
                //     clip.scaleType = 2;
                // }
                // //校正特技时长
                // for(let eff of clip.effects){
                //     for(let keyParam of eff.keyFrameParameters){
                //         keyParam.pos = keyParam.posScale*clip.duration;
                //     }
                // }
                // console.log('clip.scaleType',clip.scaleType)
                // if(clip.dataType2 === 'VideoBg'){
                //     clip.isMute = true;
                //     clip.scaleType = 0;
                // }
                break;
            case 'audio':
                clip = DataFactory.getAudioData({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: track.dataType2,
                    trackIn: tlClip.TrackIn,
                    trackOut: tlClip.TrackOut,
                    inPoint: tlClip.TrimIn,
                    outPoint: tlClip.TrimOut,
                    duration: tlClip.Duration,
                    // title: svipData.title,
                    keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                    previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                    path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                })
                if (clip.dataType2 == 'AudioBg') {
                    if(tlClip.HighPath == ''){
                        clip.path = '';
                    }
                }
                break;
            case 'image':
                if (track.dataType === DataType.TRACK_CG) {
                    if(tlData.ClipType == 'Image_Webp'){
                        clip = DataFactory.getImageDataWebp({
                            carryData: null,
                            tlData: tlClip,
                            dataType2: track.dataType2,
                            trackIn: tlClip.TrackIn,
                            trackOut: tlClip.TrackOut,
                            keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                            previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                            path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                            imageWidth: tlClip.Width,
                            imageHeight: tlClip.Height,
                            totalFrames: tlClip.TotalFrames,
                            pos: tlClip.Pos
                        })
                    }else if(tlData.ClipType == 'Image_Gif'){
                        clip = DataFactory.getImageDataGIF({
                            carryData: null,
                            tlData: tlClip,
                            dataType2: track.dataType2,
                            trackIn: tlClip.TrackIn,
                            trackOut: tlClip.TrackOut,
                            keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                            previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                            path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                            imageWidth: tlClip.Width,
                            imageHeight: tlClip.Height,
                            totalFrames: tlClip.TotalFrames,
                            pos: tlClip.Pos
                        })
                    }else{
                        clip = DataFactory.getImageData({
                            carryData: null,
                            tlData: tlClip,
                            dataType2: track.dataType2,
                            trackIn: tlClip.TrackIn,
                            trackOut: tlClip.TrackOut,
                            keyFrameUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                            previewUrl: Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                            path: Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                            imageWidth: tlClip.Width,
                            imageHeight: tlClip.Height,
                            pos: tlClip.Pos
                        })
                    }
                } else if (track.dataType === DataType.TRACK_VIDEO) {
                    clip = DataFactory.getVideoImageData({
                        dataType2: track.dataType2,
                        carryData: svipData,
                        tlData: tlClip,
                        trackIn: 0,
                        trackOut: Number(svipData.duration),
                        keyFrameUrl: svipData.cover || Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.KeyFrameUrl]),
                        previewUrl: svipData.preUrl || Utils.getURLSplicing([hostPath.HttpHost, hostPath.HttpRelativePath, tlClip.PreviewUrl]),
                        path: svipData.path || Utils.getURLSplicing([hostPath.FileHost, hostPath.FileRelativePath, tlClip.HighPath]),
                        imageWidth: Number(svipData.width),
                        imageHeight: Number(svipData.height),
                        title: svipData.title,
                    });
                    clip.effects = this.EffectAgent.transTLToEffect(tlClip, true);
                    clip.effectTrans = this.EffectAgent.transTLToTrans(tlClip.TransFX);
                    this.EffectAgent.initFillScale(clip);
                    // if(clip.imageWidth / clip.imageHeight <= this.preset.width / this.preset.height){
                    //     clip.scaleType = 1;
                    // }else{
                    //     clip.scaleType = 2;
                    // }
                    //校正特技时长
                    // for(let eff of clip.effects){
                    //     for(let keyParam of eff.keyFrameParameters){
                    //         keyParam.pos = keyParam.posScale*(clip.trackOut-clip.trackIn);
                    //     }
                    // }
                    // if(clip.dataType2 === 'VideoBg'){
                    //     clip.scaleType = 0;
                    // }
                }

                break;
        }
        return clip;
    }


     /**
     * 传入ID，返回轨道
     */
    getTrackOfID(id){
        for(let i=0;i<this.tracks.length;i++){
            if(id === this.tracks[i].modelId){
                return this.tracks[i];
            }
        }
        return null;
    }
    /** 获得主轨道  */
    getMainTrack(){
        let mainTrack = null;
        for(let i=0;i<this.tracks.length;i++){
            let _track = this.tracks[i];
            if(_track.isMain){
                mainTrack = _track;
                break;
            }
        }
        return mainTrack;
    }
    /** 获得主轨道主要素材 */
    getMainTrackClip(){
        const clips = [];
        const mainClips = this.getMainTrack().clips;
        for(let i=0;i<mainClips.length;i++){
            let clip = mainClips[i];
            if(clip.dataType2 == 'Main'){
                clips.push(clip);
            }
        }
        return clips;
    }
    /**选中某一个场景 */
    selectSceneOfId(sceneId){
        //选中主素材
        const mainTrack = this.getMainTrack();
        for(let i=0;i<mainTrack.clips.length;i++){
            let clip = mainTrack.clips[i];
            if(clip.sceneId == sceneId){
                this.selectClip(clip);
                let willTime = clip.trackIn;
                if(i-1>=0){
                    let preClip = mainTrack.clips[i-1];
                    if(preClip.effectTrans){
                        willTime += preClip.effectTrans.duration;
                    }
                }
                this.seek(willTime);
                break;
            }
        }
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            let beforeStatus = sceneData.isSelected;
            sceneData.isSelected = sceneData.sceneId == sceneId;
            if(sceneData.isSelected){
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_SELECT,sceneData);
            }
            if(beforeStatus != sceneData.isSelected){
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,sceneData);
            }
        }
    }
    /**获得一个选中的场景对象 */
    getSelectScene(){
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.isSelected){
                return sceneData;
            }
        }
        return null;
    }
    /**获得一个场景对象 */
    getSceneOfId(sceneId){
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.sceneId == sceneId){
                return sceneData;
            }
        }
        return null;
    }
    /**交换或移动场景位置 */
    exchangeScene(sourceId,targetId){
        const msg = {
            cmd:'success',
            desc:'交换成功'
        }
        const mainTrack = this.getMainTrack();
        let trackIn = 0,sourceClip = null,targetClip = null;
        let sourceI = -1,targetI = -1;
        for(let i=0;i<mainTrack.clips.length;i++){
            let clip = mainTrack.clips[i];
            if(clip.dataType2 == 'Title_Video'){
                trackIn = clip.trackOut;
            }
            if(clip.sceneId == sourceId){
                sourceClip = clip;
                sourceI = i;
            }else if(clip.sceneId == targetId){
                targetClip = clip;
                targetI = i;
            }
        }
        if(!sourceClip || !targetClip){
            msg.cmd = 'error';
            msg.desc = '未找到目标素材';
            return msg;
        }

        if(sourceI == targetI || sourceI+1 == targetI){
            msg.cmd = 'invalid';
            msg.desc = '无效拖动';
            return msg;
        }
        if(sourceI > targetI){
            mainTrack.clips.splice(sourceI,1);
            mainTrack.clips.splice(targetI,0,sourceClip);
        }else if(sourceI < targetI){
            mainTrack.clips.splice(sourceI,1);
            mainTrack.clips.splice(targetI-1,0,sourceClip);
        }

        for(let i=0;i<mainTrack.clips.length;i++){
            let clip = mainTrack.clips[i];
            if(clip.dataType2 == 'Main'){
                let dur = clip.trackOut - clip.trackIn;
                clip.trackIn = trackIn;
                clip.trackOut = trackIn + dur;
                trackIn = clip.trackOut;
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
            }
        }

        for(let i=0;i<this.sceneDatas.length;i++){
            let clip = this.sceneDatas[i];
            if(clip.sceneId == sourceId){
                sourceClip = clip;
                sourceI = i;
            }else if(clip.sceneId == targetId){
                targetClip = clip;
                targetI = i;
            }
        }
        if(sourceI > targetI){
            this.sceneDatas.splice(sourceI,1);
            this.sceneDatas.splice(targetI,0,sourceClip);
        }else if(sourceI < targetI){
            this.sceneDatas.splice(sourceI,1);
            this.sceneDatas.splice(targetI-1,0,sourceClip);
        }
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_INIT,this.sceneDatas);
        // if(sourceI > targetI){
        //     mainTrack.clips.splice(sourceI,1);
        //     mainTrack.clips.splice(targetI,0,sourceClip);
        // }else if(sourceI < targetI){
        //     mainTrack.clips.splice(sourceI,1);
        //     mainTrack.clips.splice(targetI+1,0,sourceClip);
        // }

        // console.log('sourceClip',sourceClip,targetClip)
        // return;
        //

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek(this.currentTime);
        //记录
        this.HistoryModel.record('移动场景位置');
        return msg;
    }

    /** 删除一个场景 */
    deleteSceneOfId(sceneId){
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            for(let j=0;j<track.clips.length;j++){
                let clip = track.clips[j];
                if(clip.sceneId == sceneId){
                    track.clips.splice(j,1);
                    j--;
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
                }
            }
        }
        for(let i=0;i<this.sceneDatas.length;i++){
            let scene = this.sceneDatas[i];
            if(scene.sceneId == sceneId){
                this.sceneDatas.splice(i,1);
                break;
            }
        }
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_INIT,this.sceneDatas);

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek(this.currentTime);
        //记录
        this.HistoryModel.record('删除一个场景');
    }
    /** 传入clip ids 返回多个clip对象 */
    getClipsOfIds(ids){
        const clips = [];
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            for(let j=0;j<track.clips.length;j++){
                let clip = track.clips[j];
                if(ids.indexOf(clip.modelId)>=0){
                    clips.push(clip);
                }
            }
        }
        return clips;
    }
   
    /**
     * 重新排列轨道
     */
    sortTrack() {
        //将三种轨道合并 按照CG,V,A的顺序
        this.cgTracks.sort((a, b) => { return a.trackIndex > b.trackIndex ? -1 : 1 });
        this.videoTracks.sort((a, b) => { return a.trackIndex > b.trackIndex ? -1 : 1 });
        this.audioTracks.sort((a, b) => { return a.trackIndex > b.trackIndex ? -1 : 1 });
        this.tracks = [].concat(this.cgTracks, this.videoTracks, this.audioTracks);
        const willChangeTracks = [];
        let length = this.tracks.length;
        this.tracks.forEach((ele, index) => {
            let isSame = ele.trackIndex === (length - index);
            ele.trackIndex = (length - index);
            ele.clips.forEach(clip => {
                clip.trackIndex = (length - index);
            })
            if (!isSame) {
                willChangeTracks.push(ele);
            }
        });
        for (let i = 0; i < willChangeTracks.length; i++) {
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.TRACK_CHANGE, willChangeTracks[i]);
        }
        return willChangeTracks;
    }

    // /** 设置背景层 true 为顶层 false底层  */
    // setVideoBgLayer(b) {
    //     for (let i = 0; i < this.videoTracks.length; i++) {
    //         if (this.videoTracks[i].dataType2 === 'VideoBg') {
    //             let videoBgTrack = this.videoTracks[i];
    //             if (b) {
    //                 videoBgTrack.trackIndex = 400;
    //             } else {
    //                 videoBgTrack.trackIndex = 0;
    //             }
    //             this.sortTrack();
    //             break;
    //         }
    //     }
    // }

    addTitleTail(ttData){
        if(this.ttMaterials){
            this.removeTitleTail();
        }
        this.ttMaterials = ttData;//片头片尾原始数据
        if(typeof this.ttMaterials.templateData == 'string'){
            this.ttMaterials.templateData = JSON.parse(this.ttMaterials.templateData);
        }
        const TitleTail = this.ttMaterials.templateData.TitleTail;
        let titleClip = null;
        let tailClip = null;
        let waterMarks = [];
        let mainTrack = null;
        let waterTrack = null;
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.isMain){
                mainTrack = track;
            }else if(track.dataType2 == 'Water'){
                waterTrack = track;
            }
        }
        
        for (let i = 0; i < TitleTail.length; i++) {
            let tlClip = TitleTail[i];
            if (tlClip.ClipType == 'Title_Video') {
                let clip = DataFactory.getVideoData({
                    carryData: tlClip,
                    dataType2: tlClip.ClipType,
                    trackIn: 0,
                    trackOut: 0,
                    inPoint: 0,
                    outPoint: tlClip.Duration,
                    // retainPoints:  [{inPoint:0,outPoint:tlClip.Duration}],//保留点
                    duration: tlClip.Duration,
                    keyFrameUrl: tlClip.KeyFrameUrl,
                    previewUrl: tlClip.PreviewUrl,
                    path: tlClip.HighPath,
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                });
                if (tlClip.Width / tlClip.Height <= this.preset.width / this.preset.height) {
                    clip.scaleType = 1;
                } else {
                    clip.scaleType = 2;
                }
                titleClip = clip;
            }else if (tlClip.ClipType == 'Tail_Video') {
                let clip = DataFactory.getVideoData({
                    carryData: tlClip,
                    dataType2: tlClip.ClipType,
                    trackIn: 0,
                    trackOut: 0,
                    inPoint: 0,
                    outPoint: tlClip.Duration,
                    // retainPoints:  [{inPoint:0,outPoint:tlClip.Duration}],//保留点
                    duration: tlClip.Duration,
                    keyFrameUrl: tlClip.KeyFrameUrl,
                    previewUrl: tlClip.PreviewUrl,
                    path: tlClip.HighPath,
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                });
                if (tlClip.Width / tlClip.Height <= this.preset.width / this.preset.height) {
                    clip.scaleType = 1;
                } else {
                    clip.scaleType = 2;
                }
                tailClip = clip;
            }else if (tlClip.ClipType == 'WaterMark_Image') {
                let clip = DataFactory.getImageData({
                    carryData: null,
                    tlData: tlClip,
                    dataType2: tlClip.ClipType,
                    trackIn: 0,
                    trackOut: 0,
                    keyFrameUrl: tlClip.KeyFrameUrl,
                    previewUrl: tlClip.PreviewUrl,
                    path: tlClip.HighPath,
                    imageWidth: tlClip.Width,
                    imageHeight: tlClip.Height,
                    pos: tlClip.Pos
                });
                waterMarks.push(clip);
            }
        }

        if (titleClip) {
            mainTrack.insertClip(titleClip);
        }
        if (tailClip) {
            mainTrack.insertClip(tailClip);
        }
        if (waterMarks.length > 0) {
            waterTrack.insertClips(waterMarks);
        }

        App.CONTROL.CORE.$emit(App.CONTROL.CORE.TITLETAIL_STATUS, this.ttMaterials);

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek();

        //记录
        this.HistoryModel.record('添加片头片尾');
    }

    removeTitleTail(){
        this.ttMaterials = null;//片头片尾原始数据
        this.getMainTrack().removeTitleTail();
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'Water'){
                track.clear();
                break;
            }
        }
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.TITLETAIL_STATUS, this.ttMaterials);

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek(0);

        //记录
        this.HistoryModel.record('删除片头片尾');
    }
    /**
     * 替换场景主素材
     */
    replaceSceneOfMain(svipData,sceneId=''){
        const msg = {
            cmd:'fail',
            info:''
        }
        //找到场景主素材
        let mainClip = null;
        const mainTrack = this.getMainTrack();
        let index = 0;
        if(sceneId){
            for(let i=0;i<mainTrack.clips.length;i++){
                let clip = mainTrack.clips[i];
                if(clip.dataType2 == 'Main' && clip.sceneId == sceneId){
                    mainClip = clip;
                    index = i;
                    break;
                }
            }
        }else{
            for(let i=0;i<mainTrack.clips.length;i++){
                let clip = mainTrack.clips[i];
                if(clip.dataType2 == 'Main' && clip.isSelected){
                    mainClip = clip;
                    index = i;
                    break;
                }
            }
        }
        
        if(!mainClip){
            msg.info = '请先选中场景对象！';
            return msg;
        }
        
        const newClip = this.fuseToClip(mainClip.tlData,svipData,mainTrack,this.rawtlData);
        if(newClip.dataType == DataType.CLIP_VIDEO && newClip.duration < mainClip.trackOut - mainClip.trackIn){
            msg.info = '时长不足，不能替换';
            return msg;
        }
        newClip.sceneId = mainClip.sceneId;
        newClip.trackId = mainClip.trackId;
        newClip.dataType2 = mainClip.dataType2;
        newClip.trackIn = mainClip.trackIn;
        newClip.trackOut = mainClip.trackOut;
        newClip.inPoint = 0;
        newClip.outPoint = mainClip.trackOut-mainClip.trackIn;
        newClip.isSelected = true;
        newClip.isMute = mainClip.isMute;
        mainTrack.clips.splice(index,1,newClip);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,mainClip);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,newClip);
        // this.selectClip(newClip);

        for(let i=0;i<this.sceneDatas.length;i++){
            let scene_data = this.sceneDatas[i];
            if(scene_data.sceneId == newClip.sceneId){
                let order = scene_data.clips.indexOf(mainClip.modelId);
                if(order>=0){
                    scene_data.clips[order] = newClip.modelId;
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,scene_data);
                    break;
                }
            }
        }
        // let trackIn = 0;
        // if(mainTrack.clips[0].dataType2 == 'Title_Video'){
        //     trackIn = mainTrack.clips[0].trackOut;
        // }
        // for(let i=0;i<mainTrack.clips.length;i++){
        //     let clip = mainTrack.clips[i];
        //     if(clip.dataType2 == 'Main'){
        //         let dur = clip.trackOut - clip.trackIn;
        //         clip.trackIn = trackIn;
        //         clip.trackOut = clip.trackIn + dur;
        //         trackIn = clip.trackOut;
        //         //转场可在flushSeq函数中校正
        //     }
        // }

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek(newClip.trackIn);

        //记录
        this.HistoryModel.record('替换场景主素材');
        msg.cmd = 'success';
        msg.info = '替换成功！';
        return msg;
    }
    /**替换背景音 */
    replaceAudioBg(svipData){
        //背景音轨默认为一个音频
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'AudioBg'){
                let newClip = DataFactory.getAudioData({
                    carryData:svipData,
                    duration: Number(svipData.duration),
                    inPoint:0,
                    outPoint: Number(svipData.duration),
                    previewUrl: svipData.preUrl,
                    path: svipData.path,
                    title: svipData.title
                })
                if(track.clips.length){
                    let rawClip = track.clips[0];
                    track.deleteClip(rawClip);
                    newClip.isMute = rawClip.isMute;
                }
                newClip.dataType2 = 'AudioBg';
                track.insertClip(newClip);

                this.flushSeqDuration();
                this.EffectAgent.computerTrans();
                this.seek(0);

                //记录
                this.HistoryModel.record('替换背景音乐');
                break;
            }
        }
    }
    /** 获得背景音乐对象 */
    getAudioBgClip(){
        let audioBgClip = null;
        for(let i=0;i<this.audioTracks.length;i++){
            let track = this.audioTracks[i];
            if(track.dataType2 == 'AudioBg'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    audioBgClip = clip;
                    break;
                }
            }
        }
        return audioBgClip;
    }
    /** 禁用或开启背景音乐 */
    enableAudioBg(b){
        this.systemConfig.enabledAudioBg = b;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SYSTEM_CHANGE,this.systemConfig);
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'AudioBg'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    clip.isMute = !b;
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
                }
            }
        }
    }
    /** 禁用或开启主轨声音 */
    enableAudioMain(b){
        this.systemConfig.enabledAudioMain = b;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SYSTEM_CHANGE,this.systemConfig);
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'Main'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    clip.isMute = !b;
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
                }
            }
        }
    }
    /** 禁用或开启配音轨声音 */
    enableAudioDub(b){
        this.systemConfig.enabledAudioDub = b;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SYSTEM_CHANGE,this.systemConfig);
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'Dub'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    clip.isMute = !b;
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
                }
            }
        }
    }
    /** 删除某一个场景中的某一个素材 */
    deleteClipOfSceneId(sceneId,clipId){
        let curSceneData = null;
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.sceneId == sceneId){
                curSceneData = sceneData;
                break;
            }
        }
        if(!curSceneData){
            console.log('未找到场景')
            return;
        }
        let isFind = false;
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            for(let j=0;j<track.clips.length;j++){
                let clip = track.clips[j];
                if(clip.modelId == clipId){
                    track.clips.splice(j,1);
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
                    isFind = true;
                    break;
                }
            }
            if(isFind){
                break;
            }
        }
        if(clipId == curSceneData.dubClipId){
            curSceneData.dubClipId = '';
        }
        let order = curSceneData.clips.indexOf(clipId);
        if(order>=0){
            curSceneData.clips.splice(order,1);
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,curSceneData);
        }

        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek();

        this.HistoryModel.record('删除某一个场景素材');
    }
    /** 增加模板数据 字幕或图片元素到Multilayer轨 type=dynamic/template */
    addToMultilayerOfSceneId(sceneId,rawData,type='dynamic'){
        let curSceneData = null;
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.sceneId == sceneId){
                curSceneData = sceneData;
                break;
            }
        }
        if(!curSceneData){
            console.log('未找到场景')
            return;
        }

        let mainClip = null;
        let trackMultilayer = null;
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'Multilayer'){
                trackMultilayer = track;
            }else if(track.dataType2 == 'Main'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    if(clip.sceneId == sceneId){
                        mainClip = clip;
                        break;
                    }
                }
            }
        }
        let clip = null;
        if(type == 'dynamic'){
            clip = DataFactory.getTextDynamicData({
                dataType2: trackMultilayer.dataType2,
                carryData: rawData,
                tlData: null,
                contents: [],
                trackIn: mainClip.trackIn,
                trackOut: mainClip.trackOut,
                sourceUrl: Utils.getURLSplicing([rawData.httpHost, rawData.sourceUrl]),
                width: 1920,
                height: 1080,
                duration: mainClip.trackOut-mainClip.trackIn,
                // config: JSON.parse(JSON.stringify(tlClip.Config)),
                path: Utils.getURLSplicing([rawData.httpHost, rawData.sourceUrl])
            })

            clip.sceneId = sceneId;
            curSceneData.clips.push(clip.modelId);
            trackMultilayer.insertClip(clip);
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,curSceneData);
            this.flushSeqDuration();

            this.HistoryModel.record('新增动态字幕');

        }else if(type == 'template'){
            const params = {
                carryData: rawData,
                tlData: null,
                dataType2: trackMultilayer.dataType2,
                trackIn: mainClip.trackIn,
                trackOut: mainClip.trackOut,
                duration: (rawData.templateData.duration || 0)*1000,
                keyFrameUrl: rawData.keyFrameUrl,
                previewUrl: Utils.getURLSplicing([rawData.httpHost, rawData.httpRelativePath,rawData.templateData.prevUrl]),
                path: Utils.getURLSplicing([rawData.fileHost, rawData.fileRelativePath,rawData.templateData.filePath]),
                imageWidth: rawData.templateData.highWidth,
                imageHeight: rawData.templateData.highHeight,
            }

            let lowserUrl = params.previewUrl.toLocaleLowerCase();
            if(lowserUrl.lastIndexOf('.webp')>-1){
                clip = DataFactory.getImageDataWebp(params);
            }else if(lowserUrl.lastIndexOf('.gif')>-1){
                clip = DataFactory.getImageDataGIF(params);
            }else{
                clip = DataFactory.getImageData(params);
            }
            console.log('template clip',params.trackIn,clip.trackIn,clip.trackOut,clip)

            clip.sceneId = sceneId;
            curSceneData.clips.push(clip.modelId);
            trackMultilayer.insertClip(clip);
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,curSceneData);
            this.flushSeqDuration();

            this.HistoryModel.record('新增模板数据');
        }
    }
    /** 增加音频到SoundEffect轨 */
    addToSoundEffectOfSceneId(sceneId,rawData){
        let curSceneData = null;
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.sceneId == sceneId){
                curSceneData = sceneData;
                break;
            }
        }
        if(!curSceneData){
            console.log('未找到场景')
            return;
        }

        let mainClip = null;
        let trackSoundEffect = null;
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'SoundEffect'){
                trackSoundEffect = track;
            }else if(track.dataType2 == 'Main'){
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    if(clip.sceneId == sceneId){
                        mainClip = clip;
                        break;
                    }
                }
            }
        }
        // console.log('trackSoundEffect',mainClip,trackSoundEffect,rawData)
        
        const clip = DataFactory.getAudioData({
            dataType2: trackSoundEffect.dataType2,
            carryData: rawData,
            tlData: null,
            trackIn: mainClip.trackIn,
            trackOut: mainClip.trackOut,
            previewUrl: Utils.getURLSplicing([rawData.httpHost, rawData.httpRelativePath,rawData.templateData.prevUrl]),
            path: Utils.getURLSplicing([rawData.fileHost, rawData.fileRelativePath,rawData.templateData.filePath]),
            duration: rawData.templateData.duration*1000,
        })
        clip.sceneId = sceneId;
        curSceneData.clips.push(clip.modelId);
        trackSoundEffect.insertClip(clip);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,curSceneData);
        this.flushSeqDuration();
        this.HistoryModel.record('新增音效');
    }
    /** 增加或更新配音到配音轨 */
    addDubSceneOfId(sceneId,rawData){
        let curSceneData = null;
        for(let i=0;i<this.sceneDatas.length;i++){
            let sceneData = this.sceneDatas[i];
            if(sceneData.sceneId == sceneId){
                curSceneData = sceneData;
                break;
            }
        }
        if(!curSceneData){
            console.log('未找到场景')
            return;
        }

        let mainClip = null;
        let trackDub = null;
        let oldDub = null;
        for(let i=0;i<this.tracks.length;i++){
            let track = this.tracks[i];
            if(track.dataType2 == 'Dub'){
                trackDub = track;
                for(let j=0;j<track.clips.length;j++){
                    let clip = track.clips[j];
                    if(clip.sceneId == sceneId){
                        oldDub = clip;
                        break;
                    }
                }
            }else if(track.dataType2 == 'Main'){
                for(let i=0;i<track.clips.length;i++){
                    let clip = track.clips[i];
                    if(clip.sceneId == sceneId){
                        mainClip = clip;
                        break;
                    }
                }
            }
        }

        

        if(oldDub){
            //先从配音轨删除之前的
            trackDub.deleteClip(oldDub);
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,oldDub);
        }
        //新建配音
        const clip = DataFactory.getAudioData({
            dataType2: trackDub.dataType2,
            carryData: rawData,
            tlData: null,
            trackIn: mainClip.trackIn,
            trackOut: mainClip.trackIn+rawData.duration,
            previewUrl: rawData.preUrl,
            path: rawData.path,
            duration: rawData.duration,
        })
        clip.sceneId = sceneId;
        curSceneData.clips.push(clip.modelId);
        curSceneData.dubClipId = clip.modelId;
        trackDub.insertClip(clip,true);
        
        //更新主视频长度
        let audioDur = rawData.duration + 1000;
        mainClip.trackOut = mainClip.trackIn + audioDur;
        if(mainClip.dataType == DataType.CLIP_VIDEO){
            mainClip.outPoint = mainClip.inPoint + audioDur;
            if(mainClip.outPoint > mainClip.duration && mainClip.duration >= audioDur){
                mainClip.outPoint = mainClip.duration;
                mainClip.inPoint = mainClip.duration - audioDur;
            }
        }
        this.flushSeqDuration();
        this.EffectAgent.computerTrans();

        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,curSceneData);

        this.HistoryModel.record('新增配音');
    }
    /**动态字幕内容更改，需要判断场景数据是否同时更改 */
    checkSceneOfDynamic(clip){
        // console.log('checkSceneOfDynamic',clip)
        if(clip.dataType2 == 'TextAigc'){
            for(let i=0;i<this.sceneDatas.length;i++){
                let sceneData = this.sceneDatas[i];
                if(sceneData.sceneId == clip.sceneId){
                    sceneData.content = clip.contents[0];
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.SCENE_CHANGE,sceneData);
                    break;
                }
            }
        }
    }
    /**更新素材 */
    changeClip(clip, isFlush = true,isRecord = true) {
        let track = this.getTrackOfId(clip.trackId);
        if (track) {
            track.changeClip(clip);
            if (isFlush) {
                this.flushSeqDuration();
                this.EffectAgent.computerTrans();
            }
            if(isRecord){
                //记录
                this.HistoryModel.record('更新素材');
            }
        }
    }
    /**删除素材 */
    deleteClip(clip, isFlush = true,isRecord = true) {
        let track = this.getTrackOfId(clip.trackId);
        if (track) {
            track.deleteClip(clip);
            //需要更新场景数据
            if (isFlush) {
                this.flushSeqDuration();
                this.EffectAgent.computerTrans();
                this.seek();
            }
            // this.seek(clip.trackIn);
            if(isRecord){
                //记录
                this.HistoryModel.record('删除素材');
            }
        }
    }
    /**
     * 选中切段 data传空表示取消全部选中 ，b为true，表示选中,clearBefore为true，表示清空之前的选中
     * 后两个值需要在第一个值不为空时生效
     */
    selectClip(data, b = true, clearBefore = true) {
        const allClips = this.getAllClips();
        const changeClips = [];
        for (let i = 0; i < allClips.length; i++) {
            let clip = allClips[i];
            if (data) {
                if (data.modelId === clip.modelId) {
                    clip.isSelected = b;
                    changeClips.push(clip);
                } else {
                    if (clearBefore) {
                        if (clip.isSelected) {
                            clip.isSelected = false;
                            changeClips.push(clip);
                        }
                    }
                }
            } else {
                if (clip.isSelected) {
                    clip.isSelected = false;
                    changeClips.push(clip);
                }
            }
        }
        changeClips.forEach(clip => {
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE, clip);
        })
        // console.log('changeClips.length > 0 && data',changeClips.length, data)
        if(changeClips.length > 0 && data){
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_SELECT, data);
        }
    }
    /** 更新2d特技 */
    updateEffect2D(_clip, _pos) {
        // this.EffectAgent.updateEffect2D(_clip, _pos);
        // if (_clip.sameId !== '') {
        //     for (let i = 0; i < this.videoTracks.length; i++) {
        //         let track = this.videoTracks[i];
        //         for (let j = 0; j < track.clips.length; j++) {
        //             let clip = track.clips[j];
        //             if (clip.sameId === _clip.sameId) {
        //                 this.EffectAgent.updateEffect2D(clip, _pos);
        //             }
        //         }
        //     }
        // }
    }
    /**
     * 剪切和裁剪后更新clip
     */
    cutedClip(clip,params){
        //特技更新
        let effects = clip.effects || [];
        let eff2d = null;
        for (let i = 0; i < effects.length; i++) {
            let eff = effects[i];
            if (eff.dataType === DataType.EFFECT_2D && eff.dataType2 === 'crop') {
                eff2d = eff;
                break;
            }
        }
        if(params.pos.x == 0 && params.pos.y == 0 && params.pos.w == 1 && params.pos.h == 1){
            //此时参数不需要特技
            if(eff2d){
                this.EffectAgent.removeEffect(clip,eff2d);
            }
            let isHas = false;
            for (let i = 0; i < effects.length; i++) {
                let eff = effects[i];
                if (eff.dataType === DataType.EFFECT_MULTIVIEW || eff.dataType === DataType.EFFECT_BGBLUR) {
                    isHas = true;
                    break;
                }
            }
            //有需要填充的特技
            if(isHas){
                clip.scaleType = 0;
            }else if (clip.imageWidth / clip.imageHeight <= this.preset.width / this.preset.height) {
                clip.scaleType = 1;
            } else {
                clip.scaleType = 2;
            }
        }else{
            clip.scaleType = 0;
            let transPos = this.EffectAgent.get2Dparams(clip,Object.assign({},params.pos));
            console.log(transPos,eff2d)
            if(!eff2d){
                eff2d = EffectFactory.get2DData(transPos,clip.duration);
                eff2d.dataType2 = 'crop';
                clip.effects.push(eff2d);
                App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_ADD,{
                    eventsender:'Sequence',
                    clip:clip,
                    effect: eff2d
                });
            }else{
                let dur = clip.duration;
                if(clip.dataType == DataType.CLIP_VIDEO_IMAGE){
                    dur = clip.trackOut - clip.trackIn;
                }
                for (let i = 0; i < eff2d.keyFrameParameters.length; i++) {
                    let keyItem = eff2d.keyFrameParameters[i];
                    Object.assign(keyItem.parameter, transPos);
                    keyItem.pos = keyItem.posScale * dur;
                }
                App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_CHANGE, {
                    eventsender: 'Sequence',
                    clip: clip,
                    effect: eff2d
                });
            }
        }
        clip.fillPos = Object.assign({},params.pos);
        if(clip.dataType == DataType.CLIP_VIDEO){
            clip.inPoint = params.inPoint;
            clip.outPoint = params.outPoint;
            let dur = clip.outPoint - clip.inPoint;
            clip.trackOut = clip.trackIn+dur;
        }
        


        this.flushSeqDuration();
        this.EffectAgent.computerTrans();
        this.seek();

        //记录
        this.HistoryModel.record('更新素材剪切或裁剪');
    }
    /** 拷贝某个TextAigc属性到所有 */
    copyTextAigcToAll(clip){
        const textAigcClips = [];
        for(let i=0;i<this.cgTracks.length;i++){
            let cgTrack = this.cgTracks[i];
            for(let j=0;j<cgTrack.clips.length;j++){
                let _clip = cgTrack.clips[j];
                if(_clip.dataType == DataType.CLIP_CG_TEXT_DYNAMIC && _clip.dataType2 == 'TextAigc'){
                    if(_clip.modelId != clip.modelId){
                        textAigcClips.push(_clip);
                    }
                }
            }
        }

        for(let i = 0;i<textAigcClips.length;i++){
            let _clip = textAigcClips[i];
            _clip.config = JSON.parse(JSON.stringify(clip.config));
            _clip.needFlushRender = true;
            this.changeClip(_clip,false,false);
        }
        this.HistoryModel.record('拷贝TextAigc属性');
    }
    /**
     * 通过id获得clip对象
     */
    getClipOfId(id){
        const clips = this.getAllClips();
        let findClip = null;
        for(let i=0;i<clips.length;i++){
            let clip = clips[i];
            if(clip.modelId == id){
                findClip = clip;
                break;
            }
        }
        return findClip;
    }
    
   
    /**
     * 获得选中的切段
     */
    getSelectClip() {
        let arr = [];
        this.getAllClips().forEach(clip => {
            if (clip.isSelected) arr.push(clip);
        })
        return arr;
    }

    /**
     * 获得一个选中的动态字幕
     */
    getSelectOfDynamicText() {
        let clip = null;
        for(let i=0;i<this.cgTracks.length;i++){
            let cgTrack = this.cgTracks[i];
            for(let j=0;j<cgTrack.clips.length;j++){
                let _clip = cgTrack.clips[j];
                if(_clip.dataType == DataType.CLIP_CG_TEXT_DYNAMIC){
                    clip = _clip;
                    break;
                }
            }
            if(clip) break;
        }
        return clip;
    }

    /** seek到某点 */
    seek(time, b) {
        // this.currentTime = time;
        if (time == undefined) time = this.currentTime;
        time = Math.max(0, Math.min(time, this.seqDuration - 1));
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SEEK, time);
        if (b) {
            this.updateCurrentTime(time);
        }
    }

    /**
     * 开始预览
     */
    startPreview(obj) {
        this.selectClip(null);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PREVIEW_START, obj);
    }

    /**
     * 结束预览
     */
    endPreview(obj) {
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PREVIEW_END, obj);
    }

    /**
     * 更新预览状态
     */
    updatePreviewStatus(b) {
        this.isPreview = b;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PREVIEW_STATUS, this.isPreview);
    }

    /**
     * 更新播放头
     */
    updateCurrentTime(t) {
        this.currentTime = t;
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CURRENTTIME_CHANGE, this.currentTime);
    }

    /**
     * 校验播放头
     */
    checkPlayHead(obj) {
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CHECK_CURRENTTIME, obj);
    }

    /** 预览中缓冲事件 */
    tellWaitforPreview(obj) {
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.PREVIEW_WAIT_CHANGE, obj);
    }

    /**动态字幕属性变化 */
    dynamicTextChange(obj){
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.DYNAMIC_TEXT_CHANGE, obj);
    }

    /**
     * 根据ID获得track
     */
    getTrackOfId(trackId) {
        for (let i = 0; i < this.tracks.length; i++) {
            if (this.tracks[i].modelId === trackId) {
                return this.tracks[i];
            }
        }
        return null;
    }

    /** 获得所有切段 */
    getAllClips() {
        let arr = [];
        for (let i = 0; i < this.tracks.length; i++) {
            arr = arr.concat(this.tracks[i].clips);
        }
        return arr;
    }

    /**
     * 重新计算时间线长度
     */
    flushSeqDuration() {
        
        let mainTrack = this.getMainTrack();
        let mainContentDur = 0;
        let titleClip = null,tailClip = null;
        const tempMainClips = [];
        for(let i=0;i<mainTrack.clips.length;i++){
            let clip = mainTrack.clips[i];
            if(clip.dataType2 == 'Title_Video'){
                titleClip = clip;
            }else if(clip.dataType2 == 'Tail_Video'){
                tailClip = clip;
            }else if(clip.dataType2 == 'Main'){
                tempMainClips.push(clip);
            }
        }
        //计算主轨总长度
        if(tempMainClips.length){
            tempMainClips.sort((a,b)=>{
                return a.trackIn > b.trackIn?1:-1;
            })
            for(let i=0;i<tempMainClips.length;i++){
                let clip = tempMainClips[i];
                mainContentDur += clip.trackOut - clip.trackIn;
                if(clip.effectTrans && i<tempMainClips.length-1){
                    mainContentDur -= clip.effectTrans.duration;
                }
            }
        }
        let trackIn = 0;
        if(titleClip){
            titleClip.trackIn = 0;
            titleClip.trackOut = titleClip.duration;
            trackIn = titleClip.duration;
        }
        if(tailClip){
            tailClip.trackIn = titleClip.duration + mainContentDur;
            tailClip.trackOut = tailClip.trackIn + tailClip.duration;
        }
        mainTrack.sortClipUseST(mainTrack.clips);
        let st = trackIn;
        const groupTrackIn = {};
        let order = 0;
        for(let i=0;i<tempMainClips.length;i++){
            let clip = tempMainClips[i];
            let dur = clip.trackOut - clip.trackIn;
            // console.log('dur',st,dur,clip.trackIn,clip.trackOut)
            clip.trackIn = st;
            clip.trackOut = st + dur;
            st = clip.trackOut;
            if(clip.effectTrans){
                st -= clip.effectTrans.duration;
                if(i<tempMainClips.length-1){
                    dur -= clip.effectTrans.duration;
                }
            }

            clip.order = order++;
            groupTrackIn[clip.sceneId] = {
                trackIn:clip.trackIn,
                duration: dur
            };

            for(let j=0;j<this.sceneDatas.length;j++){
                let _sdata = this.sceneDatas[j];
                if(_sdata.sceneId == clip.sceneId){
                    _sdata.duration = clip.trackOut-clip.trackIn;
                    break;
                }
            }

            //校正特技时长
            for(let eff of clip.effects){
                for(let keyParam of eff.keyFrameParameters){
                    // keyParam.pos = keyParam.posScale*dur;
                    keyParam.pos = keyParam.posScale*dur+(clip.inPoint||0);

                }
            }
        }
        // console.log('trackIn',trackIn,groupTrackIn)
        // return;
        for (let i = 0; i < this.cgTracks.length; i++) {
            let track = this.cgTracks[i];
            if(track.dataType2 == 'Water'){
                for(let i=0;i<track.clips.length;i++){
                    let clip = track.clips[i];
                    clip.trackIn = trackIn;
                    clip.trackOut = trackIn + mainContentDur;
                }
            }else{
                for(let i=0;i<track.clips.length;i++){
                    let clip = track.clips[i];
                    // let dur = clip.trackOut - clip.trackIn;
                    // console.log(groupTrackIn,clip.sceneId)
                    if(groupTrackIn.hasOwnProperty(clip.sceneId)){
                        const info = groupTrackIn[clip.sceneId];
                        
                        clip.trackIn = info.trackIn;
                        clip.trackOut = clip.trackIn+info.duration;
                        // if(clip.dataType == DataType.CLIP_CG_TEXT_DYNAMIC){
                        //     clip.duration = info.duration;
                        // }
                        // console.log(clip.dataType2,clip.trackIn,clip.trackOut)
                        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE, clip);
                    }
                }
            }
            // track.resetClipTrackInOut(mainContentDur,trackIn);
        }
        let dur = mainTrack.getEndTime();
        //背景音频时长
        for (let i = 0; i < this.audioTracks.length; i++) {
            let track = this.audioTracks[i];
            if(track.dataType2 == 'AudioBg'){
                if(track.clips.length){
                    let clip = track.clips[0];
                    let dur = Math.min(clip.duration,mainContentDur);
                    clip.trackIn = trackIn;
                    clip.trackOut = trackIn + dur;
                    clip.inPoint = 0;
                    clip.outPoint = clip.duration;
                }
            }else if(track.dataType2 == 'Dub'){
                //配音需要改变的
                for(let i=0;i<track.clips.length;i++){
                    let clip = track.clips[i];
                    if(groupTrackIn.hasOwnProperty(clip.sceneId)){
                        const info = groupTrackIn[clip.sceneId];
                        let temp_dur = clip.outPoint - clip.inPoint;
                        clip.trackIn = info.trackIn;
                        clip.trackOut = clip.trackIn+Math.min(temp_dur,info.duration);
                    }
                }
            }
            // track.resetClipTrackInOut(mainContentDur,trackIn);
        }
        this.seqDuration = dur;
        this.inPoint = 0;
        this.outPoint = dur;
        // this.flushEventPoint();
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.SEQ_DURATION, this.seqDuration);
    }


    /**
     * 获得时间线数据 true (默认)返回json对象 false返回json字符
     */
    toJSON(returnType=true){
        let obj = {
            name:this.name,
            id:this.id,
            sequenceID:this.sequenceID, //时间线id 后台保存文件时给的
            version:this.version,
            inPoint:this.inPoint,
            outPoint:this.outPoint,
            preset: this.preset,
            ttMaterials: this.ttMaterials,
            tracks: this.tracks,
            sceneDatas: this.sceneDatas,//场景数据
            systemConfig:this.systemConfig,//其它配置数据
        }
        
        if(returnType){
            return JSON.parse(JSON.stringify(obj));
        }
        return JSON.stringify(obj);
    }

    /** 设置时间线保存状态 */
    setTimeLineSaveStatus(b,updateTime){
        this.isSaved = b;
        // App.CONTROL.CORE.saveStatus({isSaved:b,updateTime:updateTime});
        // // console.log('localStorage')
        // if(b){
        //     window.localStorage.setItem('currentTimeLine','');
        // }else{
        //     if(this.getAllClips().length>0){
        //         let json = {
        //             VideoMicroData: this.toJSON(),
        //             catalog: App.MyCatalog?App.MyCatalog.getCatalogData(false):null
        //         }
        //         // console.log('json',json)
        //         window.localStorage.setItem('currentTimeLine',JSON.stringify(json));
        //     }
            
        // }
    }
}

export default Sequence;
