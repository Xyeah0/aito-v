/**
 * 开发者：tangshanghai
 * 日期：2021/07/21
 * 说明：webp缓存
 */
import Event from '../libs/Event.js';
import App from '../../src/model/App.js';
class WebpCache{
    constructor(){
        this.caches = new Array();
        this.xMux = new WebPXMux(window.AppConfig.modulePath+'/assets/libs/webpxmux.wasm');

        // this.xMux.waitRuntime().then(v=>{
        //     console.log('xMux',xMux)
        // })
        window.WebpCache = this;
    }

    /**
     * 获得一个缓存
     */
    getOneCache(url){
        return new Promise((resolve,reject)=>{
            if(!url){
                reject('地址错误');
                return;
            }
            let findCache = null;
            for(let i=0;i<this.caches.length;i++){
                if(this.caches[i].url === url){
                    findCache = this.caches[i];
                    break;
                }
            }
            if(findCache && findCache.created){
                resolve(findCache);
            }else{
                if(!findCache){
                    findCache = new WebpItemCache(url,this.xMux);
                    this.caches.push(findCache);
                }

                findCache.$on('complete',()=>{
                    resolve(findCache);
                });
                findCache.$on('error',()=>{
                    reject('加载或解码错误');
                })
            }
        })
    }


    /**
     * 获得所有缓存
     */
    getAllCache(){
        return this.caches;
    }

}
export default new WebpCache;

class WebpItemCache extends Event{
    constructor(url,xMux){
        super();
        this.url = url;
        this.xMux = xMux;
        this.created = false;
        this.webpFrames = null;

        this.loadWebp(url)
        .then(buffer=>{
            console.time('decodeFrames');
            this.xMux.decodeFrames(new Uint8Array(buffer)).then((frames) => {
                let st = 0;
                frames.frames.forEach((item,index)=>{
                    item.inPoint = st;
                    item.outPoint = (st+item.duration);
                    item.order = index;
                    st += item.duration;
                })
                frames.duration = st;
                console.timeEnd('decodeFrames')
                this.webpFrames = frames;
                this.created = true;
                this.$emit('complete');
            }).catch(error=>{
                this.$emit('error');
            })
            
        })
        .catch(error=>{
            console.log('error',error)
            this.$emit('error');
        })
    }

    loadWebp(url){
        return new Promise((resolve,reject)=>{
			let xhr = new XMLHttpRequest(); 
        
			xhr.responseType = 'arraybuffer'; 
			xhr.onreadystatechange = () => {
				if(xhr.readyState === 4){
					if((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304){
						resolve(xhr.response);
					}else{
						reject(null);
					}
				}
			}

			xhr.open('GET', url, true); 
			xhr.send();
        })
    }

}