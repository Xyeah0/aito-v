/**
 * 数据模型
 */

import Utils from "@/libs/Utils";
import DataType from "@/model/DataType";

class DataFactory{
    constructor (){

    }
    getBaseData(){
        return {
            modelId:Utils.GUID(),
            trackIndex:0,//层级 
            trackId:"",//轨道id
            isSelected : false,
            groupId:"",//成组的ID，为空表示自成一派
            sceneId:"",//一个场景组成员
            carryData: null, //携带原始资源数据
            isCanDelete: false,
            isCanOption: true,//允许操作
            uploadStatus:'',//上传状态  uploading success error
            isEndMark:false,//是否含结束标志
            canRecord: true, //是否能记录历史数据（主要用于动态字幕，需要在渲染完成后记录）
        }
    }
    /** 获取一个视频数据 */
    getVideoData(obj){
        let data = {
            dataType:DataType.CLIP_VIDEO,
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            inPoint: obj.inPoint || 0,
            outPoint: obj.outPoint || obj.duration,
            duration: obj.duration,
            keyFrameUrl: obj.keyFrameUrl,
            previewUrl: obj.previewUrl,
            imageWidth: obj.imageWidth || 0,
            imageHeight: obj.imageHeight || 0,
            title:obj.title || "",
            path: obj.path || '',
            dataType2: obj.dataType2 || 'default',
            order:obj.order || 0,
            sameId:obj.sameId || '',
            fillPos:{
                x:0,
                y:0,
                w:1,
                h:1,
            },//默认填充区域
            fillScale:{
                width: 1,
                height: 1,
            },
            isMute: !!obj.isMute,
            //视频缩放方式
            scaleType: 0,//0 填充 1以高为基准 2 以宽为基准
            // 转场特技
            /**
             * {
             *  beJoinedId:"",//与转场相关的片断id
             *  duration:0,//转场入时间
             *  type:0,//转场类型
             *  maxTime:0 //最大可设置的转场时间
             * }
             */
            effectTrans : null,
            staticFrame:null,
            //新版特技 之前的会被废弃
            effects: [],
            tlData: obj.tlData,
            carryData: obj.carryData,
            // formatDetails: obj.formatDetails || null,
        }
        return Object.assign(this.getBaseData(),data);
    }

    /** 获取一个视频图片数据 */
    getVideoImageData(obj){
        let data = {
            dataType:DataType.CLIP_VIDEO_IMAGE,
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            inPoint: obj.inPoint || 0,
            outPoint: obj.outPoint || obj.duration,
            duration: obj.duration,
            keyFrameUrl: obj.keyFrameUrl || obj.previewUrl,
            previewUrl: obj.previewUrl,
            imageWidth: obj.imageWidth || 0,
            imageHeight: obj.imageHeight || 0,
            title:obj.title || "",
            path: obj.path || '',
            dataType2: obj.dataType2 || 'default',
            sameId:obj.sameId || '',
            fillPos:{
                x:0,
                y:0,
                w:1,
                h:1,
            },//默认填充区域
            fillScale:{
                width: 1,
                height: 1,
            },
            isMute: true,
            //视频缩放方式
            scaleType: 0,//0 填充 1以高为基准 2 以宽为基准
            // 转场特技
            /**
             * {
             *  beJoinedId:"",//与转场相关的片断id
             *  duration:0,//转场入时间
             *  type:0,//转场类型
             *  maxTime:0 //最大可设置的转场时间
             * }
             */
            effectTrans : null,
            staticFrame:null,
            //新版特技 之前的会被废弃
            effects: [],
            tlData: obj.tlData,
            carryData: obj.carryData,
        }

        return Object.assign(this.getBaseData(),data);
    }
    /**
     * 获得一个音频数据
     */
    getAudioData(obj){
        let data = {
            dataType: DataType.CLIP_AUDIO,
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            inPoint: obj.inPoint || 0,
            outPoint: obj.outPoint || obj.duration,
            duration: obj.duration,
            previewUrl: obj.previewUrl,
            imageWidth: obj.imageWidth || 0,
            imageHeight: obj.imageHeight || 0,
            title:obj.title || "",
            path: obj.path || '',
            dataType2: obj.dataType2 || 'default',
            // 音量
            volume : 1,  
            isMute: !!obj.isMute,
            effect:{
                fadeIn:0,
                fadeOut:0,
            },
            // 增益
            gain: 0,//后端取值为-99~+10，前端暂时取值为-1~+1 0为中间点(0-1在播放时扩大10倍)
            // 转场入特技
            /**
             * {
             *  beJoinedId:"",//与转场相关的片断id
             *  duration:0,//转场入时间
             *  type:0,//转场类型
             *  maxTime:0 //最大可设置的转场时间
             * }
             */
            effectTrans : null,
            tlData: obj.tlData,
            carryData: obj.carryData,
        }
        return Object.assign(this.getBaseData(),data);
    }
    /**
     * 获取图片数据  x,y,width,height 全为相对于渲染区的百分比
     * @param {*} obj 
     */
    getImageData(obj){
        let data = {
            dataType : DataType.CLIP_CG_IMAGE,
            title : obj.title || "",
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            keyFrameUrl: obj.keyFrameUrl || obj.previewUrl,
            previewUrl: obj.previewUrl,
            imageWidth: obj.imageWidth || 0,
            imageHeight: obj.imageHeight || 0,
            path: obj.path || '',
            dataType2: obj.dataType2 || 'default',
            pos:Object.assign({
                x: 0,
                y: 0,
                width: 1,
                height: 1
            },obj.pos),
            tlData: obj.tlData,
            carryData: obj.carryData
            
        }
        return Object.assign(this.getBaseData(),data);
    }
    /**
     * 获取文字数据  x,y,width,height 全为相对于渲染区的百分比
     * @param {*} obj 
     */
    getTextData(obj){
        let data = {
            dataType : DataType.CLIP_CG_TEXT,
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            dataType2: obj.dataType2 || 'default',
            pos:Object.assign({
                x: 0,
                y: 0,
                width: 1,
                height: 1
            },obj.pos),
            textType: Object.assign({
                groupId:"",
                groupType:"title"
            },obj.textType),
            // 文字内容
            content : obj.content || "默认字幕",
            styles:Object.assign({
                // 透明度
                alpha : obj.alpha || 1,   //透明度 0- 1
                // 背景透明度
                bgAlpha : obj.bgAlpha || 0,   //透明度 0- 1
                // 文字颜色
                color : obj.color || "#ffffff",
                // 背景颜色
                bgColor : obj.bgColor || "#CCCCCC",
                // 字体
                fontFamily : obj.fontFamily || "Microsoft YaHei",
                // 字体大小
                fontSize : obj.fontSize || 8,
                // 是否需要阴影
                isShadow: obj.isShadow || false,
                // 阴影颜色
                shadowColor: obj.shadowColor || '#CCCCCC',
                // 阴影透明度
                shadowAlpha: obj.shadowAlpha || 1,
                // 淡入时间
                fadeInSec: obj.fadeInSec || 0,
                // 淡出时间
                fadeOutSec: obj.fadeOutSec || 0,
                // 是否加粗
                isBold : obj.isBold || false,
                // 是否斜体
                isItalic : obj.isItalic || false,
                // 是否下划线
                isUnderline : obj.isUnderline || false,
                // 是否换行
                isWarp : obj.isWarp || true,
                // 文本对齐方式
                textAlign : obj.textAlign || "center",  //left center right
                // 布局位置
                position: 'BOTTOM_CENTER', //TOP_CENTER CENTER_CENTER 等相对画面位置
            },obj.styles),
            tlData: obj.tlData,
            carryData: obj.carryData
            
        }

        return Object.assign(this.getBaseData(),data);
    }
    /** 获得动态字幕模板数据 */
    getTextDynamicData(obj){
        let data = {
            dataType: DataType.CLIP_CG_TEXT_DYNAMIC,
            title : obj.title || "",
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            dataType2: obj.dataType2 || 'default',
            keyFrameUrl: obj.keyFrameUrl,
            sourceUrl: obj.sourceUrl || '',
            previewUrl: obj.sourceUrl || '',
            //位置信息
            position: Object.assign({
                x:0,
                y:0,
                width:1,
                height:1
            },obj.position),
            needFlushRender: false,
            needLoadFontFamilys:[],
            config: obj.config || null,
            duration: obj.duration || 0,
            inAnimDur: 0,
            outAnimDur: 0,
            enabledInOut: true,
            width: obj.width || 0,
            height: obj.height || 0,
            contents: obj.contents || [],
            path: obj.path || '',
            tlData: obj.tlData,
            carryData: obj.carryData
        }

        return Object.assign(this.getBaseData(),data);
    }
    /**
     * 获取tga数据  x,y,width,height 全为相对于渲染区的百分比
     * @param {*} obj 
     */
    getTgaData(obj){
        let data = {
            dataType : DataType.CLIP_CG_TGA,
            title : obj.title || "",
            trackIn: obj.trackIn || 0,
            trackOut: obj.trackOut || 0,
            keyFrameUrl: obj.keyFrameUrl || obj.previewUrl,
            previewUrl: obj.previewUrl,
            imageWidth: obj.imageWidth || 0,
            imageHeight: obj.imageHeight || 0,
            path: obj.path || '',
            dataType2: obj.dataType2 || 'default',
            // 总帧数
            totalFrames : obj.totalFrames || 0,
            // 当前帧数
            currentFrame : obj.currentFrame || 0,
            // 开始帧数
            beginFrame : 0,
            // 结束帧数
            endFrame : obj.totalFrames || 0,
            // 帧率
            framerate : 25,
            // 每帧数据例表
            frameDatas : [],
            // 每帧tgaurl
            frameUrls : [],
            pos:Object.assign({
                x: 0,
                y: 0,
                width: 1,
                height: 1
            },obj.pos),
            tlData: obj.tlData,
            carryData: obj.carryData
            
        }
        return Object.assign(this.getBaseData(),data);
    }
    /** 获得一个图片GIF数据 */
    getImageDataGIF(obj){
        let data = this.getImageData(obj);
        data.dataType = DataType.CLIP_CG_IMAGE_GIF;
        data.playCount = 1;//播放次数
        data.totalFrames = 0;//总帧数
        data.gifTotalTime = 0;//gif总时长
        data.isTemplate = !!obj.isTemplate;
        return data;
    }

    /** 获得一个图片Webp数据 */
    getImageDataWebp(obj){
        let data = this.getImageData(obj);
        data.dataType = DataType.CLIP_CG_IMAGE_WEBP;
        data.playCount = 1;//播放次数
        data.totalFrames = 0;//总帧数
        data.webpTotalTime = 0;//webp总时长
        data.isTemplate = !!obj.isTemplate;
        return data;
    }
}

export default new DataFactory;