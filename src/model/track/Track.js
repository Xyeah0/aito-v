/* 日期：2020/5/14
* 说明：Track 数据类
* 
*/
import Utils from "@/libs/Utils";
import DataType from "@/model/DataType";

import DataFactory from './DataFactory';
// import EffectFactory from './EffectFactory';

const App = {};
class Track{
    constructor(app,seq,obj){
        App.CONTROL = app.CONTROL;
        App.SEQ = seq;

        this.modelId = Utils.GUID();

        this.rawTrackData = obj;
        this.dataType = DataType.TRACK_VIDEO;
        this.dataType2 = obj.TrackType2;
        this.isAddClip = !!obj.IsAddClip;
        /**
         * 层级号
         */
        this.trackIndex = 0;
        /**
         * 是否是主轨道
         */
        this.isMain = false;
        switch (obj.TrackType){
            case 'V':
                this.dataType = DataType.TRACK_VIDEO;
                break;
            case 'A':
                this.dataType = DataType.TRACK_AUDIO;
                break;
            case "CG":
                this.dataType = DataType.TRACK_CG;
                break;
        }

        this.clips = [];
    }
    /**
     * 返回当前静音状态
     */
    isMuted(){
        return this.volumeValue === 0;
    }
    /**
     * 设置静音状态 1静音 0 不静音
     */
    setMute(v){
        if(v === 1){
            this.recordVolume = this.volumeValue;
            this.volumeValue = 0;
        }else if(v === 0){
            this.volumeValue = this.recordVolume || 0;
        }
    }

    // /**
    //  * 初始化模板片断
    //  */
    // initTLClip(tlClips){
    //     for(let i=0;i<tlClips.length;i++){
    //         let _clip = tlClips[i];
    //         let clip = null;
    //         if(_clip.ClipType === 'Text'){
    //             clip = DataFactory.getTextData({
    //                 carryData: _clip,
    //                 content: _clip.Content,
    //                 trackIn: _clip.TrackIn,
    //                 trackOut: _clip.TrackOut,
    //                 pos: _clip.Pos,
    //                 styles: {
    //                     fontFamily: _clip.Style.FontFamily,
    //                 }
    //             })
    //         }else if(_clip.ClipType === 'Video'){
    //             let clip = DataFactory.getVideoData({
    //                 carryData: _clip,
    //                 trackIn: _clip.TrackIn,
    //                 trackOut: _clip.TrackOut,
    //                 inPoint: _clip.TrimIn,
    //                 outPoint : _clip.TrimOut,
    //                 duration: _clip.Duration,
    //                 previewUrl: _clip.PreviewUrl,
    //                 imageWidth: _clip.Width,
    //                 imageHeight: _clip.Height,
    //             })
    //             this.insertClip(clip);
    //         }else if(_clip.ClipType === 'Audio'){
    //             let clip = DataFactory.getAudioData({
    //                 carryData: _clip,
    //                 trackIn: _clip.TrackIn,
    //                 trackOut: _clip.TrackOut,
    //                 inPoint: _clip.TrimIn || 0,
    //                 outPoint : _clip.TrimOut || 0,
    //                 duration: _clip.Duration,
    //                 previewUrl: _clip.PreviewUrl,
    //                 imageWidth: _clip.Width,
    //                 imageHeight: _clip.Height,
    //             })
    //             this.insertClip(clip);
    //         }
    //         if(clip){
    //             this.insertClip(clip);
    //         }


    //         // "ClipId":"CAC98029773841db87B391C5037A911F",
    //         //         "ClipType":"Video",
    //         //         "Duration":60000,
    //         //         "TrimIn":0,
    //         //         "TrimOut":60000,
    //         //         "TrackIn":0,
    //         //         "TrackOut":60000,
	// 		// 		"Width":1920,
	// 		// 		"Height":1080,
	// 		// 		"PreviewUrl":"http://172.29.3.98:8000/media/video/test169.mp4",
    //         //         "HighPath":"X:/xxxx/xx.mp4",
    //         //         "ImageMode":"3",
    //     }
    // }

    /**
     * 嵌入片段
     */
    insertClip(clip,isSort=false) {
        clip.trackId = this.modelId;
        clip.trackIndex = this.trackIndex;
        this.clips.push(clip);
        App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,clip);
        if(isSort){
            this.sortClipUseST(this.clips);
        }
        
    }

    /**
     * 嵌入多个片段
     */
    insertClips(clips,isSort=false) {
        for(let i=0;i<clips.length;i++){
            let clip = clips[i];
            clip.trackId = this.modelId;
            clip.trackIndex = this.trackIndex;
            this.clips.push(clip);
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,clip);
        }
        if(isSort){
            this.sortClipUseST(this.clips);
        }
        // if(this.dataType2 == 'Main'){
        //     this.alginLeft();
        // }
    }

    /**
     * 改变某个切段
     */
    changeClip(_clip){
        if(_clip.trackId !== this.modelId) return;
        let willClip = null;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.modelId === _clip.modelId){
                this.clips[i] = clip;
                willClip = clip;
                break;
            }
        }
        if(willClip){
            if(willClip.effects){
                let dur = willClip.duration || willClip.trackOut - willClip.trackOut;
                if(willClip.effects){
                    for(let eff of willClip.effects){
                        for(let keyParam of eff.keyFrameParameters){
                            // keyParam.pos = keyParam.posScale*dur;
                            keyParam.pos = keyParam.posScale*dur+(willClip.inPoint||0);

                        }
                    }
                }
            }
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,willClip);
            // if(this.dataType2 == 'Main'){
            //     this.alginLeft();
            // }
        }
    }

    /** 
     * 删除某个切段 
     * */
    deleteClip(_clip){
        if(_clip.trackId !== this.modelId) return;
        let willClip = null;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.modelId === _clip.modelId){
                this.clips.splice(i,1);
                willClip = clip;
                break;
            }
        }
        if(willClip){
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,willClip);
            // if(this.dataType2 == 'Main'){
            //     this.alginLeft();
            // }
        }
    }
    /** 移动或交换位置 */
    exchangeClip(sourceId,targetId){
        let msg = {
            cmd:'success',
            desc:'交换成功'
        }
        this.sortClipUseST(this.clips);
        let trackIn = 0,sourceClip = null,targetClip = null;
        let sourceI = -1,targetI = -1;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.dataType2 == 'Title_Video'){
                trackIn = clip.trackOut;
            }
            if(clip.modelId == sourceId){
                sourceClip = clip;
                sourceI = i;
            }
            if(clip.modelId == targetId){
                targetClip = clip;
                targetI = i;
            }
        }
        if(!sourceClip || !targetClip) {
            msg.cmd = 'error';
            msg.desc = '未找到目标素材';
            return msg;
        } 
        if(sourceI == targetI){
            msg.cmd = 'invalid';
            msg.desc = '无效拖动';
            return msg;
        }
        if(sourceI > targetI){
            this.clips.splice(sourceI,1);
            this.clips.splice(targetI,0,sourceClip);
        }else if(sourceI < targetI){
            this.clips.splice(sourceI,1);
            this.clips.splice(targetI+1,0,sourceClip);
        }
        let order = 0;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.dataType2 == 'Main'){
                clip.trackIn = trackIn;
                clip.trackOut = trackIn + this.getClipDuration(clip);
                clip.order = order++;
                trackIn = clip.trackOut;
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
            }
        }
        return msg;
    }

    /**
     * 将当前数据与现有数据作对比 多出的部分 删除，少于的部分 添加，其余全部更新
     * @param {片断数组} clips 
     */
    Contrastclips (clips) {
        //第一步，先向记录数据查询是否有本身，如果没查到，则删除
        for(let i=0;i<this.clips.length;i++){
            let d1 = this.clips[i];
            let isFind = false;
            for(let j=0;j<clips.length;j++){
                let d2 = clips[j];
                if(d1.modelId === d2.modelId){
                    isFind = true;
                    break;
                }
            }
            if(!isFind){
                this.clips.splice(i,1);
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,d1);
                i--;
            }
        }
    }
    Contrastclips2 (clips){
        //第二步，向本身数据查找是否有记录的数据，如果有，则更新，如果没有，则新建
        for(let j=0;j<clips.length;j++){
            let d2 = clips[j];
            let isFind = false;
            for(let i=0;i<this.clips.length;i++){
                let d1 = this.clips[i];
                if(d1.modelId === d2.modelId){
                    isFind = true;
                    // this.clips[i] = d2;
                    // App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,d2);
                    //不改变原有对象引用
                    Object.assign(d1,d2);
                    App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,d1);
                    break;
                }
            }
            if(!isFind){
                this.insertClip(d2);
            }
        }
    }
    /** 清空所有切段 */
    clear(){
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
        }
        this.clips = [];
    }

    /**
     * 替换素材
     */
    // replaceClip(_clip,newData,fixedTime=false){
    //     this.sortClipUseST(this.clips);
    //     if(_clip.tlData){
    //         if(newData.type == 'video'){
    //             // (_clip.tlData.TrackOut - _clip.tlData.TrackIn)
    //             if(parseInt(newData.duration) < 3000){
    //                 return 'fail-duration';
    //             }
    //         }
    //     }
    //     let tempTrackIn = _clip.trackIn,tempTrackOut = _clip.trackOut;
    //     if(fixedTime && _clip.tlData){
    //         tempTrackOut = tempTrackIn+(_clip.tlData.TrackOut-_clip.tlData.TrackIn);
    //     }
        
    //     // let newData = newDatas[i];
    //     let newClip;
    //     if(newData.type == 'image'){
    //         if(this.dataType === DataType.TRACK_CG){
    //             newClip = DataFactory.getImageData({
    //                 carryData: newData,
    //                 tlData: _clip.tlData,
    //                 title:newData.title,
    //                 dataType2: _clip.dataType2,
    //                 trackIn: tempTrackIn,
    //                 trackOut: tempTrackOut,
    //                 keyFrameUrl: newData.cover || newData.preUrl,
    //                 previewUrl: newData.preUrl,
    //                 path: newData.path,
    //                 imageWidth: parseInt(newData.width),
    //                 imageHeight: parseInt(newData.height),
    //                 pos: _clip.pos || _clip.tlData.Pos
    //             })
    //         }else if(this.dataType === DataType.TRACK_VIDEO){
    //             newClip = DataFactory.getVideoImageData({
    //                 carryData: newData,
    //                 tlData: _clip.tlData,
    //                 title:newData.title,
    //                 dataType2: _clip.dataType2,
    //                 trackIn: tempTrackIn,
    //                 trackOut: tempTrackOut,
    //                 keyFrameUrl: newData.cover || newData.preUrl,
    //                 previewUrl: newData.preUrl,
    //                 path: newData.path,
    //                 imageWidth: parseInt(newData.width),
    //                 imageHeight: parseInt(newData.height),
    //                 sameId: _clip.sameId
    //             });
    //             App.SEQ.EffectAgent.copyClipEffect(newClip,_clip);
    //             App.SEQ.EffectAgent.initFillScale(newClip);
    //             // this.replaceOfVideoOther(newClip,_clip);
    //             // if(newClip.imageWidth/newClip.imageHeight <= App.SEQ.preset.width/App.SEQ.preset.height){
    //             //     newClip.scaleType = 1;
    //             // }else{
    //             //     newClip.scaleType = 2;
    //             // }
    //             // if(newClip.dataType2 == 'VideoBg'){
    //             //     newClip.scaleType = 0;
    //             // }
    //         }
    //     }else if(newData.type == 'video'){
    //         newClip = DataFactory.getVideoData({
    //             carryData: newData,
    //             tlData: _clip.tlData,
    //             title:newData.title,
    //             dataType2: _clip.dataType2,
    //             trackIn: tempTrackIn,
    //             trackOut: tempTrackIn+parseInt(newData.duration),
    //             inPoint: 0,
    //             outPoint : parseInt(newData.duration),
    //             duration: parseInt(newData.duration),
    //             keyFrameUrl: newData.cover,
    //             previewUrl: newData.preUrl,
    //             path: newData.path,
    //             imageWidth: parseInt(newData.width),
    //             imageHeight: parseInt(newData.height),
    //             isMute: _clip.dataType2=='Main'?_clip.isMute:true,
    //             sameId: _clip.sameId
    //         })
    //         if(fixedTime){
    //             newClip.trackOut = tempTrackOut;
    //             newClip.outPoint = tempTrackOut-tempTrackIn;
    //         }
    //         App.SEQ.EffectAgent.copyClipEffect(newClip,_clip);
    //         App.SEQ.EffectAgent.initFillScale(newClip);
    //         // this.replaceOfVideoOther(newClip,_clip);
    //         // console.log('newClip.scaleType',newClip.scaleType)
    //         // if(newClip.imageWidth/newClip.imageHeight <= App.SEQ.preset.width/App.SEQ.preset.height){
    //         //     newClip.scaleType = 1;
    //         // }else{
    //         //     newClip.scaleType = 2;
    //         // }
    //         // if(newClip.dataType2 == 'VideoBg'){
    //         //     newClip.scaleType = 0;
    //         // }
    //     }else if(newData.type == 'audio'){
    //         newClip = DataFactory.getAudioData({
    //             carryData: newData,
    //             tlData: _clip.tlData,
    //             title:newData.title,
    //             dataType2: _clip.dataType2,
    //             trackIn: tempTrackIn,
    //             trackOut: tempTrackOut,
    //             inPoint: 0,
    //             outPoint : parseInt(newData.duration),
    //             duration: parseInt(newData.duration),
    //             previewUrl: newData.preUrl,
    //             path: newData.path,
    //             isMute: _clip.isMute,
    //         })
    //         newClip.effect = _clip.effect;
    //         if(fixedTime){
    //             newClip.trackOut = tempTrackOut;
    //             newClip.outPoint = tempTrackOut-tempTrackIn;
    //         }
    //     }

    //     if(newClip){
    //         // newClip.trackId = this.modelId;
    //         // newClip.trackIndex = this.trackIndex;
    //         // newClip.dataType2 = this.dataType2;
    //         // newClips.push(newClip);
    //         let dur = newClip.duration || newClip.trackOut - newClip.trackIn;
    //         if(newClip.effects){
    //             for(let eff of newClip.effects){
    //                 for(let keyParam of eff.keyFrameParameters){
    //                     keyParam.pos = keyParam.posScale*dur;
    //                 }
    //             }
    //         }
    //         this.deleteClip(_clip);
    //         this.insertClip(newClip);
    //         // this.clips.push(clip);
    //         // App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,clip);

    //         return 'success';
    //     }

    //     return 'fail-none';
    //     // if(newClip){
    //     //     newClip.trackId = this.modelId;
    //     //     newClip.trackIndex = this.trackIndex;
    //     //     newClip.dataType2 = this.dataType2;
    //     //     newClips.push(newClip);
    //     //     let len = tempTrackOut-tempTrackIn;
    //     //     tempTrackIn = newClip.trackOut;
    //     //     tempTrackOut = tempTrackIn + len;
    //     // }
        
    //     // let find = false;
    //     // for(let i=0;i<this.clips.length;i++){
    //     //     let clip = this.clips[i];
    //     //     if(clip.modelId === _clip.modelId){
    //     //         find = true;
    //     //         continue;
    //     //     }
    //     //     if(find){
    //     //         let len = clip.trackOut -clip.trackIn;
    //     //         clip.trackIn = tempTrackIn;
    //     //         clip.trackOut = clip.trackIn + len;
    //     //         tempTrackIn = clip.trackOut;
    //     //     }
    //     // }
    //     // // newClips.reverse();
    //     // for(let i=0;i<this.clips.length;i++){
    //     //     let clip = this.clips[i];
    //     //     if(clip.modelId === _clip.modelId){
    //     //         this.clips.splice(i,1,...newClips);
    //     //         App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
    //     //         break;
    //     //     }
    //     // }
    //     // for(let i=0;i<newClips.length;i++){
    //     //     App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_ADD,newClips[i]);
    //     // }

    //     // if(this.dataType2 == 'Main'){
    //     //     this.alginLeft();
    //     // }
    // }

    /** 获得clip及之后的N个素材 */
    getClipAndAfters(_clip,num=1){
        this.sortClipUseST(this.clips);
        const arr = [];
        let isStart = false;
        let len = 0;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.modelId === _clip.modelId){
                isStart = true;
            }
            if(isStart){
                arr.push(clip);
                len++;
                if(len >= num){
                    break;
                }
            }
        }
        return arr;
    }
    /** 删除时间点之后的所有素材 */
    deleteOfTime(time){
        const newarr = [],willDels=[];
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.trackIn >= time){
                willDels.push(clip);
            }else{
                newarr.push(clip);
            }
        }
        this.clips = newarr;
        for(let i=0;i<willDels.length;i++){
            let clip = willDels[i];
            App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
        }
        return willDels;
    }
    /** video轨其它处理 私有方法 */
    // replaceOfVideoOther(newClip,_clip){
    //     App.SEQ.EffectAgent.copyClipEffect(newClip,_clip);
    //     App.SEQ.EffectAgent.initFillScale(newClip);
    //     if(newClip.sameId != ''){
    //         for(let i=0;i<App.SEQ.videoTracks.length;i++){
    //             let track = App.SEQ.videoTracks[i];
    //             for(let j=0;j<track.clips.length;j++){
    //                 let clip = track.clips[j];
    //                 if(clip.sameId === newClip.sameId){
    //                     clip.keyFrameUrl = newClip.keyFrameUrl;
    //                     clip.previewUrl = newClip.previewUrl;
    //                     clip.title = newClip.title;
    //                     clip.trackIn = newClip.trackOut;
    //                     clip.inPoint = newClip.inPoint;
    //                     clip.outPoint = newClip.outPoint;
    //                     clip.duration = newClip.duration;
    //                     clip.path = newClip.path;
    //                     clip.imageWidth = newClip.imageWidth;
    //                     clip.imageHeight = newClip.imageHeight;
    //                     clip.path = newClip.path;
    //                     App.SEQ.EffectAgent.initFillScale(clip);
    //                 }
    //             }
    //         }
    //     }
    // }

    /**
     * 对齐轨道
     */
    alginLeft(){
        this.sortClipUseST(this.clips);
        let st = 0;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            let len = clip.trackOut - clip.trackIn;
            let recordIn = clip.trackIn,recordOut = clip.trackOut;
            clip.trackIn = st;
            clip.trackOut = clip.trackIn+len;
            let effectTrans = null;
            if(i-1>=0){
                effectTrans = this.clips[i-1].effectTrans;
            }
            if(effectTrans){
                clip.trackIn -= effectTrans.duration;
                clip.trackOut -= effectTrans.duration;
            }
            if(recordIn != clip.trackIn || recordOut != clip.trackOut){
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_CHANGE,clip);
            }
            st = clip.trackOut;
        }
    }

    /**
     * 修正有重合的地方
     */
    // correctCover(){
    //     this.sortClipUseST(this.clips);
    //     // let isCover = false;
    //     let comparedata = this.clips[0];
    //     // let changeOfGroups = [];
    //     for(let i=1;i<this.clips.length;i++){
    //         let tempdata = this.clips[i];
    //         if(tempdata.trackIn < comparedata.trackOut){
    //             let len = tempdata.trackOut - tempdata.trackIn;
    //             tempdata.trackIn = comparedata.trackOut;
    //             tempdata.trackOut = tempdata.trackIn+len;
    //             if(comparedata.effectTrans){
    //                 tempdata.trackIn -= comparedata.effectTrans.duration;
    //                 tempdata.trackOut -= comparedata.effectTrans.duration;
    //             }
    //             App.App.CONTROL.CORE.updateSequence({
    //                 cmd: App.App.CONTROL.CORE.SeqEmumType.CLIP_CHANGE,
    //                 needStop: true,
    //                 data: tempdata
    //             });
    //         }
    //         comparedata = tempdata;
    //     }

    //     // return changeOfGroups;
    // }
    /** 重置轨道时间（不包含片头片尾） */
    resetClipTrackInOut(mainContentDur,trackIn=0){
        switch(this.dataType2){
            case 'Main':
                for(let i=0;i<this.clips.length;i++){
                    let clip = this.clips[i];
                    if(clip.dataType2 == 'Main'){
                        let dur = clip.trackOut - clip.trackIn;
                        clip.trackIn = trackIn;
                        clip.trackOut = trackIn + dur;
                        trackIn = clip.trackOut;
                    }
                }
                break;
            case 'Water':
                for(let i=0;i<this.clips.length;i++){
                    let clip = this.clips[i];
                    clip.trackIn = trackIn;
                    clip.trackOut = trackIn + mainContentDur;
                }
                break;
        }
    }
    /**
     * 删除片头片尾
     */
    removeTitleTail(){
        for(let i=this.clips.length-1;i>=0;i--){
            let clip = this.clips[i];
            if(clip.dataType2 == 'Title_Video' || clip.dataType2 == 'Tail_Video'){
                this.clips.splice(i,1);
                App.CONTROL.CORE.$emit(App.CONTROL.CORE.CLIP_DELETE,clip);
            }
        }
        this.alginLeft();
    }
    /**
     * 通过id获得切段
     */
    getClipUseId(modelId){
        for(let i=0;i<this.clips.length;i++){
            let tempdata = this.clips[i];
            if(tempdata.modelId === modelId){
                return tempdata;
            }
        }
        return null;
    }
    /**
     * 通过开始时间和结束时间获得切段
     */
    getClipUseTime(st,et){
        for(let i=0;i<this.clips.length;i++){
            let tempdata = this.clips[i];
            if(tempdata.trackIn === st && tempdata.trackOut === et){
                return tempdata;
            }
        }
        return null;
    }

    /**
     * 通过某时间范围内第一个切段
     */
    getClipRangeTime(st,et){
        for(let i=0;i<this.clips.length;i++){
            let tempdata = this.clips[i];
            if(st >= tempdata.trackIn && et < tempdata.trackOut){
                return tempdata;
            }
        }
        return null;
    }
    /** 获得轨道中内容所占的长度 */
    getContentDuration(){
        let contentIn = -1,contentOut = -1;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(contentIn == -1 || contentOut == -1){
                contentIn = clip.trackIn;
                contentOut = clip.trackOut;
            }else{
                contentIn = Math.min(clip.trackIn,contentIn);
                contentOut = Math.max(clip.trackOut,contentOut);
            }
        }
        if(contentIn == -1 || contentOut == -1){
            return 0;
        }
        return contentOut - contentIn;
    }
    /** 获得一个clip显示时长 */
    getClipDuration(clip){
        return clip.trackOut - clip.trackIn;
    }
    /**
     * 获得 Main内容长度
     */
    getContentofMain() {
        let leng = 0;
        for(let i=0;i<this.clips.length;i++){
            if(this.clips[i].dataType2=='Main'){
                leng += this.getClipDuration(this.clips[i]);
            }
        }
        return leng;
    }
    /**
     * 获得最尾部的时间 Main
     */
    getEndTimeofMain() {
        let time = 0;
        for(let i=0;i<this.clips.length;i++){
            if(this.clips[i].dataType2=='Main' && this.clips[i].trackOut > time) time = this.clips[i].trackOut;
        }
        return time;
    }
    /**
     * 获得最尾部的时间
     */
    getEndTime() {
        let time = 0;
        for(let i=0;i<this.clips.length;i++){
            if(this.clips[i].trackOut > time) time = this.clips[i].trackOut;
        }
        return time;
    }
    /** 获得切段的前面一个切段的转场 */
    getClipBeforeTrans(_clip){
        let trans = null;
        for(let i=0;i<this.clips.length;i++){
            let clip = this.clips[i];
            if(clip.modelId === _clip.modelId){
                if(i>0){
                    trans = this.clips[i-1].effectTrans;
                }
                break;
            }
        }
        return trans;
    }
    /**
     * 以开时时间对切段进行排序
     */
    sortClipUseST(clipArr){
        function sortTrackIn(obj1,obj2){
            return obj1.trackIn > obj2.trackIn?1:-1;
        }
        return clipArr.sort(sortTrackIn);
    }
   
}
export default Track;