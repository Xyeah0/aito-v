
import DataType from "../DataType.js";
import Utils from "@/libs/Utils";
class EffectFactory{
    constructor(){

    }
    /** 模糊特技 */
    getBlurData(_param,duration=0){
        let defaultkeyParams = {
            x:0.03,
            y:0.03,
            width:0.18,
            height:0.16,
            blurType:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_BLUR,
            dataType2:'',
            id:Utils.GUID(),
            name:'模糊',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }
    /** 2D特技 */
    get2DData(_param,duration=0){
        let defaultkeyParams = {
            left:0,
            right:1,
            top:0,
            bottom:1,
            scaleX:1,
            scaleY:1,
            offsetX:0,
            offsetY:0,
            rotate:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_2D,
            dataType2:'',//crop //inside_pos
            id:Utils.GUID(),
            name:'2D',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }
    /** 获得淡入特技 */
    getFadeInData(_param,duration=0){
        let defaultkeyParams = {
            duration: 1000,
            alpha:1
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_FADEIN,
            dataType2:'',
            id:Utils.GUID(),
            name:'淡入',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 获得淡出特技 */
    getFadeOutData(_param,duration=0){
        let defaultkeyParams = {
            duration: 1000,
            alpha:1
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_FADEOUT,
            dataType2:'',
            id:Utils.GUID(),
            name:'淡出',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 透明度特技 */
    getAlphaData(_param,duration=0){
        // return {
        //     dataType:DataType.EFFECT_ALPHA,
        //     id:Utils.GUID(),
        //     name:'透明度',
        //     params:Object.assign({
        //         alpha:1
        //     },_param)
        // }

        let defaultkeyParams = {
            alpha:1
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_ALPHA,
            dataType2:'',
            id:Utils.GUID(),
            name:'透明度',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 颜色调整特技 */
    getColorData(_param,duration=0){
        // let defaultkeyParams = {
        //     contrast:1,
        //     saturation:1,
        //     brightness:1,
        //     hue:0
        // }
        // if(_param){
        //     Object.assign(defaultkeyParams,_param)
        // }
        // return {
        //     dataType:DataType.EFFECT_COLOR,
        //     id:Utils.GUID(),
        //     name:'调色',
        //     params:defaultkeyParams
        // }

        let defaultkeyParams = {
            contrast:1,
            saturation:1,
            brightness:1,
            hue:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_COLOR,
            dataType2:'',
            id:Utils.GUID(),
            name:'调色',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 获得静帧特技 暂未使用*/
    getStaticData(){
        return {
            dataType:DataType.EFFECT_STATIC,
            id:Utils.GUID(),
            name:'静帧',
            params:{
                staticTime: 0,//静帧的位置
                SingleType: 2,// 0 入停留 1 出停留 2 全部停留
            }
        }
    }


    /** 一级颜色调整特技 */
    getColorCorrectorData1(_param,duration=0){
        let defaultkeyParams = {
            paramInput:{
                fBlack:0,
                fWhite:100,
                fSat:100,
                fHue:0
            },
            paramHisto:{
                gRGB:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gR:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gG:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gB:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                }
            },
            paramCB:{
                gH:{
                    byR:179,
                    byG:179,
                    byB:179,
                    byReserved:179
                },
                gM:{
                    byR:128,
                    byG:128,
                    byB:128,
                    byReserved:128
                },
                gS:{
                    byR:77,
                    byG:77,
                    byB:77,
                    byReserved:77
                }
            },
            paramArea:{
                byHM:178,
                byMS:77
            }
        }
        // if(_param){
        //     Object.assign(defaultkeyParams.paramInput,_param.paramInput);
        //     Object.assign(defaultkeyParams.paramHisto.gRGB,_param.paramHisto.gRGB);
        //     Object.assign(defaultkeyParams.paramHisto.gR,_param.paramHisto.gR);
        //     Object.assign(defaultkeyParams.paramHisto.gG,_param.paramHisto.gG);
        //     Object.assign(defaultkeyParams.paramHisto.gB,_param.paramHisto.gB);
        // }
        // return {
        //     dataType:DataType.EFFECT_COLORCORRECTOR1,
        //     id:Utils.GUID(),
        //     name:'颜色校正',
        //     extend: '',//子类型 filter_0 filter_1...
        //     params: defaultkeyParams
        // }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: JSON.parse(JSON.stringify(defaultkeyParams))
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: JSON.parse(JSON.stringify(defaultkeyParams))
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    let tempParmas = JSON.parse(JSON.stringify(defaultkeyParams));
                    Object.assign(tempParmas.paramInput,item.keyParam.paramInput);
                    Object.assign(tempParmas.paramHisto.gRGB,item.keyParam.paramHisto.gRGB);
                    Object.assign(tempParmas.paramHisto.gR,item.keyParam.paramHisto.gR);
                    Object.assign(tempParmas.paramHisto.gG,item.keyParam.paramHisto.gG);
                    Object.assign(tempParmas.paramHisto.gB,item.keyParam.paramHisto.gB);
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: tempParmas
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter.paramInput,_param.paramInput);
                Object.assign(keyFrameParameters[0].parameter.paramHisto.gRGB,_param.paramHisto.gRGB);
                Object.assign(keyFrameParameters[0].parameter.paramHisto.gR,_param.paramHisto.gR);
                Object.assign(keyFrameParameters[0].parameter.paramHisto.gG,_param.paramHisto.gG);
                Object.assign(keyFrameParameters[0].parameter.paramHisto.gB,_param.paramHisto.gB);
                Object.assign(keyFrameParameters[1].parameter.paramInput,_param.paramInput);
                Object.assign(keyFrameParameters[1].parameter.paramHisto.gRGB,_param.paramHisto.gRGB);
                Object.assign(keyFrameParameters[1].parameter.paramHisto.gR,_param.paramHisto.gR);
                Object.assign(keyFrameParameters[1].parameter.paramHisto.gG,_param.paramHisto.gG);
                Object.assign(keyFrameParameters[1].parameter.paramHisto.gB,_param.paramHisto.gB);
            }
        }
        return {
            dataType:DataType.EFFECT_COLORCORRECTOR1,
            dataType2:'',
            id:Utils.GUID(),
            name:'颜色校正',
            extend: '',//子类型 filter_0 filter_1...
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 单色蒙板特技 */
    getMonochromeData(_param,duration=0){
        // let defaultkeyParams = {
        //     r:0,
        //     g:0,
        //     b:0
        // }
        // if(_param){
        //     Object.assign(defaultkeyParams,_param)
        // }
        // return {
        //     dataType:DataType.EFFECT_MONOCHROME,
        //     id:Utils.GUID(),
        //     name:'单色特技',
        //     params:defaultkeyParams
        // }

        let defaultkeyParams = {
            r:0,
            g:0,
            b:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_MONOCHROME,
            dataType2:'',
            id:Utils.GUID(),
            name:'单色特技',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 高斯模糊特技 */
    getGaussblurData(_param,duration=0){
        // let defaultkeyParams = {
        //     x:0,
        //     y:0,
        // }
        // if(_param){
        //     Object.assign(defaultkeyParams,_param)
        // }
        // return {
        //     dataType:DataType.EFFECT_GAUSSBLUR,
        //     id:Utils.GUID(),
        //     name:'高斯模糊',
        //     params:defaultkeyParams
        // }
        let defaultkeyParams = {
            x:0,
            y:0,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]
        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_GAUSSBLUR,
            dataType2:'',
            id:Utils.GUID(),
            name:'高斯模糊',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 边缘检测特技 */
    getFindEdgesData(_param,duration=0){
        let defaultkeyParams = {
            fProcess:0,
            fReverse:0
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_FINDEDGES,
            id:Utils.GUID(),
            name:'边缘检测',
            isMultiple:false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 镜像特技 */
    getMirrorData(_param,duration=0){
        let defaultkeyParams = {
            fCenterX: 0,
            fCenterY:-1,
            fAngle:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_MIRROR,
            id:Utils.GUID(),
            name:'镜像',
            isMultiple:false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 多分屏特技 */
    getMultiViewData(_param,duration=0){
        let defaultkeyParams = {
            fNumX:1,
            fNumY:1,
            fCenterX:0.5,
            fCenterY:0.5,
            fInSoftness:0,
            fOutSoftness:0,
            fClipL:0,
            fClipT:0,
            fClipR:0,
            fClipB:0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_MULTIVIEW,
            id:Utils.GUID(),
            name:'多分屏',
            isMultiple:false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 马赛克特技 */
    getMosaicData(_param,duration=0){
        let defaultkeyParams = {
            x:0.275,
            y:0.1,
            width:0.45,
            height:0.8,
            pixelWidth:0.2
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_MOSAIC,
            id:Utils.GUID(),
            name:'马赛克',
            isMultiple:true,
            isSelected: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 背景模糊特技 */
    getBgBlurData(_param,duration=0){
        let defaultkeyParams = {
            fBlur: 0.5,
            fRange: 0,
            fScale: 0
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType:DataType.EFFECT_BGBLUR,
            id:Utils.GUID(),
            name:'背景模糊',
            isMultiple:false,
            defaultkeyParams: defaultkeyParams,
            wholeParams:{
                isEnabledKey:false,//是否启用关键帧设置
            },
            keyFrameParameters:keyFrameParameters
        }
    }

    /** 卷动特技 */
    getScrollingData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SCROLLING,
            id: Utils.GUID(),
            name: '卷动',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 灵魂出窍特技 */
    getOutofbodysoulData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33,
            range: 1,
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OUTOFBODYSOUL,
            id: Utils.GUID(),
            name: '灵魂出窍',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 摇摆特技 */
    getSwingData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33,
            range: 1,
        }

        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SWING,
            id: Utils.GUID(),
            name: '摇摆',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 摇摆2特技 */
    getSwing2Data(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33,
            range: 1,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SWING2,
            id: Utils.GUID(),
            name: '摇摆2',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 粒子模糊特技 */
    getParticleblurData(_param,duration=0) {
        let defaultkeyParams = {
            center_x:0.5,
            center_y:0.5,
            seed:0.1,
            radius: 0.3,
            angle: 20,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_PARTICLEBLUR,
            id: Utils.GUID(),
            name: '粒子模糊',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 镜头模糊特技 */
    getLensblurData(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            range:0.6,
            blur:0.8,
            blur_duration:3,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_LENSBLUR,
            id: Utils.GUID(),
            name: '镜头模糊',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 屏幕律动特技 */
    getScreenrhythmData(_param,duration=0) {
        let defaultkeyParams = {
            intensity:0.6,
            size:0.33,
            speed:0.6,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SCREENRHYTHM,
            id: Utils.GUID(),
            name: '屏幕律动',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 波纹失真特技 */
    getRippledistortionData(_param,duration=0) {
        let defaultkeyParams = {
            distortion:0.3,
            intensity:0.3,
            number:0.3,
            speed:0.3,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_RIPPLEDISTORTION,
            id: Utils.GUID(),
            name: '波纹失真',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 心跳特技 */
    getHeartbeatData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_HEARTBEAT,
            id: Utils.GUID(),
            name: '心跳',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 霓虹摇摆特技 */
    getNenoswingData(_param,duration=0) {
        let defaultkeyParams = {
            speed:1,
            alpha:1,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_NENOSWING,
            id: Utils.GUID(),
            name: '霓虹摇摆',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 雨波纹特技 */
    getRainrippleData(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_RAINRIPPLE,
            id: Utils.GUID(),
            name: '雨波纹',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 雨幕特技 */
    getRaincurtainData(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_RAINCURTAIN,
            id: Utils.GUID(),
            name: '雨幕',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 幻影故障特技 */
    getPhantomfailureData(_param,duration=0) {
        let defaultkeyParams = {
            hue: 0.5,
            saturation: 0.5,
            opacity: 0.5
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_PHANTOMFAILURE,
            id: Utils.GUID(),
            name: '幻影故障',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 波纹色差特技 */
    getChromatismditheringData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33,
            horz: 0.6,
            intensity: 1.0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_CHROMATISMDITHERING,
            id: Utils.GUID(),
            name: '波纹色差',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 抖动特技 */
    getShakeData(_param,duration=0) {
        let defaultkeyParams = {
            speed: 0.33,
            horz: 0.75,
            vert: 0.75
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SHAKE,
            id: Utils.GUID(),
            name: '抖动',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 瞬间模糊特技 */
    getInstantaneousblurData(_param,duration=0) {
        let defaultkeyParams = {
            range: 1.0,
            blur: 1.0
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_INSTANTANEOUSBLUR,
            id: Utils.GUID(),
            name: '瞬间模糊',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 毛刺特技 */
    getSkinneedlingData(_param,duration=0) {
        let defaultkeyParams = {
            horz: 0.75,
            vert: 0.5,
            intensity: 1.0,
            speed: 0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SKINNEEDLING,
            id: Utils.GUID(),
            name: '毛刺',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 信号失真特技 */
    getSignaldistortionData(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SIGNALDISTORTION,
            id: Utils.GUID(),
            name: '信号失真',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 信号失真2特技 */
    getSignaldistortion2Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SIGNALDISTORTION2,
            id: Utils.GUID(),
            name: '信号失真2',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 信号失真3特技 */
    getSignaldistortion3Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            intensity: 0.5,
            horz: 0.67
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SIGNALDISTORTION3,
            id: Utils.GUID(),
            name: '信号失真3',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 雪花特技 */
    getSnowData(_param,duration=0) {
        let defaultkeyParams = {
            snowflake_amount:200,
            blizard_factor:0,
            falling_velocity:0.2,
            snow_alpha:0.2,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_SNOW,
            id: Utils.GUID(),
            name: '雪花',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 烟花1特技 */
    getFireworksData(_param,duration=0) {
        let defaultkeyParams = {
            duration:15,
            speed:5,
            radius:0.6,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_FIREWORKS,
            id: Utils.GUID(),
            name: '烟花1',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 烟花2特技 */
    getFireworks2Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_FIREWORKS2,
            id: Utils.GUID(),
            name: '烟花2',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 烟花3特技 */
    getFireworks3Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_FIREWORKS3,
            id: Utils.GUID(),
            name: '烟花3',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 烟花4特技 */
    getFireworks4Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_FIREWORKS4,
            id: Utils.GUID(),
            name: '烟花4',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 老电影1特技 */
    getOldmoviesData(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OLDMOVIES,
            id: Utils.GUID(),
            name: '老电影1',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 老电影2特技 */
    getOldmovies2Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            isFilm: true,
            isGray: true,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OLDMOVIES2,
            id: Utils.GUID(),
            name: '老电影2',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 老电影3特技 */
    getOldmovies3Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            isGray:true,
            isCurled: false,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OLDMOVIES3,
            id: Utils.GUID(),
            name: '老电影3',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 老电影4特技 */
    getOldmovies4Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            isGray:true,
            isCurled: false,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OLDMOVIES4,
            id: Utils.GUID(),
            name: '老电影4',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }

    /** 老电影5特技 */
    getOldmovies5Data(_param,duration=0) {
        let defaultkeyParams = {
            speed:0.33,
            isGray:true,
            isCurled: false,
        }
        let keyFrameParameters = [
            {
                pos:0,
                posScale:0,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            },
            {
                pos:duration,
                posScale:1,
                checked: true,
                id:Utils.GUID(),
                parameter: Object.assign({},defaultkeyParams)
            }
        ]

        if(_param){
            if(Array.isArray(_param) && _param.length){
                for(let i=0;i<_param.length;i++){
                    let item = _param[i];
                    keyFrameParameters[i] = {
                        pos: item.pos,
                        posScale: item.posScale,
                        checked: true,
                        id:Utils.GUID(),
                        parameter: Object.assign({...defaultkeyParams},item.keyParam)
                    }
                }
            }else{
                Object.assign(keyFrameParameters[0].parameter,_param);
                Object.assign(keyFrameParameters[1].parameter,_param);
            }
        }
        return {
            dataType: DataType.EFFECT_OLDMOVIES5,
            id: Utils.GUID(),
            name: '老电影5',
            isMultiple: false,
            defaultkeyParams: defaultkeyParams,
            wholeParams: {
                isEnabledKey: false,//是否启用关键帧设置
            },
            keyFrameParameters: keyFrameParameters
        }
    }
}
export default new EffectFactory;