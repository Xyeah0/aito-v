
import DataType from "../DataType.js";
import Utils from "@/libs/Utils";
class EffectTransFactory{
    constructor(){
        // beJoinedId:data.modelId,//与转场相关的片断id
        //                 duration:dura,
        //                 type:transType,//转场类型
        //                 maxTime:Math.min(maxTime1,maxTime2)
    }

    getBaseData(){
        return {
            id:Utils.GUID(),
            dataType: DataType.TRANS_FADE,
            clipId1:'',//转场出的clipid
            clipId2:'',//转场入的clipid
            name:'',
            englishName:'',
            duration:0,
            isSelected: false,
            maxTime:0,
            extend:0,
            parameters: null
        }
    }
    /** 获得淡入淡出转场 */
    getFadeInOut(){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_FADE;
        data.name = '淡入淡出';
        data.englishName = 'fadeInOut';
        data.parameters = {
            alpha: 1
        }
        return data;
    }
    /** 获得晖光转场 */
    getGlow(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_GLOW;
        data.name = '晖光';
        data.englishName = 'glow';
        data.parameters = Object.assign({
            light: 1
        },_params);
        return data;
    }
    /** 获得像素化（扩散擦拭）转场 */
    getDiffuseWipe(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DIFFUSEWIPE;
        data.name = '像素化划像';
        data.englishName = 'diffusewipe';
        data.parameters = Object.assign({
            pixelWidth: 10,
            borderWidth: 0.2,
            reverse:0
        },_params);
        return data;
    }
    /** 获得平移划像转场 */
    getTranslation(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_TRANSLATION;
        data.name = '平移划像';
        data.englishName = 'translation';
        data.parameters = Object.assign({
            direction:0
        },_params);
        return data;
    }
    /** 获得分割划像转场 */
    getSplit(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_SPLIT;
        data.name = '分割划像';
        data.englishName = 'split';
        data.parameters = Object.assign({
            direction:0
        },_params);
        return data;
    }
    /** 获得推动划像转场 */
    getPush(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_PUSH;
        data.name = '推动划像';
        data.englishName = 'push';
        data.parameters = Object.assign({
            direction:0
        },_params);
        return data;
    }
    /** 获得压缩2划像转场 */
    getCompress2(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_COMPRESS2;
        data.name = '压缩划像';
        data.englishName = 'compress2';
        data.parameters = Object.assign({
            direction:0
        },_params);
        return data;
    }
    /** 获得方形划像转场 */
    getRect(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_RECT;
        data.name = '方形划像';
        data.englishName = 'rect';
        data.parameters = Object.assign({
            direction:0
        },_params);
        return data;
    }
    /** 获得水波纹转场 */
    getRipple(_params){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_RIPPLE;
        data.name = '水波纹';
        data.englishName = 'ripple';
        data.parameters = Object.assign({
            fRipple_Width: 0.3,
            fNum_Wave: 5.0,
            fAmplitude: 0.5,
            fEllipticity:0.18,
            fCenterX: 0,
            fCenterY: -0.5,
            fDistortion:0.138
        },_params);
        return data;
    }
    /** 获得风中划痕转场 */
    getWind() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_WIND;
        data.name = '风中划痕';
        data.englishName = 'wind';
        data.parameters = {
            size: 0.2
        }
        return data;
    }
    /** 获得立体切换转场 */
    getSwap() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_SWAP;
        data.name = '立体切换';
        data.englishName = 'swap';
        data.parameters = {
            reflection: 0.4,
            perspective: 0.2,
            depth: 3
        }
        return data;
    }
    /** 获得色差变薄转场 */
    getSqueeze() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_SQUEEZE;
        data.name = '色差变薄';
        data.englishName = 'squeeze';
        data.parameters = {
            color_separation: 0.1
        }
        return data;
    }

    /** 获得方格渐显转场 */
    getSquareswire() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_SQUARESWIRE;
        data.name = '方格渐显';
        data.englishName = 'squareswire';
        data.parameters = {
            squares_x: 10,
            squares_y: 10,
            direction_x: 1,
            direction_y: -0.5,
            smoothness: 1.6
        }
        return data;
    }

    /** 获得旋转缩放转场 */
    getRotatescalefade() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_ROTATESCALEFADE;
        data.name = '旋转缩放';
        data.englishName = 'rotatescalefade';
        data.parameters = {
            center_x: 0.5,
            center_y: 0.5,
            rotations: 1,
            scale: 8,
            back_color:'#262626FF'
        }
        return data;
    }

    /** 获得随机方块转场 */
    getRandomsquares() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_RANDOMSQUARES;
        data.name = '随机方块';
        data.englishName = 'randomsquares';
        data.parameters = {
            size_x: 10,
            size_y: 10,
            smoothness: 0.5,
        }
        return data;
    }

    /** 获得极点函数转场 */
    getPolarfunction() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_POLARFUNCTION;
        data.name = '极点函数';
        data.englishName = 'polarfunction';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得像素渐显转场 */
    getPixelize() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_PIXELIZE;
        data.name = '像素渐显';
        data.englishName = 'pixelize';
        data.parameters = {
            squares_min_x:20,
            squares_min_y:20,
            steps:50
        }
        return data;
    }

    /** 获得多重混合转场 */
    getMultiplyblend() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_MULTIPLYBLEND;
        data.name = '多重混合';
        data.englishName = 'multiplyblend';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得心形放大转场 */
    getHeart() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_HEART;
        data.name = '心形放大';
        data.englishName = 'heart';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得飞鹰之眼转场 */
    getFlyeye() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_FLYEYE;
        data.name = '飞鹰之眼';
        data.englishName = 'flyeye';
        data.parameters = {
            size:0.04,
            zoom:50,
            color_separation:0.3
        }
        return data;
    }

    /** 获得灰度融合转场 */
    getFadegrayscale() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_FADEGRAYSCALE;
        data.name = '灰度融合';
        data.englishName = 'fadegrayscale';
        data.parameters = {
            intensity: 0.3,
        }
        return data;
    }

    /** 获得纯色过渡转场 */
    getFadecolor() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_FADECOLOR;
        data.name = '纯色过渡';
        data.englishName = 'fadecolor';
        data.parameters = {
            color: '#000000FF',
            color_phase:0.4,
        }
        return data;
    }

    /** 获得开门大吉转场 */
    getDoorway() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DOORWAY;
        data.name = '开门大吉';
        data.englishName = 'doorway';
        data.parameters = {
            reflection: 0.4,
            perspective: 0.4,
            depth: 3,
        }
        return data;
    }

    /** 获得定向擦除转场 */
    getDirectionalwipe() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DIRECTIONALWIPE;
        data.name = '定向擦除';
        data.englishName = 'directionalwipe';
        data.parameters = {
            direction_x: 1,
            direction_y: -1,
            smoothness: 0.5,
        }
        return data;
    }

    /** 获得立体旋转转场 */
    getCube() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CUBE;
        data.name = '立体旋转';
        data.englishName = 'cube';
        data.parameters = {
            persp: 0.7,
            un_zoom:0.3,
            reflection:0.4,
            floating:3.0
        }
        return data;
    }

    /** 获得交叉翘曲转场 */
    getCrosswarp() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CROSSWARP;
        data.name = '交叉翘曲';
        data.englishName = 'crosswarp';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得彩色相位转场 */
    getColorphase() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_COLORPHASE;
        data.name = '彩色相位';
        data.englishName = 'colorphase';
        data.parameters = {
            from_step: '#00336600',
            to_step:'#99CCFFFF',
        }
        return data;
    }

    /** 获得圆形围栏转场 */
    getCircleopen() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CIRCLEOPEN;
        data.name = '圆形围栏';
        data.englishName = 'circleopen';
        data.parameters = {
            smoothness: 0.3,
            opening:true,
        }
        return data;
    }

    /** 获得圆形放大转场 */
    getCircle() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CIRCLE;
        data.name = '圆形放大';
        data.englishName = 'circle';
        data.parameters = {
            center_x:0.5,
            center_y:0.5,
            back_color: '#202020FF'
        }
        return data;
    }

    /** 获得骄阳似火转场 */
    getBurn() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_BURN;
        data.name = '骄阳似火';
        data.englishName = 'burn';
        data.parameters = {
            color: '#E66633FF'
        }
        return data;
    }

    /** 获得时钟同步转场 */
    getAngular() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_ANGULAR;
        data.name = '时钟同步';
        data.englishName = 'angular';
        data.parameters = {
            starting_angle: 270,
        }
        return data;
    }

    /** 获得针轮旋转转场 */
    getPinwheel() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_PINWHEEL;
        data.name = '针轮旋转';
        data.englishName = 'pinwheel';
        data.parameters = {
            speed: 2,
        }
        return data;
    }

    /** 获得末日重演转场 */
    getDoomscreentransition() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DOOMSCREENTRANSITION;
        data.name = '末日重演';
        data.englishName = 'doomscreentransition';
        data.parameters = {
            bars: 30,
            amplitude: 1,
            noise: 0.1,
            frequency: 0.5,
            drip_scale:0.5
        }
        return data;
    }

    /** 获得梦幻变焦转场 */
    getDreamyzoom() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DREAMYZOOM;
        data.name = '梦幻变焦';
        data.englishName = 'dreamyzoom';
        data.parameters = {
            rotation: 6,
            scale:1.2
        }
        return data;
    }

    /** 获得毛刺溶解转场 */
    getGlitchdisplace() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_GLITCHDISPLACE;
        data.name = '毛刺溶解';
        data.englishName = 'glitchdisplace';
        data.parameters = {
            // segments: 5,
        }
        return data;
    }

    /** 获得蜂巢溶解转场 */
    getHexagonalize() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_HEXAGONALIZE;
        data.name = '蜂巢溶解';
        data.englishName = 'hexagonalize';
        data.parameters = {
            steps:50,
            horizontal_hexagons:20,
        }
        return data;
    }

    /** 获得百叶窗转场 */
    getWindowblinds() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_WINDOWBLINDS;
        data.name = '百叶窗';
        data.englishName = 'windowblinds';
        data.parameters = {
            // segments: 5,
        }
        return data;
    }

    /** 获得万花筒转场 */
    getKaleidoscope() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_KALEIDOSCOPE;
        data.name = '万花筒';
        data.englishName = 'kaleidoscope';
        data.parameters = {
            speed: 1,
            angle: 1,
            power: 1.5,
        }
        return data;
    }

    /** 获得蝶波扰乱转场 */
    getButterflywavescrawler() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_BUTTERFLYWAVESCRAWLER;
        data.name = '蝶波扰乱';
        data.englishName = 'butterflywavescrawler';
        data.parameters = {
            amplitude: 1,
            waves: 30,
            color_separation:0.3
        }
        return data;
    }

    /** 获得疯狂音波转场 */
    getCrazywave() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CRAZYWAVE;
        data.name = '疯狂音波';
        data.englishName = 'crazywave';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得大麻叶转场 */
    getCannabisleaf() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CANNABISLEAF;
        data.name = '大麻叶';
        data.englishName = 'cannabisleaf';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得交叉线转场 */
    getCrosshatch() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CROSSHATCH;
        data.name = '交叉线';
        data.englishName = 'crosshatch';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得波动燃尽转场 */
    getUndulatingburnout() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_UNDULATINGBURNOUT;
        data.name = '波动燃尽';
        data.englishName = 'undulatingburnout';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得重画移位转场 */
    getMosaic() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_MOSAIC;
        data.name = '重画移位';
        data.englishName = 'mosaic';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得径向旋转转场 */
    getRadial() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_RADIAL;
        data.name = '径向旋转';
        data.englishName = 'radial';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得圆形放大2转场 */
    getZoomincircles() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_ZOOMINCIRCLES;
        data.name = '圆形放大2';
        data.englishName = 'zoomincircles';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得网格翻转转场 */
    getGridflip() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_GRIDFLIP;
        data.name = '网格翻转';
        data.englishName = 'gridflip';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得梦幻波浪转场 */
    getDreamywave() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_DREAMYWAVE;
        data.name = '梦幻波浪';
        data.englishName = 'dreamywave';
        data.parameters = {
            segments: 5,
        }
        return data;
    }

    /** 获得虚幻交叉转场 */
    getCrosszoom() {
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_CROSSZOOM;
        data.name = '虚幻交叉';
        data.englishName = 'crosszoom';
        data.parameters = {
            segments: 5,
        }
        return data;
    }
    /** 获得默认音频转场 */
    getAudioFade(){
        let data = this.getBaseData();
        data.dataType = DataType.TRANS_AUDIO_FADE;
        data.name = '溶解';
        data.englishName = 'audioFade';
        data.parameters = {
            gain: 1
        }
        return data;
    }
}
export default new EffectTransFactory;