/**
 * 开发者：tangshanghai
 * 日期：2019/10/23
 * 说明：特技相关方法的代理
 */

import EffectTransFactory from "./track/EffectTransFactory";
import Utils from "@/libs/Utils";
import DataType from "./DataType";
import EffectFactory from "./track/EffectFactory";
// const CoreControl = App.CORE_CONTROL;
const App = {};
class EffectAgent {
    constructor(app, seq) {

        App.CONTROL = app.CONTROL;
        App.SEQ = seq;

        //转场特技在场景中的数据结构
        // let transObj = {
        //     clip1:clip,
        //     clip2:clip2,
        //     effect:clip.effectTransOut,
        //     startTime:clip.endTime-clip.effectTransOut.duration,
        //     endTime:clip.endTime,
        //     trackId:track.modelId,
        //     id:clip.effectTransOut.id || clip2.effectTransIn.id
        // }
        this.transitionEffects = [];
        this.eff_bgMonochrome = EffectFactory.getMonochromeData();
        this.eff_main_fadeout = EffectFactory.getFadeOutData({ duration: 500 });
    }

    /**
     * 获得填充区域
     */
    getFillPos(clip) {
        for (let i = 0; i < clip.effects.length; i++) {
            let eff = clip.effects[i];
            if (eff.dataType === DataType.EFFECT_2D && eff.dataType2 === 'inside_pos') {
                clip.fillPos.width = eff.params.scaleX;
                clip.fillPos.height = eff.params.scaleY;
                break;
            }
        }
    }
    /** 初始化填充区域比值，并计算 crop2d的最大裁剪参数 */
    initFillScale(clip) {
        clip.fillScale.width = App.SEQ.preset.width;
        clip.fillScale.height = App.SEQ.preset.height;
        for (let i = 0; i < clip.effects.length; i++) {
            let eff = clip.effects[i];
            if (eff.dataType === DataType.EFFECT_2D && eff.dataType2 === 'inside_pos') {
                let keyParmas = eff.keyFrameParameters[0].parameter;
                clip.fillScale.width = Math.round(keyParmas.scaleX * App.SEQ.preset.width);
                clip.fillScale.height = Math.round(keyParmas.scaleY * App.SEQ.preset.height);
                // break;
            }else if(eff.dataType === DataType.EFFECT_MULTIVIEW || eff.dataType === DataType.EFFECT_BGBLUR){
                //某些特技不能进行裁剪
                return;
            }
        }
        if (clip.dataType2 == 'MainBg') {
            clip.fillScale.width = clip.imageWidth;
            clip.fillScale.height = clip.imageHeight;
        }
        const pos = clip.fillPos;
        if (clip.imageWidth / clip.imageHeight < clip.fillScale.width / clip.fillScale.height) {
            pos.h = (clip.imageWidth * clip.fillScale.height / clip.fillScale.width) / clip.imageHeight;
            pos.y = (1 - pos.h) * 0.5;
        } else if (clip.imageWidth / clip.imageHeight > clip.fillScale.width / clip.fillScale.height) {
            pos.w = (clip.imageHeight * clip.fillScale.width / clip.fillScale.height) / clip.imageWidth;
            pos.x = (1 - pos.w) * 0.5;
        }
        // console.log(clip.dataType2,clip.fillScale,pos)
        this.updateEffect2D(clip, pos);

        // 是否支持横转竖功能
        clip.isCanHToV = clip.imageWidth / clip.imageHeight > App.SEQ.preset.width / App.SEQ.preset.height;
        clip.isOpenHToV = false;
    }
    /**
     * 更新2D特技 传入位置
     */
    updateEffect2D(clip, _pos) {
        let effects = clip.effects || [];
        let eff2d = null;
        for (let i = 0; i < effects.length; i++) {
            let eff = effects[i];
            if (eff.dataType === DataType.EFFECT_2D && eff.dataType2 === 'crop') {
                eff2d = eff;
                break;
            }
        }
        if (eff2d) {
            // Object.assign(eff2d.params,transPos);
            let transPos = this.get2Dparams(clip,_pos);
            for (let i = 0; i < eff2d.keyFrameParameters.length; i++) {
                let keyItem = eff2d.keyFrameParameters[i];
                Object.assign(keyItem.parameter, transPos);
            }
            App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_CHANGE, {
                eventsender: 'Sequence',
                clip: clip,
                effect: eff2d
            });
        }else{
            if (clip.imageWidth / clip.imageHeight <= App.SEQ.preset.width / App.SEQ.preset.height) {
                clip.scaleType = 1;
            } else {
                clip.scaleType = 2;
            }
        }
    }

    /** 传入位置，计算2D特技参数 */
    get2Dparams(clip,_pos){
        let pos = _pos;
        let scaleXBase = 1, scaleYBase = 1;
        let transPos = {
            left: pos.x,
            right: pos.x + pos.w,
            top: pos.y,
            bottom: pos.y + pos.h
        }
        let iw = clip.imageWidth * pos.w,ih = clip.imageHeight * pos.h;
        //以高度为基准
        if (iw / ih < App.SEQ.preset.width / App.SEQ.preset.height) {
            let w = iw / (App.SEQ.preset.width / App.SEQ.preset.height * ih);
            scaleXBase = w;
            scaleYBase = 1;
        }else if(iw / ih > App.SEQ.preset.width / App.SEQ.preset.height){
            let h = ih / (App.SEQ.preset.height / App.SEQ.preset.width * iw);
            scaleYBase = h;
            scaleXBase = 1;
        }

        transPos.scaleX = scaleXBase / pos.w;
        transPos.scaleY = scaleYBase / pos.h;
        transPos.offsetX = (0.5 * (1 - transPos.right) - 0.5 * transPos.left) * transPos.scaleX;
        transPos.offsetY = -(0.5 * (1 - transPos.bottom) - 0.5 * transPos.top) * transPos.scaleY;

        return transPos;
    }

    /** 对clip添加特技 */
    addEffect(clip, effect, eventsender = 'Sequence') {
        clip.effects.push(effect);
        App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_ADD, {
            eventsender: eventsender,
            clip: clip,
            effect: effect
        });
    }

    /** 变化某一个特技 */
    changeEffect(clip, effect, eventsender = 'Sequence') {
        App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_CHANGE, {
            eventsender: eventsender,
            clip: clip,
            effect: effect
        });
    }

    /** 删除某一个特技 */
    removeEffect(clip, effect, eventsender = 'Sequence') {
        let effects = clip.effects || [];
        for (let i = 0; i < effects.length; i++) {
            let eff = effects[i];
            if (eff.id === effect.id) {
                effects.splice(i, 1);
                App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_REMOVE, {
                    eventsender: eventsender,
                    clip: clip,
                    effect: eff
                });
                break;
            }
        }
    }

    /** 选中某一个关键帧 keyId传空表示取消全部选中 ，b为true，表示选中,clearBefore为true，表示清空之前的选中*/
    selectKeyItem(clip, effect, keyId, b = true, clearBefore = true) {
        // const willTells = [];
        let findParam = null;
        for (let i = 0; i < effect.keyFrameParameters.length; i++) {
            let keyParams = effect.keyFrameParameters[i];
            if (keyId) {
                if (keyParams.id === keyId) {
                    keyParams.checked = b;
                    findParam = keyParams;
                    // willTells.push(keyParams);
                } else {
                    if (clearBefore) {
                        if (keyParams.checked) {
                            keyParams.checked = false;
                            // willTells.push(keyParams);
                        }
                    }
                }
            } else {
                if (keyParams.checked) {
                    keyParams.checked = false;
                    // willTells.push(keyParams);
                }
            }
        }
        if (findParam) {
            App.CONTROL.EFFECT.effectKeyFrameStatus({
                cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_SELECT,
                clip: clip,
                effect: effect,
                keyParams: findParam
            });
        }
    }

    // 更新位置参数
    updateParams(clip, _pos) {
        let pos = _pos;
        let scaleXBase = 1, scaleYBase = 1;
        if (clip.scaleType === 1) {
            if (clip.imageWidth / clip.imageHeight < App.SEQ.preset.width / App.SEQ.preset.height) {
                let w = clip.imageWidth / (App.SEQ.preset.width / App.SEQ.preset.height * clip.imageHeight);
                pos.x = (1 - w) * 0.5 + pos.x * w;
                pos.w *= w;
                scaleXBase = w;
            }
        } else if (clip.scaleType === 2) {
            if (clip.imageWidth / clip.imageHeight > App.SEQ.preset.width / App.SEQ.preset.height) {
                let h = clip.imageHeight / (App.SEQ.preset.height / App.SEQ.preset.width * clip.imageWidth);
                pos.y = (1 - h) * 0.5 + pos.x * h;
                pos.h *= h;
                scaleYBase = h;
            }
        }
        let transPos = {
            left: pos.x,
            right: pos.x + pos.w,
            top: pos.y,
            bottom: pos.y + pos.h
        }
        transPos.scaleX = scaleXBase / pos.w;
        transPos.scaleY = scaleYBase / pos.h;
        transPos.offsetX = (0.5 * (1 - transPos.right) - 0.5 * transPos.left) * transPos.scaleX;
        transPos.offsetY = -(0.5 * (1 - transPos.bottom) - 0.5 * transPos.top) * transPos.scaleY;
        return transPos
    }
    /** 更新或添加一个关键帧 参数coverParam*/
    updateAddEffectKeyFrame(clip, effect, time, coverParam) {
        const msg = {
            success: true,
            description: '添加成功'
        }
        if (time < 0 || time > clip.duration) {
            msg.success = false;
            msg.description = '关键帧时间超出范围';
        } else {
            let findParam = null;
            for (let i = effect.keyFrameParameters.length - 1; i >= 0; i--) {
                let tempP = effect.keyFrameParameters[i];
                if (tempP.pos == time) {
                    findParam = tempP;
                    break;
                }
            }
            if (findParam) {
                msg.description = "更新成功";
                Object.assign(findParam.parameter, coverParam);
                App.CONTROL.EFFECT.effectKeyFrameStatus({
                    cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_CHANGE,
                    clip: clip,
                    effect: effect,
                    keyParams: findParam
                });
            } else {
                let newKeyParam = {
                    pos: time,
                    posScale: time / clip.duration,
                    checked: false,
                    id: Utils.GUID(),
                    parameter: Object.assign({ ...effect.defaultkeyParams }, coverParam)
                }
                effect.keyFrameParameters.push(newKeyParam);
                effect.keyFrameParameters.sort((a, b) => {
                    return a.pos > b.pos ? 1 : -1;
                })
                App.CONTROL.EFFECT.effectKeyFrameStatus({
                    cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_ADD,
                    clip: clip,
                    effect: effect,
                    keyParams: newKeyParam
                });
                this.selectKeyItem(clip, effect, newKeyParam.id, true, true);
            }
        }
        return msg;
    }

    /** 添加一个关键帧 参数默认取前一个关键帧参数*/
    addEffectKeyFrame(clip, effect, time) {
        const msg = {
            success: true,
            description: '添加成功'
        }
        if (time < 0 || time > clip.duration) {
            msg.success = false;
            msg.description = '关键帧时间超出范围';
        } else {
            let findParam = null;
            for (let i = effect.keyFrameParameters.length - 1; i >= 0; i--) {
                let tempP = effect.keyFrameParameters[i];
                if (tempP.pos < time) {
                    findParam = tempP;
                    break;
                }
            }
            let newKeyParam = {
                pos: time,
                posScale: time / clip.duration,
                checked: false,
                id: Utils.GUID(),
                // parameter: Object.assign({},effect.defaultkeyParams)
                parameter: JSON.parse(JSON.stringify(effect.defaultkeyParams))
            }
            if (findParam) {
                // newKeyParam.parameter = Object.assign({},findParam.parameter);
                newKeyParam.parameter = JSON.parse(JSON.stringify(findParam.parameter));
            }
            effect.keyFrameParameters.push(newKeyParam);
            effect.keyFrameParameters.sort((a, b) => {
                return a.pos > b.pos ? 1 : -1;
            })
            App.CONTROL.EFFECT.effectKeyFrameStatus({
                cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_ADD,
                clip: clip,
                effect: effect,
                keyParams: newKeyParam
            });
            this.selectKeyItem(clip, effect, newKeyParam.id, true, true);
        }
        return msg;
    }

    /** 删除一个关键帧 */
    removeEffectKeyFrame(clip, effect, keyId) {
        const msg = {
            success: true,
            description: '删除成功'
        }
        if (effect.keyFrameParameters.length <= 2) {
            msg.success = false;
            msg.description = '至少需要保持2个关键帧'
            return msg;
        }
        let findParam = null;
        if (keyId) {
            for (let i = 0; i < effect.keyFrameParameters.length; i++) {
                let tempP = effect.keyFrameParameters[i];
                if (tempP.id === keyId) {
                    findParam = tempP;
                    effect.keyFrameParameters.splice(i, 1);
                    break;
                }
            }
        } else {
            for (let i = 0; i < effect.keyFrameParameters.length; i++) {
                let tempP = effect.keyFrameParameters[i];
                if (tempP.checked) {
                    findParam = tempP;
                    effect.keyFrameParameters.splice(i, 1);
                    i--;
                }
            }
        }
        if (findParam) {
            App.CONTROL.EFFECT.effectKeyFrameStatus({
                cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_REMOVE,
                clip: clip,
                effect: effect,
                keyParams: findParam
            });
            this.selectKeyItem(clip, effect, effect.keyFrameParameters[0].id, true, true);
        } else {
            msg.success = false;
            msg.description = '未找到关键帧'
        }
        return msg;
    }

    /** 重新计算特技的关键帧位置 */
    updateKeyFramePos(clip, eventsender = 'Sequence') {
        // console.log('updateKeyFramePosupdateKeyFramePosupdateKeyFramePos')
        if (!(clip.dataType === DataType.CLIP_VIDEO || clip.dataType === DataType.CLIP_VIDEO_IMAGE)) return;
        let effects = clip.effects || [];

        for (let i = 0; i < effects.length; i++) {
            let effect = effects[i];
            let isNeedTell = false;
            for (let j = 0; j < effect.keyFrameParameters.length; j++) {
                let tempP = effect.keyFrameParameters[j];
                let newPos = (clip.outPoint - clip.inPoint) * tempP.posScale + clip.inPoint;
                if (!isNeedTell && tempP.pos !== newPos) isNeedTell = true;
                tempP.pos = newPos;
            }
            if (isNeedTell) {
                App.CONTROL.EFFECT.effectKeyFrameStatus({
                    cmd: App.CONTROL.EFFECT.EffectEmumType.KEYFRAME_CHANGE,
                    clip: clip,
                    effect: effect
                });
            }
        }
    }

    /**
     * 将模板特技转换为内部特技
     */
    transTLToEffect(tlClip, isDefault2D = false) {
        const effects = [];
        let duartion = tlClip.Duration || tlClip.TrackOut - tlClip.TrackIn;
        //默认添加一个2D特技
        if (isDefault2D) {
            let eff2d = EffectFactory.get2DData(null, duartion);
            eff2d.dataType2 = 'crop';
            effects.push(eff2d);
        }
        let tlEffects = tlClip.Fxs;
        if (tlEffects) {
            // effect_2D                  2D
            // effect_blur                Blur
            // effect_alpha  Alpha
            // effect_color  Color
            // effect_colorCorrector1  Colorcorrector1
            // effect_fadein  Fadein
            // effect_fadeout  Fadeout
            // console.log('tlEffects', tlEffects)
            for (let i = 0; i < tlEffects.length; i++) {
                let tlItem = tlEffects[i];
                if (tlItem.FxType === '2D') {
                    let eff2d = EffectFactory.get2DData(tlItem.Params, duartion);
                    eff2d.dataType2 = 'inside_pos';
                    effects.push(eff2d);
                } else if (tlItem.FxType === 'ColorCorrector1') {
                    effects.push(EffectFactory.getColorCorrectorData1(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'GaussBlur' || tlItem.FxType === 'FastGaussBlur') {
                    effects.push(EffectFactory.getGaussblurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fadein') {
                    effects.push(EffectFactory.getFadeInData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fadeout') {
                    effects.push(EffectFactory.getFadeOutData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fadeout') {
                    effects.push(EffectFactory.getFadeOutData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Blur') {
                    effects.push(EffectFactory.getBlurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Color') {
                    effects.push(EffectFactory.getColorData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Monochrome') {
                    effects.push(EffectFactory.getMonochromeData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Alpha') {
                    effects.push(EffectFactory.getAlphaData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'FindEdges') {
                    effects.push(EffectFactory.getFindEdgesData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Mirror') {
                    effects.push(EffectFactory.getMirrorData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'MultiView') {
                    effects.push(EffectFactory.getMultiViewData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Mosaic') {
                    effects.push(EffectFactory.getMosaicData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'TemplateBlur') {
                    effects.push(EffectFactory.getBgBlurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Scrolling') {
                    effects.push(EffectFactory.getScrollingData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OutOfBodySoul') {
                    effects.push(EffectFactory.getOutofbodysoulData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Swing') {
                    effects.push(EffectFactory.getSwingData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Swing2') {
                    effects.push(EffectFactory.getSwing2Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'ParticleBlur') {
                    effects.push(EffectFactory.getParticleblurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'LensBlur') {
                    effects.push(EffectFactory.getLensblurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'ScreenRhythm') {
                    effects.push(EffectFactory.getScreenrhythmData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'RippleDistortion') {
                    effects.push(EffectFactory.getRippledistortionData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'HeartBeat') {
                    effects.push(EffectFactory.getHeartbeatData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'NenoSwing') {
                    effects.push(EffectFactory.getNenoswingData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'RainRipple') {
                    effects.push(EffectFactory.getRainrippleData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'RainCurtain') {
                    effects.push(EffectFactory.getRaincurtainData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'PhantomFailure') {
                    effects.push(EffectFactory.getPhantomfailureData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'ChromatismDithering') {
                    effects.push(EffectFactory.getChromatismditheringData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Shake') {
                    effects.push(EffectFactory.getShakeData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'InstantaneousBlur') {
                    effects.push(EffectFactory.getInstantaneousblurData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'SkinNeedling') {
                    effects.push(EffectFactory.getSkinneedlingData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'SignalDistortion') {
                    effects.push(EffectFactory.getSignaldistortionData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'SignalDistortion2') {
                    effects.push(EffectFactory.getSignaldistortion2Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'SignalDistortion3') {
                    effects.push(EffectFactory.getSignaldistortion3Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Snow') {
                    effects.push(EffectFactory.getSnowData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fireworks') {
                    effects.push(EffectFactory.getFireworksData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fireworks2') {
                    effects.push(EffectFactory.getFireworks2Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fireworks3') {
                    effects.push(EffectFactory.getFireworks3Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'Fireworks4') {
                    effects.push(EffectFactory.getFireworks4Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OldMovies') {
                    effects.push(EffectFactory.getOldmoviesData(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OldMovies2') {
                    effects.push(EffectFactory.getOldmovies2Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OldMovies3') {
                    effects.push(EffectFactory.getOldmovies3Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OldMovies4') {
                    effects.push(EffectFactory.getOldmovies4Data(tlItem.Params, duartion));
                } else if (tlItem.FxType === 'OldMovies5') {
                    effects.push(EffectFactory.getOldmovies5Data(tlItem.Params, duartion));
                }
            }
        }
        return effects;
    }

    /**
     * 将模板转场特技转换为内部数据
     */
    transTLToTrans(tlTrans) {
        // "TransFXType":"FadeInOut",
        // 				"Duration":2000,
        //                 "Params":{

        //                 }
        // trans_fade  Fade
        // trans_glow  Glow
        // trans_DiffuseWipe  Diffusewipe
        // trans_translation  Translation
        // trans_split  Split
        // trans_push  Push
        // trans_compress2  Compress2
        // trans_rect  Rect
        // trans_ripple  Ripple
        let trans = null;
        if (!tlTrans) return trans;
        switch (tlTrans.TransFXType) {
            case 'Fade':
                trans = EffectTransFactory.getFadeInOut();
                break;
            case 'Glow':
                trans = EffectTransFactory.getGlow(tlTrans.Params);
                break;
            case 'DiffuseWipe':
                trans = EffectTransFactory.getDiffuseWipe(tlTrans.Params);
                break;
            case 'Translation':
                trans = EffectTransFactory.getTranslation(tlTrans.Params);
                break;
            case 'Split':
                trans = EffectTransFactory.getSplit(tlTrans.Params);
                break;
            case 'Push':
                trans = EffectTransFactory.getPush(tlTrans.Params);
                break;
            case 'Compress2':
                trans = EffectTransFactory.getCompress2(tlTrans.Params);
                break;
            case 'Rect':
                trans = EffectTransFactory.getRect(tlTrans.Params);
                break;
            case 'Ripple':
                trans = EffectTransFactory.getRipple(tlTrans.Params);
                break;
            case 'Wind':
                trans = EffectTransFactory.getWind(tlTrans.Params);
                break;
            case 'Swap':
                trans = EffectTransFactory.getSwap(tlTrans.Params);
                break;
            case 'Squeeze':
                trans = EffectTransFactory.getSqueeze(tlTrans.Params);
                break;
            case 'SquareSwire':
                trans = EffectTransFactory.getSquareswire(tlTrans.Params);
                break;
            case 'RotateScaleFade':
                trans = EffectTransFactory.getRotatescalefade(tlTrans.Params);
                break;
            case 'RandomSquares':
                trans = EffectTransFactory.getRandomsquares(tlTrans.Params);
                break;
            case 'PolarFunction':
                trans = EffectTransFactory.getPolarfunction(tlTrans.Params);
                break;
            case 'Pixelize':
                trans = EffectTransFactory.getPixelize(tlTrans.Params);
                break;
            case 'MultiplyBlend':
                trans = EffectTransFactory.getMultiplyblend(tlTrans.Params);
                break;
            case 'Heart':
                trans = EffectTransFactory.getHeart(tlTrans.Params);
                break;
            case 'FlyEye':
                trans = EffectTransFactory.getFlyeye(tlTrans.Params);
                break;
            case 'FadeGrayScale':
                trans = EffectTransFactory.getFadegrayscale(tlTrans.Params);
                break;
            case 'FadeColor':
                trans = EffectTransFactory.getFadecolor(tlTrans.Params);
                break;
            case 'Doorway':
                trans = EffectTransFactory.getDoorway(tlTrans.Params);
                break;
            case 'DirectionalWipe':
                trans = EffectTransFactory.getDirectionalwipe(tlTrans.Params);
                break;
            case 'Cube':
                trans = EffectTransFactory.getCube(tlTrans.Params);
                break;
            case 'CrossWarp':
                trans = EffectTransFactory.getCrosswarp(tlTrans.Params);
                break;
            case 'ColorPhase':
                trans = EffectTransFactory.getColorphase(tlTrans.Params);
                break;
            case 'CircleOpen':
                trans = EffectTransFactory.getCircleopen(tlTrans.Params);
                break;
            case 'Circle':
                trans = EffectTransFactory.getCircle(tlTrans.Params);
                break;
            case 'Burn':
                trans = EffectTransFactory.getBurn(tlTrans.Params);
                break;
            case 'Angular':
                trans = EffectTransFactory.getAngular(tlTrans.Params);
                break;
            case 'Pinwheel':
                trans = EffectTransFactory.getPinwheel(tlTrans.Params);
                break;
            case 'DoomScreen':
                trans = EffectTransFactory.getDoomscreentransition(tlTrans.Params);
                break;
            case 'DreamyZoom':
                trans = EffectTransFactory.getDreamyzoom(tlTrans.Params);
                break;
            case 'GlitchDisplace':
                trans = EffectTransFactory.getGlitchdisplace(tlTrans.Params);
                break;
            case 'Hexagonalize':
                trans = EffectTransFactory.getHexagonalize(tlTrans.Params);
                break;
            case 'WindowBlinds':
                trans = EffectTransFactory.getWindowblinds(tlTrans.Params);
                break;
            case 'Kaleidoscope':
                trans = EffectTransFactory.getKaleidoscope(tlTrans.Params);
                break;
            case 'ButterflyWaveScrawler':
                trans = EffectTransFactory.getButterflywavescrawler(tlTrans.Params);
                break;
        }
        
        if(trans){
            trans.duration = tlTrans.Duration;
        }
        return trans;
    }
    /** 特技复制 */
    copyClipEffect(newClip, oldClip) {
        newClip.scaleType = oldClip.scaleType;
        const effects = JSON.parse(JSON.stringify(oldClip.effects));
        for (let i = 0; i < effects.length; i++) {
            let effect = effects[i];
            effect.id = Utils.GUID();
            if (effect.dataType === DataType.EFFECT_2D && effect.dataType2 === 'crop') {
                // let defaultParams = EffectFactory.get2DData().params;
                // Object.assign(effect.params,defaultParams);
                effect = EffectFactory.get2DData(null,newClip.duration);
                effect.dataType2 = 'crop';
                effects[i] = effect;
            }
        }
        newClip.effects = effects;
        if (oldClip.effectTrans) {
            const effectTrans = JSON.parse(JSON.stringify(oldClip.effectTrans));
            effectTrans.id = Utils.GUID();
            newClip.effectTrans = effectTrans;
        }
    }

    /** 重新计算时间线中的转场特技 */
    computerTrans() {
        let transEffects = [];
        for (let i = 0; i < App.SEQ.tracks.length; i++) {
            let track = App.SEQ.tracks[i];
            for (let j = 0; j < track.clips.length; j++) {
                let clip = track.clips[j];
                if (clip.effectTrans) {
                    let clip2 = track.getClipRangeTime(clip.trackOut - clip.effectTrans.duration, clip.trackOut);
                    // console.log('clip2',clip2,clip.trackOut-clip.effectTrans.duration,clip.trackOut)
                    if (!clip2) break;
                    let transObj = {
                        clip1: clip,
                        clip2: clip2,
                        effect: clip.effectTrans,
                        trackIn: clip.trackOut - clip.effectTrans.duration,
                        trackOut: clip.trackOut,
                        trackId: track.modelId,
                        id: clip.effectTrans.id
                    }
                    transEffects.push(transObj);
                }
            }
        }
        // console.log('transEffects',JSON.parse(JSON.stringify(transEffects)))
        //第一步，先向记录数据查询是否有本身，如果没查到，则删除
        for (let i = 0; i < this.transitionEffects.length; i++) {
            let t1 = this.transitionEffects[i];
            let isFind = false;
            for (let j = 0; j < transEffects.length; j++) {
                let t2 = transEffects[j];
                if (t1.id === t2.id && t1.trackId === t2.trackId) {
                    isFind = true;
                    break;
                }
            }
            if (!isFind) {
                this.transitionEffects.splice(i, 1);
                App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_TRANS_REMOVE, { effect: t1 });
                i--;
            }
        }

        //第二步，向本身数据查找是否有记录的数据，如果有，则更新，如果没有，则新建
        for (let j = 0; j < transEffects.length; j++) {
            let t2 = transEffects[j];
            let isFind = false;
            for (let i = 0; i < this.transitionEffects.length; i++) {
                let t1 = this.transitionEffects[i];
                if (t1.id === t2.id) {
                    isFind = true;
                    this.transitionEffects[i] = t2;
                    App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_TRANS_CHANGE, { effect: t2 });
                    break;
                }
            }
            if (!isFind) {
                this.transitionEffects.push(t2);
                App.CONTROL.EFFECT.$emit(App.CONTROL.EFFECT.EFFECT_TRANS_ADD, { effect: t2 });
            }
        }
        // this.transitionEffects = transEffects;
        console.log('重新计算转场特技', this.transitionEffects)
    }
}
export default EffectAgent;
