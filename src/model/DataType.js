class DataType{
    constructor(){
        //主要几种数据类型
        this.TRACK_FX = "track_fx";
        this.TRACK_VIDEO = "track_video";
        this.TRACK_AUDIO = "track_audio";
        this.TRACK_CG = "track_cg";
        this.TRACK_AI = 'track_AI';//一般只用于video的附加

        this.CLIP_FX_MARK = "clip_fx_mark";
        this.CLIP_VIDEO = "clip_video";
        this.CLIP_AUDIO = "clip_audio";
        this.CLIP_CG_IMAGE = "clip_cg_image";
        this.CLIP_CG_IMAGE_GIF = "clip_cg_image_gif";
        this.CLIP_CG_IMAGE_WEBP = "clip_cg_image_webp";
        this.CLIP_CG_TEXT = "clip_cg_text";
        this.CLIP_CG_TEXT_CON = "clip_cg_text_con";
        this.CLIP_CG_TEXT_DYNAMIC = "clip_cg_text_dynamic";
        this.CLIP_CG_BLUR = "clip_cg_blur";
        this.CLIP_CG_TGA = "clip_cg_tga";
        this.CLIP_CG_LIBRETTO = "clip_cg_libretto";
        this.CLIP_CG_LIBRETTO_TEXT = "clip_cg_libretto_text";
        this.CLIP_CG_LIBRETTO_DYNAMIC = "clip_cg_libretto_dynamic";
        this.CLIP_VIDEO_AI = "clip_video_ai";
        this.CLIP_VIDEO_IMAGE = "clip_video_image";
        this.CLIP_VIDEO_CONTAINER = "clip_video_container";

        this.EFFECT_BLUR = "effect_blur";
        this.EFFECT_2D = "effect_2D";
        this.EFFECT_ALPHA = "effect_alpha";
        this.EFFECT_COLOR = "effect_color";
        this.EFFECT_COLORCORRECTOR1 = "effect_colorCorrector1";
        this.EFFECT_FADEIN = "effect_fadein";
        this.EFFECT_FADEOUT = "effect_fadeout";
        this.EFFECT_STATIC = "effect_static";
        this.EFFECT_MONOCHROME = 'effect_monochrome';
        this.EFFECT_GAUSSBLUR = 'effect_gaussblur';
        this.EFFECT_FINDEDGES = 'effect_findEdges';
        this.EFFECT_MIRROR = 'effect_mirror';
        this.EFFECT_MULTIVIEW = 'effect_multiView';
        this.EFFECT_MOSAIC = "effect_mosaic";
        this.EFFECT_BGBLUR = 'effect_bgBlur';
        this.EFFECT_SCROLLING = 'effect_scrolling';//卷动
        this.EFFECT_OUTOFBODYSOUL = 'effect_outofbodysoul';//灵魂出窍
        this.EFFECT_SWING = 'effect_swing';//摇摆
        this.EFFECT_SWING2 = 'effect_swing2';//摇摆2
        this.EFFECT_PARTICLEBLUR = 'effect_particleblur';//粒子模糊
        this.EFFECT_LENSBLUR = 'effect_lensblur';//镜头模糊
        this.EFFECT_SCREENRHYTHM = 'effect_screenrhythm';//屏幕律动
        this.EFFECT_RIPPLEDISTORTION = 'effect_rippledistortion';//波纹失真
        this.EFFECT_HEARTBEAT = 'effect_heartbeat';//心跳
        this.EFFECT_NENOSWING = 'effect_nenoswing';//霓虹摇摆
        this.EFFECT_RAINRIPPLE = 'effect_rainripple';//雨波纹
        this.EFFECT_RAINCURTAIN = 'effect_raincurtain';//雨幕
        this.EFFECT_PHANTOMFAILURE = 'effect_phantomfailure';//幻影故障
        this.EFFECT_CHROMATISMDITHERING = 'effect_chromatismdithering';// 波纹色差
        this.EFFECT_SHAKE = 'effect_shake';// 抖动
        this.EFFECT_INSTANTANEOUSBLUR = 'effect_instantaneousblur';// 瞬间模糊
        this.EFFECT_SKINNEEDLING = 'effect_skinneedling';// 毛刺
        this.EFFECT_SIGNALDISTORTION = 'effect_signaldistortion';// 信号失真
        this.EFFECT_SIGNALDISTORTION2 = 'effect_signaldistortion2';//信号失真2
        this.EFFECT_SIGNALDISTORTION3 = 'effect_signaldistortion3';//信号失真3
        this.EFFECT_SNOW = 'effect_snow';//雪花
        this.EFFECT_FIREWORKS = 'effect_fireworks';//烟花1
        this.EFFECT_FIREWORKS2 = 'effect_fireworks2';//烟花2
        this.EFFECT_FIREWORKS3 = 'effect_fireworks3';//烟花3
        this.EFFECT_FIREWORKS4 = 'effect_fireworks4';//烟花4
        this.EFFECT_OLDMOVIES = 'effect_oldmovies';//老电影1
        this.EFFECT_OLDMOVIES2 = 'effect_oldmovies2';//老电影2
        this.EFFECT_OLDMOVIES3 = 'effect_oldmovies3';//老电影3
        this.EFFECT_OLDMOVIES4 = 'effect_oldmovies4';//老电影4
        this.EFFECT_OLDMOVIES5 = 'effect_oldmovies5';//老电影5
        
        // this.EFFECT_INVERSE = "effect_inverse";

        
        this.TRANS_FADE = 'trans_fade';
        this.TRANS_GLOW = 'trans_glow';
        this.TRANS_DIFFUSEWIPE = 'trans_DiffuseWipe';
        this.TRANS_TRANSLATION = 'trans_translation';
        this.TRANS_SPLIT = 'trans_split';
        this.TRANS_PUSH = 'trans_push';
        this.TRANS_COMPRESS2 = 'trans_compress2';
        this.TRANS_RECT = 'trans_rect';
        this.TRANS_RIPPLE = 'trans_ripple';
        this.TRANS_WIND = 'trans_wind';
        this.TRANS_SWAP = 'trans_swap';
        this.TRANS_SQUEEZE = 'trans_squeeze';
        this.TRANS_SQUARESWIRE = 'trans_squareswire';
        this.TRANS_ROTATESCALEFADE = 'trans_rotatescalefade';
        this.TRANS_RANDOMSQUARES = 'trans_randomsquares';
        this.TRANS_POLARFUNCTION = 'trans_polarfunction';
        this.TRANS_PIXELIZE = 'trans_pixelize';//像素渐显
        this.TRANS_MULTIPLYBLEND = 'trans_multiplyblend';//多重混合
        this.TRANS_HEART = 'trans_heart';//心形放大
        this.TRANS_FLYEYE = 'trans_flyeye';//飞鹰之眼
        this.TRANS_FADEGRAYSCALE = 'trans_fadegrayscale';//灰度融合
        this.TRANS_FADECOLOR = 'trans_fadecolor';//纯色过渡
        this.TRANS_DOORWAY = 'trans_doorway';//开门大吉
        this.TRANS_DIRECTIONALWIPE = 'trans_directionalwipe';//定向擦除
        this.TRANS_CUBE = 'trans_cube';//立体旋转
        this.TRANS_CROSSWARP = 'trans_crosswarp';//交叉翘曲
        this.TRANS_COLORPHASE = 'trans_colorphase';//彩色相位
        this.TRANS_CIRCLEOPEN = 'trans_circleopen';//圆形围栏
        this.TRANS_CIRCLE = 'trans_circle';//圆形放大
        this.TRANS_BURN = 'trans_burn';//骄阳似火
        this.TRANS_ANGULAR = 'trans_angular';//时钟同步
        this.TRANS_PINWHEEL = 'trans_pinwheel';//针轮旋转
        this.TRANS_DOOMSCREENTRANSITION = 'trans_doomscreentransition';//末日重演
        this.TRANS_DREAMYZOOM = 'trans_dreamyzoom';//梦幻变焦
        this.TRANS_GLITCHDISPLACE = 'trans_glitchdisplace';//毛刺溶解
        this.TRANS_HEXAGONALIZE = 'trans_hexagonalize';//蜂巢溶解
        this.TRANS_WINDOWBLINDS = 'trans_windowblinds';//百叶窗
        this.TRANS_KALEIDOSCOPE = 'trans_kaleidoscope';//万花筒
        this.TRANS_BUTTERFLYWAVESCRAWLER = 'trans_butterflywavescrawler';//蝶波扰乱
        this.TRANS_CRAZYWAVE = 'trans_crazywave';//疯狂音波
        this.TRANS_CANNABISLEAF = 'trans_cannabisleaf';//大麻叶
        this.TRANS_CROSSHATCH = 'trans_crosshatch';//交叉线
        this.TRANS_UNDULATINGBURNOUT = 'trans_undulatingburnout';//波动燃尽
        this.TRANS_MOSAIC = 'trans_mosaic';//重画移位
        this.TRANS_RADIAL = 'trans_radial';//径向旋转
        this.TRANS_ZOOMINCIRCLES = 'trans_zoomincircles';//圆形放大2
        this.TRANS_GRIDFLIP = 'trans_gridflip';//网格翻转
        this.TRANS_DREAMYWAVE = 'trans_dreamywave';//梦幻波浪
        this.TRANS_CROSSZOOM = 'trans_crosszoom';//虚幻交叉
        // this.EFFECT_TRANS_ALPHA = 'effect_trans_alpha';

        this.TRANS_AUDIO_FADE = 'trans_audio_fade';

        this.MARK_DEFAULT = 'mark_default';
        this.IN_AND_OUT = 'in_and_out';
        this.CAPTION_BASE = 'caption_base';
        this.LIBRETTO_BASE = 'libretto_base';
    }
}
export default new DataType;