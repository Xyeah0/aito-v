import Sequence from "./Sequence";
import CoreControl from "./control/CoreControl";
import EffectControl from "./control/EffectControl";
import HistoryControl from "./control/HistoryControl";

class App{
    constructor(){
        this.version = '1.0.0';

        this.CONTROL = {
            CORE: new CoreControl(),
            EFFECT: new EffectControl(),
            HISTORY: new HistoryControl()
        }
        this.activeSequence = new Sequence(this);
        this.maxzIndex = 400;

        


        

        this.CONTROL.HISTORY.$on(this.CONTROL.HISTORY.HISTORY_UNDO,historyUndoHandler.bind(this));
        this.CONTROL.HISTORY.$on(this.CONTROL.HISTORY.HISTORY_REDO,historyRedoHandler.bind(this));

        //撤销事件
        function historyUndoHandler(data){
            // console.log('撤销事件',this.activeSequence,data.data,data.nameData)
            // let seq = JSON.parse(_seq);
            let _seq = data.data;
            this.activeSequence.changeSeqData(JSON.parse(_seq));
            // console.log('data.currentTime',data,data.currentTime)
            this.activeSequence.seek(data.nameData.currentTime);
        }
        //重做事件
        function historyRedoHandler(data){
            let _seq = data.data;
            this.activeSequence.changeSeqData(JSON.parse(_seq));
            this.activeSequence.seek(data.nameData.currentTime);
        }
        //测试cookie
        // document.cookie = 'login_chinamcloud_id=3aa5a234d807ee82262ba1d7aab7f652';
        // document.cookie = 'login_chinamcloud_tid=694466584a1b165c6889cdeb61c1b069';
        // document.cookie = 'login_chinamcloud_type=1';
        // document.cookie = 'group_code=1012000141';
        // document.cookie = 'chinamcloud_version=v2';
        // document.cookie = 'chinamcloud_version=v2';

        // login_chinamcloud_id=3aa5a234d807ee82262ba1d7aab7f652; login_chinamcloud_type=1; login_chinamcloud_url=https://cmcgroup.wjtest.chinamcloud.cn/; group_code=1012000141; chinamcloud_version=v2; loginUser=%7B%22administrator%22%3Atrue%2C%22groupId%22%3A%227772f9becd75ee05d3ca41d04d73ef6e%22%2C%22tenantid%22%3A%227772f9becd75ee05d3ca41d04d73ef6e%22%2C%22token%22%3A%22%22%2C%22uid%22%3A%2211370%22%2C%22username%22%3A%22haibao2%22%7D; login_chinamcloud_tid=d7a5fae3ba0953687d19a0ef7b77d005
    }

}

export default new App;