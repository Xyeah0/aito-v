import DataType from "./DataType";
import Utils from '../libs/Utils';
import { _ } from "core-js";
class CreateRender {
    constructor() {

        //     "Type": "PostersVideo",
        // "Version": "1.0.0.0",
        // "TemplateId": "be675b5d06bbb0feac35c51a907908c0",
        // "InPoint": 0.0,
        // "OutPoint": 59.45893859863281,
        // "TLFormat": {
        // 	"Width": 1920,
        // 	"Height": 1080,
        // 	"FPS": 25
        // },
        // "HostPath":{
        // 	"FileHost": "X:\\content\\video\\vod\\",
        // 	"HttpHost": "http://contentms3.wjys.chinamcloud.com/vod/",
        // 	"HttpRelativePath": "",
        // 	"FileRelativePath": ""
        // },
        // "Materials": [],
        // "Tracks"
    }

    /**
     * 获得主素材地址列表
     */
    getMainUrlList(activeSeq){
        const arr = [];
        const mainClips = activeSeq.getMainTrackClip();
        for(let i=0;i<mainClips.length;i++){
            let clip = mainClips[i];
            arr.push(clip.path);
        }
        return arr;
    }

    /**
     * 检测时间线数据是否完整
     */
    checkTimeLine(activeSeq){
        let isComplete = true;
        let msg = '';
        const mainClips = activeSeq.getMainTrackClip();
        for(let i=0;i<mainClips.length;i++){
            let clip = mainClips[i];
            if(clip.carryData.noFind){
                isComplete = false;
                msg = '系统检测到（场景'+(i+1)+'）未匹配到相关素材，请手动匹配后再提交！';
                break;
            }
        }
        return {
            isComplete: isComplete,
            msg:msg
        };
    }

    /**
     * 获得MediaX合成数据
     */
    getMediaXData(activeSeq, catalog){
        let width = catalog.width;
        let height = catalog.height;
        let mediaData = {
            InPoint:activeSeq.inPoint,
            OutPoint:activeSeq.outPoint,
            TLFormat:{
                fps: 25,
                width: width,
                height: height
            },
            Tracks:[],
            Type:'PostersVideo',
            Version:1
        }
        
        for (let i = 0; i < activeSeq.tracks.length; i++) {
            let track = activeSeq.tracks[i];
            let tType = '';
            switch (track.dataType) {
                case DataType.TRACK_VIDEO:
                    tType = 'Video';
                    break;
                case DataType.TRACK_AUDIO:
                    tType = 'Audio';
                    break;
                case DataType.TRACK_CG:
                    tType = 'CG';
                    break;
            }
            let trackObj = {
                "TrackType": tType,
                "Fxs": [],
                "Clips": []
            }
            if (track.dataType2 == 'AudioBg') {
                if (!activeSeq.systemConfig.enabledAudioBg) {
                    continue;
                }
                let clip = null;
                if (track.clips.length > 0) {
                    clip = track.clips[0];
                    if(clip.path == ''){
                        continue;
                    }
                }
                if (clip) {
                    let num = Math.ceil((clip.trackOut - clip.trackIn) / clip.duration);
                    let st = clip.trackIn;
                    for (let a = 0; a < num; a++) {
                        let clipObj = {
                            Gain: Math.floor((1-clip.volume)*-99),
                            TrackIn: st,
                            TrackOut: Math.min(st + clip.duration, clip.trackOut),
                            TrimIn: 0,
                            TrimOut: clip.duration,
                            path: this.checkPath(clip.path),
                        }
                        if(num == 1){
                            clipObj.TrimOut = clipObj.TrackOut-clipObj.TrackIn;
                        }
                        st += clip.duration;
                        trackObj.Clips.push(clipObj);
                    }
                    if (clip.effect.fadeIn > 0) {
                        trackObj.Fxs.push({
                            FxType: 'FadeIn',
                            StartPoint: 0,
                            Params: {
                                length: clip.effect.fadeIn
                            }
                        })
                    }
                    if (clip.effect.fadeOut > 0) {
                        trackObj.Fxs.push({
                            FxType: 'FadeOut',
                            StartPoint: activeSeq.outPoint - clip.effect.fadeOut,
                            Params: {
                                length: clip.effect.fadeOut
                            }
                        })
                    }
                }
            } else if (track.dataType2 == 'Water') {
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = this.getMediaXClip(activeSeq,clip);
                    if (clipObj) {
                        mediaData.Tracks.push({
                            TrackType: 'CG',
                            Clips: [clipObj]
                        })
                    }
                }
                continue;
            }else if (track.dataType2 == 'Multilayer') {
                const mediaXTempClips = [];
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = this.getMediaXClip(activeSeq,clip);
                    if (clipObj) {
                        mediaXTempClips.push(clipObj);
                    }
                }
                //以开始时间排序
                mediaXTempClips.sort((a,b)=>{
                    return a.TrackIn>b.TrackIn?1:-1;
                })
                const temptracks = [];
                mediaXTempClips.forEach(clip=>{
                    let isFind = false;
                    for(let i=0;i<temptracks.length;i++){
                        let _temptrack = temptracks[i];
                        if(clip.TrackIn >= _temptrack.endDur){
                            _temptrack.clips.push(clip);
                            _temptrack.endDur = clip.TrackOut;
                            isFind = true;
                            break;
                        }
                    }
                    if(!isFind){
                        temptracks.push({
                            endDur:clip.TrackOut,
                            clips:[clip]
                        });
                    }
                })
                // console.log('mediaXTempClips',mediaXTempClips,temptracks)
                temptracks.forEach(_track=>{
                    mediaData.Tracks.push({
                        TrackType: 'CG',
                        Clips: _track.clips
                    })
                })
                continue;
            }else if (track.dataType2 == 'SoundEffect') {
                const mediaXTempClips = [];
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = this.getMediaXClip(activeSeq,clip);
                    if (clipObj) {
                        mediaXTempClips.push(clipObj);
                    }
                }
                //以开始时间排序
                mediaXTempClips.sort((a,b)=>{
                    return a.TrackIn>b.TrackIn?1:-1;
                })
                const temptracks = [];
                mediaXTempClips.forEach(clip=>{
                    let isFind = false;
                    for(let i=0;i<temptracks.length;i++){
                        let _temptrack = temptracks[i];
                        if(clip.TrackIn >= _temptrack.endDur){
                            _temptrack.clips.push(clip);
                            _temptrack.endDur = clip.TrackOut;
                            isFind = true;
                            break;
                        }
                    }
                    if(!isFind){
                        temptracks.push({
                            endDur:clip.TrackOut,
                            clips:[clip]
                        });
                    }
                })
                // console.log('mediaXTempClips',mediaXTempClips,temptracks)
                temptracks.forEach(_track=>{
                    mediaData.Tracks.push({
                        TrackType: 'Audio',
                        Clips: _track.clips
                    })
                })
                continue;
            }else {
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = this.getMediaXClip(activeSeq,clip);
                    if (clipObj) {
                        trackObj.Clips.push(clipObj);
                    }
                }
            }
            mediaData.Tracks.push(trackObj);
        }
        return mediaData;
    }

    getMediaXClip(activeSeq,clip){
        let clipObj = null;
        let whs = activeSeq.preset.width / activeSeq.preset.height;
        if (clip.dataType === DataType.CLIP_CG_TEXT) {
            clipObj = {
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                Text: {
                    Font: {
                        AlignType: clip.styles.textAlign,
                        Bound: clip.styles.isBold,
                        FontColor: clip.styles.color,
                        FontName: clip.styles.fontFamily,
                        FontSize: clip.styles.fontSize/100,
                        Horizontal: true,
                        Italic: clip.styles.isItalic,
                        UnderLine: clip.styles.isUnderline
                    },
                    Pos: {
                        x: clip.pos.x,
                        y: clip.pos.y
                    },
                    content: clip.content
                }
            }
        }else if (clip.dataType === DataType.CLIP_CG_TEXT_DYNAMIC || clip.dataType === DataType.CLIP_CG_LIBRETTO_DYNAMIC) {
            let configObj = {
                width:clip.width,
                height:clip.height,
                duration:clip.duration,
                inAnimDur: clip.inAnimDur,
                outAnimDur: clip.outAnimDur,
                contents: clip.contents,
                config: clip.config
            }
            let AnimationConfig = window.btoa(encodeURIComponent(JSON.stringify(configObj)));
            clipObj = {
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                Animal: {
                    Path: clip.path,
                    ScaleX: activeSeq.preset.width /clip.width,
                    ScaleY: activeSeq.preset.height /clip.height,
                    Width:clip.width,
                    Height:clip.height,
                    AnimationConfig:AnimationConfig,
                    Duration:clip.duration,
                    Pos: {
                        x: 0,
                        y: 0,
                        Width: 1,
                        Height: 1
                    }
                }
            }
        } else if (clip.dataType === DataType.CLIP_CG_TGA  || clip.dataType === DataType.CLIP_CG_IMAGE_WEBP || clip.dataType === DataType.CLIP_CG_IMAGE_GIF) {
            clipObj = {
                // ClipId: clip.modelId,
                // ClipType: 'Image_Tga',
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                Animal: {
                    Path: this.checkPath(clip.path),
                    ScaleX: activeSeq.preset.width * clip.pos.width / clip.imageWidth,
                    ScaleY: activeSeq.preset.height * clip.pos.height / clip.imageHeight,
                    Pos: {
                        x: clip.pos.x,
                        y: clip.pos.y,
                        Width: clip.pos.width,
                        Height: clip.pos.height
                    }
                },
            }
        } else if (clip.dataType === DataType.CLIP_CG_IMAGE) {
            clipObj = {
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                // Width: clip.imageWidth,
                // Height: clip.imageHeight,
                Picture: {
                    Path: this.checkPath(clip.path),
                    Scale: 1.0,
                    Pos: {
                        x: clip.pos.x,
                        y: clip.pos.y,
                        Width: clip.pos.width,
                        Height: clip.pos.height
                    }
                }
            }
        } else if (clip.dataType === DataType.CLIP_VIDEO) {
            clipObj = {
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                Duration: clip.duration,
                TrimIn: clip.inPoint,
                TrimOut: clip.outPoint,
                // Width: clip.imageWidth,
                // Height: clip.imageHeight,
                path: this.checkPath(clip.path),
                ImageMode: String(clip.scaleType),
                bMute: clip.isMute,
                FX: this.getVideoInsideEffect(clip, whs,true),
                TransFX: this.getVideoTransEffect(clip.effectTrans)
            }
        } else if (clip.dataType === DataType.CLIP_VIDEO_IMAGE) {
            clipObj = {
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                // Width: clip.imageWidth,
                // Height: clip.imageHeight,
                path: this.checkPath(clip.path),
                ImageMode: String(clip.scaleType),
                bMute: true,
                FX: this.getVideoInsideEffect(clip, whs,true),
                TransFX: this.getVideoTransEffect(clip.effectTrans)
            }
        } else if (clip.dataType === DataType.CLIP_AUDIO) {
            clipObj = {
                // ClipId: clip.modelId,
                // ClipType: 'Audio',
                Gain: Math.floor((1-clip.volume)*-99),
                TrackIn: clip.trackIn,
                TrackOut: clip.trackOut,
                TrimIn: 0,
                TrimOut: clip.duration,
                path: this.checkPath(clip.path)
            }
        }
        return clipObj;
    }

    checkPath(path){
        let newPath = path;
        try{
            newPath = window.atob(path);
        }catch(error){

        }
        return newPath;
    }


    getRenderTemplate(activeSeq, catalog) {
        let width = catalog.width;
        let height = catalog.height;
        const rawtlData = activeSeq.rawtlData;
        const tData = {
            Type: rawtlData.Type,
            Version: rawtlData.Version,
            TemplateId: rawtlData.TemplateId,
            InPoint: activeSeq.inPoint,
            OutPoint: activeSeq.outPoint,
            TLFormat: {
                "Width": width,
                "Height": height,
                "FPS": 25
            },
            HostPath: {
                FileHost: '',
                HttpHost: '',
                HttpRelativePath: '',
                FileRelativePath: ''
            },
            Materials: [],
            Tracks: []
        }
        let whs = activeSeq.preset.width / activeSeq.preset.height;
        let waterMarks = [];
        let waterMarkIndex = 0;
        for (let i = 0; i < activeSeq.tracks.length; i++) {
            let track = activeSeq.tracks[i];
            let tType = '';
            switch (track.dataType) {
                case DataType.TRACK_VIDEO:
                    tType = 'V';
                    break;
                case DataType.TRACK_AUDIO:
                    tType = 'A';
                    break;
            }
            let trackObj = {
                "TrackType": track.rawTrackData.TrackType,
                "TrackType2": track.rawTrackData.TrackType2,
                "TrackNum": "",
                "Fxs": [],
                "Clips": []
            }
            if (track.dataType2 == 'AudioBg') {
                // if (!activeSeq.systemConfig.enabledAudioBg) {
                //     continue;
                // }
                let clip = null;
                if (track.clips.length > 0) {
                    clip = track.clips[0];
                }
                if (clip) {
                    let num = Math.ceil((clip.trackOut - clip.trackIn) / clip.duration);
                    let st = clip.trackIn;
                    for (let a = 0; a < num; a++) {
                        let clipObj = {
                            ClipId: Utils.GUID(),
                            ClipType: 'Audio',
                            Volume: clip.volume,
                            Duration: clip.duration,
                            TrackIn: st,
                            TrackOut: Math.min(st + clip.duration, clip.trackOut),
                            TrimIn: 0,
                            TrimOut: clip.duration,
                            HighPath: clip.path,
                        }
                        st += clip.duration;
                        trackObj.Clips.push(clipObj);
                    }
                    if (clip.effect.fadeIn > 0) {
                        trackObj.Fxs.push({
                            FxType: 'FadeIn',
                            Params: {
                                length: clip.effect.fadeIn
                            }
                        })
                    }
                    if (clip.effect.fadeOut > 0) {
                        trackObj.Fxs.push({
                            FxType: 'FadeOut',
                            Params: {
                                length: clip.effect.fadeOut
                            }
                        })
                    }
                }
            } else if (track.dataType2 == 'Water') {
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = null; console.log(clip)
                    if (clip.dataType === DataType.CLIP_CG_IMAGE) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Image',
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Width: clip.imageWidth,
                            Height: clip.imageHeight,
                            HighPath: clip.path,
                            Pos: {
                                x: clip.pos.x,
                                y: clip.pos.y,
                                width: clip.pos.width,
                                height: clip.pos.height
                            }
                        }
                    }
                    if (clipObj) {
                        tData.Tracks.push({
                            "TrackType": track.rawTrackData.TrackType,
                            "TrackType2": track.rawTrackData.TrackType2,
                            "TrackNum": "",
                            "Clips": [clipObj]
                        })
                    }
                }
                continue;
            }
            else {
                for (let j = 0; j < track.clips.length; j++) {
                    let clip = track.clips[j];
                    let clipObj = null;
                    if (clip.dataType === DataType.CLIP_CG_TEXT) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Text',
                            Content: clip.content,
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Pos: {
                                x: clip.pos.x,
                                y: clip.pos.y
                            },
                            Style: clip.styles
                        }
                    } else if (clip.dataType === DataType.CLIP_VIDEO) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Video',
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Duration: clip.duration,
                            TrimIn: clip.inPoint,
                            TrimOut: clip.outPoint,
                            Width: clip.imageWidth,
                            Height: clip.imageHeight,
                            HighPath: clip.path,
                            ImageMode: String(clip.scaleType),
                            Mute: clip.isMute,
                            SameId: "",
                            Fxs: this.getVideoInsideEffect(clip, whs),
                            TransFX: this.getVideoTransEffect(clip.effectTrans)
                        }
                    } else if (clip.dataType === DataType.CLIP_VIDEO_IMAGE) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Image',
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Width: clip.imageWidth,
                            Height: clip.imageHeight,
                            HighPath: clip.path,
                            ImageMode: String(clip.scaleType),
                            Mute: true,
                            SameId: "",
                            Fxs: this.getVideoInsideEffect(clip, whs),
                            TransFX: this.getVideoTransEffect(clip.effectTrans)
                        }
                    } else if (clip.dataType === DataType.CLIP_CG_IMAGE) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Image',
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Width: clip.imageWidth,
                            Height: clip.imageHeight,
                            HighPath: clip.path,
                            Pos: {
                                x: clip.pos.x,
                                y: clip.pos.y,
                                width: clip.pos.width,
                                height: clip.pos.height
                            }
                        }
                    } else if (clip.dataType === DataType.CLIP_CG_TGA || clip.dataType === DataType.CLIP_CG_IMAGE_WEBP || clip.dataType === DataType.CLIP_CG_IMAGE_GIF) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Image_Tga',
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            Width: clip.imageWidth,
                            Height: clip.imageHeight,
                            HighPath: clip.path,
                            Pos: {
                                x: clip.pos.x,
                                y: clip.pos.y,
                                width: clip.pos.width,
                                height: clip.pos.height
                            },
                            ScaleX: activeSeq.preset.width * clip.pos.width / clip.imageWidth,
                            ScaleY: activeSeq.preset.height * clip.pos.height / clip.imageHeight,
                        }
                    } else if (clip.dataType === DataType.CLIP_AUDIO) {
                        clipObj = {
                            ClipId: clip.modelId,
                            ClipType: 'Audio',
                            Volume: clip.volume,
                            TrackIn: clip.trackIn,
                            TrackOut: clip.trackOut,
                            TrimIn: 0,
                            TrimOut: clip.duration,
                            Duration: clip.duration,
                            HighPath: clip.path,
                        }

                    }
                    if (clipObj) {
                        trackObj.Clips.push(clipObj);
                    }
                }
            }
            tData.Tracks.push(trackObj);
        }
        return tData;
    }

    /** 获得视频内部特技 */
    getVideoInsideEffect(clip, whs,isNew=false) {
        const effects = clip.effects;
        const arr = [];
        for (let i = 0; i < effects.length; i++) {
            let eff = effects[i];
            let effObj = {
                FxType: '',
                Params: []
            }
            for (let obj of eff.keyFrameParameters) {
                effObj.Params.push({
                    pos: obj.pos-clip.inPoint,
                    keyParam: JSON.parse(JSON.stringify(obj.parameter)),
                })
            }
            if (eff.dataType === DataType.EFFECT_2D) {
                effObj.FxType = '2D';
            } else if (eff.dataType === DataType.EFFECT_BLUR) {
                effObj.FxType = 'Blur';
            } else if (eff.dataType === DataType.EFFECT_COLOR) {
                effObj.FxType = 'Color';
            } else if (eff.dataType === DataType.EFFECT_COLORCORRECTOR1) {
                effObj.FxType = 'ColorCorrector1';
            } else if (eff.dataType === DataType.EFFECT_FADEIN) {
                effObj.FxType = 'FadeIn';
            } else if (eff.dataType === DataType.EFFECT_FADEOUT) {
                effObj.FxType = 'FadeOut';
            } else if (eff.dataType === DataType.EFFECT_ALPHA) {
                effObj.FxType = 'Alpha';
            } else if (eff.dataType === DataType.EFFECT_GAUSSBLUR) {
                // effObj.FxType = 'GaussBlur';
                effObj.FxType = 'FastGaussBlur';
                let v1 = eff.keyFrameParameters[0].parameter.x;
                // let v2 = 0;
                // if (v1 <= 0.5) {
                //     v2 = v1 * 2 * 0.025;
                // } else if (v1 <= 0.8) {
                //     v2 = (v1 - 0.5) * (1 / (0.8 - 0.5)) * (0.075 - 0.025) + 0.025;
                // } else if (v1 <= 0.9) {
                //     v2 = (v1 - 0.8) * (1 / (0.9 - 0.8)) * (0.125 - 0.075) + 0.075;
                // } else {
                //     v2 = (v1 - 0.9) * (1 / (1 - 0.9)) * (1 - 0.125) + 0.125;
                // }
                // v2 *= whs;
                // v2 = Math.min(1, v2);

                for (let obj of effObj.Params) {
                    obj.keyParam.fBlur = v1;
                    // obj.keyParam.y = v2;
                }
                // effObj.Params.x = v2;
                // effObj.Params.y = v2;
            } else if (eff.dataType === DataType.EFFECT_MONOCHROME) {
                effObj.FxType = 'Monochrome';
            }else if(eff.dataType === DataType.EFFECT_FINDEDGES){
                effObj.FxType = 'FindEdges';
            }else if(eff.dataType === DataType.EFFECT_MIRROR){
                effObj.FxType = 'Mirror';
            }else if(eff.dataType === DataType.EFFECT_MULTIVIEW){
                effObj.FxType = 'MultiView';
            }else if(eff.dataType === DataType.EFFECT_MOSAIC){
                effObj.FxType = 'Mosaic';
            }else if(eff.dataType === DataType.EFFECT_BGBLUR){
                effObj.FxType = 'TemplateBlur';
            }else if(eff.dataType === DataType.EFFECT_SCROLLING){
                effObj.FxType = 'Scrolling';
            }else if(eff.dataType === DataType.EFFECT_OUTOFBODYSOUL){
                effObj.FxType = 'OutOfBodySoul';
            }else if(eff.dataType === DataType.EFFECT_SWING){
                effObj.FxType = 'Swing';
            }else if(eff.dataType === DataType.EFFECT_SWING2){
                effObj.FxType = 'Swing2';
            }else if(eff.dataType === DataType.EFFECT_PARTICLEBLUR){
                effObj.FxType = 'ParticleBlur';
            }else if(eff.dataType === DataType.EFFECT_LENSBLUR){
                effObj.FxType = 'LensBlur';
            }else if(eff.dataType === DataType.EFFECT_SCREENRHYTHM){
                effObj.FxType = 'ScreenRhythm';
            }else if(eff.dataType === DataType.EFFECT_RIPPLEDISTORTION){
                effObj.FxType = 'RippleDistortion';
            }else if(eff.dataType === DataType.EFFECT_HEARTBEAT){
                effObj.FxType = 'HeartBeat';
            }else if(eff.dataType === DataType.EFFECT_NENOSWING){
                effObj.FxType = 'NenoSwing';
            }else if(eff.dataType === DataType.EFFECT_RAINRIPPLE){
                effObj.FxType = 'RainRipple';
            }else if(eff.dataType === DataType.EFFECT_RAINCURTAIN){
                effObj.FxType = 'RainCurtain';
            }else if(eff.dataType === DataType.EFFECT_PHANTOMFAILURE){
                effObj.FxType = 'PhantomFailure';
            }else if(eff.dataType === DataType.EFFECT_CHROMATISMDITHERING){
                effObj.FxType = 'ChromatismDithering';
            }else if(eff.dataType === DataType.EFFECT_SHAKE){
                effObj.FxType = 'Shake';
            }else if(eff.dataType === DataType.EFFECT_INSTANTANEOUSBLUR){
                effObj.FxType = 'InstantaneousBlur';
            }else if(eff.dataType === DataType.EFFECT_SKINNEEDLING){
                effObj.FxType = 'SkinNeedling';
            }else if(eff.dataType === DataType.EFFECT_SIGNALDISTORTION){
                effObj.FxType = 'SignalDistortion';
            }else if(eff.dataType === DataType.EFFECT_SIGNALDISTORTION2){
                effObj.FxType = 'SignalDistortion2';
            }else if(eff.dataType === DataType.EFFECT_SIGNALDISTORTION3){
                effObj.FxType = 'SignalDistortion3';
            }else if(eff.dataType === DataType.EFFECT_SNOW){
                effObj.FxType = 'Snow';
            }else if(eff.dataType === DataType.EFFECT_FIREWORKS){
                effObj.FxType = 'Fireworks';
            }else if(eff.dataType === DataType.EFFECT_FIREWORKS2){
                effObj.FxType = 'Fireworks2';
            }else if(eff.dataType === DataType.EFFECT_FIREWORKS3){
                effObj.FxType = 'Fireworks3';
            }else if(eff.dataType === DataType.EFFECT_FIREWORKS4){
                effObj.FxType = 'Fireworks4';
            }else if(eff.dataType === DataType.EFFECT_OLDMOVIES){
                effObj.FxType = 'OldMovies';
            }else if(eff.dataType === DataType.EFFECT_OLDMOVIES2){
                effObj.FxType = 'OldMovies2';
            }else if(eff.dataType === DataType.EFFECT_OLDMOVIES3){
                effObj.FxType = 'OldMovies3';
            }else if(eff.dataType === DataType.EFFECT_OLDMOVIES4){
                effObj.FxType = 'OldMovies4';
            }else if(eff.dataType === DataType.EFFECT_OLDMOVIES5){
                effObj.FxType = 'OldMovies5';
            }
            if(isNew){
                arr.push({
                    FxName: effObj.FxType,
                    Param: effObj.Params
                });
            }else{
                arr.push(effObj);
            }
        }
        return arr;
    }

    /** 获得视频转场特技 */
    getVideoTransEffect(transData) {
        if (!transData) return null;
        this.TRANS_FADE = 'trans_fade';
        this.TRANS_GLOW = 'trans_glow';
        this.TRANS_DIFFUSEWIPE = 'trans_DiffuseWipe';
        this.TRANS_TRANSLATION = 'trans_translation';
        this.TRANS_SPLIT = 'trans_split';
        this.TRANS_PUSH = 'trans_push';
        this.TRANS_COMPRESS2 = 'trans_compress2';
        this.TRANS_RECT = 'trans_rect';
        this.TRANS_RIPPLE = 'trans_ripple';

        const trans = {
            TransFXType: "",
            Duration: transData.duration,
            Params: transData.parameters
        }
        if (transData.dataType === DataType.TRANS_FADE) {
            trans.TransFXType = 'Fade';
        } else if (transData.dataType === DataType.TRANS_GLOW) {
            trans.TransFXType = 'Glow';
        } else if (transData.dataType === DataType.TRANS_DIFFUSEWIPE) {
            trans.TransFXType = 'DiffuseWipe';
        } else if (transData.dataType === DataType.TRANS_TRANSLATION) {
            trans.TransFXType = 'Translation';
        } else if (transData.dataType === DataType.TRANS_SPLIT) {
            trans.TransFXType = 'Split';
        } else if (transData.dataType === DataType.TRANS_PUSH) {
            trans.TransFXType = 'Push';
        } else if (transData.dataType === DataType.TRANS_COMPRESS2) {
            trans.TransFXType = 'Compress2';
        } else if (transData.dataType === DataType.TRANS_RECT) {
            trans.TransFXType = 'Rect';
        } else if (transData.dataType === DataType.TRANS_RIPPLE) {
            trans.TransFXType = 'Ripple';
        }else if(transData.dataType === DataType.TRANS_WIND){
            trans.TransFXType = 'Wind';
        }else if(transData.dataType === DataType.TRANS_SWAP){
            trans.TransFXType = 'Swap';
        }else if(transData.dataType === DataType.TRANS_SQUEEZE){
            trans.TransFXType = 'Squeeze';
        }else if(transData.dataType === DataType.TRANS_SQUARESWIRE){
            trans.TransFXType = 'SquareSwire';
        }else if(transData.dataType === DataType.TRANS_ROTATESCALEFADE){
            trans.TransFXType = 'RotateScaleFade';
        }else if(transData.dataType === DataType.TRANS_RANDOMSQUARES){
            trans.TransFXType = 'RandomSquares';
        }else if(transData.dataType === DataType.TRANS_POLARFUNCTION){
            trans.TransFXType = 'PolarFunction';
        }else if(transData.dataType === DataType.TRANS_PIXELIZE){
            trans.TransFXType = 'Pixelize';
        }else if(transData.dataType === DataType.TRANS_MULTIPLYBLEND){
            trans.TransFXType = 'MultiplyBlend';
        }else if(transData.dataType === DataType.TRANS_HEART){
            trans.TransFXType = 'Heart';
        }else if(transData.dataType === DataType.TRANS_FLYEYE){
            trans.TransFXType = 'FlyEye';
        }else if(transData.dataType === DataType.TRANS_FADEGRAYSCALE){
            trans.TransFXType = 'FadeGrayScale';
        }else if(transData.dataType === DataType.TRANS_FADECOLOR){
            trans.TransFXType = 'FadeColor';
        }else if(transData.dataType === DataType.TRANS_DOORWAY){
            trans.TransFXType = 'Doorway';
        }else if(transData.dataType === DataType.TRANS_DIRECTIONALWIPE){
            trans.TransFXType = 'DirectionalWipe';
        }else if(transData.dataType === DataType.TRANS_CUBE){
            trans.TransFXType = 'Cube';
        }else if(transData.dataType === DataType.TRANS_CROSSWARP){
            trans.TransFXType = 'CrossWarp';
        }else if(transData.dataType === DataType.TRANS_COLORPHASE){
            trans.TransFXType = 'ColorPhase';
        }else if(transData.dataType === DataType.TRANS_CIRCLEOPEN){
            trans.TransFXType = 'CircleOpen';
        }else if(transData.dataType === DataType.TRANS_CIRCLE){
            trans.TransFXType = 'Circle';
        }else if(transData.dataType === DataType.TRANS_BURN){
            trans.TransFXType = 'Burn';
        }else if(transData.dataType === DataType.TRANS_ANGULAR){
            trans.TransFXType = 'Angular';
        }else if(transData.dataType === DataType.TRANS_PINWHEEL){
            trans.TransFXType = 'Pinwheel';
        }else if(transData.dataType === DataType.TRANS_DOOMSCREENTRANSITION){
            trans.TransFXType = 'DoomScreen';
        }else if(transData.dataType === DataType.TRANS_DREAMYZOOM){
            trans.TransFXType = 'DreamyZoom';
        }else if(transData.dataType === DataType.TRANS_GLITCHDISPLACE){
            trans.TransFXType = 'GlitchDisplace';
        }else if(transData.dataType === DataType.TRANS_HEXAGONALIZE){
            trans.TransFXType = 'Hexagonalize';
        }else if(transData.dataType === DataType.TRANS_WINDOWBLINDS){
            trans.TransFXType = 'WindowBlinds';
        }else if(transData.dataType === DataType.TRANS_KALEIDOSCOPE){
            trans.TransFXType = 'Kaleidoscope';
        }else if(transData.dataType === DataType.TRANS_BUTTERFLYWAVESCRAWLER){
            trans.TransFXType = 'ButterflyWaveScrawler';
        }
        return trans;
    }

    /**
     * AiToVideo clip 转 微编 clip
     */
    clipToClip(nowClip,nextClip,trackType){
        let clip = nowClip;
        clip.startTime = clip.trackIn/1000;
        clip.endTime = clip.trackOut/1000;
        clip.totalTime = clip.endTime - clip.startTime;
        clip.sourceUrl = clip.previewUrl;
        if(clip.dataType == DataType.CLIP_VIDEO || clip.dataType == DataType.CLIP_AUDIO ){
            clip.inPoint = clip.inPoint/1000;
            clip.outPoint = clip.outPoint/1000;
            clip.duration = clip.duration/1000;
            clip.videoWidth = clip.imageWidth || 0;
            clip.videoHeight = clip.imageHeight || 0;

            if(!clip.carryData){
                clip.carryData = {
                    channels: 2,
                    frameRate: 25,
                    duration: clip.duration,
                    cover: clip.keyFrameUrl,
                    preUrl: clip.previewUrl,
                    path: clip.path,
                    type: clip.dataType == DataType.CLIP_VIDEO?'video':'audio',
                    title: clip.title
                }
            }
            if(!clip.carryData.channels){
                clip.carryData.channels = '2';
            }
            clip.mediaInfo = {
                Channels:parseInt(clip.carryData.channels),
                FrameRate:parseInt(clip.carryData.frameRate)
            };
            //转场
            if(clip.effectTrans && nextClip && nextClip.trackIn < clip.trackOut){
                clip.effectTransOut = clip.effectTrans;
                nextClip.effectTransIn = JSON.parse(JSON.stringify(clip.effectTrans));
                clip.effectTransOut.duration /= 1000;
                nextClip.effectTransIn.duration /= 1000;

                let nextStartTime = nextClip.trackIn/1000;

                clip.effectTransOut.clipId1 = clip.modelId;
                clip.effectTransOut.clipId2 = nextClip.modelId;
                nextClip.effectTransIn.clipId1 = clip.modelId;
                nextClip.effectTransIn.clipId2 = nextClip.modelId;

                let maxTime = (clip.endTime-nextStartTime)-(clip.effectTransIn?clip.effectTransIn.duration:0);
                clip.effectTransOut.maxTime = maxTime;
                nextClip.effectTransIn.maxTime = maxTime;
            }
            delete clip.effectTrans;
            if(trackType == DataType.TRACK_AUDIO){
                clip.dataType = DataType.CLIP_AUDIO;
                if(nextClip && nextClip.trackIn < clip.trackOut ){
                    const transEff = {
                        id: Utils.GUID(),
                        dataType: 'trans_audio_fade',
                        clipId1: clip.modelId,
                        clipId2: nextClip.modelId,
                        name: '溶解',
                        englishName: 'audioFade',
                        duration:  (clip.trackOut - nextClip.trackIn)/1000,
                        maxTime: (clip.trackOut - nextClip.trackIn)/1000,
                        extend:0,
                        parameters:{gain:1}
                    }
                    clip.effectTransOut = transEff;
                    nextClip.effectTransIn = JSON.parse(JSON.stringify(transEff));
                }
                
            }
        }
        if(clip.dataType == DataType.CLIP_CG_TEXT_DYNAMIC){
            clip.carryData = {};
        }
        if(clip.dataType == DataType.CLIP_CG_IMAGE || clip.dataType == DataType.CLIP_CG_IMAGE_WEBP || clip.dataType == DataType.CLIP_CG_IMAGE_GIF){
            clip.highWidth = clip.imageWidth;
            clip.highHeight = clip.imageHeight;
            clip.rawWidth = clip.imageWidth;
            clip.rawHeight = clip.imageHeight;

            clip.x = clip.pos.x;
            clip.y = clip.pos.y;
            clip.width = clip.pos.width;
            clip.height = clip.pos.height;
            if(!clip.carryData){
                clip.carryData = {
                    contentId: Utils.GUID32(),
                    cover: clip.keyFrameUrl,
                    path: clip.path,
                    preUrl: clip.previewUrl,
                    type: 'image',
                    title: clip.title
                }
            }
        }
        if(clip.dataType == DataType.CLIP_VIDEO || clip.dataType == DataType.CLIP_VIDEO_IMAGE){
            for(let i=0;i<clip.effects.length;i++){
                const eff = clip.effects[i];
                if(eff.dataType == DataType.EFFECT_2D && eff.dataType2 == 'crop'){
                    eff.isBuiltIn = true;
                }
                for(let j=0;j<eff.keyFrameParameters.length;j++){
                    eff.keyFrameParameters[j].pos /= 1000;
                }
                delete eff.dataType2;
            }
        }
        delete clip.trackIn;
        delete clip.trackOut;
        delete clip.tlData;
        return clip;
    }

    /** 获得微编时间线 */
    getMicroEditTimeLine(activeSeq){

        const preset = JSON.parse(JSON.stringify(activeSeq.preset));
        //降低预设分辨率
        let presetW = preset.width;
		let willW = presetW/2;
		willW = Math.min(960,willW);
		willW = Math.max(540,willW);
		let willH = willW * preset.height/preset.width;
		preset.width = willW;
        preset.height = willH;

        let scale = Utils.getRatioWH(activeSeq.preset.width,activeSeq.preset.height);
        preset.formatScale = scale;
        preset.labelScale = scale;
        preset.displayLabel = scale;
        preset.videostandard = 'HQY_VideoStandard_'+preset.width+'_'+preset.height+'_25i';


        // {"id":170,"displayLabel":"超清","formatScale":"16:9","videostandard":"HQY_VideoStandard_1920_1080_25i","jsonFormat":"mediaX","policyId":"11129","tenantId":"226ebba143878ba1ce9f2590328045ad","sort":0,"md5":"d74e737681b1e0283a84d61704d1cca0"}

        const outPreset = {
            displayLabel: '超清',
            formatScale: scale,
            videostandard:'HQY_VideoStandard_'+activeSeq.preset.width+'_'+activeSeq.preset.height+'_25i',
            jsonFormat: 'mediaX',
        }
        let obj = {
            name:activeSeq.name,
            id:activeSeq.id,
            sequenceID:activeSeq.sequenceID, //时间线id 后台保存文件时给的
            version:'4.1.0.0',
            inPoint:activeSeq.inPoint/1000,
            outPoint:activeSeq.outPoint/1000,
            preset: preset,
            outPreset: outPreset,
            tracks:[]
        }
        const cgTracks = [],videoTracks = [],audioTracks = [];
        //CG轨
        for(let i=0;i<activeSeq.cgTracks.length;i++){
            const r_track = activeSeq.cgTracks[i];
            if(r_track.dataType2 == 'Multilayer'){
                const tempClips = r_track.clips.slice();
                //以开始时间排序
                tempClips.sort((a,b)=>{
                    return a.trackIn>b.trackIn?1:-1;
                })
                const temptracks = [];
                tempClips.forEach(clip=>{
                    let isFind = false;
                    for(let i=0;i<temptracks.length;i++){
                        let _temptrack = temptracks[i];
                        if(clip.trackIn >= _temptrack.endDur){
                            _temptrack.clips.push(clip);
                            _temptrack.endDur = clip.trackOut;
                            isFind = true;
                            break;
                        }
                    }
                    if(!isFind){
                        temptracks.push({
                            endDur:clip.trackOut,
                            clips:[clip]
                        });
                    }
                })
                temptracks.forEach(_track=>{
                    cgTracks.push({
                        dataType: r_track.dataType,
                        clips: _track.clips
                    })
                })
            }else{
                if(r_track.clips.length){
                    cgTracks.push({
                        dataType: r_track.dataType,
                        clips: r_track.clips
                    })
                }
            }
        }
        //视频轨
        const MainAudioTracks = [
            {
                dataType: DataType.TRACK_AUDIO,
                clips: []
            },
            {
                dataType: DataType.TRACK_AUDIO,
                clips: []
            }
        ]
        for(let i=0;i<activeSeq.videoTracks.length;i++){
            const r_track = activeSeq.videoTracks[i];
            videoTracks.push({
                dataType: r_track.dataType,
                clips: r_track.clips
            })
            if(r_track.dataType2 == 'Main'){
                for(let j=0;j<r_track.clips.length;j++){
                    const tempClip = r_track.clips[j];
                    if(!tempClip.isMute){
                        //这里判断如果Main没有channel，将不创建音轨
                        // console.log('tempClip.dataType2',tempClip.dataType2,tempClip.carryData.channels)
                        if(tempClip.dataType2 == 'Main'){
                            if(tempClip.carryData.channels){
                                MainAudioTracks[0].clips.push(tempClip);
                                MainAudioTracks[1].clips.push(tempClip);
                            }
                        }else{
                            MainAudioTracks[0].clips.push(tempClip);
                            MainAudioTracks[1].clips.push(tempClip);
                        }
                    }
                }
            }
        }
        if(MainAudioTracks[0].clips.length){
            audioTracks.push(MainAudioTracks[0]);
        }
        if(MainAudioTracks[1].clips.length){
            audioTracks.push(MainAudioTracks[1]);
        }
        //音频轨
        for(let i=0;i<activeSeq.audioTracks.length;i++){
            const r_track = activeSeq.audioTracks[i];
            if(r_track.dataType2 == 'SoundEffect'){
                const tempClips = r_track.clips.slice();
                //以开始时间排序
                tempClips.sort((a,b)=>{
                    return a.trackIn>b.trackIn?1:-1;
                })
                const temptracks = [];
                tempClips.forEach(clip=>{
                    let isFind = false;
                    for(let i=0;i<temptracks.length;i++){
                        let _temptrack = temptracks[i];
                        if(clip.trackIn >= _temptrack.endDur){
                            _temptrack.clips.push(clip);
                            _temptrack.endDur = clip.trackOut;
                            isFind = true;
                            break;
                        }
                    }
                    if(!isFind){
                        temptracks.push({
                            endDur:clip.trackOut,
                            clips:[clip]
                        });
                    }
                })
                temptracks.forEach(_track=>{
                    audioTracks.push({
                        dataType: r_track.dataType,
                        clips: _track.clips
                    })
                })
            }else if(r_track.dataType2 == 'AudioBg'){
                if(activeSeq.systemConfig.enabledAudioBg){
                    audioTracks.push({
                        dataType: r_track.dataType,
                        clips: r_track.clips
                    })
                    audioTracks.push({
                        dataType: r_track.dataType,
                        clips: r_track.clips
                    })
                }
            }else{
                if(r_track.clips.length){
                    audioTracks.push({
                        dataType: r_track.dataType,
                        clips: r_track.clips
                    })
                }
            }
        }

        for(let i=0;i<cgTracks.length;i++){
            cgTracks[i].trackName = 'C'+(cgTracks.length-i);
            cgTracks[i].trackIndex = i;
        }
        for(let i=0;i<videoTracks.length;i++){
            videoTracks[i].trackName = 'V'+(videoTracks.length-i);
            videoTracks[i].trackIndex = i;
        }
        for(let i=0;i<audioTracks.length;i++){
            audioTracks[i].trackName = 'A'+(i+1);
            audioTracks[i].trackIndex = i;
        }
        // console.log('audioTracks',audioTracks)
        const tracks = cgTracks.concat(videoTracks).concat(audioTracks);
        for(let i=0;i<tracks.length;i++){
            let _track = tracks[i];
            //深拷贝
            _track.clips = JSON.parse(JSON.stringify(_track.clips));
            _track.clips.forEach(ele=>{
                ele.modelId = Utils.GUID();
            })
            _track.modelId = Utils.GUID();
            for(let j=0;j<_track.clips.length;j++){
                let clip = _track.clips[j];
                let nextClip = null;
                if(j+1 <= _track.clips.length-1){
                    nextClip = _track.clips[j+1];
                }
                _track.clips[j] = this.clipToClip(clip,nextClip,_track.dataType);
                _track.clips[j].trackId = _track.modelId;
            }

            // if(_track.dataType == DataType.TRACK_VIDEO || _track.dataType == DataType.TRACK_AUDIO){
            //     _track.clips = [];
            // }
        }
        obj.tracks = tracks;
        return obj;
    }
}

export default new CreateRender;