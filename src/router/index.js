import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
// import Login from '../views/login/Login.vue';
import PageCreative from '../views/creative/PageCreative.vue';
import PageDocument from '../views/document/PageDocument.vue';
import RefineDocument from '../views/document/refineDocument/RefineDocument.vue';


import PageTemplate from '../views/template/PageTemplate.vue';
import PageEdit from '../views/edit/PageEdit.vue';
import PageCompose from '../views/compose/PageCompose.vue';
Vue.use(VueRouter)
const modulePath = window.AppConfig.modulePath;
console.log('modulePath',modulePath)
const routes = [
	{
		path: modulePath+'/home',
		name: 'Home',
		// component: Login,
		// redirect: '/login',
	},
	{
		path: modulePath+'/creative',
		name: 'creative',
		component: PageCreative,
		meta: {
		    keepAlive: false 
		}
		// redirect:'/login',
	},
	{
		path: modulePath+'/creative',
		name: 'creative',
		component: PageCreative,
		meta: {
		    keepAlive: false 
		}
		// redirect:'/login',
	},
	{
		path: modulePath+'/document',
		name: 'document',
		component: PageDocument,
		meta: {
		    keepAlive: false 
		}
		// redirect:'/login',
	},{
		path: modulePath+'/template',
		name: 'template',
		component: PageTemplate,
		meta: {
		    keepAlive: false 
		}
		// redirect:'/login',
	}, {
		path: modulePath+'/edit',
		name: 'edit',
		component: PageEdit,
		meta: {
		    keepAlive: false // 需要被缓存
		}
		// meta: {
		//     keepAlive: false
		// }
		// redirect:'/login',
	}, {
		path: modulePath+'/compose',
		name: 'compose',
		component: PageCompose,
		meta: {
		    keepAlive: false
		}
		// meta: {
		//     keepAlive: false
		// }
		// redirect:'/login',
	},{
		path: modulePath+'/refine',
		name: 'refine',
		component: RefineDocument,
		meta: {
		    keepAlive: true 
		}
		// redirect:'/login',
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
