/**
 * 视频播放器
 */

import MediaPlayer from "./MediaPlayer";

class VideoPlayer extends MediaPlayer{
    constructor(data,gl){
        super(data,gl);
        let self = this;
        this.media = document.createElement('video');

        this.video = this.media;
        this.video.crossOrigin = "*";
        this.video.src = this.data.previewUrl;
        this.video.muted = this.data.isMute;
        this.isFirst = true;

        this.video.addEventListener("loadedmetadata",loadedmetadataHandler);
        this.video.addEventListener("durationchange",durationchangeHandler);
        this.video.addEventListener("canplaythrough",canplaythroughHandler);
        this.video.addEventListener("error",errorHandler);
        this.video.addEventListener("play",playHandler);
        this.video.addEventListener("playing",playingHandler);
        this.video.addEventListener("waiting",waitingHandler);
        this.video.addEventListener("pause",pauseHandler);
        this.video.addEventListener("ended",endedHandler);
        this.video.addEventListener("timeupdate",timeupdateHandler);
        this.video.addEventListener("seeking",seekingHandler);
        this.video.addEventListener("seeked",seekedHandler);


        /*以下为video事件
         ============================================================
         * */
        function loadedmetadataHandler(){
            self.duration = self.video.duration*1000;
            self.imageWidth = self.video.videoWidth;
            self.imageHeight = self.video.videoHeight;
            // console.log("loadedmetadataHandler"+self.video.duration);
        }
        function durationchangeHandler(){
        }
        function canplaythroughHandler(){
            // if(self.seekedBack && self.isFirst){
            //     self.seekedBack(self);
            //     self.isFirst = false;
            // }
            // console.log("可以流畅播放"+self.video.duration);
            //self.loadingImg.style.display = "none";
            if(self.waitingBack){
                self.waitingBack({playerId:self.data.modelId,isWaiting:false});
            }
        }
        function errorHandler(e){
            console.log(e);

        }
        function playHandler(){
        }
        function playingHandler(){
            // if(self.waitingBack){
            //     self.waitingBack({playerId:self.data.modelId,isWaiting:false});
            // }
        }
        function pauseHandler(){
            // if(self.waitingBack){
            //     self.waitingBack({playerId:self.data.modelId,isWaiting:false});
            // }
        }
        function waitingHandler(){
            //需要向上发送缓冲事件
            if(self.waitingBack){
                self.waitingBack({playerId:self.data.modelId,isWaiting:true});
            }
        }
        function endedHandler(){
            
        }
        function timeupdateHandler(){
            
        }
        function seekingHandler(){

        }
        function seekedHandler(){
            if(self.seekedBack){
                self.seekedBack(self);
            }
        }


        this.videodestroy = ()=>{
            this.video.removeEventListener("loadedmetadata",loadedmetadataHandler);
            this.video.removeEventListener("durationchange",durationchangeHandler);
            this.video.removeEventListener("canplaythrough",canplaythroughHandler);
            this.video.removeEventListener("error",errorHandler);
            this.video.removeEventListener("play",playHandler);
            this.video.removeEventListener("playing",playingHandler);
            this.video.removeEventListener("waiting",waitingHandler);
            this.video.removeEventListener("pause",pauseHandler);
            this.video.removeEventListener("ended",endedHandler);
            this.video.removeEventListener("timeupdate",timeupdateHandler);
            this.video.removeEventListener("seeking",seekingHandler);
            this.video.removeEventListener("seeked",seekedHandler);
        }
    }

    /** 刷新数据 */
    flushData (data) {
        super.flushData(data);
        this.video.muted = data.isMute;
    }


    seek(time){
        if(time >= this.data.trackIn && time < this.data.trackOut){
            let willSeekTime = this.data.inPoint+(time-this.data.trackIn) || 1;
            if(this.data.dataType2 == 'VideoBg'){
                let dur = Math.min(this.data.trackOut-this.data.trackIn,this.data.duration);
                if(time > dur){
                    willSeekTime = time%dur;
                }
                // let dur = Math.min(this.data.trackOut-this.data.trackIn,this.data.duration);
                // if(time > 0){
                //     willSeekTime = dur%time;
                //     console.log('willSeekTime',willSeekTime)
                // }
                // console.log(dur,time,time)
                // var willSeekTime1 = dur%time;
                
            }
            // if(this.data.staticFrame){
            //     const params = this.data.staticFrame.wholeParams;
            //     if(params.SingleType === 2){
            //         willSeekTime = params.staticTime;
            //     }else if(params.SingleType === 0){
            //         if(willSeekTime <= params.staticTime){
            //             willSeekTime = params.staticTime;
            //         }
            //         // console.log('willSeekTime',willSeekTime,params.staticTime)
            //     }else if(params.SingleType === 1){
            //         if(willSeekTime >= params.staticTime){
            //             willSeekTime = params.staticTime;
            //         }
            //     }
            // }
            this.video.currentTime = willSeekTime/1000;//this.data.inPoint+(time-this.data.trackIn) || 0.0001;
        }
    }

    /** 处理有静帧下的播放 */
    staticFramePlay = (time) => {
        if(this.data.staticFrame){
            const params = this.data.staticFrame.wholeParams;
            let willCT = this.data.inPoint+(time-this.data.trackIn);
            if(params.SingleType === 2){
                if(!this.video.paused){
                    this.video.pause();
                }
                if(Math.floor(this.video.currentTime*1000) !== Math.floor(params.trackIn)){
                    this.video.currentTime = params.staticTime/1000;
                }
            }else if(params.SingleType === 0){
                if(willCT <= params.staticTime){
                    if(!this.video.paused){
                        this.video.pause();
                    }
                    if(Math.floor(this.video.currentTime*1000) !== Math.floor(params.trackIn)){
                        this.video.currentTime = params.staticTime/1000;
                    }
                }else{
                    if(this.video.paused){
                        this.video.currentTime = (this.data.inPoint+(time-this.data.trackIn))/1000;
                        this.video.play();
                    }
                }
            }else if(params.SingleType === 1){
                if(willCT >= params.staticTime){
                    if(!this.video.paused){
                        this.video.pause();
                    }
                    if(Math.floor(this.video.currentTime*1000) !== Math.floor(params.trackIn)){
                        this.video.currentTime = params.staticTime/1000;
                    }
                }else{
                    if(this.video.paused){
                        this.video.currentTime = (this.data.inPoint+(time-this.data.trackIn))/1000;
                        this.video.play();
                    }
                }
            }
        }
    }
   
    play (){
        this.video.play();
    }

    pause (){
        this.video.pause();
    }

    /** 获得播放器是否处理暂停状态 */
    isPause = () =>{
        return this.video.paused;
    }
    /** 获得播放器当前时间 */
    getCurrentTime = () => {
        return this.video.currentTime*1000;
    }

    destroy(){
        super.destroy();
        this.videodestroy();
        this.video.pause();
        this.video.src = '';
        this.video.load();
        this.isEmptyMemory = true;
        // this.effectManager.uninstallProgramAll();
        // this.operationArea.destroy();
    }
}
export default VideoPlayer;