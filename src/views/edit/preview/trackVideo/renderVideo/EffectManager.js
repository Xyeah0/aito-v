
import EffectBlur from "../effectVideo/effectBlur/EffectBlur";
import Effect2D from "../effectVideo/effect2D/Effect2D";
import EffectAlpha from "../effectVideo/effectAlpha/EffectAlpha";
import EffectColor from "../effectVideo/effectColor/EffectColor";
import EffectColorCorrector1 from "../effectVideo/effectColorCorrector1/EffectColorCorrector1";
import EffectMonochrome from "../effectVideo/effectMonochrome/EffectMonochrome";

import App from 'AppCore';
import DataType from '@/model/DataType';
import EffectGaussblur from "../effectVideo/effectGaussblur/EffectGaussblur";
import EffectFindEdges from "../effectVideo/effectFindEdges/EffectFindEdges";
import EffectMirror from "../effectVideo/effectMirror/EffectMirror";
import EffectMultiView from "../effectVideo/effectMultiView/EffectMultiView";
import EffectMosaic from "../effectVideo/effectMosaic/EffectMosaic";
import EffectBgBlur from "../effectVideo/effectBgBlur/EffectBgBlur";
import EffectScrolling from "../effectVideo/effectScrolling/EffectScrolling";
import EffectOutofbodysoul from "../effectVideo/effectOutofbodysoul/EffectOutofbodysoul";
import EffectSwing from "../effectVideo/effectSwing/EffectSwing";
import EffectSwing2 from "../effectVideo/effectSwing2/EffectSwing2";
import EffectParticleblur from "../effectVideo/effectParticleblur/EffectParticleblur";
import EffectLensblur from "../effectVideo/effectLensblur/EffectLensblur";
import EffectScreenrhythm from "../effectVideo/effectScreenrhythm/EffectScreenrhythm";
import EffectRippledistortion from "../effectVideo/effectRippledistortion/EffectRippledistortion";
import EffectHeartbeat from "../effectVideo/effectHeartbeat/EffectHeartbeat";
import EffectNenoswing from "../effectVideo/effectNenoswing/EffectNenoswing";
import EffectRainRipple from "../effectVideo/effectRainRipple/EffectRainRipple";
import EffectRainCurtain from "../effectVideo/effectRainCurtain/EffectRainCurtain";
import EffectPhantomFailure from "../effectVideo/effectPhantomFailure/EffectPhantomFailure";
import EffectChromatismDithering from "../effectVideo/effectChromatismDithering/EffectChromatismDithering";
import EffectShake from "../effectVideo/effectShake/EffectShake";
import EffectInstantaneousBlur from "../effectVideo/effectInstantaneousBlur/EffectInstantaneousBlur";
import EffectSkinNeedling from "../effectVideo/effectSkinNeedling/EffectSkinNeedling";
import EffectSignalDistortion from "../effectVideo/effectSignalDistortion/EffectSignalDistortion";
import EffectSignalDistortion2 from "../effectVideo/effectSignalDistortion2/EffectSignalDistortion2";
import EffectSignalDistortion3 from "../effectVideo/effectSignalDistortion3/EffectSignalDistortion3";
import EffectSnow from "../effectVideo/effectSnow/EffectSnow";
import EffectFireworks from "../effectVideo/effectFireworks/EffectFireworks";
import EffectFireworks2 from "../effectVideo/effectFireworks2/EffectFireworks2";
import EffectFireworks3 from "../effectVideo/effectFireworks3/EffectFireworks3";
import EffectFireworks4 from "../effectVideo/effectFireworks4/EffectFireworks4";
import EffectOldMovies from "../effectVideo/effectOldMovies/EffectOldMovies";
import EffectOldMovies2 from "../effectVideo/effectOldMovies2/EffectOldMovies2";
import EffectOldMovies3 from "../effectVideo/effectOldMovies3/EffectOldMovies3";
import EffectOldMovies4 from "../effectVideo/effectOldMovies4/EffectOldMovies4";
import EffectOldMovies5 from "../effectVideo/effectOldMovies5/EffectOldMovies5";

const CORECONTROL = App.CONTROL.CORE;
const EFFECTCONTROL = App.CONTROL.EFFECT;

class EffectManager{
    constructor(clip,gl){
        this.clip = clip;//clip;
        this.gl = gl;
        this.effectWholes = [];//包含应用程序及特技对象
        this.init();
        // CoreControl.addEventListener(CoreControl.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_ADD,this.effectAddHandler);
        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_CHANGE,this.effectChangeHandler);
        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_REMOVE,this.effectRemoveHandler);
    }

    init(){
        for(let i=0;i<this.clip.effects.length;i++){
            let effData = this.clip.effects[i];
            this.effectWholes.push({
                effData: effData,
                programEff: this.installProgram(effData)
            })
        }
    }

    effectAddHandler = (obj) =>{
        if(this.clip.modelId === obj.clip.modelId){
            this.addEffect(obj);
        }
    }

    effectChangeHandler = (obj) =>{
        if(this.clip.modelId === obj.clip.modelId){
            this.changeEffect(obj);
        }
    }

    effectRemoveHandler = (obj) =>{
        if(this.clip.modelId === obj.clip.modelId){
            this.removeEffect(obj);
        }
    }

    /** 新增特技 */
    addEffect(obj){
        let effData = obj.effect;
        this.effectWholes.push({
            effData: effData,
            programEff: this.installProgram(effData)
        })
    }

    /** 更新特技 */
    changeEffect(obj){
        let effData = obj.effect;
        for(let j=0;j<this.effectWholes.length;j++){
            let effWhole = this.effectWholes[j];
            if(effWhole.effData.id === effData.id){
                effWhole.effData = effData;
                break;
            }
        }
    }

    /** 删除特技 */
    removeEffect(obj){
        let effData = obj.effect;
        for(let j=0;j<this.effectWholes.length;j++){
            let effWhole = this.effectWholes[j];
            if(effWhole.effData.id === effData.id){
                this.uninstallProgram(effWhole);
                this.effectWholes.splice(j,1);
                break;
            }
        }
    }

    /** 刷新数据 */
    flushData(clip){
        this.clip = clip;
        // let effects = this.clip.effects;
        // //从数据中查找到特技综合对象，若查找到，则更新，否则新建
        // for(let i=0;i<effects.length;i++){
        //     let effData = effects[i];
        //     let cureffWhole = null;
        //     for(let j=0;j<this.effectWholes.length;j++){
        //         let effWhole = this.effectWholes[j];
        //         if(effWhole.effData.id === effData.id){
        //             cureffWhole = effWhole;
        //             break;
        //         }
        //     }
        //     if(cureffWhole){
        //         cureffWhole.effData = effData;
        //     }else{
        //         this.effectWholes.push({
        //             effData: effData,
        //             programEff: this.installProgram(effData)
        //         })
        //         // console.log(this.effectWholes)
        //     }
        // }
        // //从特技综合例表中查找数据，若未查找到，则删除
        // for(let i=0;i<this.effectWholes.length;i++){
        //     let effWhole = this.effectWholes[i];
        //     let isFind = false;
        //     for(let j=0;j<effects.length;j++){
        //         let effData = effects[j];
        //         if(effData.id === effWhole.effData.id){
        //             isFind = true;
        //             break;
        //         }
        //     }
        //     if(!isFind){
        //         this.uninstallProgram(effWhole);
        //         this.effectWholes.splice(i,1);
        //         i--;
        //     }
        // }
    }

    /** 撤销和重做完成 */
    undoOrRedoFinishHandler = () =>{
        let effects = this.clip.effects;
        //从数据中查找到特技综合对象，若查找到，则更新，否则新建
        for(let i=0;i<effects.length;i++){
            let effData = effects[i];
            let cureffWhole = null;
            for(let j=0;j<this.effectWholes.length;j++){
                let effWhole = this.effectWholes[j];
                if(effWhole.effData.id === effData.id){
                    cureffWhole = effWhole;
                    break;
                }
            }
            if(cureffWhole){
                cureffWhole.effData = effData;
            }else{
                this.effectWholes.push({
                    effData: effData,
                    programEff: this.installProgram(effData)
                })
                // console.log(this.effectWholes)
            }
        }
        //从特技综合例表中查找数据，若未查找到，则删除
        for(let i=0;i<this.effectWholes.length;i++){
            let effWhole = this.effectWholes[i];
            let isFind = false;
            for(let j=0;j<effects.length;j++){
                let effData = effects[j];
                if(effData.id === effWhole.effData.id){
                    isFind = true;
                    break;
                }
            }
            if(!isFind){
                this.uninstallProgram(effWhole);
                this.effectWholes.splice(i,1);
                i--;
            }
        }
    }

    /** 创建webgl应用程序 */
    installProgram (effData){
        let programEff = null;
        switch(effData.dataType){
            case DataType.EFFECT_BLUR:
                programEff = new EffectBlur();
                break;
            case DataType.EFFECT_2D:
                programEff = new Effect2D();
                break;
            case DataType.EFFECT_ALPHA:
            case DataType.EFFECT_FADEIN:
            case DataType.EFFECT_FADEOUT:
                programEff = new EffectAlpha();
                break;
            case DataType.EFFECT_COLOR:
                programEff = new EffectColor();
                break;
            case DataType.EFFECT_COLORCORRECTOR1:
                programEff = new EffectColorCorrector1();
                break;
            case DataType.EFFECT_MONOCHROME:
                programEff = new EffectMonochrome();
                break;
            case DataType.EFFECT_GAUSSBLUR:
                programEff = new EffectGaussblur();
                break;
            case DataType.EFFECT_FINDEDGES:
                programEff = new EffectFindEdges();
                break;
            case DataType.EFFECT_MIRROR:
                programEff = new EffectMirror();
                break;
            case DataType.EFFECT_MULTIVIEW:
                programEff = new EffectMultiView();
                break;
            case DataType.EFFECT_MOSAIC:
                programEff = new EffectMosaic();
                break;
            case DataType.EFFECT_BGBLUR:
                programEff = new EffectBgBlur();
                break;
            case DataType.EFFECT_SCROLLING:
                programEff = new EffectScrolling();
                break;
            case DataType.EFFECT_OUTOFBODYSOUL:
                programEff = new EffectOutofbodysoul();
                break;
            case DataType.EFFECT_SWING:
                programEff = new EffectSwing();
                break;
            case DataType.EFFECT_SWING2:
                programEff = new EffectSwing2();
                break;
            case DataType.EFFECT_PARTICLEBLUR:
                programEff = new EffectParticleblur();
                break;
            case DataType.EFFECT_LENSBLUR:
                programEff = new EffectLensblur();
                break;
            case DataType.EFFECT_SCREENRHYTHM:
                programEff = new EffectScreenrhythm();
                break;
            case DataType.EFFECT_RIPPLEDISTORTION:
                programEff = new EffectRippledistortion();
                break;
            case DataType.EFFECT_HEARTBEAT:
                programEff = new EffectHeartbeat();
                break;
            case DataType.EFFECT_NENOSWING:
                programEff = new EffectNenoswing();
                break;
            case DataType.EFFECT_RAINRIPPLE:
                programEff = new EffectRainRipple();
                break;
            case DataType.EFFECT_RAINCURTAIN:
                programEff = new EffectRainCurtain();
                break;
            case DataType.EFFECT_PHANTOMFAILURE:
                programEff = new EffectPhantomFailure();
                break;
            case DataType.EFFECT_CHROMATISMDITHERING:
                programEff = new EffectChromatismDithering();
                break;
            case DataType.EFFECT_SHAKE:
                programEff = new EffectShake();
                break;
            case DataType.EFFECT_INSTANTANEOUSBLUR:
                programEff = new EffectInstantaneousBlur();
                break;
            case DataType.EFFECT_SKINNEEDLING:
                programEff = new EffectSkinNeedling();
                break;
            case DataType.EFFECT_SIGNALDISTORTION:
                programEff = new EffectSignalDistortion();
                break;
            case DataType.EFFECT_SIGNALDISTORTION2:
                programEff = new EffectSignalDistortion2();
                break;
            case DataType.EFFECT_SIGNALDISTORTION3:
                programEff = new EffectSignalDistortion3();
                break;
            case DataType.EFFECT_SNOW:
                programEff = new EffectSnow();
                break;
            case DataType.EFFECT_FIREWORKS:
                programEff = new EffectFireworks();
                break;
            case DataType.EFFECT_FIREWORKS2:
                programEff = new EffectFireworks2();
                break;
            case DataType.EFFECT_FIREWORKS3:
                programEff = new EffectFireworks3();
                break;
            case DataType.EFFECT_FIREWORKS4:
                programEff = new EffectFireworks4();
                break;
            case DataType.EFFECT_OLDMOVIES:
                programEff = new EffectOldMovies();
                break; 
            case DataType.EFFECT_OLDMOVIES2:
                programEff = new EffectOldMovies2();
                break; 
            case DataType.EFFECT_OLDMOVIES3:
                programEff = new EffectOldMovies3();
                break;
            case DataType.EFFECT_OLDMOVIES4:
                programEff = new EffectOldMovies4();
                break; 
            case DataType.EFFECT_OLDMOVIES5:
                programEff = new EffectOldMovies5();
                break; 
        }
        if(programEff){
            programEff.initProgram(this.gl);
        }
        return programEff;
    }

    /** 卸载webgl应用程序 */
    uninstallProgram (effWhole){
        if(effWhole.programEff){
            effWhole.programEff.uninstallProgram();
        }
    }

    /** 卸载所有webgl应用程序 */
    uninstallProgramAll(){
        for(let j=0;j<this.effectWholes.length;j++){
            if(this.effectWholes[j].programEff){
                this.effectWholes[j].programEff.uninstallProgram();
            }
        }
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_ADD,this.effectAddHandler);
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_CHANGE,this.effectChangeHandler);
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_REMOVE,this.effectRemoveHandler);
    }
}
export default EffectManager;