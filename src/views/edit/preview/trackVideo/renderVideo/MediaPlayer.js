/**
 * 基础播放器
 */

import EffectManager from "./EffectManager";

class MediaPlayer{
	constructor(data,gl){
		this.data = data;
		this.gl = gl;
		
		this.imageWidth = 0;
		this.imageHeight = 0;

		this.media = null;
		this.seekedBack = null; //seek完成后的回调
		this.waitingBack = null; //缓冲状态回调
		this.isSeekEnd = false;
		//特技管理对象
		this.effectManager = new EffectManager(this.data,this.gl);
	}

	/** 刷新数据 */
	flushData (data) {
		this.data = data;
	}


	seek(time){
	}

	staticFramePlay(time){

	}
	play (){
		
	}

	pause (){
		
	}

	/** 获得播放器是否处理暂停状态 */
	isPause = () =>{
		return true;
	}

	/** 获得播放器当前时间 */
	getCurrentTime = () => {
		return 0;
	}

	destroy(){
		this.effectManager.uninstallProgramAll();
	}
}
export default MediaPlayer;