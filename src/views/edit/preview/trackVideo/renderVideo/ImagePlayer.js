/**
 * 图片播放器
 */
import MediaPlayer from "./MediaPlayer";

class ImagePlayer extends MediaPlayer{
    constructor(data,gl){
        super(data,gl);
        
        this.image = new Image();

        this.media = this.image;
        this.image.crossOrigin = "*";
        

        this.image.onload = (e) =>{
            // console.log('imagePlayer',this.image.width,this.image.height)

            this.imageWidth = this.image.width;
            this.imageHeight = this.image.height;
            //由于webgl在draw图时会消耗大量性能，先转为canvas.估计webgl存在bug
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');
            canvas.width = this.image.width;
            canvas.height = this.image.height;
            // console.time('canvasdraw');
            ctx.drawImage(this.image,0,0);
            // console.timeEnd('canvasdraw');
            this.media = canvas;

            if(this.seekedBack){
                this.seekedBack(this);
            }
        }
        this.image.src = this.data.previewUrl;
        
    }

    /** 刷新数据 */
    flushData (data) {
        super.flushData(data);
        // this.data = data;
        // this.effectManager.flushData(this.data);
        // this.operationArea.flushData(this.data);
    }


    seek = (time)=>{
        if(this.seekedBack){
            clearTimeout(this.timer);
            this.timer = setTimeout(()=>{
                this.seekedBack(this);
            },5);
        }
    }



    destroy(){
        super.destroy();
        this.image.src = '';
        this.isEmptyMemory = true;
        // this.effectManager.uninstallProgramAll();
        // this.operationArea.destroy();
    }
}
export default ImagePlayer;