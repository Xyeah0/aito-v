#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec2 direction; // = vec2(1.0, -1.0)
uniform float smoothness; // = 0.5
 
const vec2 center = vec2(0.5, 0.5);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	vec2 v = normalize(direction);
	v /= abs(v.x)+abs(v.y);
	float d = v.x * center.x + v.y * center.y;
	float m =
		(1.0-step(progress, 0.0)) * // there is something wrong with our formula that makes m not equals 0.0 with progress is 0.0
		(1.0 - smoothstep(-smoothness, 0.0, v.x * uv.x + v.y * uv.y - (d-0.5+progress*(1.+smoothness))));
	o_result = mix(getFromColor(uv), getToColor(uv), m);
}
