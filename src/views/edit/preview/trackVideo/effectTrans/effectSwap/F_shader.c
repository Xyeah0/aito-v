#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float reflection; // = 0.4
uniform float perspective; // = 0.2
uniform float depth; // = 3.0
 
const vec4 black = vec4(0.0, 0.0, 0.0, 1.0);
const vec2 boundMin = vec2(0.0, 0.0);
const vec2 boundMax = vec2(1.0, 1.0);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, vec2(_uv.x,1.0-_uv.y));
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, vec2(_uv.x,1.0-_uv.y));
}
 
bool inBounds (vec2 p) {
  	return all(lessThan(boundMin, p)) && all(lessThan(p, boundMax));
}
 
vec2 project (vec2 p) {
  	return p * vec2(1.0, -1.2) + vec2(0.0, -0.02);
}
 
vec4 bgColor (vec2 p, vec2 pfr, vec2 pto) {
	vec4 c = black;
	pfr = project(pfr);
	if (inBounds(pfr)) {
		c += mix(black, getFromColor(pfr), reflection * mix(1.0, 0.0, pfr.y));
	}
	pto = project(pto);
	if (inBounds(pto)) {
		c += mix(black, getToColor(pto), reflection * mix(1.0, 0.0, pto.y));
	}
	return c;
}



void main() {

	vec2 pfr, pto = vec2(-1.);
	vec2 uv2 = vec2(uv.x,1.0-uv.y);
	float size = mix(1.0, depth, progress);
	float persp = perspective * progress;
	pfr = (uv2 + vec2(-0.0, -0.5)) * vec2(size/(1.0-perspective*progress), size/(1.0-size*persp*uv.x)) + vec2(0.0, 0.5);
	
	size = mix(1.0, depth, 1.-progress);
	persp = perspective * (1.-progress);
	pto = (uv2 + vec2(-1.0, -0.5)) * vec2(size/(1.0-perspective*(1.0-progress)), size/(1.0-size*persp*(0.5-uv.x))) + vec2(1.0, 0.5);

	if (progress < 0.5) {
		if (inBounds(pfr)) {
			o_result = getFromColor(pfr);
			return;
		}
		if (inBounds(pto)) {
			o_result = getToColor(pto);
			return;
		}  
	}
	if (inBounds(pto)) {
		o_result = getToColor(pto);
		return;
	}
	if (inBounds(pfr)) {
		o_result = getFromColor(pfr);
		return;
	}
	o_result = bgColor(uv, pfr, pto);
	return ;
}