#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform int         g_direction;

out vec4 o_result;


void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);

    vec4 color;
    float areax1 = 0.5 + -0.5*g_percentage;
    float areax2 = 0.5 + 0.5*g_percentage;
    float areay1 = 0.5 + -0.5*g_percentage;
    float areay2 = 0.5 + 0.5*g_percentage;
    if(g_direction == 0){
        if(inTex.x >= areax1 && inTex.x <= areax2){
            color = texture(samplerB, inTex);
        }else{
            if(inTex.x < areax1){
                inTex.x = inTex.x + 0.5 - areax1;
            }
            if(inTex.x > areax2){
                inTex.x = inTex.x + 0.5 - areax2;
            }
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 1){
        if(inTex.y >= areay1 && inTex.y <= areay2){
            color = texture(samplerB, inTex);
        }else{
            if(inTex.y < areay1){
                inTex.y = inTex.y + 0.5 - areay1;
            }
            if(inTex.y > areay2){
                inTex.y = inTex.y + 0.5 - areay2;
            }
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 2){
        if((inTex.x >= areax1 && inTex.x <= areax2) || (inTex.y >= areay1 && inTex.y <= areay2)){
            color = texture(samplerB, inTex);
        }else{
            if(inTex.x < areax1){
                inTex.x = inTex.x + 0.5 - areax1;
            }
            if(inTex.x > areax2){
                inTex.x = inTex.x + 0.5 - areax2;
            }
            if(inTex.y < areay1){
                inTex.y = inTex.y + 0.5 - areay1;
            }
            if(inTex.y > areay2){
                inTex.y = inTex.y + 0.5 - areay2;
            }
            color = texture(samplerA, inTex);
        }
    }
    
    o_result = color;
}