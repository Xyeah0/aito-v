#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float size; // = 0.04
uniform float zoom; // = 50.0
uniform float colorSeparation; // = 0.3

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	float inv = 1. - progress;
	vec2 disp = size*vec2(cos(zoom*uv.x), sin(zoom*uv.y));
	vec4 texTo = getToColor(uv + inv*disp);
	vec4 texFrom = vec4(
		getFromColor(uv + progress*disp*(1.0 - colorSeparation)).r,
		getFromColor(uv + progress*disp).g,
		getFromColor(uv + progress*disp*(1.0 + colorSeparation)).b,
		1.0);
	o_result = texTo*progress + texFrom*inv;
}
