#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;

out vec4 o_result;


void main() {
    vec4 colora = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    vec4 colorb = texture(samplerB, vec2(vTexCoord.x,vTexCoord.y));

    vec4 color;
    color.r = colora.r*(1.0-g_percentage)+colorb.r*g_percentage;
    color.g = colora.g*(1.0-g_percentage)+colorb.g*g_percentage;
    color.b = colora.b*(1.0-g_percentage)+colorb.b*g_percentage;
    color.a = colora.a*(1.0-g_percentage)+colorb.a*g_percentage;
    
    o_result = color;
}