#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform int         g_direction;

out vec4 o_result;

vec2 getScalePos(vec2 v,float sx,float sy,int g_direction){
    float scalex = 1.0f;
    float scaley = 1.0f;
    if(sx == 0.0){
        scalex = 1.0/0.001;
    }else{
        scalex = 1.0/sx;
    }
    if(sy == 0.0){
        scaley = 1.0/0.001;
    }else{
        scaley = 1.0/sy;
    }
    mat2 scale22 = mat2(
        scalex, 0.0f,
        0.0f, scaley);
    if(g_direction == 0){
        v.x = (v.x - 0.5);
        v.y = (v.y - 0.5);
        v = v * scale22;
        v.x = (v.x + 0.5);
        v.y = (v.y + 0.5);
        return v;
    }else if(g_direction == 1){
        v.y = (v.y - 0.5);
        v = v * scale22;
        v.y = (v.y + 0.5);
        return v;
    }else if(g_direction == 2){
        v.x = (v.x - 1.0);
        v.y = (v.y - 0.5);
        v = v * scale22;
        v.x = (v.x + 1.0);
        v.y = (v.y + 0.5);
        return v;
    }else if(g_direction == 3){
        v.x = (v.x - 0.5);
        v.y = (v.y - 0.0);
        v = v * scale22;
        v.x = (v.x + 0.5);
        v.y = (v.y + 0.0);
        return v;
    }else if(g_direction == 4){
        v.x = (v.x - 0.5);
        v.y = (v.y - 1.0);
        v = v * scale22;
        v.x = (v.x + 0.5);
        v.y = (v.y + 1.0);
        return v;
    }else if(g_direction == 5){
        v = v * scale22;
        return v;
    }else if(g_direction == 6){
        v.x = (v.x - 1.0);
        v = v * scale22;
        v.x = (v.x + 1.0);
        return v;
    }else if(g_direction == 7){
        v.x = (v.x - 1.0);
        v.y = (v.y - 1.0);
        v = v * scale22;
        v.x = (v.x + 1.0);
        v.y = (v.y + 1.0);
        return v;
    }else if(g_direction == 8){
        v.y = (v.y - 1.0);
        v = v * scale22;
        v.y = (v.y + 1.0);
        return v;
    }else if(g_direction == 9){
        scale22[1][1] = 1.0;
        v = v * scale22;
        return v;
    }else if(g_direction == 10){
        scale22[1][1] = 1.0;
        v.x = (v.x - 1.0);
        v = v * scale22;
        v.x = (v.x + 1.0);
        return v;
    }else if(g_direction == 11){
        scale22[0][0] = 1.0;
        v = v * scale22;
        return v;
    }else if(g_direction == 12){
        scale22[0][0] = 1.0;
        v.y = (v.y - 1.0);
        v = v * scale22;
        v.y = (v.y + 1.0);
        return v;
    }else if(g_direction == 13){
        scale22[0][0] = 1.0;
        v.x = (v.x - 0.5);
        v.y = (v.y - 0.5);
        v = v * scale22;
        v.x = (v.x + 0.5);
        v.y = (v.y + 0.5);
        return v;
    }else if(g_direction == 14){
        scale22[1][1] = 1.0;
        v.x = (v.x - 0.5);
        v.y = (v.y - 0.5);
        v = v * scale22;
        v.x = (v.x + 0.5);
        v.y = (v.y + 0.5);
        return v;
    }else{
        return v * scale22;
    }
    
    return v * scale22;
}

void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);
    vec4 color;
    float per = 1.0 - g_percentage;
    vec2 apos = getScalePos(inTex,per,per,g_direction);
    if(apos.x > 1.0 || apos.x < 0.0 || apos.y > 1.0 || apos.y < 0.0){
        color = texture(samplerB, inTex);
    }else{
        color = texture(samplerA, apos);
    }
    
    o_result = color;
}