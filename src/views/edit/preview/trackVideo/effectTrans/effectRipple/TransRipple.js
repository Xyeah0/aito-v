import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class TransRipple{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 设置程序参数 */
    setParameter (_gl,percentage,params,aspectratio=1){
        let gl = _gl || this.gl;
        let program = this.program;
        // let params = {
        //     fRipple_Width: 0.3,
        //     fNum_Wave: 5.0,
        //     fAmplitude: 0.5,
        //     fEllipticity:0.18,
        //     fCenterX: 0,
        //     fCenterY: -0.5,
        //     fDistortion:0.138
        // }
        // let gl = _gl || this.gl;
        // let program = this.program;
        let scale_x   = Math.pow( 10.0, -params.fEllipticity );
        let scale_y   = 1.0 / Math.pow( 10.0, -params.fEllipticity );
        let rot = [params.fDistortion,params.fDistortion,aspectratio,0];//TPFLOAT4( d  ,d  ,fAspect,0);
        let width  = params.fRipple_Width * 10.0;
        let freq   = params.fNum_Wave * 2.0;
        let amp    = params.fAmplitude  * 2.0 / params.fNum_Wave;

        let pos = width + Math.sqrt(Math.pow(scale_x*aspectratio+Math.abs(params.fCenterX),2)+Math.pow(scale_y+Math.abs(params.fCenterY),2));
        pos *= percentage;
        const wave = [pos,amp,1.0/width,freq];
        const misc = [0,0.5,scale_x,scale_y];
        const mat4 = [
            1,      0,      0.5/1920,       0,
            0,      1,      0.5/1080,       0,
            0,      0,      1,              0,
            0,      0,      0,              1
        ]
        // [
        //     1,          0,          0,          0,
        //     0,          1,          0,          0,
        //     0.5/1920,   0.5/1080,   1,          0,
        //     0,          0,          0,          1,
        //   ]
        gl.uniform1f(gl.getUniformLocation(program, 'g_percentage'), percentage);
        gl.uniform4fv(gl.getUniformLocation(program, 'rot'), rot);
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vWave'), wave);
        // gl.uniform4fv(gl.getUniformLocation(program, 'g_vLight'), [0.70710677,0.70710677,0,0.5]);
        // gl.uniform4fv(gl.getUniformLocation(program, 'g_vAlpha'), [percentage,percentage,1,1]);
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vMisc'), misc);
        gl.uniformMatrix4fv(gl.getUniformLocation(program, 'g_matTex'), false, mat4);

        // console.log('wave',wave);
        // console.log('rot',rot);
        // console.log('vMisc',misc);
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }
}
export default TransRipple;