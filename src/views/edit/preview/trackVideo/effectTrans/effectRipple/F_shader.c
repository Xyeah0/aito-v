#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform vec4        rot;
uniform vec4        g_vWave;
uniform vec4        g_vLight;
uniform vec4        g_vAlpha;
uniform vec4        g_vMisc;
uniform mat4        g_matTex;

const float PI =  3.141592653589793238462643383279;

out vec4 o_result;

vec4 Mul(vec4 v,mat4 mat44){
    vec4 newv;
    newv.x = v.x* mat44[0][0] + v.y * mat44[1][0] + v.z * mat44[2][0] + v.w * mat44[3][0];
    newv.y = v.x* mat44[0][1] + v.y * mat44[1][1] + v.z * mat44[2][1] + v.w * mat44[3][1];
    newv.z = v.x* mat44[0][2] + v.y * mat44[1][2] + v.z * mat44[2][2] + v.w * mat44[3][2];
    newv.w = v.x* mat44[0][3] + v.y * mat44[1][3] + v.z * mat44[2][3] + v.w * mat44[3][3];
    return newv;
}

void main() {
    float g_fAspect = rot.z;
    float g_fAlpha = rot.w;
    vec2 g_vCenter = g_vMisc.xy;
    vec2 g_vAspect = g_vMisc.zw;

    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);
    vec2 fTex = (inTex - 0.5f ) * 2.0f;
    fTex.x *= g_fAspect;
    vec2 tex_fx = fTex * g_vAspect - g_vCenter;	


    float r    = length(tex_fx);
    float r_i = 1.0f / r;
    float ratio   = clamp( ( g_vWave.x - r ) * g_vWave.z, 0.0f, 1.0f );
    float angle   = PI * ratio;
    float angle_f = angle * g_vWave.w;

    float d0 = 0.5f  * g_vWave.y * ( g_vWave.w * ( cos( angle ) + 1.0f ) * sin( angle_f ) + sin( angle ) *   cos( angle_f ) );
    float d1 = 0.25f * g_vWave.y * ( g_vWave.w * ( cos( angle ) + 1.0f ) * sin( angle_f ) + sin( angle ) * ( cos( angle_f ) - 1.0f ) );
    float d = ( g_vWave.w * ratio >= 1.0f ) ? d0 : d1;

    float div = d * d + 1.0f;

    float nx = tex_fx.x * r_i * d;
    float ny = tex_fx.y * r_i * d;

    // float light = ( 2.0f * ( nx * g_vLight.x + ny * g_vLight.y ) + g_vLight.z ) / div;
    // light = max( light, 0.0f );
    // light *= light;
    // light *= light;
    // light *= light;
    // light *= g_vLight.w;

    //nx /= div;
    //ny /= div;

    vec2 tex = fTex + vec2(nx,ny) * rot.xy;
    tex.x /= g_fAspect;
    
    tex = tex / 2.0f + 0.5f;
    
    tex = Mul(vec4(tex,1.0f,1.0f),g_matTex).xy;
    
    vec4 vColor0 = texture(samplerA,tex);
    vec4 vColor1 = texture(samplerB,inTex);
    // vColor0.a *= 1.0-g_vAlpha.x;
    // vColor1.a *= g_vAlpha.y;
    // vColor0.x += light;
        
    vec4 oColor = mix(vColor1,vColor0,mix(1.0f,0.0f,ratio));	
    
    o_result = oColor;
    
}