#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float reflection; // = 0.4
uniform float perspective; // = 0.4
uniform float depth; // = 3

const vec4 black = vec4(0.0, 0.0, 0.0, 1.0);
const vec2 boundMin = vec2(0.0, 0.0);
const vec2 boundMax = vec2(1.0, 1.0);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

bool inBounds (vec2 p) {
  	return all(lessThan(boundMin, p)) && all(lessThan(p, boundMax));
}

vec2 project (vec2 p) {
  	return p * vec2(1.0, -1.2) + vec2(0.0, -0.02);
}

vec4 bgColor (vec2 p, vec2 pto) {
	vec4 c = black;
	pto = project(pto);
	if (inBounds(pto)) {
		c += mix(black, getToColor(vec2(pto.x,1.0-pto.y)), reflection * mix(1.0, 0.0, pto.y));
	}
	return c;
}

 
void main() {
	vec2 pfr = vec2(-1.), pto = vec2(-1.);
	float middleSlit = 2.0 * abs(uv.x-0.5) - progress;
	if (middleSlit > 0.0) {
		pfr = uv + (uv.x > 0.5 ? -1.0 : 1.0) * vec2(0.5*progress, 0.0);
		float d = 1.0/(1.0+perspective*progress*(1.0-middleSlit));
		pfr.y -= d/2.;
		pfr.y *= d;
		pfr.y += d/2.;
	}
	float size = mix(1.0, depth, 1.-progress);
	pto = (vec2(uv.x,1.0-uv.y) + vec2(-0.5, -0.5)) * vec2(size, size) + vec2(0.5, 0.5);
	if (inBounds(pfr)) {
		o_result = getFromColor(pfr);
	}
	else if (inBounds(pto)) {
		pto.y = 1.0-pto.y;
		o_result = getToColor(pto);
	}
	else {
		o_result = bgColor(uv, pto);
	}
}
