#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float color_separation; // = 0.04

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}
 
void main() {
	float y = 0.5 + (uv.y-0.5) / (1.0-progress);
	if (y < 0.0 || y > 1.0) {
		o_result =  getToColor(uv);
	}
	else {
		vec2 fp = vec2(uv.x, y);
		vec2 off = progress * vec2(0.0, color_separation);
		vec4 c = getFromColor(fp);
		vec4 cn = getFromColor(fp - off);
		vec4 cp = getFromColor(fp + off);
		o_result =  vec4(cn.r, c.g, cp.b, c.a);
	}
}