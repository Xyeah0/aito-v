#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec3 color /* = vec3(0.9, 0.4, 0.2) */;

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	o_result = mix(
		getFromColor(uv) + vec4(progress*color, 1.0),
		getToColor(uv) + vec4((1.0-progress)*color, 1.0),
		progress
	);
}