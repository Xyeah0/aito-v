#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec2 squares;// = ivec2(10,10)
uniform vec2 direction;// = vec2(1.0, -0.5)
uniform float smoothness; // = 1.6
const vec2 center = vec2(0.5, 0.5);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}
 
void main() {
	vec2 v = normalize(direction);
	v /= abs(v.x)+abs(v.y);
	float d = v.x * center.x + v.y * center.y;
	float offset = smoothness;
	float pr = smoothstep(-offset, 0.0, v.x * uv.x + v.y * uv.y - (d-0.5+progress*(1.+offset)));
	vec2 squarep = fract(uv*vec2(squares));
	vec2 squaremin = vec2(pr/2.0);
	vec2 squaremax = vec2(1.0 - pr/2.0);
	float a = (1.0 - step(progress, 0.0)) * step(squaremin.x, squarep.x) * step(squaremin.y, squarep.y) * step(squarep.x, squaremax.x) * step(squarep.y, squaremax.y);
	o_result = mix(getFromColor(uv), getToColor(uv), a);
}