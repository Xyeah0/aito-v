#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;
uniform float       size;

out vec4 o_result;

float rand (vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

void main() {

    float r = rand(vec2(0, uv.y));
    float m = smoothstep(0.0, -size, uv.x*(1.0-size) + size*r - (progress * (1.0 + size)));

    o_result = mix( getFromColor(uv), getToColor(uv), m );
}