#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

#define PI 3.141592653589

uniform float startingAngle; // = 90;

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	float offset = startingAngle * PI / 180.0;
	float angle = atan(uv.y - 0.5, uv.x - 0.5) + offset;
	float normalizedAngle = (angle + PI) / (2.0 * PI);
	
	normalizedAngle = normalizedAngle - floor(normalizedAngle);

	o_result = mix(
		getFromColor(uv),
		getToColor(uv),
		step(normalizedAngle, progress)
		);
}