#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec2 center; // = vec2(0.5, 0.5);
uniform vec3 backColor; // = vec3(0.1, 0.1, 0.1);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	float distance = length(uv - center);
	float radius = sqrt(8.0) * abs(progress - 0.5);
	vec4 color;
	if (distance > radius) {
		color = vec4(backColor, 1.0);
	}
	else {
		if (progress < 0.5) color = getFromColor(uv);
		else color = getToColor(uv);
	}
	o_result = color;
}