#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

// Number of total bars/columns
uniform int bars; // = 30

// Multiplier for speed ratio. 0 = no variation when going down, higher = some elements go much faster
uniform float amplitude; // = 2

// Further variations in speed. 0 = no noise, 1 = super noisy (ignore frequency)
uniform float noise; // = 0.1

// Speed variation horizontally. the bigger the value, the shorter the waves
uniform float frequency; // = 0.5

// How much the bars seem to "run" from the middle of the screen first (sticking to the sides). 0 = no drip, 1 = curved drip
uniform float dripScale; // = 0.5

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, vec2(_uv.x,1.0-_uv.y));
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, vec2(_uv.x,1.0-_uv.y));
}

float rand(int num) {
  	return fract(mod(float(num) * 67123.313, 12.0) * sin(float(num) * 10.3) * cos(float(num)));
}

float wave(int num) {
	float fn = float(num) * frequency * 0.1 * float(bars);
	return cos(fn * 0.5) * cos(fn * 0.13) * sin((fn+10.0) * 0.3) / 2.0 + 0.5;
}

float drip(int num) {
  	return sin(float(num) / float(bars - 1) * 3.141592) * dripScale;
}

float pos(int num) {
  	return (noise == 0.0 ? wave(num) : mix(wave(num), rand(num), noise)) + (dripScale == 0.0 ? 0.0 : drip(num));
}
 
void main() {
	vec2 uv2 = vec2(uv.x,1.0-uv.y);
	int bar = int(uv2.x * (float(bars)));
	float scale = 0.5 + pos(bar) * amplitude;
	float phase = progress * scale;
	float posY = uv2.y / vec2(1.0).y;
	vec2 p;
	vec4 c;
	if (phase + posY < 1.0) {
		p = vec2(uv2.x, uv2.y + mix(0.0, vec2(1.0).y, phase)) / vec2(1.0).xy;
		c = getFromColor(p);
	} else {
		p = uv2.xy / vec2(1.0).xy;
		c = getToColor(p);
	}

	// Finally, apply the color
	o_result = c;
}
