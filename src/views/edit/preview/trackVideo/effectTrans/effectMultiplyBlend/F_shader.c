#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;


out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

vec4 blend(vec4 a, vec4 b) {
  return a * b;
}
 
void main() {

	vec4 blended = blend(getFromColor(uv), getToColor(uv));
  
	if (progress < 0.5)
		o_result = mix(getFromColor(uv), blended, 2.0 * progress);
	else
		o_result = mix(blended, getToColor(uv), 2.0 * progress - 1.0);
}
