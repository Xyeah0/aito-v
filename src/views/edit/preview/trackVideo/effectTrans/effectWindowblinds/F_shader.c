#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;


out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

vec4 transition (vec2 uv) {
    float t = progress;
    
    if (mod(floor(uv.y*100.*progress),2.)==0.)
        t*=2.-.5;
    
    return mix(
        getFromColor(uv),
        getToColor(uv),
        mix(t, progress, smoothstep(0.8, 1.0, progress))
    );
}
 
void main() {
	o_result = transition(uv);
}