#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec2 squaresMin/* = ivec2(20) */; // minimum number of squares (when the effect is at its higher level)
uniform int steps /* = 50 */; // zero disable the stepping


out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}
 
void main() {
	float d = min(progress, 1.0 - progress);
	float dist = steps>0 ? ceil(d * float(steps)) / float(steps) : d;
	vec2 squareSize = 2.0 * dist / vec2(squaresMin);

	vec2 p = dist>0.0 ? (floor(uv / squareSize) + 0.5) * squareSize : uv;
  	o_result = mix(getFromColor(p), getToColor(p), progress);
}
