#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

#define PI 3.14159265359

uniform vec2 center; // = vec2(0.5, 0.5);
uniform float rotations; // = 1;
uniform float scale; // = 8;
uniform vec4 backColor; // = vec4(0.15, 0.15, 0.15, 1.0);

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}
 
void main() {
	vec2 difference = uv - center;
	vec2 dir = normalize(difference);
	float dist = length(difference);
	
	float angle = 2.0 * PI * rotations * progress;
	
	float c = cos(angle);
	float s = sin(angle);
	
	float currentScale = mix(scale, 1.0, 2.0 * abs(progress - 0.5));
	
	vec2 rotatedDir = vec2(dir.x  * c - dir.y * s, dir.x * s + dir.y * c);
	vec2 rotatedUv = center + rotatedDir * dist / currentScale;
	
	if (rotatedUv.x < 0.0 || rotatedUv.x > 1.0 || rotatedUv.y < 0.0 || rotatedUv.y > 1.0){
		o_result = backColor;
		return;
	}
	o_result = mix(getFromColor(rotatedUv), getToColor(rotatedUv), progress);
}