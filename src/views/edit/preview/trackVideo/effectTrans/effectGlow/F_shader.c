#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;

out vec4 o_result;


void main() {
    vec4 color;
    float percentage;
    if(g_percentage<=0.5){
        percentage = g_percentage*2.0;
        color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    }else{
        percentage = 1.0-(g_percentage-0.5)*2.0;
        color = texture(samplerB, vec2(vTexCoord.x,vTexCoord.y));
    }
    // float fGlow = percentage;//1.0-sqrt(1.0-pow(percentage,2.0));
    float fY = max(235.0 / 255.0, percentage/2.0);

    color.r = min(max(235.f/255.f,min(fY,color.r)),color.r + pow(percentage,2.0)+color.r*percentage);
    color.g = min(max(235.f/255.f,min(fY,color.g)),color.g + pow(percentage,2.0)+color.g*percentage);
    color.b = min(max(235.f/255.f,min(fY,color.b)),color.b + pow(percentage,2.0)+color.b*percentage);

    o_result = color;
    // float _Brightness = 20.0;
    // float _Saturation = 1.0;
    // float _Contrast = 1.0;
    // vec3 finalColor = vec3(color.r,color.g,color.b) * ((_Brightness-1.0)*percentage+1.0);
    // float gray = 0.2125 * color.r + 0.7154 * color.g + 0.0721 * color.b;  
    // vec3 grayColor = vec3(gray, gray, gray);
    // finalColor = mix(grayColor, finalColor, _Saturation);
    // vec3 avgColor = vec3(0.5, 0.5, 0.5);  
    // finalColor = mix(avgColor, finalColor, _Contrast);  
    // color = vec4(finalColor.r,finalColor.g,finalColor.b,color.a);

    // color.r = mix(color.r,1.0,percentage);
    // color.g = mix(color.g,1.0,percentage);
    // color.b = mix(color.b,1.0,percentage);
    // o_result = color;
}