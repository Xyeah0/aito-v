#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

#define PI 3.14159265359
uniform int segments; // = 5;

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

float rand (vec2 co) {
  	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
 
void main() {
	float angle = atan(uv.y - 0.5, uv.x - 0.5) - 0.5 * PI;
	float normalized = (angle + 1.5 * PI) * (2.0 * PI);
	
	float radius = (cos(float(segments) * angle) + 4.0) / 4.0;
	float difference = length(uv - vec2(0.5, 0.5));
	
	if (difference > radius * progress)
		o_result = getFromColor(uv);
	else
		o_result = getToColor(uv);
}