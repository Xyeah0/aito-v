#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform int         g_direction;

out vec4 o_result;

void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);

    vec4 colora = texture(samplerA, inTex);
    vec4 colorb = texture(samplerB, inTex);
    vec4 color;
    if(g_direction == 0){
        if(inTex.x < g_percentage){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 1){
        if(1.0 - inTex.x < g_percentage){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 2){
        if(inTex.y < g_percentage){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 3){
        if(1.0 - inTex.y < g_percentage){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 4){
        float per = g_percentage * 2.0;
        if(distance(inTex,vec2(0.0,0.0)) < distance(inTex,vec2(per,per))){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 5){
        float per = g_percentage * 2.0;
        inTex.x = 1.0 - inTex.x;
        if(distance(inTex,vec2(0.0,0.0)) < distance(inTex,vec2(per,per))){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 6){
        float per = g_percentage * 2.0;
        inTex.x = 1.0 - inTex.x;
        inTex.y = 1.0 - inTex.y;
        if(distance(inTex,vec2(0.0,0.0)) < distance(inTex,vec2(per,per))){
            color = colorb;
        }else{
            color = colora;
        }
    }else if(g_direction == 7){
        float per = g_percentage * 2.0;
        inTex.y = 1.0 - inTex.y;
        if(distance(inTex,vec2(0.0,0.0)) < distance(inTex,vec2(per,per))){
            color = colorb;
        }else{
            color = colora;
        }
    }
    
    o_result = color;
}