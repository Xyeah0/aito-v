#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;


out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

float inHeart (vec2 p, vec2 center, float size) {
	if (size==0.0) return 0.0;
	vec2 o = (p-center)/(1.6*size);
	float a = o.x*o.x+o.y*o.y-0.3;
	return step(a*a*a, o.x*o.x*o.y*o.y*o.y);
}
 
void main() {
	o_result = mix(getFromColor(uv),getToColor(uv),inHeart(vec2(uv.x,1.0-uv.y), vec2(0.5, 0.4), progress));
}
