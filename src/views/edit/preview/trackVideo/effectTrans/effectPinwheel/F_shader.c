#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float speed; // = 2.0;

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	vec2 p = uv.xy / vec2(1.0).xy;
  
	float circPos = atan(p.y - 0.5, p.x - 0.5) + progress * speed;
	float modPos = mod(circPos, 3.1415 / 4.);
	float signed = sign(progress - modPos);
	
	o_result = mix(getToColor(p), getFromColor(p), step(signed, 0.5));
}