#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec3 color;// = vec3(0.0)
uniform float colorPhase/* = 0.4 */; // if 0.0, there is no black phase, if 0.9, the black phase is very important

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	o_result = mix(
		mix(vec4(color, 1.0), getFromColor(uv), smoothstep(1.0-colorPhase, 0.0, progress)),
		mix(vec4(color, 1.0), getToColor(uv), smoothstep(    colorPhase, 1.0, progress)),
		progress);
}
