#version 300 es

precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       g_fLeftBorder;
uniform float       g_fWidth;
uniform float       whs;
uniform int         g_reverse;

out vec4 o_result;


void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);
    vec4 colora = texture(samplerA, inTex);
    vec4 colorb = texture(samplerB, inTex);
    vec4 color;
    if(g_reverse == 1){
        inTex.x = 1.0 - inTex.x;
    }
    float point = -g_fWidth;
    float per = g_percentage*(1.0+g_fWidth);
    float area1 = per-g_fWidth;
    float area2 = per;
    if(inTex.x < area1){
        color = colorb;
    }else if(inTex.x > area2){
        color = colora;
    }else{
        float pixelPer = g_fLeftBorder * 0.0005;
        float percentage = (inTex.x-area1)/g_fWidth;
        float percentagex = inTex.x-mod(inTex.x,pixelPer);
        float percentagey = inTex.y-mod(inTex.y,pixelPer*whs);
        
        vec4 imageInfo = texture(samplerA, vec2(percentagex,percentagey));
        vec2 co = vec2((imageInfo.x+percentagex)/2.0,(imageInfo.y+percentagey)/2.0);
        float fOutAlpha = fract(sin(dot(co, vec2(12.9898,78.233))) * 43758.5453);
        if(fOutAlpha < 0.5){
            fOutAlpha += percentage;
        }else{
            fOutAlpha *= percentage;
        }
        
        if(fOutAlpha > 0.5){
            color = colora;
        }else{
            color = colorb;
        }
    }
    o_result = color;
}