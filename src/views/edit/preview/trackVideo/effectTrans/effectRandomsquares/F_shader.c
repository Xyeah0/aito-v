#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform vec2 size; // = ivec2(10, 10)
uniform float smoothness; // = 0.5

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

float rand (vec2 co) {
  	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
 
void main() {
	float r = rand(floor(vec2(size) * uv));
	float m = smoothstep(0.0, -smoothness, r - (progress * (1.0 + smoothness)));
	o_result = mix(getFromColor(uv), getToColor(uv), m);
}
