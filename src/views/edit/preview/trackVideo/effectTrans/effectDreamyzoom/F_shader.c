#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

// Definitions --------
#define DEG2RAD 0.03926990816987241548078304229099 // 1/180*PI


// Transition parameters --------

// In degrees
uniform float rotation; // = 6

// Multiplier
uniform float scale; // = 1.2

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}
 
void main() {
	float ratio = whs;
	// Massage parameters
	float phase = progress < 0.5 ? progress * 2.0 : (progress - 0.5) * 2.0;
	float angleOffset = progress < 0.5 ? mix(0.0, rotation * DEG2RAD, phase) : mix(-rotation * DEG2RAD, 0.0, phase);
	float newScale = progress < 0.5 ? mix(1.0, scale, phase) : mix(scale, 1.0, phase);
	
	vec2 center = vec2(0, 0);

	// Calculate the source point
	vec2 assumedCenter = vec2(0.5, 0.5);
	vec2 p = (uv.xy - vec2(0.5, 0.5)) / newScale * vec2(ratio, 1.0);

	// This can probably be optimized (with distance())
	float angle = atan(p.y, p.x) + angleOffset;
	float dist = distance(center, p);
	p.x = cos(angle) * dist / ratio + 0.5;
	p.y = sin(angle) * dist + 0.5;
	vec4 c = progress < 0.5 ? getFromColor(p) : getToColor(p);

	// Finally, apply the color
	o_result = c + (progress < 0.5 ? mix(0.0, 1.0, phase) : mix(1.0, 0.0, phase));
}