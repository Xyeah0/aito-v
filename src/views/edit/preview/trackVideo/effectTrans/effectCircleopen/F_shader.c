#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       progress;
uniform float       whs;

uniform float smoothness; // = 0.3
uniform bool opening; // = true

const vec2 center = vec2(0.5, 0.5);
const float SQRT_2 = 1.414213562373;

out vec4 o_result;

vec4 getFromColor(vec2 _uv){
	return texture(samplerA, _uv);
}

vec4 getToColor(vec2 _uv){
	return texture(samplerB, _uv);
}

 
void main() {
	float x = opening ? progress : 1.-progress;
	float m = smoothstep(-smoothness, 0.0, SQRT_2*distance(center, uv) - x*(1.+smoothness));
	o_result = mix(getFromColor(uv), getToColor(uv), opening ? 1.-m : m);
}
