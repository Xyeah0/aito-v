#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform int         g_direction;

out vec4 o_result;

vec2 getCutShape(vec2 v,float per,int g_direction){
    
    if(g_direction == 0){
        v.x = per;
        v.y = per;
        return v;
    }else if(g_direction == 1){
        v.x = 1.0 - per;
        v.y = per;
        return v;
    }else if(g_direction == 2){
        return v;
    }
    
    return v;
}

bool pointInTriangle(float x0,float y0,float x1,float y1,float x2,float y2,float x3,float y3) {
    float divisor = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3);
    float a = ((y2 - y3)*(x0 - x3) + (x3 - x2)*(y0 - y3)) / divisor;
    float b = ((y3 - y1)*(x0 - x3) + (x1 - x3)*(y0 - y3)) / divisor;
    float c = 1.0 - a - b;
    
    return a >= 0.0 && a <= 1.0 && b >= 0.0 && b <= 1.0 && c >= 0.0 && c <= 1.0;
}

void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);
    vec4 color;
    float per = g_percentage;
    vec2 apos = getCutShape(inTex,g_percentage,g_direction);
    if(inTex.x < apos.x && inTex.y < apos.y){
        color = texture(samplerB, inTex);
    }else{
        color = texture(samplerA, inTex);
    }
    if(g_direction == 0){
        if(inTex.x <= per && inTex.y <= per){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 1){
        if(inTex.x >= (1.0 - per) && inTex.y <= per){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 2){
        if(inTex.x >= (1.0 - per) && inTex.y >= (1.0 - per)){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 3){
        if(inTex.x <= per && inTex.y >= (1.0 - per)){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 4){
        float per2 = 0.5 * per;
        if(inTex.x <= per && inTex.y >= (0.5 - per2) && inTex.y <= (0.5 + per2) ){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 5){
        float per2 = 0.5 * per;
        if(inTex.x >= 1.0 - per && inTex.y >= (0.5 - per2) && inTex.y <= (0.5 + per2) ){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 6){
        float per2 = 0.5 * per;
        if(inTex.x >= (0.5 - per2) && inTex.x <= (0.5 + per2)  && inTex.y <= per){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 7){
        float per2 = 0.5 * per;
        if(inTex.x >= (0.5 - per2) && inTex.x <= (0.5 + per2)  && inTex.y >= (1.0 - per)){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 8){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,0.0,(0.5-g_percentage),0.0,(0.5+g_percentage),per2,0.5);
        if(isIn){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 9){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,1.0,(0.5-g_percentage),1.0,(0.5+g_percentage),1.0 - per2,0.5);
        if(isIn){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 10){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,(0.5-g_percentage),0.0,(0.5+g_percentage),0.0,0.5,per2);
        if(isIn){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 11){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,(0.5-g_percentage),1.0,(0.5+g_percentage),1.0,0.5,1.0 - per2);
        if(isIn){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 12){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,0.0,(0.5-g_percentage),0.0,(0.5+g_percentage),per2,0.5);
        bool isIn2 = pointInTriangle(inTex.x,inTex.y,1.0,(0.5-g_percentage),1.0,(0.5+g_percentage),1.0 - per2,0.5);
        if(isIn || isIn2){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 13){
        float per2 = g_percentage * 2.0;
        bool isIn = pointInTriangle(inTex.x,inTex.y,(0.5-g_percentage),0.0,(0.5+g_percentage),0.0,0.5,per2);
        bool isIn2 = pointInTriangle(inTex.x,inTex.y,(0.5-g_percentage),1.0,(0.5+g_percentage),1.0,0.5,1.0 - per2);
        if(isIn || isIn2){
            color = texture(samplerB, inTex);
        }else{
            color = texture(samplerA, inTex);
        }
    }
    
    
    o_result = color;
}