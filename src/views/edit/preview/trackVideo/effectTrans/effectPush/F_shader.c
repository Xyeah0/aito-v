#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform sampler2D   samplerB;
uniform float       g_percentage;
uniform float       whs;
uniform int         g_direction;

out vec4 o_result;


void main() {
    vec2 inTex = vec2(vTexCoord.x,vTexCoord.y);
    vec4 color;
    float per = g_percentage;
    if(g_direction == 0){
        per = 1.0 - per;
        if(inTex.x < per){
            inTex.x = max(inTex.x + (1.0 - per),0.0);
            color = texture(samplerA, inTex);
        }else{
            inTex.x = min(inTex.x - per,1.0);
            color = texture(samplerB, inTex);
        }
    }else if(g_direction == 1){
        if(inTex.x < per){
            inTex.x = max(inTex.x + (1.0 - per),0.0);
            color = texture(samplerB, inTex);
        }else{
            inTex.x = min(inTex.x - per,1.0);
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 2){
        per = 1.0 - per;
        if(inTex.y < per){
            inTex.y = max(inTex.y + (1.0 - per),0.0);
            color = texture(samplerA, inTex);
        }else{
            inTex.y = min(inTex.y - per,1.0);
            color = texture(samplerB, inTex);
        }
    }else if(g_direction == 3){
        if(inTex.y < per){
            inTex.y = max(inTex.y + (1.0 - per),0.0);
            color = texture(samplerB, inTex);
        }else{
            inTex.y = min(inTex.y - per,1.0);
            color = texture(samplerA, inTex);
        }
    }else if(g_direction == 4){
        if(inTex.x <= per && inTex.y <= per){
            inTex.x = max(inTex.x + (1.0 - per),0.0);
            inTex.y = max(inTex.y + (1.0 - per),0.0);
            color = texture(samplerB, inTex);
        }else if(inTex.x >= per && inTex.y >= per){
            inTex.x = min(inTex.x - per,1.0);
            inTex.y = min(inTex.y - per,1.0);
            color = texture(samplerA, inTex);
        }else{
            color.a = 0.0;
        }
    }else if(g_direction == 5){
        if(inTex.x >= 1.0 - per && inTex.y <= per){
            inTex.x = min(inTex.x - (1.0 - per),1.0);
            inTex.y = max(inTex.y + (1.0 - per),0.0);
            color = texture(samplerB, inTex);
        }else if(inTex.x <= 1.0 - per && inTex.y >= per){
            inTex.x = max(inTex.x + (per),0.0);
            inTex.y = min(inTex.y - (per),1.0);
            color = texture(samplerA, inTex);
        }else{
            color.a = 0.0;
        }
    }else if(g_direction == 6){
        per = 1.0 - per;
        if(inTex.x <= per && inTex.y <= per){
            inTex.x = max(inTex.x + (1.0 - per),0.0);
            inTex.y = max(inTex.y + (1.0 - per),0.0);
            color = texture(samplerA, inTex);
        }else if(inTex.x >= per && inTex.y >= per){
            inTex.x = min(inTex.x - per,1.0);
            inTex.y = min(inTex.y - per,1.0);
            color = texture(samplerB, inTex);
        }else{
            color.a = 0.0;
        }
    }else if(g_direction == 7){
        if(inTex.x < per && inTex.y > (1.0 - per)){
            inTex.x = max(inTex.x + (1.0 - per),0.0);
            inTex.y = min(inTex.y - (1.0 - per),1.0);
            color = texture(samplerB, inTex);
        }else if(inTex.x >= per && inTex.y < (1.0 - per)){
            inTex.x = min(inTex.x - per,1.0);
            inTex.y = max(inTex.y + (per),0.0);
            color = texture(samplerA, inTex);
        }else{
            color.a = 0.0;
        }
    }
    
    o_result = color;
}