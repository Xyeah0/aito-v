#version 300 es

precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D samplerA;
uniform sampler2D samplerB;

out vec4 o_result;

void main() {
	vec4 color = texture(samplerA, vTexCoord);
	o_result = color;
}