#version 300 es

precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D samplerA;

out vec4 o_result;

void main() {
    vec4 color = texture(samplerA, vTexCoord);
    vec4 alphaColor = vec4(0.0,0.0,0.0,0.0);
    color = mix(alphaColor,color,step(0.0,vTexCoord.x));
    color = mix(alphaColor,color,step(vTexCoord.x,1.0));
    color = mix(alphaColor,color,step(0.0,vTexCoord.y));
    color = mix(alphaColor,color,step(vTexCoord.y,1.0));
    // if(vTexCoord.x < 0.0 || vTexCoord.x > 1.0 || vTexCoord.y < 0.0 || vTexCoord.y > 1.0 ){
    //     color = vec4(0.0,0.0,0.0,0.0);
    // }
    
    o_result = color;
}