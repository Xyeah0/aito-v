#version 300 es

precision highp float;
precision highp int;

layout(location = 0) in vec4 g_pos;

out vec2 vTexCoord;

// uniform int scaleType;
// uniform float textureWidth;
// uniform float textureHeight;
// uniform float viewWidth;
// uniform float viewHeight;
uniform vec2  textureScale;

void main() {

    // float scalex = 1.0f;
    // float scaley = 1.0f;
    // if(scaleType == 1){
    //     scalex = (textureHeight*viewWidth/viewHeight)/textureWidth;
    // }else if(scaleType == 2){
    //     scaley = (textureWidth*viewHeight/viewWidth)/textureHeight;
    // }
    // float curX = (g_pos.x*scalex + 1.) / 2.;
    // float curY = (g_pos.y*scaley + 1.) / 2.;
    float curX = (g_pos.x*textureScale.x + 1.) / 2.;
    float curY = (g_pos.y*textureScale.y + 1.) / 2.;
    vTexCoord = vec2(curX, curY);
    
    
    gl_Position =  g_pos;
}