import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
import BaseRenderTrack from '../BaseRenderTrack.js';
import EffectTransManager from './EffectTransManager.js';
import RenderGL from './RenderGL.js';
import VideoPlayer from './renderVideo/VideoPlayer.js';
import ImagePlayer from './renderVideo/ImagePlayer.js';
/**
 * 视频预览轨道
 */
let testTime = 0;
class RenderTrackVideo extends BaseRenderTrack{
	constructor (data) {
		super(data);

		
		// this.ctx = this.canvas.getContext("2d");
		

		this.players = [];//播放器组
		this.currentPlayers = [];//当前正在渲染的播放器组
		this.currentTime = 0;
		
		this.canvas = document.createElement("canvas");//此轨道的canvas对象
		// this.canvas.width = 202.5;
		// this.canvas.height = 360;
		this.flushCanvasWH();
		// this.canvas.width = App.activeSequence.preset.width/2;
		// this.canvas.height = App.activeSequence.preset.height/2;
		// this.canvas.width = App.activeSequence.presetPreview.width;
		// this.canvas.height = App.activeSequence.presetPreview.height;
		this.canvas.style.width = '100%';
		this.canvas.style.height = '100%';
		this.root.appendChild(this.canvas);

		CORECONTROL.$on(CORECONTROL.CLIP_ADD,this.clipAddHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$on(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$on(CORECONTROL.PREVIEW_STATUS,this.previewStatusHandler);
		CORECONTROL.$on(CORECONTROL.PRESET_CHANGE,this.presetHandler);
		CORECONTROL.$on(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_VOLUME_CHANGE,this.clipVolumeHandler);
		
		this.canvas.addEventListener('mousedown',this.canvasclickHandler);

		this.trackGL = new RenderGL(this,new EffectTransManager(this.data));
		window.testTrackVideo = this;
	}

	flushCanvasWH(){
		let presetW = App.activeSequence.preset.width;
		let willW = presetW/2;
		// if(willW > 960) willW = 960;
		// if(willW < 540) willW = 540; 
		willW = Math.min(960,willW);
		willW = Math.max(540,willW);
		let willH = willW * App.activeSequence.preset.height/App.activeSequence.preset.width;
		this.canvas.width = willW;
		this.canvas.height = willH;
	}

	presetHandler = ()=>{
		this.flushCanvasWH();
		this.mergeCanvas();
	}

	/** 添加片断 */
	clipAddHandler = (clip) =>{
		
		if(clip.trackId === this.data.modelId){
			let player = null;
			if(clip.dataType === DataType.CLIP_VIDEO){
				player = new VideoPlayer(clip,this.trackGL.gl);
			}else if(clip.dataType === DataType.CLIP_VIDEO_IMAGE){
				player = new ImagePlayer(clip,this.trackGL.gl);
			}
			if(player == null){
				console.log("创建播放器失败请检查数据",clip);
				return;
			}
			player.seekedBack = this.seekedHandler;
			player.waitingBack = this.waitingHandler;
			this.players.push(player);

			this.trackGL.clearRender();
		}
	}

	/** 片断改变 */
	clipChangeHandler = (clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].flushData(clip);
				break;
			}
		}
	}

	/** 删除片断 */
	clipDeleteHandler = (clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].destroy();
				this.players.splice(i,1);
				this.currentPlayers = this.getPlayerUseTime(App.activeSequence.currentTime);
				break;
			}
		}
	}

	/** 音量改变事件 */
	clipVolumeHandler =(clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].flushData(clip);
				break;
			}
		}
	}

	/**
	 * 预览状态发生改变
	 */
	previewStatusHandler = (b) => {
		if(b){
			for(let i=0;i<this.players.length;i++){
				this.players[i].seek(this.players[i].data.trackIn);
			}
			this.currentTime = App.activeSequence.currentTime;
            for(let i=0;i<this.players.length;i++){
                let player = this.players[i];
                if(player.data.trackIn > this.currentTime){
                    player.seek(player.data.trackIn);
                }
			}
			this.currentPlayers = this.getPlayerUseTime(this.currentTime);
			for(let i=0;i<this.currentPlayers.length;i++){
				// this.currentPlayers[i].seek(this.currentTime);
				this.currentPlayers[i].play();
			}

		}else{
			// console.log('end预览') 
			for(let i=0;i<this.players.length;i++){
				this.players[i].pause();
			} 
		}
	}
	/** 
	 * 每帧更新事件
	 */
	currentTimeHandler = (v) =>{
		let time = App.activeSequence.currentTime;
		if(App.activeSequence.isPreview){
			// console.log('开始预览')
			// this.mergeCanvas();
			//找到time所在的播放器
			this.currentPlayers=[];
			for(let i=0;i<this.players.length;i++){
				let player = this.players[i];
				if(time >= player.data.trackIn && time < player.data.trackOut){
					this.currentPlayers.push(player);
					// console.log(player.video.currentTime,App.currentTime,App.currentTime-player.video.currentTime);
					
					if(player.isPause()){
						// player.seek(time);//player.getStartTime());
						player.play();
						// player.enterframe(time);
					}
					else if(player.data.dataType == DataType.CLIP_VIDEO){
						// player.enterframe(time);
						let videoST = player.data.trackIn+player.getCurrentTime()-player.data.inPoint;
						// console.log('videoST',videoST,time);
						//当时间线播放器和当前视频差了2帧时，校验播放头
						if((Math.abs(videoST - time) > 80 && Math.abs(videoST - time) < 400)&& this.currentPlayers.length == 1 && this.data.isMain){
							// player.seek(time);
							// console.log('videoST222',videoST,time,videoST-time);
							// App.activeSequence.checkPlayHead({diff:videoST - time});
						}
					}
				}else{
					if(!player.isPause()){
						player.pause();
					}
				}
			}
			this.mergeCanvas();
			// this.preLoading(time);
			return;
		}
	}

	/** seek事件 */
	seekHandler = (currentTime) =>{
		
		this.currentPlayers = this.getPlayerUseTime(currentTime);
		// console.log('seekHandler',currentTime,this.currentPlayers,this.players.length)
		if(this.currentPlayers.length > 0){
			for(let i=0;i<this.currentPlayers.length;i++){
				let player = this.currentPlayers[i];
				player.isSeekEnd = false;
				player.seek(currentTime);
			}
		}else{
			this.trackGL.clearRender();//console.log("播放头外部改变clearVideoCanvas")
		}
	}


	//播放器seek完成事件
	seekedHandler =(_player)=>{
		_player.isSeekEnd = true;

		//判断播放器是否都到齐了
		let isAll = true;
		for(let i=0;i<this.currentPlayers.length;i++){
			let p = this.currentPlayers[i];
			if(!p.isSeekEnd){
				isAll = false;
				break;
			}
		}
		// console.log('判断播放器是否都到齐了',isAll,_player)
		if(isAll){
			this.mergeCanvas();
		}
	}

	//canvas单击事件
	canvasclickHandler=(event)=>{
		let e = event ? event : window.event;
		e.preventDefault();
		e.stopPropagation();
		App.activeSequence.selectClip(null);
	}

	//播放器缓冲状态事件
	waitingHandler = (obj) => {
		// CoreControl.tellWaitforPreview(obj);
		App.activeSequence.tellWaitforPreview(obj);
	}

	/** 外部调用设置缓冲状态 */
    setWaiting(b){
        for(let i=0;i<this.currentPlayers.length;i++){
            let player = this.currentPlayers[i];
            if(b){
                player.pause();
            }else{
                player.play();
            }
        }
    }


	//合成canvas
	mergeCanvas(){
		// console.log("播放器个数",this.currentPlayers.length,App.activeSequence.currentTime)
		if(this.currentPlayers.length == 1){
			let player = this.currentPlayers[0];
			this.trackGL.render(player);
			// let imgdata = player.shotScreenVideo(this.canvas.width,this.canvas.height);
			// this.ctx.putImageData(imgdata,0,0);
		}else if(this.currentPlayers.length == 2){
			let player1 = this.currentPlayers[0];
			let player2 = this.currentPlayers[1];
			//默认认为1在2之前，如果不是，交换位置
			if(player1.data.trackIn>player2.data.trackIn){
				player1 = this.currentPlayers[1];
				player2 = this.currentPlayers[0];
			}
			this.trackGL.render(player1,player2);
		}else{
			this.trackGL.clearRender();
		}
	}


	/**
	 * 通过time找对应播放器 可能会有多个
	 */
	getPlayerUseTime(time){
		let players = [];
		for(let i=0;i<this.players.length;i++){
			if(time >= this.players[i].data.trackIn && time < this.players[i].data.trackOut){
				players.push(this.players[i]);
			}
		}
		return players;
	}


	destroy(){
		super.destroy();

		CORECONTROL.$off(CORECONTROL.CLIP_ADD,this.clipAddHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$off(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$off(CORECONTROL.PREVIEW_STATUS,this.previewStatusHandler);
		CORECONTROL.$off(CORECONTROL.PRESET_CHANGE,this.presetHandler);
		CORECONTROL.$off(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_VOLUME_CHANGE,this.clipVolumeHandler);
		this.canvas.removeEventListener('click',this.canvasclickHandler);

		for(let i=0;i<this.players.length;i++){
			this.players[i].destroy();
		}
		this.players = [];
		this.trackGL.destroy();
	}
}

export default RenderTrackVideo;