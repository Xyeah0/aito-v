#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
in vec2 uv_def;
in vec2 uv_zoom;
in vec2 vBlurTexCoords[15];
in vec2 uv_add;
uniform sampler2D   samplerA;
uniform sampler2D   raw_texture;

uniform int         stepMode;


out vec4 o_result;

vec4 blurTexture(){
    vec4 color = vec4(0.0);
    color += texture(samplerA, vBlurTexCoords[0]) * 0.000489;
    color += texture(samplerA, vBlurTexCoords[1]) * 0.002403;
    color += texture(samplerA, vBlurTexCoords[2]) * 0.009246;
    color += texture(samplerA, vBlurTexCoords[3]) * 0.02784;
    color += texture(samplerA, vBlurTexCoords[4]) * 0.065602;
    color += texture(samplerA, vBlurTexCoords[5]) * 0.120999;
    color += texture(samplerA, vBlurTexCoords[6]) * 0.174697;
    color += texture(samplerA, vBlurTexCoords[7]) * 0.197448;
    color += texture(samplerA, vBlurTexCoords[8]) * 0.174697;
    color += texture(samplerA, vBlurTexCoords[9]) * 0.120999;
    color += texture(samplerA, vBlurTexCoords[10]) * 0.065602;
    color += texture(samplerA, vBlurTexCoords[11]) * 0.02784;
    color += texture(samplerA, vBlurTexCoords[12]) * 0.009246;
    color += texture(samplerA, vBlurTexCoords[13]) * 0.002403;
    color += texture(samplerA, vBlurTexCoords[14]) * 0.000489;

    return color;
}

void main() {
    // if(stepMode == 1){
    //     vec4 zoomColor = texture(samplerA,uv_zoom);
    //     zoomColor = mix(zoomColor,vec4(0.,0.,0.,0.),step(1.0,uv_zoom.x));
    //     zoomColor = mix(zoomColor,vec4(0.,0.,0.,0.),step(1.0,uv_zoom.y));
    //     o_result = zoomColor;
    // }else if(stepMode == 2){
    //     vec4 blurColor = blurTexture();
    //     o_result = blurColor;
    // }

    if(stepMode == 0){
        vec4 colorCheck = texture(samplerA,uv_def);
        o_result = colorCheck;
    }else if(stepMode == 1){
        vec4 zoomColor = texture(samplerA,uv_zoom);
        zoomColor = mix(zoomColor,vec4(0.,0.,0.,0.),step(1.0,uv_zoom.x));
        zoomColor = mix(zoomColor,vec4(0.,0.,0.,0.),step(1.0,uv_zoom.y));
        o_result = zoomColor;
    }else if(stepMode == 2){
        vec4 blurColor = blurTexture();
        o_result = blurColor;
    }else if(stepMode == 3){
        vec4 colorBg = texture(samplerA,uv);
        vec4 colorFront = texture(raw_texture,uv_add);
        vec4 alphaColor = vec4(0.0,0.0,0.0,0.0);
        colorFront = mix(alphaColor,colorFront,step(0.0,uv_add.x));
        colorFront = mix(alphaColor,colorFront,step(uv_add.x,1.0));
        colorFront = mix(alphaColor,colorFront,step(0.0,uv_add.y));
        colorFront = mix(alphaColor,colorFront,step(uv_add.y,1.0));
        o_result = mix(colorBg,colorFront,colorFront.a);//colorFront+colorBg;
    }
}