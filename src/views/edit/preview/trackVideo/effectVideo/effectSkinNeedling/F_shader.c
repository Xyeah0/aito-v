#version 300 es

precision highp float;
precision highp int;

in vec2 uv;
uniform  sampler2D   samplerA;

uniform highp float uScanLineJitter_x; 
uniform highp float uScanLineJitter_y; 
uniform highp float uColorDrift_x; 
uniform highp float uColorDrift_y; 
uniform float intensity;
uniform float horzIntensity;
uniform float vertIntensity;

uniform float uTimeStamp; 

vec2 uVerticalJump = vec2(0.0,0.0); // (amount, time)
uniform float uHorizontalShake;

out vec4 o_result;

float nrand(float x, float y)
{
    return fract(sin(dot(vec2(x, y), vec2(12.9898, 78.233))) * 43758.5453);
}

void main( void ){

    float u = uv.x;
    float v = uv.y;

    float jitter = nrand(v, uTimeStamp) * 2.0 - 1.0;
    jitter *= step(uScanLineJitter_y, abs(jitter)) * uScanLineJitter_x * intensity;
    float jump = mix(v, fract(v + uVerticalJump.y), uVerticalJump.x);
    float shake = (nrand(uTimeStamp, 2.0) - 0.5) * uHorizontalShake;
    
    // Color drift
    float drift = sin(jump + uColorDrift_y) * uColorDrift_x * horzIntensity;
    
    vec4 src1 = texture(samplerA, fract(vec2(u + jitter + shake, jump)));
    vec4 src2 = texture(samplerA, fract(vec2(u + jitter + shake + drift, jump + uColorDrift_y * vertIntensity)));

    vec4 resultColor = vec4(src1.r, src2.g, src1.b, 1.);

    o_result = resultColor;

    // o_result = texture(samplerA, uv);
}