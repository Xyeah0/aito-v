import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectOutofbodysoul{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.raw_texture = null;//原始纹理
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);
        this.raw_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.raw_texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            speed:0.33,
            range:1,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
                params.range = preParameter.range*(1-percentage)+nowParameter.range*percentage;
            }
        }
        // console.log('curPos',curPos,params.brightness)
        const mixturePercent = [0.411498,0.340743,0.283781,0.237625,0.199993,0.169133,0.143688,0.122599,0.037117,0.028870,0.022595,0.017788,0.010000,0.010000,0.010000,0.010000]
        const scalePercent = [1.084553,1.173257,1.266176,1.363377,1.464923,1.570877,1.681300,1.796254,1.915799,2.039995,2.168901,2.302574,2.302574,2.302574,2.302574,2.302574]
        let gl = _gl || this.gl;
        let program = this.program;
        let uTime = curPos - clip.inPoint || 0;
        gl.uniform1f(gl.getUniformLocation(program, 'uTime'), uTime * (0.5 + 1.5 * params.speed));
        // gl.uniform1i(gl.getUniformLocation(program, 'blurRadius'), 10);
        let id = Math.floor(uTime * (0.5 + 1.5 * params.speed) *30);
        let cur_id = id%mixturePercent.length;
        gl.uniform1f(gl.getUniformLocation(program, 'mixturePercent'), mixturePercent[cur_id]);
        cur_id = id%scalePercent.length;
        gl.uniform1f(gl.getUniformLocation(program, 'scalePercent'), scalePercent[cur_id] * (1 + 0.5 * params.range));
    }
}
export default EffectOutofbodysoul;