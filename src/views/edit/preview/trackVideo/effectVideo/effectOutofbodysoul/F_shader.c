#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform float uTime;

uniform lowp float mixturePercent;
uniform highp float scalePercent;


out vec4 o_result;

void main( void ){

    lowp vec4 textureColor = texture(samplerA, uv);
    
    highp vec2 textureCoordinateToUse = uv;
    highp vec2 center = vec2(0.5, 0.5);
    textureCoordinateToUse -= center;
    textureCoordinateToUse = textureCoordinateToUse / scalePercent;
    textureCoordinateToUse += center;
    lowp vec4 textureColor2 = texture(samplerA, textureCoordinateToUse);
    
    o_result = mix(textureColor, textureColor2, mixturePercent);
}