#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
out vec4 o_result;

uniform  sampler2D   samplerA;
uniform  sampler2D   noiseTexture;
uniform vec2 uRenderSize;
uniform float uTime;

float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

vec4 interlace(vec2 co, vec4 col) {
    if (int(co.y) % 3 == 0) {
        return col * ((sin(uTime * 4.) * 0.1) + 0.75) + (rand(vec2(uTime, uTime)) * 0.05);
    }
    return col;
}

void main( void ){

    bool normal = false;

    //vec2 uv = fragCoord/iResolution.xy;
    vec3 iResolution = vec3(uRenderSize,1.0);
    vec2 fragCoord = uv*uRenderSize;
    
    vec2 uvneg = vec2(fragCoord.x/iResolution.x - (rand(vec2(fragCoord.y+uTime*0.0001, 0))*0.06),fragCoord.y/iResolution.y);
    vec2 uvnegR = vec2(uvneg.x+(rand(vec2(uTime*0.005,iResolution.x))*0.1), uvneg.y);
    vec2 uvnegG = vec2(uvneg.x+(rand(vec2(uTime*0.005,iResolution.y))*0.1), uvneg.y);
    vec2 uvnegB = vec2(uvneg.x+(rand(vec2(uTime*0.005,iResolution.z))*0.1), uvneg.y);
    
    vec2 uvpos = vec2(fragCoord.x/iResolution.x + (rand(vec2(fragCoord.y+uTime*0.0001, 0))*0.03),fragCoord.y/iResolution.y);
    vec2 uvposR = vec2(uvpos.x+(rand(vec2(uTime*0.005,iResolution.x))*0.1), uvpos.y);
    vec2 uvposG = vec2(uvpos.x+(rand(vec2(uTime*0.005,iResolution.y))*0.1), uvpos.y);
    vec2 uvposB = vec2(uvpos.x+(rand(vec2(uTime*0.005,iResolution.z))*0.1), uvpos.y);
    
    vec2 uvNormal = vec2(fragCoord.x/iResolution.x + (rand(vec2(fragCoord.y+uTime*0.001, 0))*0.006),fragCoord.y/iResolution.y);
    
    float displace = 0.005;
    if (rand(vec2(uTime*0.001,iResolution.x)) > 0.8) {
        displace = rand(vec2(uTime*0.1,iResolution.y))/10.0+(uv.y/5.0);
    }
    
    vec2 uvGlitch = vec2(uv.x+(rand(vec2(uTime*0.005,iResolution.x))*displace),uv.y);
    float startGlitch = rand(vec2(uTime*.004,iResolution.y));
    float startGlitch2 = rand(vec2(uTime*.004,iResolution.x));        

    float Pi = 6.28318530718; // multiplié par 2 jui pas bête

    float dir = 16.0;
    float qual = 10.0;
    float size = rand(vec2(uTime,fragCoord.y))*20.0;

    vec2 Radius = size/iResolution.xy;
    
    vec4 Color = texture(samplerA, uv);
    vec4 col = texture(samplerA, uv);
    
    for( float d=0.0; d<Pi; d+=Pi/dir)
    {
        for(float i=1.0/qual; i<=1.0; i+=1.0/qual)
        {

            if (uv.y+(float(sin(d))*Radius*i).y < startGlitch+0.02 && uv.y+(float(sin(d))*Radius*i).y > startGlitch-rand(vec2(uTime*.002,iResolution.y))/5.00) Color.r += texture( samplerA, uvnegR+vec2(cos(d),sin(d))*Radius*i).r;
            else if (uv.y+(float(sin(d))*Radius*i).y < startGlitch+0.04 && uv.y+(float(sin(d))*Radius*i).y > startGlitch-rand(vec2(uTime*.002,iResolution.x))/5.2) Color.r += texture( samplerA, uvposR+vec2(cos(d),sin(d))*Radius*i).r;

            
            if (uv.y+(float(sin(d))*Radius*i).y < startGlitch && uv.y+(float(sin(d))*Radius*i).y > startGlitch-rand(vec2(uTime*.002,iResolution.y))/2.0) Color.g += texture( samplerA, uvnegG+vec2(cos(d),sin(d))*Radius*i).g;
            else if (uv.y+(float(sin(d))*Radius*i).y < startGlitch2 && uv.y+(float(sin(d))*Radius*i).y > startGlitch2-rand(vec2(uTime*.002,iResolution.x))/3.0) Color.g += texture( samplerA, uvposG+vec2(cos(d),sin(d))*Radius*i).g;
            
            if (uv.y+(float(sin(d))*Radius*i).y < startGlitch+0.01 && uv.y+(float(sin(d))*Radius*i).y > startGlitch-rand(vec2(uTime*.002,iResolution.y))/4.0) Color.b += texture( samplerA, uvnegB+vec2(cos(d),sin(d))*Radius*i).b;
            else if (uv.y+(float(sin(d))*Radius*i).y < startGlitch2+0.03 && uv.y+(float(sin(d))*Radius*i).y > startGlitch2-rand(vec2(uTime*.002,iResolution.x))/5.4) Color.b += texture( samplerA, uvposB+vec2(cos(d),sin(d))*Radius*i).b;
            else {
                Color.g += texture(samplerA,vec2((uvNormal+vec2(cos(d),sin(d))*Radius*i + displace).x,uvNormal.y + rand(vec2(uTime*0.001,iResolution.y))*0.075)).g;
                Color.b += texture(samplerA,uvGlitch).b;
                Color.r += texture(samplerA,uvGlitch+vec2(cos(d),sin(d))*Radius*i).r;
                normal = true;
            }
        }
    }
    float alpha = Color.a;
    if (normal == true) {
        Color /= qual * dir - 15.0;
        Color = interlace(fragCoord, Color);
    }else{
        Color /= qual * dir - 15.0;
        Color =  interlace(fragCoord, Color);
    }
    Color.a = alpha;
    
    o_result =  Color;
    
    //o_result = vec4(color, 1.);

    // o_result = texture(samplerA, vTexCoord);
}