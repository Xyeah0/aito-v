#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform  vec4    g_vProcess;

out vec4 o_result;

vec4 rgbToYUV(vec4 rgb){
    float R = rgb.r;
    float G = rgb.g;
    float B = rgb.b;
    float Y =  0.299*R + 0.587*G + 0.114*B;
    float U = -0.169*R - 0.331*G + 0.5  *B;
    float V =  0.5  *R - 0.419*G - 0.081*B;
    return vec4(Y,U,V,rgb.a);
}
vec4 YUVTorgb(vec4 yuv){
    float Y = yuv.r;
    float U = yuv.g;
    float V = yuv.b;
    float R = Y + 1.4075 * V;  
    float G = Y - 0.3455 * U - 0.7169*V;  
    float B = Y + 1.779 * U;
    return vec4(R,G,B,yuv.a);
}

void main() {
    vec4 c = texture(samplerA,vTexCoord);
    vec4 c1 = texture(samplerA, vTexCoord + vec2(g_vProcess.z, g_vProcess.w));
    vec4 c2 = texture(samplerA, vTexCoord + vec2(g_vProcess.z, 0.f));
    vec4 c3 = texture(samplerA, vTexCoord + vec2(0.f, g_vProcess.w));

    c = rgbToYUV(c);
    c1 = rgbToYUV(c1);
    c2 = rgbToYUV(c2);
    c3 = rgbToYUV(c3);

    vec4 tmp = clamp(abs(c - c1) + abs(c2 - c3), 0.f, 1.f);
    tmp = YUVTorgb(tmp);
    if(g_vProcess.y>0.f)
    tmp = 1.0f - tmp;
    tmp.a = 1.0f;   

    vec4 result = mix(tmp, c, g_vProcess.x);    
    result.a *=  c.a;
    
    o_result = result;
}