import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectFindEdges{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas){
        let params = {
            fProcess:0,
            fReverse:0
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.fProcess = preParameter.fProcess*(1-percentage)+nowParameter.fProcess*percentage;
            }
        }
        if(keyFrameParameters.length>0){
            params.fReverse = keyFrameParameters[0].parameter.fReverse
        }
        // console.log('curPos',curPos,params.brightness)
        let gl = _gl || this.gl;
        let program = this.program;
        const g_vProcess = [
            params.fProcess,params.fReverse,1/canvas.width,1/canvas.height
        ]
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vProcess'), g_vProcess);
    }
}
export default EffectFindEdges;