import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectSnow{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            snowflake_amount:200,
            blizard_factor:0,
            falling_velocity:0.2,
            snow_alpha:0.2,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.snowflake_amount = preParameter.snowflake_amount*(1-percentage)+nowParameter.snowflake_amount*percentage;
                params.blizard_factor = preParameter.blizard_factor*(1-percentage)+nowParameter.blizard_factor*percentage;
                params.falling_velocity = preParameter.falling_velocity*(1-percentage)+nowParameter.falling_velocity*percentage;
                params.snow_alpha = preParameter.snow_alpha*(1-percentage)+nowParameter.snow_alpha*percentage;
            }
        }

        let gl = _gl || this.gl;
        let program = this.program;
        let cw = canvas.width,ch = canvas.height;
        gl.uniform1f(gl.getUniformLocation(program,'iTime'), curPos);
        gl.uniform2fv(gl.getUniformLocation(program, 'iResolution'), [cw,ch]);
        gl.uniform1i(gl.getUniformLocation(program,'snowflake_amount'), params.snowflake_amount);
        gl.uniform1f(gl.getUniformLocation(program,'blizard_factor'), params.blizard_factor);
        gl.uniform1f(gl.getUniformLocation(program,'falling_velocity'), params.falling_velocity);
        gl.uniform1f(gl.getUniformLocation(program,'snow_alpha'), params.snow_alpha);
    }
}
export default EffectSnow;