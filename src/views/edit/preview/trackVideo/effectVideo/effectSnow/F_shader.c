#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform float   iTime;
uniform vec2    iResolution;
uniform int     snowflake_amount;// Number of snowflakes
uniform float   blizard_factor;// Fury of the storm !
uniform float   falling_velocity;//下落速度
uniform float   snow_alpha;//雪花透明度


out vec4 o_result;

vec2 uv_snow;
float rnd(float x)
{
    return fract(sin(dot(vec2(x+47.49,38.2467/(x+2.3)), vec2(12.9898, 78.233)))* (43758.5453));
}

float drawCircle(vec2 center, float radius)
{
    return 1.0 - smoothstep(0.0, radius, length(uv_snow - center));
}

void main() {
    vec4 fragColor = texture(samplerA,uv);//vec4(0.808, 0.89, 0.918, 1.0);//
    float j;
    
    uv_snow = gl_FragCoord.xy / max(iResolution.x,iResolution.y);
    uv_snow.y = 1.0-uv_snow.y;
    for(int i=0; i<snowflake_amount; i++)
    {
        j = float(i);
        float speed = 0.3+rnd(cos(j))*(0.7+0.5*cos(j/(float(snowflake_amount)*0.25)));
        vec2 center = vec2((0.25-uv_snow.y)*blizard_factor*0.2+rnd(j)+0.1*cos(iTime+sin(j)), mod(sin(j)-speed*(iTime*1.5*(0.1+falling_velocity)), 1.0));
        fragColor += vec4(snow_alpha*drawCircle(center, 0.001+speed*0.012));
    }
    
    o_result = fragColor;
}