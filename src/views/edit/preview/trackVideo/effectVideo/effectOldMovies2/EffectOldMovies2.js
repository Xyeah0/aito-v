import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
import App from '../../../../../../model/App';
import Utils from '../../../../../../libs/Utils';
class EffectOldMovies2{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.bitmap_filter = null;
        this.texture_filter = null;

        Utils.getBitmapOfZip("./static/img/effect-source/a993a3a878aeafb4ad88f0b5671dbe9c.zip")
        .then(imglist=>{
            // this.bitmap_fronts = imglist;
            this.bitmap_filter = imglist[0].canvas;
            // console.log('imglist',imglist)
            // trackGL.render();

            App.activeSequence.seek(App.activeSequence.currentTime);
        })
        .catch(err=>{
            console.log('err',err)
        })
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            speed:0.33,
            isFilm: true,
            isGray: true,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
                params.isFilm = nowParameter.isFilm;
                params.isGray = nowParameter.isGray;
            }
        }
        console.log('params.isFilm',params.isFilm,params.isGray)

        let gl = _gl || this.gl;
        let program = this.program;
        let cw = canvas.width,ch = canvas.height;
        gl.uniform1f(gl.getUniformLocation(program,'iTime'), curPos*params.speed*3);
        gl.uniform2fv(gl.getUniformLocation(program, 'iResolution'), [cw,ch]);
        gl.uniform1i(gl.getUniformLocation(program, 'isFilm'), params.isFilm);
        gl.uniform1i(gl.getUniformLocation(program, 'isGray'), params.isGray);

        if(this.bitmap_filter && !this.bitmap_filter_create){
            // console.log('this.bitmap_filter.width',this.bitmap_filter.width,this.bitmap_filter.height,gl)

            gl.activeTexture(gl.TEXTURE1);

            const texture_filter = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, texture_filter);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.GL_REPEAT);
            // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.GL_REPEAT);
            // gl.bindTexture(gl.TEXTURE_2D, null);

            // gl.bindTexture(gl.TEXTURE_2D, texture_filter);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.bitmap_filter.width, this.bitmap_filter.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, this.bitmap_filter);
            gl.uniform1i(gl.getUniformLocation(program,'texture_filter'), 1);
            this.bitmap_filter_create = true;
            gl.activeTexture(gl.TEXTURE0);
        }
    }
}
export default EffectOldMovies2;