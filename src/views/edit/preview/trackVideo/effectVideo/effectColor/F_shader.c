#version 300 es
        
precision highp float;
precision highp int;


#define PI      3.141592653589793238462643383279;
#define PI2     6.283185307179586476925286766559;
in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform float       g_contrast;//0.5~2.0,1.0
uniform float       g_saturation;//0.0~2.0,1.0
uniform float       g_brightness;//0.0~2.0,1.0
uniform float       g_hue;//-1.0~1.0,0.0

out vec4 o_result;

vec3 rgbToYUV(vec3 rgb){
    float R = rgb.r;
    float G = rgb.g;
    float B = rgb.b;
    float Y =  0.299*R + 0.587*G + 0.114*B;
    float U = -0.169*R - 0.331*G + 0.5  *B;
    float V =  0.5  *R - 0.419*G - 0.081*B;
    return vec3(Y,U,V);
}
vec3 YUVTorgb(vec3 yuv){
    float Y = yuv.r;
    float U = yuv.g;
    float V = yuv.b;
    float R = Y + 1.4075 * V;  
    float G = Y - 0.3455 * U - 0.7169*V;  
    float B = Y + 1.779 * U;
    return vec3(R,G,B);
}

void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    vec4 retColor = color;
        
    vec3 rawYUV = rgbToYUV(color.xyz);// - vec3(0.f,0.5f,0.5f);

    vec3 outputYUV;
    float bright = rawYUV.r;
    float sat = length(rawYUV.gb);
    float hue = atan(rawYUV.b, rawYUV.g);
    if ( hue < 0.0 )
    {
        hue += PI2;
    }
    vec3 tempValue;
    tempValue.r = clamp( (bright - 219.0/510.0) * g_contrast + 219.0/510.0, 0.0, 1.0);
    tempValue.g = clamp(sat * g_saturation, 0.0, 1.0);  //Saturation Adjust
    tempValue.r = clamp(tempValue.r * g_brightness, 0.0, 1.0);   //Brightness Adjust
    tempValue.b = hue + g_hue*PI;     //Hue Adjust

    outputYUV.r = tempValue.r;
    outputYUV.g = tempValue.g * cos(tempValue.b);   //,-0.4392,0.4392; U
    outputYUV.b = tempValue.g * sin(tempValue.b);   //,-0.4392,0.4392; V
    // outputYUV += vec3(0.f,0.5f,0.5f);
    retColor.rgb = YUVTorgb(outputYUV);
    o_result = retColor;

}