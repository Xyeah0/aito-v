#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform vec2 uRenderSize;
uniform mediump vec2 ubuf_uvperpixel;
uniform mediump float ubuf_seed;
uniform mediump float ubuf_radius;
uniform mediump float ubuf_angle;


out vec4 o_result;

float rand(float co)
{
    float a = 78.233001708984375;
    float c = 43758.546875;
    float dt = co * a;
    float sn = mod(dt, 3.1400001049041748046875);
    return (2.0 * fract(sin(sn) * c)) - 1.0;
}

void main( void ){

    mediump float rad = radians(ubuf_angle);
    mediump vec2 dir = vec2(cos(rad), sin(rad));
    float param = (uv.x * uv.y) * ubuf_seed;
    mediump vec4 randColor = texture(samplerA, uv + (((dir * rand(param)) * ubuf_radius) * ubuf_uvperpixel));
    mediump vec4 originColor = texture(samplerA, uv);
    o_result = mix(originColor, randColor, vec4(originColor.w));
}