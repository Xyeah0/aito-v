#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform float iTime;
uniform float intensity;
uniform float speed;
uniform float size;


out vec4 o_result;

void main( void ){

    float s = 1.0 - intensity * 2.0;

    float ox = uv.x;
    float oy = uv.y;

    ox = sin(mod(uv.y * size * 2.0, 2.0) * 3.14159) * intensity;
    ox = (uv.x) + ox * sin(iTime);
    if (ox < 0.0){
        ox = -ox;
    }else if(ox > 1.0){
        ox = 2.0 - ox;

    }

    vec2 uv0 = vec2(ox, oy);

    vec4 color = texture(samplerA, uv0);

    o_result = color;
}