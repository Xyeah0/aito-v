#version 300 es

precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   texture_filter;
uniform float   iTime;
uniform vec2    iResolution;
uniform bool    isGray;
uniform bool    isCurled;

out vec4 o_result;

#define LINES_AND_FLICKER
#define BLOTCHES
#define GRAIN

#define FREQUENCY 5.0

const float uIntensity = 0.03f; // Frame distortion intensity. 0.02 ~ 0.05 recommended. 
const float uThreshold = 0.85f; // 0.75 ~ 0.90 would be recommended.
const float uMax       = 64.0f; // Distortion for edge of threshold.
const float uMargin    = 8.0f;  // Margin.

float GetOverThreadsholdIntensity(const float a, const float t) {
	float b = pow(t, 2.0f) * (1.0f - (1.0f / uMax));
	return uMax * pow(a - (t - (t / uMax)), 2.0f) + b;
}

bool IsOob(const vec2 inputTexCoord) {
	return inputTexCoord.x > 1.0f || inputTexCoord.y > 1.0f 
		|| inputTexCoord.x < 0.0f || inputTexCoord.y < 0.0f;
}

vec2 ApplyMargin(const vec2 texel, const float margin) {
	vec2 m = vec2(margin * 4.0f) / iResolution.xy;
	return (texel - 0.5f) * (1.0f + m) + 0.5f;
}

vec2 getCurledUv(vec2 uv){
	float x = uv.x * 2.0f - 1.0f;
	float y = uv.y * 2.0f - 1.0f;
	
	// Distort uv coordinate, and if closer to frame bound, do more distortion.
	float x_intensity = uIntensity;
	float y_intensity = uIntensity;
	if (abs(x) >= uThreshold && abs(y) >= uThreshold) {
		y_intensity *= GetOverThreadsholdIntensity(abs(x), uThreshold);
		x_intensity *= GetOverThreadsholdIntensity(abs(y), uThreshold);     
	}
	else {
		y_intensity *= pow(x, 2.0f);
		x_intensity *= pow(y, 2.0f);
	}
	
	// Get texel and apply margin (px)
	float y_offset 	= y_intensity * y;
	float x_offset 	= x_intensity * x;
	vec2 finalTexel = ApplyMargin(uv + vec2(x_offset, y_offset), uMargin);
	return finalTexel;
	// ShaderToy does not support border (to be out-of-bound black color),
	// so checking texel is out of bound.
	// vec3 col = vec3(0.0);
	// if (IsOob(finalTexel) == false){
	//     col = texture(samplerA, finalTexel).rgb;
	// }
	// return col;
}

float rand(vec2 co){
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand(float c){
	return rand(vec2(c,1.0));
}

float randomLine(float seed, vec2 uv)
{
	float b = 0.01 * rand(seed);
	float a = rand(seed+1.0);
	float c = rand(seed+2.0) - 0.5;
	float mu = rand(seed+3.0);
	
	float l = 1.0;
	
	if ( mu > 0.2)
		l = pow(  abs(a * uv.x + b * uv.y + c ), 1.0/8.0 );
	else
		l = 2.0 - pow( abs(a * uv.x + b * uv.y + c), 1.0/8.0 );				
	
	return mix(0.5, 1.0, l);
}

float randomBlotch(float seed, vec2 uv)
{
	vec2 iResolution = vec2(1920.0,1080.0);
	float x = rand(seed);
	float y = rand(seed+1.0);
	float s = 0.01 * rand(seed+2.0);
	
	vec2 p = vec2(x,y) - uv;
	p.x *= iResolution.x / iResolution.y;
	float a = atan(p.y,p.x);
	float v = 1.0;
	float ss = s*s * (sin(6.2831*a*x)*0.1 + 1.0);
	
	if ( dot(p,p) < ss ) v = 0.2;
	else
		v = pow(dot(p,p) - ss, 1.0/16.0);
	
	return mix(0.3 + 0.2 * (1.0 - (s / 0.02)), 1.0, v);
}

vec4 getOldColor(vec2 uv0){
	float iGlobalTime = iTime;
	vec2 iResolution = vec2(1920.0,1080.0);
	vec2 uv = uv0;

	int Frequency = 5;//频次
	int Colorful = 0;//Colorful
	int Fade = 300;//Fade
	int alpha = 100;//

	if(isCurled){
		uv = getCurledUv(uv);
	}
	
	float t = float(int(iGlobalTime * FREQUENCY * float(Frequency)));
	vec2 suv = uv + 0.002 * vec2( rand(t), rand(t + 23.0));
	vec3 image = texture(samplerA, vec2(suv.x, suv.y) ).xyz;
	
	vec3 oldImage = image;
	// if(isGray){
	//     oldImage = vec3(dot(oldImage, vec3(.299, .587, .114)));
	// }
	if (isGray){
		float luma = dot( vec3(0.2126, 0.7152, 0.0722), image );
		oldImage = luma * vec3(0.7, 0.7, 0.7);
	}
	
	
	float vI = 16.0 * (uv.x * (1.0-uv.x) * uv.y * (1.0-uv.y));
	vI *= mix( 0.7, 1.0, rand(t + 0.5));
	vI += 1.0 + 0.4 * rand(t+8.);
	vI *= pow(16.0 * uv.x * (1.0-uv.x) * uv.y * (1.0-uv.y), 0.4);
	#ifdef LINES_AND_FLICKER
	int l = int(8.0 * rand(t+7.0));
	
	if ( 0 < l ) vI *= randomLine( t+6.0+17.* float(0), uv);
	if ( 1 < l ) vI *= randomLine( t+6.0+17.* float(1), uv);
	if ( 2 < l ) vI *= randomLine( t+6.0+17.* float(2), uv);		
	if ( 3 < l ) vI *= randomLine( t+6.0+17.* float(3), uv);
	if ( 4 < l ) vI *= randomLine( t+6.0+17.* float(4), uv);
	if ( 5 < l ) vI *= randomLine( t+6.0+17.* float(5), uv);
	if ( 6 < l ) vI *= randomLine( t+6.0+17.* float(6), uv);
	if ( 7 < l ) vI *= randomLine( t+6.0+17.* float(7), uv);
	
	#endif
	
	
	#ifdef BLOTCHES
	int s = int( max(8.0 * rand(t+18.0) -2.0, 0.0 ));

	if ( 0 < s ) vI *= randomBlotch( t+6.0+19.* float(0), uv);
	if ( 1 < s ) vI *= randomBlotch( t+6.0+19.* float(1), uv);
	if ( 2 < s ) vI *= randomBlotch( t+6.0+19.* float(2), uv);
	if ( 3 < s ) vI *= randomBlotch( t+6.0+19.* float(3), uv);
	if ( 4 < s ) vI *= randomBlotch( t+6.0+19.* float(4), uv);
	if ( 5 < s ) vI *= randomBlotch( t+6.0+19.* float(5), uv);

	#endif

	
	vec3 color = oldImage * vI;
	

	#ifdef GRAIN
	color *= (1.0+(rand(uv+t*.01)-.2)*.15);		
	#endif		
	
	vec4 col = texture(samplerA, uv0);
	col.rgb = mix(color, col.rgb, 1.0 - float(Fade)/300.0*float(alpha)/100.0);
	
	return col;
}


void main()
{   
	o_result = getOldColor(uv);
} 