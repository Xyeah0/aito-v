import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectRippledistortion{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.raw_texture = null;//原始纹理
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);
        this.raw_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.raw_texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            distortion:0.3,
            intensity:0.3,
            number:0.3,
            speed:0.3,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.distortion = preParameter.distortion*(1-percentage)+nowParameter.distortion*percentage;
                params.intensity = preParameter.intensity*(1-percentage)+nowParameter.intensity*percentage;
                params.number = preParameter.number*(1-percentage)+nowParameter.number*percentage;
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
            }
        }
        // console.log('curPos',curPos,params.brightness)
        let gl = _gl || this.gl;
        let program = this.program;

        const mix = function(x, y, a){
            return x + (y - x) * a
        }
        const step = function(edge0, edge1, value){
            return Math.min(Math.max(0, (value - edge0) / (edge1 - edge0)), 1)
        }
        const smoothstep = function(edge0, edge1, value){
            let t = Math.min(Math.max(0, (value - edge0) / (edge1 - edge0)), 1)
            return t * t * (3 - t - t)
        }
        const remap = function(x, a, b){
            return x * (b - a) + a
        }

        let cw = canvas.width,ch = canvas.height;
        const x_base_value = {x:2.9000000953674, y:2.2999999523163,z:5};
        const y_base_value = {x:6, y:1.1499999761581,z:3};
        const x_value = [
            mix(x_base_value.x, x_base_value.x * 4, params.number), 
            mix(x_base_value.y, x_base_value.y * 2, params.distortion), 
            x_base_value.z
        ]
        const y_value = [
            y_base_value.x, 
            mix(y_base_value.y, y_base_value.y * 2, params.distortion), 
            y_base_value.z
        ]
        let uTime = curPos - clip.inPoint || 0;
        let progress = uTime * mix(0.5, 3, params.speed);
        let value = params.intensity*0.8*0.35 + 0.6;
        let radius = remap(params.intensity * 0.6,0.1,2.3)

        gl.uniform2fv(gl.getUniformLocation(program, 'u_ScreenParams'), new Float32Array([cw,ch]));
        gl.uniform3fv(gl.getUniformLocation(program, 'x_value'), x_value);
        gl.uniform3fv(gl.getUniformLocation(program, 'y_value'), y_value);
        gl.uniform1f(gl.getUniformLocation(program, 'progress'), progress);
        gl.uniform1f(gl.getUniformLocation(program, 'value'), value);
        gl.uniform1f(gl.getUniformLocation(program, 'radius'), radius);
        gl.uniform2fv(gl.getUniformLocation(program, 'face_center'), new Float32Array([0.5,0.5]));
    }
}
export default EffectRippledistortion;