#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform vec2 u_ScreenParams;
uniform vec3 x_value;
uniform vec3 y_value;
uniform vec2 face_center;
uniform float progress;
uniform float value;
uniform float radius;

const float pi = 3.1415926;


out vec4 o_result;

vec2 mirror(vec2 x){
    return abs(mod(x+1., 2.)-1.);
}

float Mask(vec2 uv){
    vec2 uv_center = uv;
    uv_center -= 0.5;
    uv_center.y *= u_ScreenParams.y / u_ScreenParams.x;
    float d = length(uv_center + vec2(1.-face_center.x, 1.-face_center.y) - 0.5);
    d = 1. - d;
    d = smoothstep(0., 1., d);
    d = smoothstep(0., 1., d);
    return d;
}

vec2 fish_eye(vec2 uv) 
{
    float ratio = u_ScreenParams.x/u_ScreenParams.y;

    vec2 uv2 = uv;
    vec2 uv3 = uv;
    uv3 -= .5;
    vec2 scale = vec2(1, 1);
    if (ratio < 1.) {
        scale.x *= ratio;
    } else {
        scale.y /= ratio;
    }
    uv3 *= scale;
    uv3 += .5;

    vec2 center = vec2(.5, .5);
    vec2 d = uv3 - center;
    float r0 = sqrt(dot(center, center));
    float r = sqrt(dot(d, d));

    float power = (2.0 * 3.141592 / (2.0 * r0)) * (value - 0.5);

    float bind = mix(r0, mix(center.x, center.y, step(1., ratio)), step(power, 0.));

    if (power > 0.0) {
        uv2 = center + mix(normalize(d) * tan(r * power) * bind / tan(bind * power), d, smoothstep(0., radius, r)) * (1. / scale);
    } else if (power < 0.0) {
        uv2 = center + mix(normalize(d) * atan(r * -power * 10.0) * bind / atan(-power * bind * 10.0), d, smoothstep(0., radius, r)) * (1. / scale);
    }
    return uv2;
}

void main( void ){

    vec2 uv0 = uv;
    float x_range = uv0.x * 2. - 1.;

    float mask = Mask(uv);

    float x_power = (x_range * x_range * x_range);
    uv0.x += sin(uv0.y * x_value.x + x_value.z * progress) * x_value.y * (u_ScreenParams.y/u_ScreenParams.x) * 0.01
    * (smoothstep(0.,1.,smoothstep(0.,1.,uv0.x + sin(3. + progress * pi * 0.5) * 0.05))*2.-1.)
    * ((-abs(uv0.x * 2. - 1.)+1.) * 1. + 0.0) * mask;
    ;

    uv0.y += sin(uv0.y * y_value.x + y_value.z * progress) * y_value.y * (u_ScreenParams.x/u_ScreenParams.y) * 0.01 * mask;
    uv0.y += sin(uv0.x * 10. + progress * 0.8) * sin(uv0.x * 4. + progress * 0.6) * (u_ScreenParams.x/u_ScreenParams.y) * 0.03 * mask;
    

    
    vec4 res = texture(samplerA, mirror(fish_eye(uv0)));

    // res = vec4(mask);
    o_result = res;
}