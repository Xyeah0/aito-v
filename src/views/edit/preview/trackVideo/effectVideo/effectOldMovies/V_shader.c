#version 300 es

precision highp float;
precision highp int;

layout(location = 0) in vec4 g_pos;
out vec2 uv;

void main() {
	float curX = (g_pos.x + 1.) / 2.;
	float curY = (g_pos.y + 1.) / 2.;
	uv = vec2(curX, curY);
	
	gl_Position = g_pos;
}