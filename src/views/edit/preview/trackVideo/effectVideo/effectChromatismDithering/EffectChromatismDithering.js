import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectChromatismDithering{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.scale = [1.0,1.07,1.1,1.13,1.17,1.2,1.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0];
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            speed: 0.33,
            horz: 0.6,
            intensity: 1.0
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
                params.horz = preParameter.horz*(1-percentage)+nowParameter.horz*percentage;
                params.intensity = preParameter.intensity*(1-percentage)+nowParameter.intensity*percentage;
            }
        }

        let gl = _gl || this.gl;
        let program = this.program;
        let uTime = curPos - clip.inPoint || 0;
        let cw = canvas.width,ch = canvas.height;
        let localTime = uTime * (0.5 + 1.5 * params.speed);
        let count = Math.floor(localTime / 0.05);
        
        gl.uniform1f(gl.getUniformLocation(program, 'timer'), count/40);
        gl.uniform1f(gl.getUniformLocation(program, 'intensity'), params.intensity);
        gl.uniform1f(gl.getUniformLocation(program, 'horzIntensity'), (params.horz-0.5)*2);
        gl.uniform1f(gl.getUniformLocation(program, 'GLITCH'), 0.1);
        gl.uniform1f(gl.getUniformLocation(program, 'swing'), 16);
        gl.uniform1f(gl.getUniformLocation(program, 'yStep'), 10);
    }
}
export default EffectChromatismDithering;