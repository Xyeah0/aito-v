#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform int         g_iRenderPassType;
uniform float       g_x;
uniform float       g_y;
uniform float       g_w;
uniform float       g_h;

out vec4 o_result;


void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    o_result = color;
    if(vTexCoord.x >= g_x && vTexCoord.x <= g_x+g_w && vTexCoord.y >= g_y && vTexCoord.y <= g_y+g_h){
        if(g_iRenderPassType == 0){
            vec4 color1 = texture(samplerA, vec2(vTexCoord.x,g_y));
            vec4 color2 = texture(samplerA, vec2(vTexCoord.x,g_y+g_h));
            float f = (vTexCoord.y-g_y)/g_h;
            o_result = color1 + (color2 - color1 )* f;
        }else{
            vec4 color1 = texture(samplerA, vec2(g_x,vTexCoord.y));
            vec4 color2 = texture(samplerA, vec2(g_x+g_w,vTexCoord.y));
            float f = (vTexCoord.x-g_x)/g_w;
            o_result = color1 + (color2 - color1 )* f;
        }
        // vec4 color3 = vec4(1.0,.0,.0,vTexCoord.y);
        // o_result = color3;
    }else{
        o_result = color;
    }
}