#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform highp float frameShiftDis;
uniform highp float frameShiftDisLeft1;
uniform highp float frameShiftDisLeft2;
uniform highp float frameShiftDisRight1;
uniform highp float frameShiftDisRight2;
uniform highp float frameShiftDisRight3;
// uniform highp float mixPercentLeft;
// uniform highp float mixPercentRight;
uniform float timeStampV;

const highp float mixPercentLeft = 0.15;
const highp float mixPercentRight = 0.15;


out vec4 o_result;

void main( void ){

    vec2 uv0 = uv;

    highp vec4 textureColor1 = texture(samplerA, vec2(uv0.x-frameShiftDis, uv0.y));
    highp vec4 textureColorLeft11 = texture(samplerA, vec2(uv0.x-frameShiftDisLeft1-frameShiftDis, uv0.y));
    highp vec4 textureColorLeft12 = texture(samplerA, vec2(uv0.x-frameShiftDisLeft2-frameShiftDis, uv0.y));
    highp vec4 textureMixLeft11 = mix(textureColor1,textureColorLeft11,4.0*mixPercentLeft);
    highp vec4 textureMixLeft12 = mix(textureMixLeft11,textureColorLeft12,2.0*mixPercentLeft);
    
    highp vec4 textureColorRight11 = texture(samplerA, vec2(uv0.x-frameShiftDisRight1-frameShiftDis, uv0.y));
    highp vec4 textureColorRight12 = texture(samplerA, vec2(uv0.x-frameShiftDisRight2-frameShiftDis, uv0.y));
    highp vec4 textureColorRight13 = texture(samplerA, vec2(uv0.x-frameShiftDisRight3-frameShiftDis, uv0.y));
    
    highp vec4 textureMixRight11 = mix(textureColor1,textureColorRight11,4.0*mixPercentRight);
    highp vec4 textureMixRight12 = mix(textureMixRight11,textureColorRight12,3.0*mixPercentRight);
    highp vec4 textureMixRight13 = mix(textureMixRight12,textureColorRight13,2.0*mixPercentRight);
    
    highp vec4 textureColorFinal1 = mix(textureMixLeft12,textureMixRight13,0.5);

    if (frameShiftDis == 0.0) {
        o_result = textureColor1;
    }else{
        o_result = textureColorFinal1;
    }
}
