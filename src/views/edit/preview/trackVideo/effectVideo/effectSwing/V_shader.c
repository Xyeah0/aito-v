#version 300 es

precision highp float;
precision highp int;

layout(location = 0) in vec4 g_pos;
out vec2 uv;

uniform float timeStampV;

void main() {
    float curX = (g_pos.x + 1.) / 2.;
    float curY = (g_pos.y + 1.) / 2.;
    uv = vec2(curX, curY);
    
    // gl_Position = g_pos;
    if(timeStampV < 16.0){
        gl_Position = vec4(g_pos.xy*1.05,0.0,1.0);
    }else{
        gl_Position = vec4(g_pos.xy,0.0,1.0);
    }
}