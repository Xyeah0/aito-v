import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectSwing{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.raw_texture = null;//原始纹理
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);
        this.raw_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.raw_texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            speed:0.33,
            range:1,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
                params.range = preParameter.range*(1-percentage)+nowParameter.range*percentage;
            }
        }
        // console.log('curPos',curPos,params.brightness)
        const timeStampV = [0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0]
        const frameShiftDis = [0.0,0.022,-0.024,0.0,0.009,-0.024,0.02,-0.009,-0.022,0.0185,0.022,-0.0185,-0.009,-0.022,0.009,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        const frameShiftDisLeft1 = [0.0,-0.037,-0.037,-0.037,-0.037,-0.074,-0.074,-0.074,-0.037,-0.037,-0.037,-0.074,-0.074,-0.074,-0.037,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        const frameShiftDisLeft2 = [0.0,-0.0185,-0.0185,-0.0185,-0.0185,-0.0648,-0.0648,-0.0648,-0.028,-0.028,-0.0648,-0.0648,-0.0648,-0.0648,-0.0185,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        const frameShiftDisRight1 = [0.0,0.037,0.037,0.037,0.037,0.056,0.056,0.056,0.074,0.074,0.074,0.056,0.056,0.056,0.037,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        const frameShiftDisRight2 = [0.0,0.056,0.056,0.056,0.056,0.074,0.074,0.074,0.148,0.148,0.148,0.074,0.074,0.074,0.056,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        const frameShiftDisRight3 = [0.0,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0185,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        let gl = _gl || this.gl;
        let program = this.program;

        let range = params.range;
        let uTime = curPos - clip.inPoint || 0;
        let id = Math.floor(uTime * (0.5 + 1.5 * params.speed) * 30);
        let cur_id = id%timeStampV.length;
        gl.uniform1f(gl.getUniformLocation(program, 'timeStampV'), timeStampV[cur_id]);
        cur_id = id%frameShiftDis.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDis'), frameShiftDis[cur_id] * range);
        cur_id = id%frameShiftDisLeft1.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDisLeft1'), frameShiftDisLeft1[cur_id] * range);
        cur_id = id%frameShiftDisLeft2.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDisLeft2'), frameShiftDisLeft2[cur_id] * range);
        cur_id = id%frameShiftDisRight1.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDisRight1'), frameShiftDisRight1[cur_id] * (range));
        cur_id = id%frameShiftDisRight2.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDisRight2'), frameShiftDisRight2[cur_id] * (range));
        cur_id = id%frameShiftDisRight3.length;
        gl.uniform1f(gl.getUniformLocation(program, 'frameShiftDisRight3'), frameShiftDisRight3[cur_id] * (range));
    }
}
export default EffectSwing;