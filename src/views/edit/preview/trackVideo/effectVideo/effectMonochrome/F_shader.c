#version 300 es
        
precision highp float;
precision highp int;


in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform float       g_r;
uniform float       g_g;
uniform float       g_b;

out vec4 o_result;


void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    vec4 retColor = vec4(g_r,g_g,g_b,color.a);
    o_result = retColor;

}