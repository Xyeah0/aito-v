#version 300 es
        
precision highp float;
precision highp int;

uniform sampler2D   samplerA;
in vec2 uv;
out vec4 o_result;

uniform float iTime;
uniform float speed;
uniform float alpha;

#define PI 3.14159

vec4 screen(vec4 base, vec4 blend)
{
    vec4 white = vec4(1.0);
    vec4 result = white - ((white - blend) * (white - base));
    
    return result;
}

float Sample(vec2 uv, vec2 offset)
{
    vec2 iResolution = vec2(1920.0,1080.0);
    float factor = iResolution.y / 433.0;
    vec4 color = texture(samplerA, uv + (offset / iResolution.xy) * factor);
    
    return dot(color.rgb, vec3(0.114, 0.587, 0.299));
}

float SobelFilter(vec2 uv)
{
    float tl = Sample(uv, vec2(-1.0, 1.0));
    float tc = Sample(uv, vec2(0.0, 1.0));
    float tr = Sample(uv, vec2(1.0, 1.0));
    
    float l = Sample(uv, vec2(-1.0, 0.0));
    float c = Sample(uv, vec2(0.0, 0.0));
    float r = Sample(uv, vec2(1.0, 0.0));
    
    float bl = Sample(uv, vec2(-1.0, -1.0));
    float bc = Sample(uv, vec2(0.0, -1.0));
    float br = Sample(uv, vec2(1.0, -1.0));
    
    float gx = tl - tr + 2.0 * l - 2.0 * r + bl - br;
    float gy = -tl - 2.0 * tc - tr + bl + 2.0 * bc + br;
    
    return gx * gx + gy * gy;
}

vec3 gradChange(int frameID)
{
    int num = 4;
    frameID = frameID - num * (frameID / num);
    vec3 res = vec3(0.0);
    
    if(frameID == 0){
        res = vec3(1.0);
    }else if(frameID == 1){
        res = vec3(0.75);
    }else if(frameID == 2){
        res = vec3(0.25);
    }else if(frameID == 3){
        res = vec3(0.0);
    }
    
    return res;
}

vec3 pal(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
    return a + b*cos( 6.28318*(c*t+d) );
}

vec2 posNeg(int frameID, int num)
{
    vec2 index = vec2(0.0);
    frameID = frameID - num * (frameID / num);
    
    if(frameID / (num / 2) == 0){
        index.x = 1.0;
        index.y = sin(float(frameID)*PI/float(num/2));
    }
    else{
        index.x = -1.0;
        index.y = sin(float(frameID-num/2)*PI/float(num/2));
    }
    
    return index;
}

void main( void )
{
    // float speed = 1.0;//透明度 0-1.0
    // float alpha = 1.0;//速度 0-1.0
    float time = iTime*speed;
    int frameID = int(time * 25.0);

    vec2 tc = uv;
    vec4 orig = texture(samplerA, tc);
    vec2 uv0 = 2.0 * tc - 1.0;

    vec3 col = pal( abs(sin(time*0.25)), vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,1.0),vec3(0.0,0.33,0.67) );
    vec3 vary = gradChange(frameID) * col * 0.6;

    vec4 retColor = vec4(0.0);
    float p = 10.0;
    uv0 = abs(uv0);
    retColor.rgb += (pow(uv0.x, p) + pow(uv0.y, p)-pow(uv0.x,p)*pow(uv0.y, p)) * vary;

    vec3 offset = vec3(0.45, 0.1, 0.15);
    int num = 15;
    float displaceCoff = log2( (fract( time*25.0/float(num) ) * 31.0) + 1.0 )* 0.20;

    vec2 index = posNeg(frameID, num * 2);
    vec2 uv1 = vec2(tc.x + displaceCoff * offset.x * index.x + 0.07, tc.y + displaceCoff * offset.y + 0.02)* (1.0 - displaceCoff * offset.z);
    float matt = step(0.0, uv1.x)*step(uv1.x, 1.0)*step(0.0, uv1.y)*step(uv1.y, 1.0);

    float sobel = SobelFilter(uv1);
    retColor.rgb += vec3( SobelFilter(uv1) * matt ) * col * 2.0;
    retColor.a = sobel * index.y;

    retColor = screen(orig, retColor);
    
    o_result = mix(orig, retColor, alpha);
}