#version 300 es

precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform  vec2    Segments;
uniform  vec2    Center;

out vec4 o_result;

void main() {
    vec2 texCoord = vec2(vTexCoord.x,vTexCoord.y);
    vec2 block_length  = 1.0f / Segments.xy;
    vec2 remain_length = 1.0f - block_length;
    vec2 topleft_offset= remain_length / 2.0f;

    texCoord -= topleft_offset;
    if ( texCoord.x < 0.0f )
    texCoord.x += floor(Segments.x+1.0f)*block_length.x;
    if ( texCoord.y < 0.0f )
    texCoord.y += floor(Segments.y+1.0f)*block_length.y;

    texCoord += (Center.xy+0.5f) * block_length;
    vec2 distToBlockTopLeft = mod(texCoord, block_length);
    vec2 tc_clip = distToBlockTopLeft / block_length;
    vec4 oColor = texture(samplerA,tc_clip);
    
    o_result = oColor;
}