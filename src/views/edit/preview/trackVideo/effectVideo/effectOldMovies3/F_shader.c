#version 300 es

precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   texture_filter;
uniform float   iTime;
uniform vec2    iResolution;
uniform bool    isGray;
uniform bool    isCurled;

out vec4 o_result;

const float uIntensity = 0.03f; // Frame distortion intensity. 0.02 ~ 0.05 recommended. 
const float uThreshold = 0.85f; // 0.75 ~ 0.90 would be recommended.
const float uMax       = 64.0f; // Distortion for edge of threshold.
const float uMargin    = 8.0f;  // Margin.

float GetOverThreadsholdIntensity(const float a, const float t) {
	float b = pow(t, 2.0f) * (1.0f - (1.0f / uMax));
	return uMax * pow(a - (t - (t / uMax)), 2.0f) + b;
}

bool IsOob(const vec2 inputTexCoord) {
	return inputTexCoord.x > 1.0f || inputTexCoord.y > 1.0f 
		|| inputTexCoord.x < 0.0f || inputTexCoord.y < 0.0f;
}

vec2 ApplyMargin(const vec2 texel, const float margin) {
	vec2 m = vec2(margin * 4.0f) / iResolution.xy;
	return (texel - 0.5f) * (1.0f + m) + 0.5f;
}

vec3 getCurled(vec2 uv){
	float x = uv.x * 2.0f - 1.0f;
	float y = uv.y * 2.0f - 1.0f;
	
	// Distort uv coordinate, and if closer to frame bound, do more distortion.
	float x_intensity = uIntensity;
	float y_intensity = uIntensity;
	if (abs(x) >= uThreshold && abs(y) >= uThreshold) {
		y_intensity *= GetOverThreadsholdIntensity(abs(x), uThreshold);
		x_intensity *= GetOverThreadsholdIntensity(abs(y), uThreshold);     
	}
	else {
		y_intensity *= pow(x, 2.0f);
		x_intensity *= pow(y, 2.0f);
	}
	
	// Get texel and apply margin (px)
	float y_offset 	= y_intensity * y;
	float x_offset 	= x_intensity * x;
	vec2 finalTexel = ApplyMargin(uv + vec2(x_offset, y_offset), uMargin);

	// ShaderToy does not support border (to be out-of-bound black color),
	// so checking texel is out of bound.
	vec3 col = vec3(0.0);
	if (IsOob(finalTexel) == false){
		col = texture(samplerA, finalTexel).rgb;
	}
	return col;
}

vec2 hash(vec2 p)
{
	vec3 p3 = fract(vec3(p.xyx) * vec3(166.1031, 147.1030, 142.0973));
	p3 += dot(p3, p3.yzx + 33.33);
	return fract((p3.xx + p3.yz) * p3.zy);
}

float simplexNoise(vec2 uv)
{
	const float k1 = 0.366025f;
	const float k2 = 0.211324f;

	vec2 idx = floor(uv + (uv.x + uv.y) * k1);
	vec2 a = uv - (vec2(idx) - float(idx.x + idx.y) * k2);
	vec2 tb = a.y > a.x ? vec2(0, 1) : vec2(1, 0);
	vec2 b = a - tb + k2;
	vec2 c = a - 1.f + k2 * 2.f;
	
	vec3 kernel = max(0.5f - vec3(dot(a, a), dot(b, b), dot(c, c)), 0.f);
	vec3 noise = kernel * kernel * kernel * kernel * 
	vec3(dot(a, hash(idx)*2.f-1.), 
		dot(b, hash(idx + tb)*2.-1.), 
		dot(c, hash(idx + 1.f)*2.-1.));
	
	return dot(vec3(70.), noise);
}


float verticalLine(vec2 uv, float time)
{
	float uvX = uv.x + time*0.0000003;
	vec2 xHash = hash(vec2(uvX,uvX));
	float vertical = step(0.9999996,sin(uvX*1000.0 * (xHash.x*0.01+0.01)));
	
	float uvY = uv.y + time*0.000001;
	vec2 yHash = hash(vec2(uvY,uvY));
	vertical *= sin(uvY*1000.0 * (yHash.x));
	
	return clamp(1.0 - vertical,0.,1.);
}


void main()
{   
	float t = iTime;
	
	vec3 col = vec3(0.0,0.0,0.0);
	col += verticalLine(uv-1.0,t)*0.5;
	col += (1.0 - verticalLine(uv-3.0,t*5.0))*0.5;
	col *= smoothstep(0.9,0.83, simplexNoise((uv + hash(vec2(t,t))*154.4541-154.4541)*10.0));
	col *= 1.f - hash(uv + t * 0.01f).x * 0.25;
	col *= smoothstep(1.5 + hash(floor(vec2(t,t)*15.)).x*0.3,0.0, length(uv-0.5));

	vec4 baseColor = texture(samplerA,uv);
	if(isCurled){
		baseColor.rgb = getCurled(uv);
	}
	vec3 tex = baseColor.rgb;
	if(isGray){
		tex = vec3( (tex.r + tex.g + tex.b)/3.0 );
	}
	
	o_result = vec4(col * tex,baseColor.a);
}