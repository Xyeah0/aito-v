#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform  sampler2D   samplerA;

uniform highp float uScanLineJitter_x; // line length
uniform highp float uScanLineJitter_y; // line density
uniform highp float uColorDrift_x; // 
uniform highp float uColorDrift_y; // 
uniform float uHorizontalShake;
uniform float uTimeStamp; 
uniform vec2  uRenderSize;
uniform int blurRadius;
uniform vec2 blurStep;

out vec4 o_result;

float gaussianWeight(float dist, float stdDev)
{
    return exp(-dist / (2.0 * stdDev));
}

vec4 gaussianBlur(sampler2D inputTexture, vec2 textureCoordinate, int radius, vec2 stepUV, vec2 screenSize)
{
    // vec4 sumColor       = vec4(0.0);
    // vec4 resultColor    = vec4(0.0);
    vec2 unitUV         = stepUV/screenSize;//vec2(blurSize/screenSize.x,blurSize/screenSize.y)*1.25;
    float stdDev        = 112.0;
    float sumWeight     = gaussianWeight(0.0,stdDev);
    vec4 curColor       = texture(inputTexture, textureCoordinate); 

    // curColor.a = texture(inputImageTexture,textureCoordinate).b;
    
    vec4 sumColor       = curColor*sumWeight;
    //horizontal
    for(int i=1;i<=13;i++)
    {
        vec2 textureCoordinateA = textureCoordinate+float(i)*unitUV;
        vec2 textureCoordinateB = textureCoordinate+float(-i)*unitUV;
        vec4 colorA = texture(inputTexture,textureCoordinateA);
        vec4 colorB = texture(inputTexture,textureCoordinateB);

        // colorA.a = texture(inputImageTexture,textureCoordinateA).b;
        // colorB.a = texture(inputImageTexture,textureCoordinateB).b;

        float curWeight = gaussianWeight(float(i),stdDev);
        sumColor += colorA*curWeight;
        sumColor += colorB*curWeight;
        sumWeight+= curWeight*2.0;
    }
    
    vec4 resultColor = sumColor/sumWeight;

    return resultColor;
}

float nrand(float x, float y)
{
    return fract(sin(dot(vec2(x, y), vec2(12.9898, 78.233))) * 43758.5453);
}

vec2 lm_compute_burr_uv()
{
    vec2 uVerticalJump = vec2(0.0,0.0); // (amount, time)
    vec2 textureCoordinate = uv;
    float u = textureCoordinate.x;
    float v = textureCoordinate.y;

    float jitter = nrand(v, uTimeStamp) * 2.0 - 1.0;
    jitter *= step(uScanLineJitter_y, abs(jitter)) * uScanLineJitter_x;
    float jump = mix(v, fract(v + uVerticalJump.y), uVerticalJump.x);
    float shake = (nrand(uTimeStamp, 2.0) - 0.5) * uHorizontalShake;
    
    // Color drift
    float drift = sin(jump + uColorDrift_y) * uColorDrift_x;
    
    float new_u1 = clamp(u + jitter + shake,0.0,0.99990);   // if set 1, maybe out of bounds
    float new_u2 = clamp(u + jitter + shake+drift,0.0,0.9999);
    
    vec2 res_uv = vec2(new_u1,jump);
    res_uv = mix(uv,res_uv,0.2);
    return res_uv;
}

void main( void ){

    // vec4 curColor = texture(inputImageTexture,uv0);
    // vec4 resultColor = curColor;

    vec2 new_uv = lm_compute_burr_uv();//uv0;//curColor.rg;
    vec4 videoColor = texture(samplerA,uv);


    vec4 resultColor = gaussianBlur(samplerA,  new_uv, blurRadius,blurStep,uRenderSize);

    float max_color = resultColor.a;
    resultColor = mix(videoColor,resultColor,max_color);//smoothstep(0.0,1.0,max_color));
    resultColor.a = videoColor.a;

    o_result = resultColor;

}