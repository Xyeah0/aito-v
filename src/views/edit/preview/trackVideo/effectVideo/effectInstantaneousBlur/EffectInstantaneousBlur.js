import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectInstantaneousBlur{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.fps = 30.0;
        this.uScanLineJitter_x = [0.03,0.01,0.02,0.06,0.065,0.05,0.03,0.04,0.0,0.0,0.0,0.0,0.04,0.07,0.06,0.075,0.03,0.035,0.05,0.065,0.0,0.0,0.0,0.0,0.073,0.05,0.08,0.05,0.07,0.035,0.017,0.04,0.0,0.0,0.0,0.0,0.038,0.02,0.025,0.08,0.055,0.025,0.06,0.02,0.0,0.0,0.0,0.0,0.022,0.04,0.03,0.016,0.028,0.02,0.045,0.03,0.0,0.0,0.0,0.0,0.014,0.03,0.022,0.034,0.075,0.056,0.012,0.034,0.0,0.0,0.0,0.0]
        this.uScanLineJitter_y = [0.9,0.77,0.8,0.65,0.45,0.7,0.35,0.65,0.0,0.0,0.0,0.0,0.9,0.68,0.7,0.85,0.44,0.56,0.78,0.89,0.0,0.0,0.0,0.0,0.94,0.88,0.65,0.32,0.63,0.85,0.92,0.95,0.0,0.0,0.0,0.0,0.82,0.95,0.82,0.72,0.35,0.25,0.4,0.74,0.0,0.0,0.0,0.0,0.87,0.6,0.39,0.24,0.49,0.6,0.88,0.85,0.0,0.0,0.0,0.0,0.95,0.88,0.64,0.28,0.37,0.54,0.66,0.82,0.0,0.0,0.0,0.0]
        this.uColorDrift_x = [0.025,0.035,0.05,0.025,0.04,0.035,0.03,0.02,0.0,0.0,0.0,0.0,0.035,0.055,0.04,0.02,0.025,0.07,0.006,0.04,0.0,0.0,0.0,0.0,0.025,0.025,0.015,0.025,0.0075,0.075,0.0175,0.015,0.0,0.0,0.0,0.0,0.04,0.035,0.025,0.035,0.02,0.045,0.03,0.025,0.0,0.0,0.0,0.0,0.015,0.02,0.035,0.0275,0.02,0.015,0.01,0.008,0.0,0.0,0.0,0.0,0.02,0.026,0.046,0.032,0.016,0.015,0.04,0.02,0.0,0.0,0.0,0.0]
        this.uColorDrift_y = [0.05,0.04,0.03,0.08,0.07,0.06,0.05,0.02,0.0,0.0,0.0,0.0,0.04,0.06,0.15,0.1,0.2,0.1,0.04,0.03,0.0,0.0,0.0,0.0,0.08,0.05,0.07,0.03,0.09,0.07,0.06,0.15,0.0,0.0,0.0,0.0,0.03,0.07,0.09,0.08,0.05,0.02,0.01,0.04,0.0,0.0,0.0,0.0,0.05,0.02,0.04,0.06,0.08,0.05,0.02,0.01,0.0,0.0,0.0,0.0,0.04,0.06,0.09,0.08,0.06,0.02,0.01,0.03,0.0,0.0,0.0,0.0]

    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            range: 1.0,
            blur: 1.0
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.range = preParameter.range*(1-percentage)+nowParameter.range*percentage;
                params.blur = preParameter.blur*(1-percentage)+nowParameter.blur*percentage;
            }
        }

        let gl = _gl || this.gl;
        let program = this.program;
        let cw = canvas.width,ch = canvas.height;
        let uTime = curPos - clip.inPoint || 0;
        let id = Math.floor(uTime*this.fps);
        let cur_id = id%this.uScanLineJitter_x.length;
        gl.uniform1f(gl.getUniformLocation(program, 'uScanLineJitter_x'), this.uScanLineJitter_x[cur_id] * params.range);
        cur_id = id%this.uScanLineJitter_y.length;
        gl.uniform1f(gl.getUniformLocation(program, 'uScanLineJitter_y'), this.uScanLineJitter_y[cur_id] * params.range);
        cur_id = id%this.uColorDrift_x.length;
        gl.uniform1f(gl.getUniformLocation(program, 'uColorDrift_x'), this.uColorDrift_x[cur_id] * params.range);
        cur_id = id%this.uColorDrift_y.length;
        gl.uniform1f(gl.getUniformLocation(program, 'uColorDrift_y'), this.uColorDrift_y[cur_id] * params.range);

        // gl.uniform1f(gl.getUniformLocation(program, 'timer'), timer);
        gl.uniform1f(gl.getUniformLocation(program, 'uHorizontalShake'), 0);
        gl.uniform1f(gl.getUniformLocation(program, 'uTimeStamp'), uTime);
        gl.uniform1i(gl.getUniformLocation(program, 'blurRadius'), 10);

        gl.uniform2fv(gl.getUniformLocation(program, 'blurStep'), new Float32Array([3*params.blur,0]));
        gl.uniform2fv(gl.getUniformLocation(program, 'uRenderSize'), new Float32Array([cw,ch]));
    }
}
export default EffectInstantaneousBlur;