#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform  sampler2D   samplerA;
uniform float scale;
uniform float horzIntensity;
uniform float vertIntensity;
out vec4 o_result;


void main( void ){

    vec2 newTextureCoordinate = vec2((scale - 1.0) * 0.5 + uv.x / scale,
                                (scale - 1.0) * 0.5 + uv.y / scale);
    vec4 textureColor = texture(samplerA, newTextureCoordinate);
    
    // shift color
    vec4 shiftColor1 = texture(samplerA, newTextureCoordinate + vec2(-0.05 * (scale - 1.0) * horzIntensity * 2., -0.05 * (scale - 1.0) * vertIntensity * 2.));
    vec4 shiftColor2 = texture(samplerA, newTextureCoordinate + vec2(-0.1 * (scale - 1.0) * horzIntensity * 2., -0.1 * (scale - 1.0) * vertIntensity * 2.));
    
    // 3d blend color
    vec3 blendFirstColor = vec3(textureColor.r, textureColor.g, shiftColor1.b);
    vec3 blend3DColor = vec3(shiftColor2.r, blendFirstColor.g, blendFirstColor.b);
    o_result = vec4(blend3DColor, textureColor.a);

    // o_result = texture(samplerA, vTexCoord);
}