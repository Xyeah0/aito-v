#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
out vec4 o_result;

uniform  sampler2D   samplerA;
// uniform vec2 uRenderSize;
uniform float uTime;
uniform float speedIntensity;
uniform float intensity;
uniform float horzIntensity;
#define PI 3.14159265

highp float sat(highp float t) {
    return clamp(t, 0.0, 1.0);
}

highp vec2 sat(highp vec2 t) {
    return clamp(t, 0.0, 1.0);
}

//remaps inteval [a;b] to [0;1]
highp float remap(highp float t, highp float a, highp float b) {
    return sat((t - a) / (b - a));
}

//note: /\ t=[0;0.5;1], y=[0;1;0]
highp float linterp(highp float t) {
    return sat(1.0 - abs(2.0 * t - 1.0));
}

highp vec3 spectrum_offset(highp float t) {
    highp float t0 = 3.0 * t - 1.5;
    return clamp(vec3(-t0, 1.0 - abs(t0), t0), 0.0, 1.0);
}

//note: [0;1]
highp float rand(highp vec2 n) {
    return fract(sin(dot(n.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

//note: [-1;1]
highp float srand(highp vec2 n) {
    return rand(n) * 2.0 - 1.0;
}

highp float mytrunc(highp float x, highp float num_levels) {
    return floor(x * num_levels) / num_levels;
}

highp vec2 mytrunc(highp vec2 x, highp float num_levels) {
    return floor(x * num_levels) / num_levels;
}
vec3 shakeAndDriftColor(sampler2D tex,vec2 texCoord,float time,int level)
{
    highp float GLITCH = 0.1;
    highp float swing = 6.0;
    highp float yStep = 40.0;

    if (level == 0) 
    {
        GLITCH = 0.09;
    } 
    else if (level == 1) 
    {
        GLITCH = 0.2;
    } 
    else 
    {
        GLITCH = 0.2;
        swing = 16.0;
        yStep = 10.0;
    }

    //texCoord.y = 1.0 - texCoord.y;
    highp float ITime = mod(time, 32.0); // + modelmat[0].x + modelmat[0].z;

    GLITCH *= intensity;
    highp float gnm = sat(GLITCH);
    highp float rnd0 = rand(mytrunc(vec2(ITime, ITime), swing));
    highp float r0 = sat((1.0 - gnm) * 0.7 + rnd0);
    highp float rnd1 = rand(vec2(mytrunc(texCoord.x, 10.0 * r0), ITime)); //horz
    //float r1 = 1.0f - sat( (1.0f-gnm)*0.5f + rnd1 );
    highp float r1 = 0.5 - 0.5 * gnm + rnd1;
    r1 = 1.0 - max(0.0, ((r1 < 1.0) ? r1 : 0.9999999)); //note: weird ass bug on old drivers
    highp float rnd2 = rand(vec2(mytrunc(texCoord.y, yStep * r1), ITime)); //vert
    highp float r2 = sat(rnd2);

    highp float rnd3 = rand(vec2(mytrunc(texCoord.y, 10.0 * r0), ITime));
    highp float r3 = (1.0 - sat(rnd3 + 0.8)) - 0.1;

    highp float pxrnd = rand(texCoord + ITime);

    highp float ofs = 0.05 * r2 * GLITCH * (rnd0 > 0.5 ? 1.0 : -1.0);
    ofs += 0.5 * pxrnd * ofs;

    texCoord.y += 0.1 * r3 * GLITCH;

    const int NUM_SAMPLES = 6;//10
    const highp float RCP_NUM_SAMPLES_F = 1.0 / float(NUM_SAMPLES);

    highp vec3 sum = vec3(0.0);
    highp vec3 wsum = vec3(0.0);
    for (int i = 0; i < NUM_SAMPLES; ++i) 
    {
        highp float t = float(i) * RCP_NUM_SAMPLES_F;
        texCoord.x = sat(texCoord.x + ofs * horzIntensity * t);
        highp vec3 samplecol = texture(tex, texCoord).rgb;
        highp vec3 s = spectrum_offset(t);
        // s = 1. + (s - 1.) * horzIntensity;
        samplecol.rgb = samplecol.rgb * s;
        sum += samplecol;
        wsum += s;
    }
    sum /= wsum;

    return sum;
}

float easeInOut(float x,float inflectionPoint)
{
    // if(x < 0.5)
    //     return pow(x,3.0) * 8.0;
    // else
    //     return pow(1.0 - x,3.0) * 8.0;    
    float result = 0.0;
    if(x < inflectionPoint && x > 0.01)
        result =  pow(x,3.0)/pow(inflectionPoint,3.0);
    else if(x < 1.0 && x >= inflectionPoint)
        result =  pow(1.0 - x,2.0)/pow(1.0 - inflectionPoint,2.0);      

    return result;
}

float easeInOut2(float x,float inflectionPoint)
{
    float result = 0.0;    
    if(x < inflectionPoint && x > 0.01 )
        result = pow(x,2.0)/pow(inflectionPoint,2.0);
    else if(x < 1.0 && x >= inflectionPoint)
        result = (1.0 - x)/(1.0 - inflectionPoint);
    return result;
}
float rand(float n) {
    return fract(sin(n) * 43758.5453123);
}

void main( void ){

    vec2 newUV = uv; 

    // float uTime = mod(time,2.0);
    float duration = 2.0;
    if(uTime < duration)
    {

        lowp float y1 = 0.2;
        lowp float y2 = 0.4;
        lowp float y3 = 0.6;
        lowp float y4 = 0.65;

        float progress = clamp(uTime/duration,0.0,1.0);
        float curveInflectionPoint = 0.35;
        float progress_x = easeInOut2(progress,curveInflectionPoint);
        float progress_y = easeInOut(progress,curveInflectionPoint);

        float scale = 1.0 - 0.2 * progress_x ;

        //calculate vertical boundary     
        float timeNode1 = curveInflectionPoint;
        float timeNode2 = 0.6;
        float timeNode3 = 0.8;    
        float timeNodeProgress1 = easeInOut(timeNode1,curveInflectionPoint);
        float timeNodeProgress2 = easeInOut(timeNode2,curveInflectionPoint);
        float timeNodeProgress3 = easeInOut(timeNode3,curveInflectionPoint);
        
        float state1_expanse1 = 0.15;
        float state1_expanse2 = 0.2;
        
        float state2_expanse1 = 0.15;
        float state2_expanse2 = 0.35;
        float state2_expanse3 = 0.2;
        
        float state3_expanse1 = 0.35;
        float state3_expanse2 = 0.55;    
        
        if(uTime < duration * timeNode1)
        {
            y1 = y1 - state1_expanse1 * progress_y;
            y2 = y2 + state1_expanse1 * progress_y;
        
            y3 = y3 - state1_expanse2 * progress_y;
            y4 = y4 + state1_expanse2 * progress_y;
        }
        else if(uTime < duration * timeNode2)
        {   
            y1 = y1 - state1_expanse1 * timeNodeProgress1 + state2_expanse1 * (timeNodeProgress1 - progress_y);
            y2 = y2 + state1_expanse1 * timeNodeProgress1 - state2_expanse1 * (timeNodeProgress1 - progress_y);
        
            y3 = y3 - state1_expanse2 * timeNodeProgress1 + state2_expanse2 * (timeNodeProgress1 - progress_y);
            y4 = y4 + state1_expanse2 * timeNodeProgress1 + state2_expanse3 * (timeNodeProgress1 - progress_y);        
        }
        else if(uTime < duration * timeNode3)
        {
            y1 = y1 - state1_expanse1 * timeNodeProgress1 + state2_expanse1 * (timeNodeProgress1 - progress_y);
            y2 = y2 + state1_expanse1 * timeNodeProgress1 - state2_expanse1 * (timeNodeProgress1 - progress_y);
            
            float temp3 = y3 - state1_expanse2 * timeNodeProgress1 + state2_expanse2 * (timeNodeProgress1 - timeNodeProgress2);
            float temp4 = y4 + state1_expanse2 * timeNodeProgress1 + state2_expanse3 * (timeNodeProgress1 - timeNodeProgress2);
            y3 = temp3 - state3_expanse1 * (timeNodeProgress2 - progress_y);
            y4 = temp4 - state3_expanse2 * (timeNodeProgress2 - progress_y);     
        }
        else
        {
            y1 = y1 - state1_expanse1 * timeNodeProgress1 + state2_expanse1 * (timeNodeProgress1 - progress_y);
            y2 = y2 + state1_expanse1 * timeNodeProgress1 - state2_expanse1 * (timeNodeProgress1 - progress_y);
            
            float temp3 = y3 - state1_expanse2 * timeNodeProgress1 + state2_expanse2 * (timeNodeProgress1 - timeNodeProgress2) - state3_expanse1 * (timeNodeProgress2 - timeNodeProgress3);
            float temp4 = y4 + state1_expanse2 * timeNodeProgress1 + state2_expanse3 * (timeNodeProgress1 - timeNodeProgress2) - state3_expanse2 * (timeNodeProgress2 - timeNodeProgress3);
            y3 = temp3 + 0.65 * (timeNodeProgress3 - progress_y);
            y4 = temp4 + 0.6 * (timeNodeProgress3 - progress_y);      
        }
        
        float temp2 = min(min(y2,y3),y4);        
        float temp4 = max(max(y2,y3),y4);
        float temp3 = y3;
        if(y2 > y3 && y2 < y4)
            temp3 = y2;
        else if(y2 > y4)
            temp3 = y4;

        y2 = temp2;
        y3 = temp3;
        y4 = temp4;


        //calculate horizonal offset   
        if(uv.y < y1)
        {
            float offsetCenter = 0.35;
            newUV.x = offsetCenter + (uv.x - offsetCenter) * scale;
        }
        else if(uv.y < y2)
        {
            float offsetCenter = 1.0;
            newUV.x = offsetCenter + (uv.x - offsetCenter) * scale;
        }
        else if(uv.y < y3)
        {
            // float offsetCenter = 0.3 +  progress_x * 0.4;
            float offsetCenter = 0.35 +  sin(progress * PI) * 0.35;            
            newUV.x = offsetCenter + (uv.x - offsetCenter) * scale;
        }
        else if(uv.y < y4)
        {
            scale =1.0 -  sin(progress * PI) * 0.3;
            float offsetCenter = 0.9 - progress_x * 0.4;
            newUV.x = offsetCenter + (uv.x - offsetCenter) * scale;
        }       
        else
        {
            // if(uTime > 0.5 * duration)
            //     scale = smoothstep(0.5,1.0,progress) * 0.2 + 0.8;            
            // float offsetCenter = 0.5;
            float offsetCenter = 0.5 +  sin(progress * PI) * 0.3;
            newUV.x = offsetCenter + (uv.x - offsetCenter) * scale;     
        }            
    }

    lowp vec4 color = texture(samplerA,newUV);  

    //shake and drift color
    if(uTime > duration * 0.5)
    {
        int level = 0;
        color.rgb = shakeAndDriftColor(samplerA,newUV,uTime,level);
    }
    
    o_result =  color;
    

    // o_result = texture(samplerA, vTexCoord);
}