#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform vec2 uRenderSize;
uniform float timer;
uniform float swing_period;
uniform float swing_max_angle;
uniform vec2 swing_center;
uniform float rangeIntensity;

#define PI 3.14159265


out vec4 o_result;

float mirror(float x)
{
    if(x<0.0)
        return (-x);
    else if(x>1.0)
        return (2.0-x);
    else
        return x;
}

vec2 mirror(vec2 uv)
{
    return vec2(mirror(uv.x),mirror(uv.y));
}

void main( void ){

    float progress = mod(timer,swing_period);
    float half_swing_period = swing_period*0.5;
    if(progress<half_swing_period)
    {
        progress = (progress*2.-half_swing_period)/half_swing_period;
    }
    else
    {
        progress = (swing_period-half_swing_period*0.5-progress)*2.0/half_swing_period;
    }

    vec2 screenSize = uRenderSize;;
    vec2 curCoord = uv*screenSize;
    vec2 center = swing_center*screenSize;
    

    float max_angle = PI*swing_max_angle/180.0 * rangeIntensity;
    float zoom_factor = 1.0;
    vec2 pt_right_bottom = vec2(1.0,0.0)*screenSize;
    vec2 pt_right_top = vec2(1.0,1.0)*screenSize;
    if(uRenderSize.x<=uRenderSize.y)
    {
        float k = tan(PI*0.5-max_angle);     
        float dis = (k*(pt_right_bottom.x-center.x)-(pt_right_bottom.y-center.y))/sqrt(1.0+pow(k,2.0));
        float half_width = screenSize.x*0.5;
        zoom_factor = half_width/dis;
    }
    else
    {                       
        float k = tan(PI-max_angle);
        float dis = abs((k*(pt_right_top.x-center.x)-(pt_right_top.y-center.y)))/sqrt(1.0+pow(k,2.0));
        float half_height = screenSize.y-center.y;//screenSize.y*0.5;
        zoom_factor = half_height/dis;
    }

    //rotate
    float angle = max_angle*progress;   
    mat2 rotate = mat2(cos(angle),-sin(angle),sin(angle),cos(angle));
    curCoord = (curCoord-center)*rotate+center;

    //zoom
    curCoord = center + (curCoord - center)*zoom_factor;

    curCoord = curCoord/screenSize;
    // curCoord = mirror(curCoord);
    o_result = texture(samplerA,curCoord);
}