#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform float timer;
uniform float fRadius;
uniform float blurSize;
uniform vec2  uRenderSize;

#define TWO_PI (3.141592653*2.0)


out vec4 o_result;

void main( void ){

    float radius = fRadius;
    float weight = 1.0;
    float time = mod(timer,1.0);
    vec2 centerUV = vec2(0.5)*uRenderSize;
    vec2 realUV = uv*uRenderSize;
    float zoomCoeff = 1.0;
    float blurCoeff = 1.0;
    float brightnessCoeff = 1.0;

    if(time<0.2)
    {
        zoomCoeff = 0.8+0.1*time/0.2;
        realUV = centerUV+(realUV-centerUV)*zoomCoeff;
    }
    else if(time<0.4)
    {
        zoomCoeff = 0.9;
        realUV = centerUV+(realUV-centerUV)*zoomCoeff;
    }
    else if(time<0.8)
    {
        zoomCoeff = 0.9+0.1*(time-0.4)/0.4;
        realUV = centerUV+(realUV-centerUV)*zoomCoeff;
    }
    
    if(time<=0.92 && time>=0.24)
    {
        float mid = (0.92+0.24)*0.5;
        float range = (0.92-0.24)*0.5;
        blurCoeff = 1.0-abs(time-mid)/range;
        radius *= blurCoeff;
    }
    else
    {
        radius = 0.0;
    }

    if(time>0.4 && time<=1.0)
    {
        float mid = (1.0+0.4)*0.5;
        float range = (1.0-0.4)*0.5;
        brightnessCoeff = 1.0-abs(time-mid)/range;
        brightnessCoeff = 1.0+0.6*brightnessCoeff;
    }

    vec2 direction = realUV - centerUV;
    float coeff = length(direction)/length(centerUV);
    vec2 step = direction/distance(realUV,centerUV)*blurSize*pow(coeff,0.85);

    vec4 curColor = texture(samplerA, uv)*weight*brightnessCoeff;
    vec4 sumColor = curColor;
    float sumWeight    = weight;
    //radial blur

    const float MAX_VALUE = 25.;
    for(float i=1.0;i<=MAX_VALUE;i+=1.0)
    {
        if (i > radius) break;
        vec2 curStep = step*float(i);
        vec2 curRightCoordinate = (realUV+curStep)/uRenderSize;//textureCoordinate+curStep;
        vec2 curLeftCoordinate  = (realUV-curStep)/uRenderSize;//textureCoordinate-curStep;
        vec4 rightColor = texture(samplerA,curRightCoordinate)*brightnessCoeff;
        vec4 leftColor = texture(samplerA,curLeftCoordinate)*brightnessCoeff;
        sumColor+=rightColor*weight;
        sumColor+=leftColor*weight;
        sumWeight+=weight*2.0;
    }
    vec4 resultColor = sumColor/sumWeight;
    // resultColor.a = texture(samplerA, uv).a;
    o_result = resultColor;
}