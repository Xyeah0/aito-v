#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform float       alpha;

out vec4 o_result;


void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    color.a = min(color.a,alpha);
    o_result = color;
}