#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;
uniform sampler2D   fireworks_texture;
uniform float   iTime;
uniform vec2    iResolution;
uniform float   count;//爆炸次数
uniform float   sparks;//爆炸火花数量
uniform float   duration;//爆炸持续时间
uniform float   speed;//爆炸速度
uniform float   radius;//爆炸半径
uniform int     stepMode;
uniform float   u_zoom;//缩小倍数

#define PI 3.141592653589793

// Hash function by Dave_Hoskins.
#define MOD3 vec3(.1031,.11369,.13787)

out vec4 o_result;
vec3 hash31(float p) {
    vec3 p3 = fract(vec3(p) * MOD3);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract(vec3((p3.x + p3.y) * p3.z, (p3.x + p3.z) * p3.y, (p3.y + p3.z) * p3.x));
}

vec3 create_fireworks(vec2 temp_uv,vec2 iRes){
    float aspectRatio = iRes.x / iRes.y;
    vec2 uv0 = temp_uv*iRes/iRes.y;
    float t = mod(iTime + 10., 7200.);;
    vec3 col = vec3(0.); 
    vec2 origin = vec2(0.);
    
    for (float j = 0.; j < count; ++j)
    {
        vec3 oh = hash31((j + 1234.1939) * 641.6974);
        origin = vec2(oh.x, oh.y) * .6 + .2; // .2 - .8 to avoid boundaries
        origin.x *= aspectRatio;
        // Change t value to randomize the spawning of explosions
        t += (j + 1.) * 9.6491 * oh.z;
        for (float i = 0.; i < sparks; ++i)
        {
            // Thanks Dave_Hoskins for the suggestion
            vec3 h = hash31(j * 963.31 + i + 497.8943);
            // random angle (0 - 2*PI)
            float a = h.x * PI * 2.;
            // random radius scale for spawning points anywhere in a circle
            float rScale = h.y * radius;
            // explosion loop based on time
            if (mod(t * speed, duration) > 2.)
            {
                // random radius 
                float r = mod(t * speed, duration) * rScale;
                // explosion spark polar coords 
                vec2 sparkPos = vec2(r * cos(a), r * sin(a));
                    // sparkPos.y -= pow(abs(sparkPos.x), 4.); // fake gravity
                // fake-ish gravity
                float poopoo = 0.04;
                float peepee = (length(sparkPos) - (rScale - poopoo)) / poopoo;
                sparkPos.y -= pow(peepee, 3.0) * 6e-5;
                // shiny spark particles
                float spark = .0002/pow(length(uv0 - sparkPos - origin), 1.65);
                // Make the explosion spark shimmer/sparkle
                float sd = 2. * length(origin-sparkPos);
                float shimmer = max(0., sqrt(sd) * (sin((t + h.y * 2. * PI) * 20.)));
                float shimmerThreshold = duration * .32;
                // fade the particles towards the end of explosion
                float fade = max(0., (duration - 5.) * rScale - r);
                // mix it all together
                col += spark * mix(1., shimmer, smoothstep(shimmerThreshold * rScale,
                    (shimmerThreshold + 1.) * rScale , r)) * fade * oh;
            }
        }
    }

    return col;
}

void main()
{
    
    if(stepMode == 0){
        vec3 col = vec3(0.);
        vec2 uv1 = uv*u_zoom;
        if(uv1.x < 1. && uv1.y < 1.){
            col = create_fireworks(uv1,iResolution/u_zoom);
        }
        o_result = vec4(col,1.);
    }else{
        vec4 baseColor = texture(samplerA,uv);
        vec4 sucaiColor = texture(fireworks_texture,uv/u_zoom);
        o_result = vec4(1.0-(1.0-baseColor.rgb)*(1.0-sucaiColor.rgb),baseColor.a);
    }
    
}