import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectFireworks{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);

        this.fireworks_texture = null;//烟花纹理
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.fireworks_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.fireworks_texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            duration:15,
            speed:5,
            radius:0.6,
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.duration = preParameter.duration*(1-percentage)+nowParameter.duration*percentage;
                params.speed = preParameter.speed*(1-percentage)+nowParameter.speed*percentage;
                params.radius = preParameter.radius*(1-percentage)+nowParameter.radius*percentage;
            }
        }

        let gl = _gl || this.gl;
        let program = this.program;
        let cw = canvas.width,ch = canvas.height;
        gl.uniform1f(gl.getUniformLocation(program,'iTime'), curPos);
        gl.uniform2fv(gl.getUniformLocation(program, 'iResolution'), [cw,ch]);

        gl.uniform1f(gl.getUniformLocation(program,'count'), 8);
        gl.uniform1f(gl.getUniformLocation(program,'sparks'), 128);
        gl.uniform1f(gl.getUniformLocation(program,'duration'), params.duration);
        gl.uniform1f(gl.getUniformLocation(program,'speed'), params.speed);
        gl.uniform1f(gl.getUniformLocation(program,'radius'), params.radius/10);
        gl.uniform1f(gl.getUniformLocation(program,'u_zoom'), Math.max(cw,ch)/640);

        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 0);
        gl.clearColor(.0, .0, .0, .0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

        //记录之前纹理
        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, this.fireworks_texture);
        gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);
        gl.uniform1i(gl.getUniformLocation(program,'fireworks_texture'), 1);
        

        gl.activeTexture(gl.TEXTURE0);
        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 1);
    }
}
export default EffectFireworks;