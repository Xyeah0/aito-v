#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform float       left;
uniform float       right;
uniform float       top;
uniform float       bottom;

out vec4 o_result;


void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    if(vTexCoord.x < left || vTexCoord.x > right || vTexCoord.y < top || vTexCoord.y > bottom ){
        color = vec4(0.0,0.0,0.0,0.0);
    }
    o_result = color;
}