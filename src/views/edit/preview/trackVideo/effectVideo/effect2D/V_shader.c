#version 300 es

precision highp float;
precision highp int;

layout(location = 0) in vec4 g_pos;

out vec2 vTexCoord;

uniform float       scaleX;
uniform float       scaleY;
uniform float       offsetX;
uniform float       offsetY;
uniform float       rotate;
uniform float       whs;
const float PI =  3.141592653589793238462643383279;


void main() {

    // const float PI =  3.141592653589793238462643383279;
    
    mat4 translate44 = mat4(
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        -offsetX*2.0, offsetY*2.0, 0.0f, 1.0f);
    float scalex = 1.0f;
    float scaley = 1.0f;
    if(scaleX == 0.0){
        scalex = 1.0/0.001;
    }else{
        scalex = 1.0/scaleX;
    }
    if(scaleY == 0.0){
        scaley = 1.0/0.001;
    }else{
        scaley = 1.0/scaleY;
    }
    mat4 scale44 = mat4(
        scalex, 0.0f, 0.0f, 0.0f,
        0.0f, scaley, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    float ase = rotate*PI/180.0;
    mat4 rotate44 = mat4(
        cos(ase), -sin(ase), 0.0f, 0.0f,
        sin(ase), cos(ase), 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    
    vec4 result = translate44*g_pos;
    result = scale44*result;
    result.x *= whs;
    result = rotate44*result;
    result.x /= whs;
    
    float curX = (result.x+1.)/2.;
    float curY = (result.y+1.)/2.;

    vTexCoord = vec2(curX, curY);
    
    gl_Position = g_pos;
}