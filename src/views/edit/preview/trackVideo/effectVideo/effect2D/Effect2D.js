import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class Effect2D{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas){
        let params = {
            left:0,
            right:1,
            top:0,
            bottom:1,
            scaleX:1.0,
            scaleY:1.0,
            offsetX:0.0,
            offsetY:0.0,
            rotate:0
        }
        let aspectratio = canvas.width/canvas.height;
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.left = preParameter.left*(1-percentage)+nowParameter.left*percentage;
                params.right = preParameter.right*(1-percentage)+nowParameter.right*percentage;
                params.top = preParameter.top*(1-percentage)+nowParameter.top*percentage;
                params.bottom = preParameter.bottom*(1-percentage)+nowParameter.bottom*percentage;
                params.scaleX = preParameter.scaleX*(1-percentage)+nowParameter.scaleX*percentage;
                params.scaleY = preParameter.scaleY*(1-percentage)+nowParameter.scaleY*percentage;
                params.offsetX = preParameter.offsetX*(1-percentage)+nowParameter.offsetX*percentage;
                params.offsetY = preParameter.offsetY*(1-percentage)+nowParameter.offsetY*percentage;
                params.rotate = preParameter.rotate*(1-percentage)+nowParameter.rotate*percentage;
            }
        }
        // console.log('curPos',curPos,params.rotate,aspectratio)//
        // if(keyFrameParameters.length>0){
        //     params = keyFrameParameters[0].parameter;
        // }
        let gl = _gl || this.gl;
        let program = this.program;
        // console.log(params.scaleX,params.scaleY)
        gl.uniform1f(gl.getUniformLocation(program, 'left'), params.left);
        gl.uniform1f(gl.getUniformLocation(program, 'right'), params.right);
        gl.uniform1f(gl.getUniformLocation(program, 'top'), params.top);
        gl.uniform1f(gl.getUniformLocation(program, 'bottom'), params.bottom);
        gl.uniform1f(gl.getUniformLocation(program, 'scaleX'), params.scaleX);
        gl.uniform1f(gl.getUniformLocation(program, 'scaleY'), params.scaleY);
        gl.uniform1f(gl.getUniformLocation(program, 'offsetX'), params.offsetX);
        gl.uniform1f(gl.getUniformLocation(program, 'offsetY'), params.offsetY);
        gl.uniform1f(gl.getUniformLocation(program, 'rotate'), params.rotate);
        //传入高宽比
        gl.uniform1f(gl.getUniformLocation(program, 'whs'), aspectratio);
    }
}
export default Effect2D;