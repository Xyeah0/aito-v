#version 300 es
        
precision highp float;
precision highp int;


in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform  mat4    g_matTex;
uniform	 vec4	 g_vCenter;
uniform  vec4    g_vDir;
uniform  vec4    g_vNormal;

out vec4 o_result;

void main() {

    vec2 vTex = vTexCoord;
    vec2 fTex = (vTexCoord-0.5);
    fTex.y = -fTex.y;	

    float fAlpha = 1.0;

    bool bMirror = false;
    
    vec2 v1 = fTex - g_vCenter.xy;
    if(dot(g_vNormal.xy,normalize(v1))>0.0)
    {
        vec2 v2 = reflect(normalize(-v1),g_vDir.xy)*length(v1)+g_vCenter.xy;
        
        v2.y = -v2.y;
        v2+=0.5;
    
        vTex = v2;
            
        if(vTex.x<0.0||vTex.x>1.0
        ||vTex.y<0.0||vTex.y>1.0)
        {
            //clip(-1);
            fTex = 1.0 - (abs(vTex-0.5) - 0.5)*200.0;
            fAlpha = clamp(min(fTex.x,fTex.y),0.0,1.0);		
        }
    
        bMirror = true;
    }	
    
    vTex  = ( g_matTex*vec4(vTex, 1.0, 0.0) ).xy; 
        
    vec4 oColor = texture(samplerA,vTex);
    oColor.a *= fAlpha;	
    
    o_result = oColor;
}