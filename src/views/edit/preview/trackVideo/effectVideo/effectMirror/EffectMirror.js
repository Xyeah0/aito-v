import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectMirror{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos){
        let params = {
            fCenterX: 0,
            fCenterY:-1,
            fAngle:0
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.fCenterX = preParameter.fCenterX*(1-percentage)+nowParameter.fCenterX*percentage;
                params.fCenterY = preParameter.fCenterY*(1-percentage)+nowParameter.fCenterY*percentage;
                params.fAngle = preParameter.fAngle*(1-percentage)+nowParameter.fAngle*percentage;
            }
        }
        let angle = params.fAngle*Math.PI/180;
        const matRot = {
            _11:Math.cos(angle),
            _12:-Math.sin(angle),
            _13:0,
            _14:0,
            _21:Math.sin(angle),
            _22:Math.cos(angle),
            _23:0,
            _24:0,
            _31:0,
            _32:0,
            _33:1,
            _34:0,
            _41:0,
            _42:0,
            _43:0,
            _44:1,
        }
        const vPt = [
            {
                x:0,y:1,z:0
            },{
                x:1,y:0,z:0
            }
        ]

        function ESVec3TransformCoord(pDesV,pSrcV,pSrcM){
            let SrcV4={x:0,y:0,z:0,w:0},DesV4 = {x:0,y:0,z:0,w:0};
            SrcV4.x = pSrcV.x;
            SrcV4.y = pSrcV.y;
            SrcV4.z = pSrcV.z;
            SrcV4.w = 1;
            NXVECTOR4F_MulMat(DesV4,SrcV4,pSrcM);
            pDesV.x = DesV4.x/DesV4.w;
            pDesV.y = DesV4.y/DesV4.w;
            pDesV.z = DesV4.z/DesV4.w;
        }

        function NXVECTOR4F_MulMat(pDesV,pSrcV,pSrcM){
            let pTempVec = pSrcV;
            pDesV.x = pTempVec.x*pSrcM._11+pTempVec.y*pSrcM._21+pTempVec.z*pSrcM._31+pTempVec.w*pSrcM._41;
            pDesV.y = pTempVec.x*pSrcM._12+pTempVec.y*pSrcM._22+pTempVec.z*pSrcM._32+pTempVec.w*pSrcM._42;
            pDesV.z = pTempVec.x*pSrcM._13+pTempVec.y*pSrcM._23+pTempVec.z*pSrcM._33+pTempVec.w*pSrcM._43;
            pDesV.w = pTempVec.x*pSrcM._14+pTempVec.y*pSrcM._24+pTempVec.z*pSrcM._34+pTempVec.w*pSrcM._44;
        }

        function TPFLOAT3F_Normalize(pDesV,pSrcV){
            //增加微量以防止奇异计算
            let k = 1/(NXVECTOR3F_Mod(pSrcV)+0.0000011);
            pDesV.x = pSrcV.x*k;
            pDesV.y = pSrcV.y*k;
            pDesV.z = pSrcV.z*k;
        }

        function NXVECTOR3F_Mod(pV){
            return Math.sqrt(pV.x*pV.x+pV.y*pV.y+pV.z*pV.z)
        }

        ESVec3TransformCoord(vPt[0],vPt[0],matRot);
        ESVec3TransformCoord(vPt[1],vPt[1],matRot);

        TPFLOAT3F_Normalize(vPt[0],vPt[0]);
        TPFLOAT3F_Normalize(vPt[1],vPt[1]);
        // console.log(vPt[0],vPt[1])
        let gl = this.gl;
        let program = this.program;
        const vCenter = [params.fCenterX, params.fCenterY, 0, 0];
        const vDir = [vPt[0].x, vPt[0].y, 0, 0];
        const vNormal = [vPt[1].x, vPt[1].y, 0, 0];
        let matTexture = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vCenter'), vCenter);
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vDir'), vDir);
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vNormal'), vNormal);
        gl.uniformMatrix4fv(gl.getUniformLocation(program, 'g_matTex'), false, matTexture);
    }
}
export default EffectMirror;