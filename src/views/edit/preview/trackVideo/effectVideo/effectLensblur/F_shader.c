#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform sampler2D   samplerA;

uniform vec2 uRenderSize;
uniform float intensity;
uniform float BRIGHT_SPOT_TRESHOLD; //亮度预置
uniform int CONV_SIDE; //不超过20


out vec4 o_result;

vec3 BriSp(vec3 p){
    if(p.x + p.y + p.z > BRIGHT_SPOT_TRESHOLD*3.)
    p = p*(1. + BRIGHT_SPOT_TRESHOLD);
    return p;
}
    
vec3 color(vec2 n){
    vec3 p = texture(samplerA,n).rgb;
    p = BriSp(p);
    return p;
}

void main( void ){

    vec2 un = uv * uRenderSize.xy;

    vec2 temp_un = vec2(0);
    float n = 0.;
    vec3 res = vec3(0);
    
    for(int x = -CONV_SIDE;x <= CONV_SIDE;x++){
        for(int y = -CONV_SIDE;y <= CONV_SIDE;y++){
            temp_un = un + vec2(x,y) * intensity;
            if(distance(temp_un,un) <= float(CONV_SIDE) * intensity){
                temp_un /= uRenderSize.xy;
                res += color(temp_un);
                n++;
            }
        }
    }
    res /= n;
    o_result = vec4(res,texture(samplerA,uv).a);
}