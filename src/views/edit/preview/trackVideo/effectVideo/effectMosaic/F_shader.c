#version 300 es
        
precision highp float;
precision highp int;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
// uniform int         g_viewWidth;
// uniform int         g_viewHeight;
// uniform int         g_pixelWidth;
// uniform float       g_x;
// uniform float       g_y;
// uniform float       g_w;
// uniform float       g_h;

uniform vec2        g_texSize;
uniform vec2        g_pixelSize;
uniform vec4        g_area;

out vec4 o_result;


void main() {
    if(vTexCoord.x >= g_area.x && vTexCoord.x <= g_area.x+g_area.z && vTexCoord.y >= g_area.y && vTexCoord.y <= g_area.y+g_area.w){
        vec2 intXY = vec2(vTexCoord.x * g_texSize.x, vTexCoord.y * g_texSize.y);
        vec2 UVMosaic = vec2((intXY.x - mod(intXY.x,g_pixelSize.x))/g_texSize.x, (intXY.y - mod(intXY.y,g_pixelSize.y))/g_texSize.y);
        o_result = texture(samplerA, UVMosaic);
    }else{
        o_result = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    }
}