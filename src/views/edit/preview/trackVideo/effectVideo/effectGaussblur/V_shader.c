#version 300 es

precision highp float;
precision highp int;

layout(location = 0) in vec4 g_pos;

uniform float strength;
uniform vec2 canvasSize;
// uniform int horizontal;
uniform vec2  zoomScale;
uniform vec2 dir;

out vec2 uv;
out vec2 uv_zoom;
out vec2 vBlurTexCoords[15];

// const float strength = 1.0;

void main() {
    float curX = (g_pos.x + 1.) / 2.;
    float curY = (g_pos.y + 1.) / 2.;
    uv = vec2(curX, curY);

    gl_Position = sign(vec4(g_pos.xy,0.0,1.0));

    uv_zoom = uv*zoomScale;

    vec2 textureCoord = uv*canvasSize;

    vBlurTexCoords[0] =  (textureCoord + ( -7.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[1] =  (textureCoord + (-6.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[2] =  (textureCoord + (-5.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[3] =  (textureCoord + (-4.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[4] =  (textureCoord + (-3.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[5] =  (textureCoord + (-2.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[6] =  (textureCoord + (-1.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[7] =  (textureCoord + (0.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[8] =  (textureCoord + (1.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[9] =  (textureCoord + (2.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[10] =  (textureCoord + (3.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[11] =  (textureCoord + (4.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[12] =  (textureCoord + (5.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[13] =  (textureCoord + (6.0 * strength)*dir)/canvasSize;
    vBlurTexCoords[14] =  (textureCoord + (7.0 * strength)*dir)/canvasSize;
}