import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectGaussblur{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /**创建一个纹理*/
    createTexture (gl){
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D,null);
        return texture;
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas){
        const params = {
            x:0,
            y:0,
        }
        
        let gl = _gl || this.gl;
        let program = this.program;
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.x = preParameter.x*(1-percentage)+nowParameter.x*percentage;
                params.y = preParameter.y*(1-percentage)+nowParameter.y*percentage;
            }
        }

        let cw = canvas.width,ch = canvas.height;
        let baseScale = (cw+ch)*64/(1920+1080);
        let scaleImg = baseScale * params.x;
        let canvasWidth = Math.floor(cw/Math.max(1,scaleImg)),
            canvasHeight = Math.floor(ch/Math.max(1,scaleImg));
        //缩放后的画布不能大于原始画布
        canvasWidth = Math.min(canvasWidth,cw);
        canvasHeight = Math.min(canvasHeight,ch);

        //缩小纹理
        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 1);
        gl.uniform2fv(gl.getUniformLocation(program, 'zoomScale'), new Float32Array([cw/canvasWidth,ch/canvasHeight]));
        gl.clearColor(.0, .0, .0, .0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
        gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,canvasWidth,canvasHeight,0);

        //模糊模式 垂直方向
        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 2);
        gl.uniform2fv(gl.getUniformLocation(program, 'dir'), [0,1]);
        gl.uniform1f(gl.getUniformLocation(program, 'strength'), Math.ceil(1*params.x));
        gl.uniform2fv(gl.getUniformLocation(program, 'canvasSize'), new Float32Array([canvasWidth,canvasHeight]));
        gl.clearColor(.0, .0, .0, .0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
        gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);
        // console.log('canvasWidth',canvasWidth,canvasHeight)
        //再次缩小
        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 1);
        gl.clearColor(.0, .0, .0, .0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
        gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,canvasWidth,canvasHeight,0);

        // return;
        //水平方向
        gl.uniform1i(gl.getUniformLocation(program,'stepMode'), 2);
        gl.uniform2fv(gl.getUniformLocation(program, 'dir'), [1,0]);
        
    }
}
export default EffectGaussblur;