#version 300 es
        
precision highp float;
precision highp int;


#define PI      3.141592653589793238462643383279;
#define PI2     6.283185307179586476925286766559;
#define COEF_255_INV		1.f / 255.f;

in vec2 vTexCoord;
uniform sampler2D   samplerA;
uniform vec4	    g_vecOffset;
uniform vec4	    g_vecInput;
uniform vec4	    g_tableRGBA;
uniform vec4	    g_tableHMSA;
uniform float       g_fRTable[256];
uniform float       g_fGTable[256];
uniform float       g_fBTable[256];

out vec4 o_result;

vec3 rgbToYUV(vec3 rgb){
    float R = rgb.r;
    float G = rgb.g;
    float B = rgb.b;
    float Y =  0.299*R + 0.587*G + 0.114*B;
    float U = -0.169*R - 0.331*G + 0.5  *B;
    float V =  0.5  *R - 0.419*G - 0.081*B;
    return vec3(Y,U,V);
}
vec3 YUVTorgb(vec3 yuv){
    float Y = yuv.r;
    float U = yuv.g;
    float V = yuv.b;
    float R = Y + 1.4075 * V;  
    float G = Y - 0.3455 * U - 0.7169*V;  
    float B = Y + 1.779 * U;
    return vec3(R,G,B);
}

void main() {
    vec4 color = texture(samplerA, vec2(vTexCoord.x,vTexCoord.y));
    // vec4 retColor = color;

    color.r = g_fRTable[int(color.r*255.0)]/255.0;
    color.g = g_fGTable[int(color.g*255.0)]/255.0;
    color.b = g_fBTable[int(color.b*255.0)]/255.0;

    vec3 fYUVA = rgbToYUV(color.xyz);
    
    fYUVA.x = fYUVA.x*g_vecInput.x + g_vecInput.y;
    fYUVA.y = fYUVA.y*g_vecInput.z - fYUVA.z*g_vecInput.w;
    fYUVA.z = fYUVA.y*g_vecInput.w + fYUVA.z*g_vecInput.z;
    vec3 fInputOut = YUVTorgb(fYUVA.xyz);

    // // RGBA table
    //  fInputOut.r = g_tableRGBA.Sample(samCommonPoint, float2(fInputOut.r, 0.5f/3.f)).r;
    //  fInputOut.g = g_tableRGBA.Sample(samCommonPoint, float2(fInputOut.g, 1.5f/3.f)).r;
    //  fInputOut.b = g_tableRGBA.Sample(samCommonPoint, float2(fInputOut.b, 2.5f/3.f)).r;
    //  fInputOut *= COEF_255_INV;		// 0 - 1
    //  // HMS table
    //  let fY = CS_RGB2Y(fInputOut);//dot(fInputOut, matRGBA2LUVA[0]);
    //  float3 f3MapHMS = g_tableHMSA.Sample(samCommonPoint, fY).rgb;
    //  fInputOut = saturate(f3MapHMS * COEF_255_INV + fInputOut);
    // fInputOut = CS_RGB2YUV(fInputOut);
    //  return float4(fInputOut.xyz, fYUVA.a) + CS_YUVA_OFFSET;
    
    // fInputOut.r = g_fRTable[int(fInputOut.r*255.0)]/255.0;
    // fInputOut.g = g_fGTable[int(fInputOut.g*255.0)]/255.0;
    // fInputOut.b = g_fBTable[int(fInputOut.b*255.0)]/255.0;

    o_result = vec4(fInputOut.xyz,color.a);
}