import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectColorCorrector1{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        // let posAtrLoc = gl.getAttribLocation(program,"g_pos");
        // gl.enableVertexAttribArray(posAtrLoc);
        // gl.vertexAttribPointer(posAtrLoc, 2, gl.FLOAT, false, 0, 0);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos){
        const pParamData = {
            paramInput:{
                fBlack:0,
                fWhite:100,
                fSat:100,
                fHue:0
            },
            paramHisto:{
                gRGB:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gR:{
                    byIB:70,
                    byIW:154,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gG:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                },
                gB:{
                    byIB:0,
                    byIW:255,
                    byOB:0,
                    byOW:255,
                    fGamma:1
                }
            },
            paramCB:{
                gH:{
                    byR:179,
                    byG:179,
                    byB:179,
                    byReserved:179
                },
                gM:{
                    byR:128,
                    byG:128,
                    byB:128,
                    byReserved:128
                },
                gS:{
                    byR:77,
                    byG:77,
                    byB:77,
                    byReserved:77
                }
            },
            paramArea:{
                byHM:178,
                byMS:77
            }
        }

        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                //paramInput
                pParamData.paramInput.fBlack = preParameter.paramInput.fBlack*(1-percentage)+nowParameter.paramInput.fBlack*percentage;
                pParamData.paramInput.fWhite = preParameter.paramInput.fWhite*(1-percentage)+nowParameter.paramInput.fWhite*percentage;
                pParamData.paramInput.fSat = preParameter.paramInput.fSat*(1-percentage)+nowParameter.paramInput.fSat*percentage;
                pParamData.paramInput.fHue = preParameter.paramInput.fHue*(1-percentage)+nowParameter.paramInput.fHue*percentage;
                //paramHisto.gRGB
                pParamData.paramHisto.gRGB.byIB = preParameter.paramHisto.gRGB.byIB*(1-percentage)+nowParameter.paramHisto.gRGB.byIB*percentage;
                pParamData.paramHisto.gRGB.byIW = preParameter.paramHisto.gRGB.byIW*(1-percentage)+nowParameter.paramHisto.gRGB.byIW*percentage;
                pParamData.paramHisto.gRGB.byOB = preParameter.paramHisto.gRGB.byOB*(1-percentage)+nowParameter.paramHisto.gRGB.byOB*percentage;
                pParamData.paramHisto.gRGB.byOW = preParameter.paramHisto.gRGB.byOW*(1-percentage)+nowParameter.paramHisto.gRGB.byOW*percentage;
                pParamData.paramHisto.gRGB.fGamma = preParameter.paramHisto.gRGB.fGamma*(1-percentage)+nowParameter.paramHisto.gRGB.fGamma*percentage;
                //paramHisto.gR
                pParamData.paramHisto.gR.byIB = preParameter.paramHisto.gR.byIB*(1-percentage)+nowParameter.paramHisto.gR.byIB*percentage;
                pParamData.paramHisto.gR.byIW = preParameter.paramHisto.gR.byIW*(1-percentage)+nowParameter.paramHisto.gR.byIW*percentage;
                pParamData.paramHisto.gR.byOB = preParameter.paramHisto.gR.byOB*(1-percentage)+nowParameter.paramHisto.gR.byOB*percentage;
                pParamData.paramHisto.gR.byOW = preParameter.paramHisto.gR.byOW*(1-percentage)+nowParameter.paramHisto.gR.byOW*percentage;
                pParamData.paramHisto.gR.fGamma = preParameter.paramHisto.gR.fGamma*(1-percentage)+nowParameter.paramHisto.gR.fGamma*percentage;
                //paramHisto.gG
                pParamData.paramHisto.gG.byIB = preParameter.paramHisto.gG.byIB*(1-percentage)+nowParameter.paramHisto.gG.byIB*percentage;
                pParamData.paramHisto.gG.byIW = preParameter.paramHisto.gG.byIW*(1-percentage)+nowParameter.paramHisto.gG.byIW*percentage;
                pParamData.paramHisto.gG.byOB = preParameter.paramHisto.gG.byOB*(1-percentage)+nowParameter.paramHisto.gG.byOB*percentage;
                pParamData.paramHisto.gG.byOW = preParameter.paramHisto.gG.byOW*(1-percentage)+nowParameter.paramHisto.gG.byOW*percentage;
                pParamData.paramHisto.gG.fGamma = preParameter.paramHisto.gG.fGamma*(1-percentage)+nowParameter.paramHisto.gG.fGamma*percentage;
                //paramHisto.gB
                pParamData.paramHisto.gB.byIB = preParameter.paramHisto.gB.byIB*(1-percentage)+nowParameter.paramHisto.gB.byIB*percentage;
                pParamData.paramHisto.gB.byIW = preParameter.paramHisto.gB.byIW*(1-percentage)+nowParameter.paramHisto.gB.byIW*percentage;
                pParamData.paramHisto.gB.byOB = preParameter.paramHisto.gB.byOB*(1-percentage)+nowParameter.paramHisto.gB.byOB*percentage;
                pParamData.paramHisto.gB.byOW = preParameter.paramHisto.gB.byOW*(1-percentage)+nowParameter.paramHisto.gB.byOW*percentage;
                pParamData.paramHisto.gB.fGamma = preParameter.paramHisto.gB.fGamma*(1-percentage)+nowParameter.paramHisto.gB.fGamma*percentage;

            }
        }

        // Object.assign(pParamData.paramInput,keyFrameParameters[0].parameter.paramInput);
        // console.log(pParamData.paramInput,keyFrameParameters[0].parameter.paramInput)
        let gl = _gl || this.gl;
        let program = this.program;
        const tex = this.getGPUParams(pParamData);
        // m_pFRGB
        // m_aFHMS
        // console.log(tex.m_pFRGB,tex.m_aFHMS)

        // let fOffset = pParamData.paramInput.fBlack * 0.01;	// [-1, 1]
        let fAngle = pParamData.paramInput.fHue * 0.017453;		// [-PI, PI]
        let fSat = pParamData.paramInput.fSat * 0.01;		// [0, 1.1f]
        let fCosCoef = fSat * Math.cos(fAngle);
        let fSinCoef = fSat * Math.sin(fAngle);
        // const vecOffset = [fOffset, fOffset, fOffset, 0];
        // add by xp 9/1/2009
        // (0, 16);  (200, 454)
        let fWhiteNew = (454-16)/(200-0)*pParamData.paramInput.fWhite + 16;
        // (-100, -203); (100, 235)
        let fBlackNew = (235+203)/(100+100)*pParamData.paramInput.fBlack + 16;
        let fK = (fWhiteNew - fBlackNew) / 219;
        let fB = (fBlackNew - fK*16) / 255;
        const vecInput = [fK, fB, fCosCoef, fSinCoef];
        gl.uniform4fv(gl.getUniformLocation(program, 'g_vecInput'), vecInput);
        gl.uniform1fv(gl.getUniformLocation(program,'g_fRTable'),tex.fRTable,0,256);
        gl.uniform1fv(gl.getUniformLocation(program,'g_fGTable'),tex.fGTable,0,256);
        gl.uniform1fv(gl.getUniformLocation(program,'g_fBTable'),tex.fBTable,0,256);
    }

    getGPUParams (pParamData){
        const DEFAULT_GRAY_COUNT = 256;
        const DEFAULT_GRAY_COUNT_INV = 0.00390625;	// 1.f/256.f
        const DEFAULT_MAX_RGB = 255;
        const TABLE_TONE_COUNT  = 3;
        const TABLE_CHANNEL_COUNT  = 3;
        const TABLE_TONE_HIGHLIGHT  = 0 ;
        const TABLE_TONE_MIDTONE  = 1 ;
        const TABLE_TONE_SHADOW  = 2 ;
        const TABLE_CHANNEL_R  = 0;
        const TABLE_CHANNEL_G  = 1 ;
        const TABLE_CHANNEL_B  = 2 ;
        const STANDARD_DELTA  = 32 ;
        const m_pFRGB = [];
        const fRTable = [];
        const fGTable = [];
        const fBTable = [];
        for(let i=0;i<DEFAULT_GRAY_COUNT;i++){
            fRTable[i] = 0;
            fGTable[i] = 0;
            fBTable[i] = 0;
        }
        // const m_aFHMS = [];
        // for(let i=0;i<DEFAULT_GRAY_COUNT;i++){
        //     m_aFHMS[i] = [0,0,0,0];
        // }
        
        // console.time('NSCE_PCC_MakeTable');
        // NSCE_PCC_MakeTable(fRTable,fGTable,fBTable,m_aFHMS,pParamData);
        // NSCE_PCC_MakeTable2(fRTable,fGTable,fBTable,pParamData)
        const byFObj = {
            byA:0,
            byB:0,
            byC:0,
            byD:0,
            fGamma:0
        }
        NSCE_PCC_MakeRGBTable2(fRTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gR);
        NSCE_PCC_MakeRGBTable2(fGTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gG);
        NSCE_PCC_MakeRGBTable2(fBTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gB);
        // for(let i=0;i<DEFAULT_GRAY_COUNT;i++){
        //     m_pFRGB[i] = [fRTable[i],fGTable[i],fBTable[i],0];
        // }
        
        // console.timeEnd('NSCE_PCC_MakeTable');
        return {
            fRTable:fRTable,
            fGTable:fGTable,
            fBTable:fBTable
        }

        // function NSCE_PCC_MakeTable2(fRTable,fGTable,fBTable,pParamData){
        //     // 生成 R 表
        //     const byFObj = {
        //         byA:0,
        //         byB:0,
        //         byC:0,
        //         byD:0,
        //         fGamma:0
        //     }
        //     NSCE_PCC_MakeRGBTable2(fRTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gR);
        //     NSCE_PCC_MakeRGBTable2(fGTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gG);
        //     NSCE_PCC_MakeRGBTable2(fBTable,byFObj,pParamData.paramHisto.gRGB, pParamData.paramHisto.gB);
        // }

        function NSCE_PCC_MakeRGBTable2(pTable,byFObj,pRGBAll,pRGBOne){
            byFObj.byA = ((pRGBAll.byIW - pRGBAll.byIB) * pRGBOne.byIB * DEFAULT_GRAY_COUNT_INV + pRGBAll.byIB + 0.5);//NSCE_PCC_CalcCoefAC(pRGBAll.byIW, pRGBAll.byIB, pRGBOne.byIB);
            byFObj.byB = (pRGBAll.byIW - (pRGBAll.byIW - pRGBAll.byIB) * (DEFAULT_MAX_RGB - pRGBOne.byIW) * DEFAULT_GRAY_COUNT_INV + 0.5);//NSCE_PCC_CalcCoefBD(pRGBAll.byIW, pRGBAll.byIB, pRGBOne.byIW);
            byFObj.byC = ((pRGBAll.byOW - pRGBAll.byOB) * pRGBOne.byOB * DEFAULT_GRAY_COUNT_INV + pRGBAll.byOB + 0.5);//NSCE_PCC_CalcCoefAC(pRGBAll.byOW, pRGBAll.byOB, pRGBOne.byOB);
            byFObj.byD = (pRGBAll.byOW - (pRGBAll.byOW - pRGBAll.byOB) * (DEFAULT_MAX_RGB - pRGBOne.byOW) * DEFAULT_GRAY_COUNT_INV + 0.5);//NSCE_PCC_CalcCoefBD(pRGBAll.byOW, pRGBAll.byOB, pRGBOne.byOW);
            byFObj.fGamma = 1.0 / (pRGBAll.fGamma * pRGBOne.fGamma);



            let i=0;
            let fIn_Inv = 1.0 / (byFObj.byB - byFObj.byA), fOut = (byFObj.byD - byFObj.byC);
            let order = 0;
            // 填充 A 部分
            for (; i<=byFObj.byA; i++)
            {
                pTable[order] = byFObj.byC;
                order++;
            }
            // 计算 Gamma 部分
            for (; i<byFObj.byB; i++)
            {
                pTable[order] = (fOut * Math.pow((i - byFObj.byA) * fIn_Inv, byFObj.fGamma) + byFObj.byC + 0.5);
                order++;
            }
            // 填充 B 部分
            for (; i<DEFAULT_GRAY_COUNT; i++)
            {
                pTable[order] = byFObj.byD;
                order++;
            }
        }
        
        // function NSCE_PCC_MakeTable(fRTable,fGTable,fBTable,fOffsetTable, pParamData){
        //     // let byA, byB, byC, byD;
        //     // let fGamma;
        //     const byFObj = {
        //         byA:0,
        //         byB:0,
        //         byC:0,
        //         byD:0,
        //         fGamma:0
        //     }

        //     // 生成 R 表
        //     NSCE_PCC_CalcCoefRGB(byFObj, pParamData.paramHisto.gRGB, pParamData.paramHisto.gR);
        //     NSCE_PCC_MakeRGBTable(fRTable, byFObj);
        //     // 生成 G 表
        //     NSCE_PCC_CalcCoefRGB(byFObj, pParamData.paramHisto.gRGB, pParamData.paramHisto.gG);
        //     NSCE_PCC_MakeRGBTable(fGTable, byFObj);
        //     // 生成 B 表
        //     NSCE_PCC_CalcCoefRGB(byFObj, pParamData.paramHisto.gRGB, pParamData.paramHisto.gB);
        //     NSCE_PCC_MakeRGBTable(fBTable, byFObj);
        //     // 生成偏移表
        //     // NSCE_PCC_MakeOffsetTable(fOffsetTable, pParamData.paramCB, pParamData.paramArea);
        // }

        // function NSCE_PCC_CalcCoefRGB(byFObj,
        //                     pRGBAll,	// 整体通道参数
        //                     pRGBOne)	// 单独通道参数
        // {
        //     byFObj.byA = NSCE_PCC_CalcCoefAC(pRGBAll.byIW, pRGBAll.byIB, pRGBOne.byIB);
        //     byFObj.byB = NSCE_PCC_CalcCoefBD(pRGBAll.byIW, pRGBAll.byIB, pRGBOne.byIW);
        //     byFObj.byC = NSCE_PCC_CalcCoefAC(pRGBAll.byOW, pRGBAll.byOB, pRGBOne.byOB);
        //     byFObj.byD = NSCE_PCC_CalcCoefBD(pRGBAll.byOW, pRGBAll.byOB, pRGBOne.byOW);
        //     byFObj.fGamma = 1.0 / (pRGBAll.fGamma * pRGBOne.fGamma);
        // }

        // function NSCE_PCC_CalcCoefAC(rgb_w,		// 整体通道白
        //                             rgb_b,		// 整体通道黑
        //                             b)			// 所求通道黑
        // {
        //     return ((rgb_w - rgb_b) * b * DEFAULT_GRAY_COUNT_INV + rgb_b + 0.5);
        // }

        // function NSCE_PCC_CalcCoefBD(rgb_w,		// 整体通道白
        //                             rgb_b,		// 整体通道黑
        //                             w)			// 所求通道白
        // {
        //     return (rgb_w - (rgb_w - rgb_b) * (DEFAULT_MAX_RGB - w) * DEFAULT_GRAY_COUNT_INV + 0.5);
        // }

        // function NSCE_PCC_MakeRGBTable(pTable,byFObj)
        // {
        //     let i=0;
        //     let fIn_Inv = 1.0 / (byFObj.byB - byFObj.byA), fOut = (byFObj.byD - byFObj.byC);
        //     let order = 0;
        //     // 填充 A 部分
        //     for (; i<=byFObj.byA; i++)
        //     {
        //         pTable[order] = byFObj.byC;
        //         order++;
        //     }
        //     // 计算 Gamma 部分
        //     for (; i<byFObj.byB; i++)
        //     {
        //         pTable[order] = (fOut * Math.pow((i - byFObj.byA) * fIn_Inv, byFObj.fGamma) + byFObj.byC + 0.5);
        //         order++;
        //     }
        //     // 填充 B 部分
        //     for (; i<DEFAULT_GRAY_COUNT; i++)
        //     {
        //         pTable[order] = byFObj.byD;
        //         order++;
        //     }
        // }

        // function NSCE_PCC_MakeOffsetTable(fOffsetTable,	pParamCB,pParamArea)	// Area Range 参数包
        // {
        //     const DeltaRGB = [];
        //     for(let i=0;i<TABLE_TONE_COUNT;i++){
        //         DeltaRGB[i] = [];
        //         for(let j=0;j<TABLE_CHANNEL_COUNT;j++){
        //             DeltaRGB[i][j] = 0;
        //         }
        //     }

        //     // Highlight
        //     NSCE_PCC_CalcCoefHMS(DeltaRGB[TABLE_TONE_HIGHLIGHT], pParamCB.gH, TABLE_TONE_HIGHLIGHT);
        //     // Midtone
        //     NSCE_PCC_CalcCoefHMS(DeltaRGB[TABLE_TONE_MIDTONE], pParamCB.gM, TABLE_TONE_MIDTONE);
        //     // Shadow
        //     NSCE_PCC_CalcCoefHMS(DeltaRGB[TABLE_TONE_SHADOW], pParamCB.gS, TABLE_TONE_SHADOW);
        //     // 生成表
        //     for (let i=0; i<DEFAULT_GRAY_COUNT; i++)
        //     {
        //         fOffsetTable[i][TABLE_CHANNEL_R] = NSCE_PCC_CalcOffset(DeltaRGB[TABLE_TONE_SHADOW][TABLE_CHANNEL_R],
        //             DeltaRGB[TABLE_TONE_MIDTONE][TABLE_CHANNEL_R],
        //             DeltaRGB[TABLE_TONE_HIGHLIGHT][TABLE_CHANNEL_R],
        //             pParamArea.byMS,
        //             pParamArea.byHM,
        //             i);
        //         fOffsetTable[i][TABLE_CHANNEL_G] = NSCE_PCC_CalcOffset(DeltaRGB[TABLE_TONE_SHADOW][TABLE_CHANNEL_G],
        //             DeltaRGB[TABLE_TONE_MIDTONE][TABLE_CHANNEL_G],
        //             DeltaRGB[TABLE_TONE_HIGHLIGHT][TABLE_CHANNEL_G],
        //             pParamArea.byMS,
        //             pParamArea.byHM,
        //             i);
        //         fOffsetTable[i][TABLE_CHANNEL_B] = NSCE_PCC_CalcOffset(DeltaRGB[TABLE_TONE_SHADOW][TABLE_CHANNEL_B],
        //             DeltaRGB[TABLE_TONE_MIDTONE][TABLE_CHANNEL_B],
        //             DeltaRGB[TABLE_TONE_HIGHLIGHT][TABLE_CHANNEL_B],
        //             pParamArea.byMS,
        //             pParamArea.byHM,
        //             i);
        //         fOffsetTable[i][3] = 0.0;
        //     }
        // }


        // function NSCE_PCC_CalcCoefHMS(pDeltaRGB,pRGB,nTone)					// 色调索引
        // {
        //     const Standard = [178, 128, 77];

        //     pDeltaRGB[TABLE_CHANNEL_R] = pRGB.byR - Standard[nTone];
        //     pDeltaRGB[TABLE_CHANNEL_G] = pRGB.byG - Standard[nTone];
        //     pDeltaRGB[TABLE_CHANNEL_B] = pRGB.byB - Standard[nTone];
        // }

        // function NSCE_PCC_CalcOffset(nDS,nDM,nDH,byMS,byHM,x)			// 灰度
        // {
        //     let nL = byMS - STANDARD_DELTA;
        //     let nH = byMS + STANDARD_DELTA;

        //     if (x <= nL)
        //         return nDS;

        //     if (x < nH)
        //         return ((nDM - nDS) * (x - nL) * 0.015625) + nDS;

        //     nL = byHM - STANDARD_DELTA;

        //     if (x <= nL)
        //         return nDM;

        //     nH = byHM + STANDARD_DELTA;

        //     if (x < nH)
        //         return ((nDH - nDM) * (x - nL) * 0.015625) + nDM;

        //     return nDH;
        // }
    }
}
export default EffectColorCorrector1;