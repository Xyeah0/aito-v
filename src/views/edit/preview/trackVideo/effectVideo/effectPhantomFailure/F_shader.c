#version 300 es
        
precision highp float;
precision highp int;

in vec2 uv;
uniform  sampler2D   samplerA;
uniform float u_Time; 
uniform float u_HueIntensity;
uniform float u_SaturationIntensity;
uniform float u_OpacityIntensity;
out vec4 o_result;

const float AMT = 0.62; //0 - 1 glitch amount
const float SPEED = 0.26; //0 - 1 speed
const float MAX_OFFSET = 0.62;  

//2D (returns 0 - 1)
float random2d(vec2 n) { 
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float randomRange (vec2 seed, float min, float max) {
        return min + random2d(seed) * (max - min);
}

// return 1 if v inside 1d range
float insideRange(float v, float bottom, float top) {
    return step(bottom, v) - step(top, v);
}

// snow color
vec3 RGBToHSL(vec3 color){
    vec3 hsl;
    float fmin = min(min(color.r, color.g), color.b);
    float fmax = max(max(color.r, color.g), color.b);
    float delta = fmax - fmin;
    
    hsl.z = (fmax + fmin) / 2.0;
    
    if (delta == 0.0)
    {
        hsl.x = 0.0;
        hsl.y = 0.0;
    }
    else
    {
        if (hsl.z < 0.5)
            hsl.y = delta / (fmax + fmin);
        else
            hsl.y = delta / (2.0 - fmax - fmin);
        
        float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
        float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
        float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;
        
        if (color.r == fmax )
            hsl.x = deltaB - deltaG;
        else if (color.g == fmax)
            hsl.x = (1.0 / 3.0) + deltaR - deltaB;
        else if (color.b == fmax)
            hsl.x = (2.0 / 3.0) + deltaG - deltaR;
        
        if (hsl.x < 0.0)
            hsl.x += 1.0;
        else if (hsl.x > 1.0)
            hsl.x -= 1.0;
    }
    
    return hsl;
}

float HueToRGB(float f1, float f2, float hue){
    if (hue < 0.0)
        hue += 1.0;
    else if (hue > 1.0)
        hue -= 1.0;
    float res;
    if ((6.0 * hue) < 1.0)
        res = f1 + (f2 - f1) * 6.0 * hue;
    else if ((2.0 * hue) < 1.0)
        res = f2;
    else if ((3.0 * hue) < 2.0)
        res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
    else
        res = f1;
    return res;
}

vec3 HSLToRGB(vec3 hsl){
    vec3 rgb;
    
    if (hsl.y == 0.0)
        rgb = vec3(hsl.z);
    else
    {
        float f2;
        
        if (hsl.z < 0.5)
            f2 = hsl.z * (1.0 + hsl.y);
        else
            f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);
        
        float f1 = 2.0 * hsl.z - f2;
        
        rgb.r = HueToRGB(f1, f2, hsl.x + (1.0/3.0));
        rgb.g = HueToRGB(f1, f2, hsl.x);
        rgb.b= HueToRGB(f1, f2, hsl.x - (1.0/3.0));
    }
    
    return rgb;
}

void main( void ){

    float iTime = u_Time;
    float time = floor(iTime * SPEED * 60.0);   
    
    vec4 resultColor = texture(samplerA, uv);
    vec3 outCol = resultColor.rgb;
    
    //randomly offset slices horizontally
    float maxOffset = MAX_OFFSET/2.0;
    float i_AMT = 10.0 * AMT;
    for (float i = 0.0; i < 6.2; i += 1.0) 
    {
        if(i>=i_AMT)break;
        float sliceY = random2d(vec2(time , 2345.0 + float(i)));
        float sliceH = random2d(vec2(time , 9035.0 + float(i))) * 0.125* sliceY; // the height of slices
        // maxOffset = maxOffset*sliceY;
        float hOffset = randomRange(vec2(time , 9625.0 + float(i)), -maxOffset, maxOffset); 
        vec2 uvOff = uv;
        uvOff.x += hOffset;
        if (insideRange(uv.y, sliceY, fract(sliceY+sliceH)) == 1.0 )
        {
            vec4 tempColor = texture(samplerA, uvOff);
            outCol = tempColor.rgb;
            resultColor.a = tempColor.a;
        }
    }
    
    //do slight offset on one entire channel
    float maxColOffset = AMT/6.0;
    float rnd = random2d(vec2(time , 9545.0));
    vec2 colOffset = vec2(randomRange(vec2(time , 9545.0),-maxColOffset,maxColOffset), 
                    randomRange(vec2(time , 7205.0),-maxColOffset,maxColOffset));
    colOffset.x *=1.0;
    colOffset.y = 0.0;      // only horizonal offset
    
    if (rnd < 0.33){
        outCol.r = texture(samplerA, uv + colOffset).r;
    }else if (rnd < 0.66){
        outCol.g = texture(samplerA, uv + colOffset).g;
    } else{
        outCol.b = texture(samplerA, uv + colOffset).b;  
    }

    outCol = mix(resultColor.rgb,outCol,resultColor.a*u_OpacityIntensity);
    outCol = abs(outCol-resultColor.rgb);

    vec3 hsv0 = RGBToHSL(outCol);
    hsv0.r = mod(hsv0.r*u_HueIntensity,1.0);
    hsv0.g = mod(hsv0.g*u_SaturationIntensity,1.0);
    outCol = HSLToRGB(hsv0);

    outCol += resultColor.rgb;
    
    o_result = vec4(outCol,resultColor.a);

    // o_result = texture(samplerA, vTexCoord);
}