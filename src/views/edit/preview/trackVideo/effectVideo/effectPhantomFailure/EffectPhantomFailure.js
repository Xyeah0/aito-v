import v_shader from './V_shader.c';
import f_shader from './F_shader.c';
class EffectPhantomFailure{
    constructor(){
        this.gl = null;
        this.program = null;
        this.v_shader_code = atob(v_shader);
        this.f_shader_code = atob(f_shader);
    }


    /** 初始化着色器 */
    initProgram (gl){
        this.gl = gl;
        //取得顶点和片断shader
        let vshader = this.getShader(gl,this.v_shader_code,gl.VERTEX_SHADER);
        let fshader = this.getShader(gl,this.f_shader_code,gl.FRAGMENT_SHADER);
        //创建一个着色器程序
        let program = gl.createProgram();
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        gl.linkProgram(program);
        gl.useProgram(program);

        this.program = program;
        this.vshader = vshader;
        this.fshader = fshader;
    }

    /** 获得一个编译后的shader */
    getShader (gl,code, type) {
        // console.log('gl',gl,'code',code)
        const shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
            throw new Error("compile: " + gl.getShaderInfoLog(shader));
        // this.gl.attachShader(this.program, shader);
        return shader;
    }

    /** 从gl里卸载shader等 */
    uninstallProgram(){
        let gl = this.gl;
        gl.detachShader(this.program, this.vshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
        gl.detachShader(this.program, this.fshader);
        gl.deleteShader(this.vshader);
        gl.deleteShader(this.fshader);
        gl.deleteProgram(this.program);
    }

    /** 设置程序参数 */
    setParameter (_gl,keyFrameParameters,curPos,canvas,video,clip){
        let params = {
            hue: 0.5,
            saturation: 0.5,
            opacity: 0.5
        }
        for(let i=1;i<keyFrameParameters.length;i++){
            let preParam = keyFrameParameters[i-1];
            let nowParam = keyFrameParameters[i];
            let preParameter = preParam.parameter;
            let nowParameter = nowParam.parameter;
            if(curPos >= preParam.pos && curPos <= nowParam.pos){
                let percentage = (curPos-preParam.pos)/(nowParam.pos-preParam.pos);//console.log('percentage',percentage)
                //x*(1-a)+y*a
                params.hue = preParameter.hue*(1-percentage)+nowParameter.hue*percentage;
                params.saturation = preParameter.saturation*(1-percentage)+nowParameter.saturation*percentage;
                params.opacity = preParameter.opacity*(1-percentage)+nowParameter.opacity*percentage;
            }
        }
        // console.log('curPos',curPos,params.hue, params.saturation,params.opacity)
        let gl = _gl || this.gl;
        let program = this.program;

        let cw = canvas.width,ch = canvas.height;
        let uTime = curPos - clip.inPoint || 0;
        gl.uniform1f(gl.getUniformLocation(program, 'u_Time'), uTime);
        gl.uniform1f(gl.getUniformLocation(program, 'u_HueIntensity'), params.hue);
        gl.uniform1f(gl.getUniformLocation(program, 'u_SaturationIntensity'), params.saturation);
        gl.uniform1f(gl.getUniformLocation(program, 'u_OpacityIntensity'), params.opacity);
    }
}
export default EffectPhantomFailure;