
/** 为父轨道处理GL相关 */
import v_shader from './shader/V_shader.c';
import f_shader from './shader/F_shader.c';
import Flip_v_shader from './shader/Flip_v_shader.c';
import Flip_f_shader from './shader/Flip_f_shader.c';
import App from 'AppCore';
import DataType from '@/model/DataType';
class RenderGL{
	constructor(parent,transManager){
		this.parent = parent;
		
		this.gl = this.parent.canvas.getContext("webgl2",{
			preserveDrawingBuffer:true,
			premultipliedAlpha:false,
		});
		this.scaleType = 0;
		this.defaultProgram = null;
		this.FlipProgram = null;
		this.trackTexture0 = null;
		this.trackTexture1 = null;
		this.fbtexture = null;//帧缓冲和纹理


		this.v_shader_code = atob(v_shader);
		this.f_shader_code = atob(f_shader);
		this.Flip_v_shader_code = atob(Flip_v_shader);
		this.Flip_f_shader_code = atob(Flip_f_shader);

		this.defaultvshader = null;
		this.defaultfshader = null;
		this.Flipvshader = null;
		this.Flipfshader = null;
		
		// console.log(this.v_shader_code,this.parent.canvas,this.parent.canvas.getContext("webgl2"))

		this.initGL();

		this.transManager = transManager;
		this.transManager.initGL(this.gl);
	}


	//初始化GL
	initGL (){
		// for(let i=0;i<this.data.players.length;i++){
		//     let player = this.data.players[i];
		//     player.seekedBack = this.seekedBack;
		// }
		//默认shader程序
		this.defaultvshader = this.initShader(this.v_shader_code, this.gl.VERTEX_SHADER);
		this.defaultfshader = this.initShader(this.f_shader_code, this.gl.FRAGMENT_SHADER);
		this.defaultProgram = this.gl.createProgram();
		this.gl.attachShader(this.defaultProgram,this.defaultvshader);
		this.gl.attachShader(this.defaultProgram,this.defaultfshader);

		this.gl.linkProgram(this.defaultProgram);
		this.gl.useProgram(this.defaultProgram);

		//创建一个缓冲
		let vertexBuffer = this.gl.createBuffer();
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, vertexBuffer);
		let vecPosXArr = new Float32Array([-1, -1, 1, -1, -1, 1, 1, 1]);
		// let vecPosXArr = new Float32Array([-16, -9, 16, -9, -16, 9,16, 9]);
		// let vecPosXArr = new Float32Array([0, 0, 1, 0, 0, 1, 1, 1]);
		this.gl.bufferData(this.gl.ARRAY_BUFFER, vecPosXArr, this.gl.STATIC_DRAW);

		// let posAtrLoc = this.gl.getAttribLocation(this.defaultProgram,"g_pos");
		this.gl.enableVertexAttribArray(0);
		this.gl.vertexAttribPointer(0, 2, this.gl.FLOAT, false, 0, 0);

		// console.log('sfsfsfsfsfsf',this.gl.getUniformLocation(this.defaultProgram,'samplerA'))
		this.gl.enable(this.gl.BLEND);
		this.gl.blendFunc(this.gl.ONE, this.gl.ONE);
		// this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
		

		//翻转shader程序
		this.Flipvshader = this.initShader(this.Flip_v_shader_code, this.gl.VERTEX_SHADER);
		this.Flipfshader = this.initShader(this.Flip_f_shader_code, this.gl.FRAGMENT_SHADER);
		this.FlipProgram = this.gl.createProgram();
		this.gl.attachShader(this.FlipProgram,this.Flipvshader);
		this.gl.attachShader(this.FlipProgram,this.Flipfshader);
		this.gl.linkProgram(this.FlipProgram);
		this.gl.useProgram(this.FlipProgram);

		this.trackTexture0 = this.createTexture(this.gl);
		this.trackTexture1 = this.createTexture(this.gl);

		// this.transEffect = new TransEffectDissolve();
		// this.transEffect.initProgram(this.gl);
		
	}

	//初始化shader
	initShader (code, type) {
		const shader = this.gl.createShader(type);
		this.gl.shaderSource(shader, code);
		this.gl.compileShader(shader);
		if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS))
			throw new Error("compile: " + this.gl.getShaderInfoLog(shader));
		// this.gl.attachShader(this.program, shader);
		return shader;
	}

	/**创建一个帧缓冲和一个纹理 */
	createFrameAndTexture (gl,dep){
		//创建一个纹理对象
		let texture = gl.createTexture();
		//使用如下的设置来创建texture，这样对texture的设置可以使我们对任何尺寸的图片进行处理
		gl.bindTexture(gl.TEXTURE_2D, texture);
		// gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, cw, ch, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);


		let fb = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER,fb);
		//使用该方法将texture的颜色值与FBO进行绑定
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, dep);

		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.bindTexture(gl.TEXTURE_2D, null);
		return {
			fb:fb,
			texture:texture
		}
	}

	/**创建一个纹理*/
	createTexture  (gl){
		const texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.bindTexture(gl.TEXTURE_2D,null);
		return texture;
	}


	// getVideoTexture (player,cw,ch,index){

	//     let gl = this.gl;
	//     let program = this.defaultProgram;
	//     let video = player.video;
	//     let fbtexture = this.fbtexture;
	//     let scaleType = player.data.scaleType || 0;
	//     //先设置缓冲纹理的高宽
	//     gl.bindTexture(gl.TEXTURE_2D,fbtexture.texture);
	//     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, cw,ch, 0,gl.RGBA, gl.UNSIGNED_BYTE, null);
	//     gl.bindTexture(gl.TEXTURE_2D,null);

	//     gl.linkProgram(program);
	//     gl.useProgram(program);

	//     gl.uniform1i(gl.getUniformLocation(program,'scaleType'), scaleType);
	//     gl.uniform1f(gl.getUniformLocation(program,'textureWidth'), video.videoWidth);
	//     gl.uniform1f(gl.getUniformLocation(program,'textureHeight'), video.videoHeight);
	//     gl.uniform1f(gl.getUniformLocation(program,'viewWidth'), cw);
	//     gl.uniform1f(gl.getUniformLocation(program,'viewHeight'), ch);
	//     // this.gl.activeTexture(this.gl[`TEXTURE${index}`]);
	//     gl.activeTexture(gl.TEXTURE0);
	//     // let texture = index==0?this.trackTexture0:this.trackTexture1
	//     gl.bindTexture(gl.TEXTURE_2D, this[`trackTexture${index}`]);
	//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	//     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, video.videoWidth, video.videoHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, video);
	//     gl.uniform1i(gl.getUniformLocation(program,'samplerA'), 0);

	//     gl.bindFramebuffer(gl.FRAMEBUFFER,fbtexture.fb);
	//     gl.clearColor(.0, .0, .0, .0);
	//     gl.clear(gl.COLOR_BUFFER_BIT);
	//     gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	//     gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);


	//     let effectWholes = player.effectManager.effectWholes;
	//     for(let i=0;i<effectWholes.length;i++){
	//         let item = effectWholes[i];
	//         if(!item.programEff) continue;
	//         gl.linkProgram(item.programEff.program);
	//         gl.useProgram(item.programEff.program);

	//         let curPos = App.currentTime - player.data.startTime + player.data.inPoint;//console.log('curPos',curPos,player.data.inPoint)
	//         item.programEff.setParameter(gl,item.effData.keyFrameParameters,curPos,cw/ch);
	//         gl.uniform1i(gl.getUniformLocation(item.programEff.program,'samplerA'), 0);
	//         // this.gl.uniform1i(this.gl.getUniformLocation(item.program.program,'samplerA'), 0);
	//         gl.clearColor(.0, .0, .0, .0);
	//         gl.clear(this.gl.COLOR_BUFFER_BIT);
	//         gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	//         gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);
	//     }

	//     gl.bindTexture(gl.TEXTURE_2D,fbtexture.texture);
				
	//     gl.bindFramebuffer(gl.FRAMEBUFFER,null);

	//     // gl.clearColor(.0, .0, .0, .0);
	//     // gl.clear(gl.COLOR_BUFFER_BIT);
	//     // gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	//     gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);
	//     gl.bindTexture(gl.TEXTURE_2D,null);

	//     return this[`trackTexture${index}`];
		
	// }
	getMediaTexture (player,cw,ch,index){

		let gl = this.gl;
		let program = this.defaultProgram;
		let media = player.media;
		let imageWidth = player.imageWidth;
		let imageHeight = player.imageHeight;
		let scaleType = player.data.scaleType || 0;

		// console.log('media',media)
		// gl.linkProgram(program);
		gl.useProgram(program);

		// gl.uniform1i(gl.getUniformLocation(program,'scaleType'), scaleType);
		// gl.uniform1f(gl.getUniformLocation(program,'textureWidth'), imageWidth);
		// gl.uniform1f(gl.getUniformLocation(program,'textureHeight'), imageHeight);
		// gl.uniform1f(gl.getUniformLocation(program,'viewWidth'), cw);
		// gl.uniform1f(gl.getUniformLocation(program,'viewHeight'), ch);
		//优化参数传递
        let scalex = 1;
        let scaley = 1;
        if(scaleType == 1){
            scalex = (imageHeight*cw/ch)/imageWidth;
        }else if(scaleType == 2){
            scaley = (imageWidth*ch/cw)/imageHeight;
        }
        gl.uniform2fv(gl.getUniformLocation(program,'textureScale'), new Float32Array([scalex,scaley]));
		// this.gl.activeTexture(this.gl[`TEXTURE${index}`]);
		gl.activeTexture(gl.TEXTURE0);
		// let texture = index==0?this.trackTexture0:this.trackTexture1
		gl.bindTexture(gl.TEXTURE_2D, this[`trackTexture${index}`]);
		// gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		// gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		// gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		// gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, imageWidth, imageHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, media);
		gl.uniform1i(gl.getUniformLocation(program,'samplerA'), 0);
		gl.clearColor(.0, .0, .0, .0);
		gl.clear(gl.COLOR_BUFFER_BIT);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
		gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);


		let effectWholes = player.effectManager.effectWholes;//effectWholes = []
		if(effectWholes.length>0){

			for(let i=0;i<effectWholes.length;i++){
				let item = effectWholes[i];
				if(!item.programEff) continue;
				

				let curPos = App.activeSequence.currentTime - player.data.trackIn + player.data.inPoint;//console.log('curPos',curPos,player.data.inPoint)
				gl.useProgram(item.programEff.program);
				// console.timeEnd('switchProgram');

				
				gl.uniform1i(gl.getUniformLocation(item.programEff.program,'samplerA'), 0);
				// this.gl.uniform1i(this.gl.getUniformLocation(item.program.program,'samplerA'), 0);,this.parent.canvas
				item.programEff.setParameter(gl,item.effData.keyFrameParameters,curPos,this.parent.canvas,media,player.data);

				// this.gl.uniform1i(this.gl.getUniformLocation(item.program.program,'samplerA'), 0);
				gl.clearColor(.0, .0, .0, .0);
				gl.clear(this.gl.COLOR_BUFFER_BIT);
				gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
				gl.copyTexImage2D(gl.TEXTURE_2D,0,gl.RGBA,0,0,cw,ch,0);
			}
		}
		

		return this[`trackTexture${index}`];
		
	}

	/** 渲染 */
	render(player1,player2){
		if(!player1){
			return;
		} 
		
		let cw = this.parent.canvas.width;
		let ch = this.parent.canvas.height;
		let imageWidth = player1.imageWidth;
		let imageHeight = player1.imageHeight;
		if(cw==0||ch==0||imageWidth==0||imageHeight==0) return;
		// if(player.isFirst) return;
		// if(player2 && player2.isFirst) return;
		// console.log(cw,ch,imageWidth,imageHeight)
		// console.time('renderGL')
		this.gl.viewport(0,0,cw,ch);
		let texture0,texture1;
		texture0 = this.getMediaTexture(player1,cw,ch,0);
		if(player2){
			texture1 = this.getMediaTexture(player2,cw,ch,1);

			const effectTrans = player1.data.effectTrans;
			const transEffect = this.transManager.getProgramOfId(effectTrans.id);
			if(transEffect){
				// this.gl.linkProgram(transEffect.program);
				this.gl.useProgram(transEffect.program);
				this.gl.activeTexture(this.gl.TEXTURE0);
				this.gl.bindTexture(this.gl.TEXTURE_2D,texture0);
				this.gl.uniform1i(this.gl.getUniformLocation(transEffect.program,'samplerA'), 0);
				this.gl.activeTexture(this.gl.TEXTURE1);
				this.gl.bindTexture(this.gl.TEXTURE_2D,texture1);
				this.gl.uniform1i(this.gl.getUniformLocation(transEffect.program,'samplerB'), 1);
				let percentage = Math.max(0,(App.activeSequence.currentTime-(player1.data.trackOut-effectTrans.duration))/effectTrans.duration);
				percentage = Math.min(1,percentage);
				transEffect.setParameter(this.gl,percentage,effectTrans.parameters,cw/ch);
			}
			
			// console.log(percentage)


			this.gl.clearColor(.0, .0, .0, .0);
			this.gl.clear(this.gl.COLOR_BUFFER_BIT);
			this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
			this.gl.activeTexture(this.gl.TEXTURE0);
			this.gl.bindTexture(this.gl.TEXTURE_2D,this.trackTexture0);
			this.gl.copyTexImage2D(this.gl.TEXTURE_2D,0,this.gl.RGBA,0,0,cw,ch,0);

			// this.gl.linkProgram(this.FlipProgram);
			this.gl.useProgram(this.FlipProgram);

			this.gl.clearColor(.0, .0, .0, .0);
			this.gl.clear(this.gl.COLOR_BUFFER_BIT);
			this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
		}else{
			this.gl.activeTexture(this.gl.TEXTURE0);
			this.gl.bindTexture(this.gl.TEXTURE_2D,texture0);

			// this.gl.linkProgram(this.FlipProgram);
			this.gl.useProgram(this.FlipProgram);

			this.gl.clearColor(.0, .0, .0, .0);
			this.gl.clear(this.gl.COLOR_BUFFER_BIT);
			this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
		}
		// console.timeEnd('renderGL')
	}

	/** 清空显示 */
	clearRender(){
		this.gl.clearColor(.0, .0, .0, .0);
		this.gl.clear(this.gl.COLOR_BUFFER_BIT);
	}

	/** 释放 */
	destroy(){
		let gl = this.gl;
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.bindTexture(gl.TEXTURE_2D, null);
		gl.detachShader(this.defaultProgram, this.defaultvshader); //从一个WebGLProgram中分离一个先前附加的片段或者顶点着色器;
		gl.detachShader(this.defaultProgram, this.defaultfshader);
		gl.detachShader(this.FlipProgram, this.Flipvshader); 
		gl.detachShader(this.FlipProgram, this.Flipfshader);
		gl.deleteShader(this.defaultvshader);
		gl.deleteShader(this.defaultfshader);
		gl.deleteShader(this.Flipvshader);
		gl.deleteShader(this.Flipfshader);
		gl.deleteProgram(this.defaultProgram);
		gl.deleteProgram(this.FlipProgram);
		this.transManager.destroy();
		gl.getExtension('WEBGL_lose_context').loseContext();
		
	}
}
export default RenderGL;