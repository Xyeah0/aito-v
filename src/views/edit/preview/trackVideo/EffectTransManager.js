/**
 * 基本预览轨道
 */
import App from 'AppCore';
import DataType from '@/model/DataType';

import TransDissolve from './effectTrans/effectDissolve/TransDissolve.js';
import TransDiffuseWipe from "./effectTrans/effectDiffuseWipe/TransDiffuseWipe.js";
import TransTranslation from "./effectTrans/effectTranslation/TransTranslation.js";
import TransGlow from "./effectTrans/effectGlow/TransGlow.js";
import TransSplit from "./effectTrans/effectSplit/TransSplit.js";
import TransPush from "./effectTrans/effectPush/TransPush.js";
import TransCompress2 from "./effectTrans/effectCompress2/TransCompress2.js";
import TransRect from "./effectTrans/effectRect/TransRect.js";
import TransRipple from "./effectTrans/effectRipple/TransRipple.js";
import TransWind from "./effectTrans/effectWind/TransWind.js";
import TransSwap from "./effectTrans/effectSwap/TransSwap.js";
import TransSqueeze from "./effectTrans/effectSqueeze/TransSqueeze.js";
import TransSquareswire from "./effectTrans/effectSquareswire/TransSquareswire.js";
import TransRotatescalefade from "./effectTrans/effectRotatescalefade/TransRotatescalefade.js";
import TransRandomsquares from "./effectTrans/effectRandomsquares/TransRandomsquares.js";
import TransPolarfunction from "./effectTrans/effectPolarfunction/TransPolarfunction.js";
import TransPixelize from "./effectTrans/effectPixelize/TransPixelize.js";
import TransMultiplyBlend from "./effectTrans/effectMultiplyBlend/TransMultiplyBlend.js";
import TransHeart from "./effectTrans/effectHeart/TransHeart.js";
import TransFlyeye from "./effectTrans/effectFlyeye/TransFlyeye.js";
import TransFadegrayscale from "./effectTrans/effectFadegrayscale/TransFadegrayscale.js";
import TransFadecolor from "./effectTrans/effectFadecolor/TransFadecolor.js";
import TransDoorway from "./effectTrans/effectDoorway/TransDoorway.js";
import TransDirectionalwipe from "./effectTrans/effectDirectionalwipe/TransDirectionalwipe.js";
import TransCube from "./effectTrans/effectCube/TransCube.js";
import TransCrosswarp from "./effectTrans/effectCrosswarp/TransCrosswarp.js";
import TransColorphase from "./effectTrans/effectColorphase/TransColorphase.js";
import TransCircleopen from "./effectTrans/effectCircleopen/TransCircleopen.js";
import TransCircle from "./effectTrans/effectCircle/TransCircle.js";
import TransBurn from "./effectTrans/effectBurn/TransBurn.js";
import TransAngular from "./effectTrans/effectAngular/TransAngular.js";
import TransPinwheel from "./effectTrans/effectPinwheel/TransPinwheel.js";
import TransDoomscreentransition from "./effectTrans/effectDoomscreentransition/TransDoomscreentransition.js";
import TransDreamyzoom from "./effectTrans/effectDreamyzoom/TransDreamyzoom.js";
import TransGlitchdisplace from "./effectTrans/effectGlitchdisplace/TransGlitchdisplace.js";
import TransHexagonalize from "./effectTrans/effectHexagonalize/TransHexagonalize.js";
import TransWindowblinds from "./effectTrans/effectWindowblinds/TransWindowblinds.js";
import TransKaleidoscope from "./effectTrans/effectKaleidoscope/TransKaleidoscope.js";
import TransButterflywavescrawler from "./effectTrans/effectButterflywavescrawler/TransButterflywavescrawler.js";

const CORECONTROL = App.CONTROL.CORE;
const EFFECTCONTROL = App.CONTROL.EFFECT;
// const LayoutControl = App.CONTROL.LAYOUT;
class EffectTransManager{
    constructor (data) {
        this.trackData = data;
        // this.root = $('<div class="render-track"></div>');
        
        // this.flushData(data);
        this.gl;
        this.effectWholes = [];//包含应用程序及特技对象

        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_TRANS_ADD,this.transAddHandler);
        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_TRANS_CHANGE,this.transChangeHandler);
        EFFECTCONTROL.$on(EFFECTCONTROL.EFFECT_TRANS_REMOVE,this.transRemoveHandler);
    }

    /** 初始化gl */
    initGL(gl){
        this.gl = gl;

    }
    /** 转场特技添加事件 */
    transAddHandler = (obj) =>{
        let effect = obj.effect;
        if(effect.trackId !== this.trackData.modelId) return;
        let effData = effect.effect;
        this.effectWholes.push({
            effData: effData,
            programEff: this.installProgram(effData)
        })
    }

    /** 转场特技变化事件 */
    transChangeHandler = (obj) =>{
        let effect = obj.effect;
        if(effect.trackId !== this.trackData.modelId) return;
        let effData = effect.effect;
        for(let j=0;j<this.effectWholes.length;j++){
            let effWhole = this.effectWholes[j];
            if(effWhole.effData.id === effData.id){
                effWhole.effData = effData;
                break;
            }
        }
    }
    /** 转场特技删除事件 */
    transRemoveHandler = (obj) =>{
        let effect = obj.effect;
        if(effect.trackId !== this.trackData.modelId) return;
        let effData = effect.effect;
        for(let j=0;j<this.effectWholes.length;j++){
            let effWhole = this.effectWholes[j];
            if(effWhole.effData.id === effData.id){
                this.uninstallProgram(effWhole);
                this.effectWholes.splice(j,1);
                break;
            }
        }
    }

    /** 传入转场特技ID，返回应用程序 */
    getProgramOfId(id){
        for(let j=0;j<this.effectWholes.length;j++){
            let effWhole = this.effectWholes[j];
            if(effWhole.effData.id === id){
                return effWhole.programEff;
            }
        }
        return null;
    }

    /**
     * 销毁自己
     */
    destroy () {
        // EffectControl.removeEventListener(EffectControl.EFFECT_TRANS_ADD,this.transAddHandler);
        // EffectControl.removeEventListener(EffectControl.EFFECT_TRANS_CHANGE,this.transChangeHandler);
        // EffectControl.removeEventListener(EffectControl.EFFECT_TRANS_REMOVE,this.transRemoveHandler);
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_TRANS_ADD,this.transAddHandler);
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_TRANS_CHANGE,this.transChangeHandler);
        EFFECTCONTROL.$off(EFFECTCONTROL.EFFECT_TRANS_REMOVE,this.transRemoveHandler);
        this.uninstallProgramAll();
    }
    

    /** 创建webgl应用程序 */
    installProgram (effData){
        let programEff = null;
        switch(effData.dataType){
            case DataType.TRANS_FADE:
                programEff = new TransDissolve();
                break;
            case DataType.TRANS_GLOW:
                programEff = new TransGlow();
                break;
            case DataType.TRANS_DIFFUSEWIPE:
                programEff = new TransDiffuseWipe();
                break;
            case DataType.TRANS_TRANSLATION:
                programEff = new TransTranslation();
                break;
            case DataType.TRANS_SPLIT:
                programEff = new TransSplit();
                break;
            case DataType.TRANS_PUSH:
                programEff = new TransPush();
                break;
            case DataType.TRANS_COMPRESS2:
                programEff = new TransCompress2();
                break;
            case DataType.TRANS_RECT:
                programEff = new TransRect();
                break;
            case DataType.TRANS_RIPPLE:
                programEff = new TransRipple();
                break;
            case DataType.TRANS_WIND:
                programEff = new TransWind();
                break;
            case DataType.TRANS_SWAP:
                programEff = new TransSwap();
                break;
            case DataType.TRANS_SQUEEZE:
                programEff = new TransSqueeze();
                break;
            case DataType.TRANS_SQUARESWIRE:
                programEff = new TransSquareswire();
                break;
            case DataType.TRANS_ROTATESCALEFADE:
                programEff = new TransRotatescalefade();
                break;
            case DataType.TRANS_RANDOMSQUARES:
                programEff = new TransRandomsquares();
                break;
            case DataType.TRANS_POLARFUNCTION:
                programEff = new TransPolarfunction();
                break;
            case DataType.TRANS_PIXELIZE:
                programEff = new TransPixelize();
                break;
            case DataType.TRANS_MULTIPLYBLEND:
                programEff = new TransMultiplyBlend();
                break;
            case DataType.TRANS_HEART:
                programEff = new TransHeart();
                break;
            case DataType.TRANS_FLYEYE:
                programEff = new TransFlyeye();
                break;
            case DataType.TRANS_FADEGRAYSCALE:
                programEff = new TransFadegrayscale();
                break;
            case DataType.TRANS_FADECOLOR:
                programEff = new TransFadecolor();
                break;
            case DataType.TRANS_DOORWAY:
                programEff = new TransDoorway();
                break;
            case DataType.TRANS_DIRECTIONALWIPE:
                programEff = new TransDirectionalwipe();
                break;
            case DataType.TRANS_CUBE:
                programEff = new TransCube();
                break;
            case DataType.TRANS_CROSSWARP:
                programEff = new TransCrosswarp();
                break;
            case DataType.TRANS_COLORPHASE:
                programEff = new TransColorphase();
                break;
            case DataType.TRANS_CIRCLEOPEN:
                programEff = new TransCircleopen();
                break;
            case DataType.TRANS_CIRCLE:
                programEff = new TransCircle();
                break;
            case DataType.TRANS_BURN:
                programEff = new TransBurn();
                break;
            case DataType.TRANS_ANGULAR:
                programEff = new TransAngular();
                break;
            case DataType.TRANS_PINWHEEL:
                programEff = new TransPinwheel();
                break;
            case DataType.TRANS_DOOMSCREENTRANSITION:
                programEff = new TransDoomscreentransition();
                break;
            case DataType.TRANS_DREAMYZOOM:
                programEff = new TransDreamyzoom();
                break;
            case DataType.TRANS_GLITCHDISPLACE:
                programEff = new TransGlitchdisplace();
                break;
            case DataType.TRANS_HEXAGONALIZE:
                programEff = new TransHexagonalize();
                break;
            case DataType.TRANS_WINDOWBLINDS:
                programEff = new TransWindowblinds();
                break;
            case DataType.TRANS_KALEIDOSCOPE:
                programEff = new TransKaleidoscope();
                break;
            case DataType.TRANS_BUTTERFLYWAVESCRAWLER:
                programEff = new TransButterflywavescrawler();
                break;
        }
        if(programEff){
            programEff.initProgram(this.gl);
        }
        return programEff;
    }

    /** 卸载webgl应用程序 */
    uninstallProgram (effWhole){
        if(effWhole.programEff){
            effWhole.programEff.uninstallProgram();
        }
    }

    /** 卸载所有webgl应用程序 */
    uninstallProgramAll(){
        for(let j=0;j<this.effectWholes.length;j++){
            if(this.effectWholes[j].programEff){
                this.effectWholes[j].programEff.uninstallProgram();
            }
        }
    }
    
}

export default EffectTransManager;