/**
 * 基本预览轨道
 */
import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
class BaseRenderTrack{
	constructor (data) {
		this.data = data;

		this.root = document.createElement('div');
		this.root.classList.add('render-track');
		
		this.flushData(data);

		CORECONTROL.$on(CORECONTROL.TRACK_CHANGE,this.trackChangeHandler);
		CORECONTROL.$on(CORECONTROL.TRACK_DELETE,this.trackDeleteHandler);
		CORECONTROL.$on(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);
	}

	/** 刷新数据 */
	flushData(data){
		this.data = data;
		this.root.setAttribute('modelId',data.modelId);
		this.root.setAttribute('dataType',data.dataType);
		this.root.style.zIndex = data.trackIndex;
		this.root.setAttribute('z-index',App.maxzIndex-data.trackIndex);
	}

	/** 轨道改变事件 */
	trackChangeHandler = (track) =>{
		if(track.modelId === this.data.modelId){
			this.flushData(track);
		}
	}

	/** 轨道删除事件 */
	trackDeleteHandler = (track) =>{
		if(track.modelId === this.data.modelId){
			this.destroy();
		}
	}

	/**
	 * 清空时间线
	 */
	clearTimelineHandler=()=>{
		this.destroy();
	}
	/**
	 * 销毁
	 */
	destroy(){
		CORECONTROL.$off(CORECONTROL.TRACK_CHANGE,this.trackChangeHandler);
		CORECONTROL.$off(CORECONTROL.TRACK_DELETE,this.trackDeleteHandler);
		CORECONTROL.$off(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);
		this.root.remove();
	}
	
}

export default BaseRenderTrack;