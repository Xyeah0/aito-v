import App from 'AppCore';
import DataType from '@/model/DataType';
import AudioPlayer from './renderAudio/AudioPlayer';
const CORECONTROL = App.CONTROL.CORE;
import BaseRenderTrack from '../BaseRenderTrack.js';
/**
 * 音频预览轨道
 */

class RenderTrackAudio extends BaseRenderTrack{
	constructor (data) {
		super(data);

		this.players = [];//播放器组
		this.currentPlayers = [];//当前正在渲染的播放器组
		this.currentTime = 0;
		

		CORECONTROL.$on(CORECONTROL.CLIP_ADD,this.clipAddHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$on(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$on(CORECONTROL.PREVIEW_STATUS,this.previewStatusHandler);
		CORECONTROL.$on(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_VOLUME_CHANGE,this.clipVolumeHandler);
	}

	 /** 添加片断 */
	 clipAddHandler = (clip) =>{
		if(clip.trackId === this.data.modelId){
			let player = null;
			if(clip.dataType === DataType.CLIP_AUDIO){
				player = new AudioPlayer(clip);
			}

			if(player == null){
				console.log("创建播放器失败请检查数据",clip);
				return;
			}
			player.waitingBack = this.waitingHandler;
			this.players.push(player);
		}
	}

	/** 片断改变 */
	clipChangeHandler = (clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].flushData(clip);
				break;
			}
		}
	}

	/** 删除片断 */
	clipDeleteHandler = (clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].destroy();
				this.players.splice(i,1);
				break;
			}
		}
	}

	/** 音量改变事件 */
	clipVolumeHandler =(clip) =>{
		for(let i=0;i<this.players.length;i++){
			if(this.players[i].data.modelId === clip.modelId){
				this.players[i].flushData(clip);
				break;
			}
		}
	}

	/**
	 * 预览状态发生改变
	 */
	previewStatusHandler = (b) => {
		if(b){
			for(let i=0;i<this.players.length;i++){
				this.players[i].seek(this.players[i].data.trackIn);
			}
			this.currentTime = App.activeSequence.currentTime;
			this.currentPlayers = this.getPlayerUseTime(this.currentTime);
			for(let i=0;i<this.currentPlayers.length;i++){
				this.currentPlayers[i].seek(this.currentTime);
				this.currentPlayers[i].play();
			}
			// this.preLoading(this.currentTime);
		}else{
			// console.log('end预览') 
			for(let i=0;i<this.players.length;i++){
				this.players[i].pause();
			} 
		}
	}
	/** 
	 * 每帧更新事件
	 */
	currentTimeHandler = (v) =>{
		let time = App.activeSequence.currentTime;
		if(App.activeSequence.isPreview){
			// console.log('开始预览')
			// this.mergeCanvas();
			//找到time所在的播放器
			this.currentPlayers=[];
			for(let i=0;i<this.players.length;i++){
				let player = this.players[i];
				if(time >= player.data.trackIn && time < player.data.trackOut){
					this.currentPlayers.push(player);
					player.enterframe(time);
					if(player.isPause()){
						// player.seek(time);//player.getStartTime());
						player.play();
					}
				}else{
					if(!player.isPause()){
						player.pause();
					}
				}
			}
		}
	}

	

	/** seek事件 */
	seekHandler = (currentTime) =>{
		
		// this.currentPlayers = this.getPlayerUseTime(currentTime);
		// // console.log('seekHandler',currentTime,this.currentPlayers)
		// if(this.currentPlayers.length > 0){
		//     for(let i=0;i<this.currentPlayers.length;i++){
		//         let player = this.currentPlayers[i];
		//         player.isSeekEnd = false;
		//         player.seek(currentTime);
		//     }
		// }
	}



	//播放器缓冲状态事件
	waitingHandler = (obj) => {
		// CoreControl.tellWaitforPreview(obj);
		App.activeSequence.tellWaitforPreview(obj);
	}

	/** 外部调用设置缓冲状态 */
    setWaiting(b){
        // this.isWaiting = b;
        // if(b){
        //     this.players.forEach(player => {
        //         player.pause();
        //     });
		// }
		for(let i=0;i<this.currentPlayers.length;i++){
            let player = this.currentPlayers[i];
            if(b){
                player.pause();
            }else{
                player.play();
            }
        }
    }

	/**
	 * 通过time找对应播放器 可能会有多个
	 */
	getPlayerUseTime(time){
		let players = [];
		for(let i=0;i<this.players.length;i++){
			if(time >= this.players[i].data.trackIn && time < this.players[i].data.trackOut){
				players.push(this.players[i]);
			}
		}
		return players;
	}


	destroy(){
		super.destroy();

		CORECONTROL.$off(CORECONTROL.CLIP_ADD,this.clipAddHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$off(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$off(CORECONTROL.PREVIEW_STATUS,this.previewStatusHandler);
		CORECONTROL.$off(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_VOLUME_CHANGE,this.clipVolumeHandler);

		for(let i=0;i<this.players.length;i++){
			this.players[i].destroy();
		}
		this.players = [];
	}
	
}

export default RenderTrackAudio;