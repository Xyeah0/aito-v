/**
 * 音频基础播放器
 */

class AudioPlayer{
	constructor(data){
		let self = this;
		this.data = data;
		// this.duration = 0;
		// this.seekedBack = null;
		// this.waitingBack = null; //缓冲状态回调
		// this.isSeekEnd = false;
		// this.isFadeIn = false; //是否已经进入淡入
		// this.isFadeOut = false;
		// this.globalVolume = 1;
		// // this.seekCount = 0;

		this.audio = new Audio();
		// this.audio.loop = true;

		// this.audio.crossOrigin = "*";
		// console.log('this.data.previewUrl',this.data.previewUrl)
		this.audio.src = this.data.previewUrl;
		this.audio.muted = this.data.isMute;
		this.error = false;
		// console.log('AudioPlayer',this.audio.src)

		this.audio.addEventListener("loadedmetadata",loadedmetadataHandler);
		this.audio.addEventListener("durationchange",durationchangeHandler);
		this.audio.addEventListener("canplaythrough",canplaythroughHandler);
		this.audio.addEventListener("error",errorHandler);
		this.audio.addEventListener("play",playHandler);
		this.audio.addEventListener("playing",playingHandler);
		this.audio.addEventListener("waiting",waitingHandler);
		this.audio.addEventListener("pause",pauseHandler);
		this.audio.addEventListener("ended",endedHandler);
		this.audio.addEventListener("timeupdate",timeupdateHandler);
		this.audio.addEventListener("seeking",seekingHandler);
		this.audio.addEventListener("seeked",seekedHandler);


		 /*以下为video事件
		 ============================================================
		 * */
		function loadedmetadataHandler(){
			self.duration = self.audio.duration;
			self.error = false;
		}
		function durationchangeHandler(){
		}
		function canplaythroughHandler(){
			// console.log("canplaythroughHandler");
			
			// if(this.seekedBack && this.isFirst){
			//     this.seekedBack(this);
			//     this.isFirst = false;
			// }
			//console.log("可以流畅播放"+self.audio.duration);
			//self.loadingImg.style.display = "none";
			if(self.waitingBack){
				self.waitingBack({playerId:self.data.modelId,isWaiting:false});
			}
		}
		function errorHandler(e){
			console.log(e);
			self.error = true;
			// self.audio.src = 'http://172.29.3.98:8000/media/audio/左手指月.m4a'
		}
		function playHandler(){
			
		}
		function playingHandler(){
			// if(self.waitingBack){
			// 	self.waitingBack({playerId:self.data.modelId,isWaiting:false});
			// }
		}
		function pauseHandler(){
			// if(self.waitingBack){
			// 	self.waitingBack({playerId:self.data.modelId,isWaiting:false});
			// }
		}
		function waitingHandler(){
			//需要向上发送缓冲事件
			if(self.waitingBack){
				self.waitingBack({playerId:self.data.modelId,isWaiting:true});
			}
		}
		function endedHandler(){
			
		}
		function timeupdateHandler(){
			// this.setVolume(this.globalVolume);
		}
		function seekingHandler(){

		}
		function seekedHandler(){
			// console.log("查找完成");
			// if(this.seekedBack){
			//     this.seekedBack(this);
			// }
		}


		this.audiodestroy = ()=>{
			this.audio.removeEventListener("loadedmetadata",loadedmetadataHandler);
			this.audio.removeEventListener("durationchange",durationchangeHandler);
			this.audio.removeEventListener("canplaythrough",canplaythroughHandler);
			this.audio.removeEventListener("error",errorHandler);
			this.audio.removeEventListener("play",playHandler);
			this.audio.removeEventListener("playing",playingHandler);
			this.audio.removeEventListener("waiting",waitingHandler);
			this.audio.removeEventListener("pause",pauseHandler);
			this.audio.removeEventListener("ended",endedHandler);
			this.audio.removeEventListener("timeupdate",timeupdateHandler);
			this.audio.removeEventListener("seeking",seekingHandler);
			this.audio.removeEventListener("seeked",seekedHandler);
		}
	}

	/** 刷新数据 */
	flushData (data) {
		this.data = data;
		this.audio.volume = data.volume;
		this.audio.muted = data.isMute;
	}


	seek(time){
		if(time >= this.data.trackIn && time < this.data.trackOut){
			let willSeekTime = this.data.inPoint+(time-this.data.trackIn) || 1;
			if(this.data.dataType2 == 'AudioBg'){
				let dur = Math.min(this.data.trackOut-this.data.trackIn,this.data.duration);
				if(time > dur){
					willSeekTime = time%dur;
				}
				// let dur = Math.min(this.data.trackOut-this.data.trackIn,this.data.duration);
				// if(time > 0){
				//     willSeekTime = dur%time;
				//     console.log('willSeekTime',willSeekTime)
				// }
				// console.log(dur,time,time)
				// var willSeekTime1 = dur%time;
				
			}
			this.audio.currentTime = willSeekTime/1000 || 0.0001;//this.data.inPoint+(time-this.data.trackIn) || 0.0001;
		}
		// else if(this.data.dataType2 == 'AudioBg'){
		//     let dur = Math.min(this.data.trackOut-this.data.trackIn,this.data.duration);
		//     let willSeekTime = dur%dur;
		//     this.audio.currentTime = willSeekTime/1000;
		// }
	}

   
	play (){
		if(!this.error){
			this.audio.play();
		}
	}

	pause (){
		this.audio.pause();
	}

	/** 获得播放器是否处理暂停状态 */
	isPause = () =>{
		return this.audio.paused;
	}
	/** 获得播放器当前时间 */
	getCurrentTime = () => {
		return this.audio.currentTime*1000;
	}

	enterframe(time){
		if(this.data.effect.fadeOut>0){
			let sTrackIn = this.data.trackOut - this.data.effect.fadeOut;
			let sTrackOut = this.data.trackOut;
			if(time >= sTrackIn && time < sTrackOut){
				let per = (time-sTrackIn)/this.data.effect.fadeOut;
				per = 1-per;
				this.audio.volume = per;
			}else if(this.audio.volume != this.data.volume){
				this.audio.volume = this.data.volume;
			}
		}
	}

	// /** 设置音量 */
	// setVolume(){
	//     this.audio.volume = this.data.volume;
	// }

	destroy(){
		// super.destroy();
		this.audiodestroy();
		this.audio.pause();
		this.audio.src = '';
		this.audio.load();
		this.isEmptyMemory = true;
		// this.effectManager.uninstallProgramAll();
		// this.operationArea.destroy();
	}
}
export default AudioPlayer;