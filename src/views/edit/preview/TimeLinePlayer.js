/**
 * 时间线播放器
 */
import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
class TimeLinePlayer{
	constructor(){

		this.timer = 0;
		this.frameRate = 25;
		this.currentTime = 0;
		this.recordGmt = 0;
		this.recordCT = 0;
		this.STARTTIME = 0;
		this.ENDTIME = 0;
		this.currentWaits = [];//当前正处于缓冲的播放器

		
		// CoreControl.addEventListener(CoreControl.UPDATE_SEQUENCE,this.seqChangeHandler);
	   
		// CoreControl.addEventListener(CoreControl.PREVIEW_START,this.previewStartHandler);
		// CoreControl.addEventListener(CoreControl.PREVIEW_END,this.previewEndHandler);

		// CoreControl.addEventListener(CoreControl.PREVIEW_WAIT_CHANGE,this.previewWaitHandler);
		
		// CoreControl.addEventListener(CoreControl.SEEK_SEQUENCE,this.seekHandler);
		// CoreControl.addEventListener(CoreControl.CHECK_CURRENTTIME,this.checkCurrentTimeHandler);
		// DataControl.addEventListener(DataControl.START_SHOTSCREEN,this.shotScreenHandler);

		CORECONTROL.$on(CORECONTROL.PREVIEW_START,this.previewStartHandler);
		CORECONTROL.$on(CORECONTROL.PREVIEW_END,this.previewEndHandler);
		CORECONTROL.$on(CORECONTROL.PREVIEW_WAIT_CHANGE,this.previewWaitHandler);
		CORECONTROL.$on(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$on(CORECONTROL.CHECK_CURRENTTIME,this.checkCurrentTimeHandler);
	}

	// /**
	//  * 时间线改变事件
	//  */
	// seqChangeHandler = (obj) => {
	//     if(obj.needStop){
	//         // console.log('需要暂停')
	//         this.previewEndHandler();
	//     }
	// }
	/**
	 * seek事件
	 * */
	seekHandler = (v) =>{
		this.previewEndHandler();
		App.activeSequence.updateCurrentTime(v);
	}
   
	/**
	 * 开始预览
	 * */
	previewStartHandler=(obj)=>{
		// console.log("输出开始预览",App.activeSequence.getClipEndTime())
		this.ENDTIME = App.activeSequence.seqDuration;
		if(obj){
			if(obj.endTime){
				this.ENDTIME = obj.endTime;
			}
			if(obj.startTime){
				this.STARTTIME = obj.startTime;
			}
		}
		this.currentTime = App.activeSequence.currentTime;
		if(this.currentTime < this.ENDTIME-40){
			this.recordGmt = +new Date;
			clearInterval(this.timer);
			this.timer = setInterval(this.enterFrameHandler,1000/this.frameRate);
			App.activeSequence.updatePreviewStatus(true);
		}
	}

	/**
	 * 结束预览
	 */
	previewEndHandler=()=>{
		// console.log("输出结束预览",App.currentTime,this.ENDTIME)
		clearInterval(this.timer);
		App.activeSequence.updatePreviewStatus(false);
	}

	/**校验播放头*/
	checkCurrentTimeHandler=(obj)=>{
		this.currentTime += obj.diff || 0;
		App.activeSequence.updateCurrentTime(this.currentTime);
	}
	/**
	 * 预览播放事件
	 */
	enterFrameHandler=()=>{
		let newGmt = +new Date;
		this.currentTime += newGmt - this.recordGmt;//console.log('this.currentTime',this.currentTime,1000/(newGmt - this.recordGmt))
		this.recordGmt = newGmt;
		// this.currentTime += 40;
		
		if(this.currentTime >= this.ENDTIME){
			this.previewEndHandler();
			//播入结束后，自动跳到第一帧
			// App.activeSequence.seek(this.STARTTIME);
		}else{
			App.activeSequence.updateCurrentTime(Math.min(this.currentTime,this.ENDTIME));
		}
	}
	/** 
	 * 收到缓冲状态事件
	 */
	previewWaitHandler = (obj) => {
		if(obj.isWaiting){
			this.currentWaits.push(obj.playerId);
		}
		
		//去除重复的
		this.currentWaits = [...new Set(this.currentWaits)];
		if(!obj.isWaiting){
			this.currentWaits.splice(this.currentWaits.indexOf(obj.playerId),1);
		}
		// console.log('收到缓冲事件',this.currentWaits,App.activeSequence.isPreview)
		if(this.currentWaits.length > 0){
			//发出缓冲事件
			if(this.waitingBack){
				this.waitingBack(true);
			}
			if(App.activeSequence.isPreview){
				clearInterval(this.timer);
			}
		}else{
			//取消缓冲事件
			if(this.waitingBack){
				this.waitingBack(false);
			}
			if(App.activeSequence.isPreview){
				clearInterval(this.timer);
				this.recordGmt = +new Date;
				this.timer = setInterval(this.enterFrameHandler,1000/this.frameRate);
			}
		}
	}

	destroyed(){
		CORECONTROL.$off(CORECONTROL.PREVIEW_START,this.previewStartHandler);
		CORECONTROL.$off(CORECONTROL.PREVIEW_END,this.previewEndHandler);
		CORECONTROL.$off(CORECONTROL.PREVIEW_WAIT_CHANGE,this.previewWaitHandler);
		CORECONTROL.$off(CORECONTROL.SEEK,this.seekHandler);
		CORECONTROL.$off(CORECONTROL.CHECK_CURRENTTIME,this.checkCurrentTimeHandler);
		this.waitingBack = null;
		clearInterval(this.timer);
	}
}

export default TimeLinePlayer;