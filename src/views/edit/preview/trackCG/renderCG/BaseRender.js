
import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
class BaseRender{
	constructor(obj){
		this.clip = obj;
		
		this.root = document.createElement('div');
		this.root.classList.add(this.clip.dataType);
		this.root.setAttribute('dataType',this.clip.dataType);
		this.root.style.position = 'absolute';

		this.root.empty = ()=>{
			this.root.innerHTML = '';
		}
		// this.render();
		// this.beforeX = 0;
		// this.beforeY = 0;
		// this.beforeW = 0;
		// this.beforeH = 0;
		// this.beforestime = 0;
		// this.beforeetime = 0;

		// this.nowPos = {
		// 	x:0,
		// 	y:0,
		// 	width:0,
		// 	height:0
		// }

		// this.calcPercentage();
		// this.root.css({
		// 	position:'absolute',
		// 	left:this.nowPos.x,
		// 	top:this.nowPos.y,
		// 	width:this.nowPos.width,
		// 	height:this.nowPos.height,
		// 	"z-index":0
		// });

		// this.beforePosOfDEF = Object.assign({},this.nowPos);
		
		
		// let img = $('<img crossOrigin="*"/>');
		// img.css({
		// 	width:"100%",
		// 	height:"100%"
		// });
		// img.attr("src",obj.sourceUrl);
		// this.root.append(img);
		// // this.deftool = new DeformationTool(this.root[0],this.deftoolChangeing,this.deftoolChanged);
		// this.deftool = new DefTool(this.root[0],this.deftoolChangeing,this.deftoolChanged,false);//最后一个值代表是否限制到父级范围
		// this.deftool.setMinWH(18,18);
		// this.deftool.flushPosition({x:this.nowPos.x,y:this.nowPos.y,w:this.nowPos.width,h:this.nowPos.height});
		
		// CoreControl.addEventListener(CoreControl.DESTROY_SEQUENCE,this.destroySeqHandler);
		// CoreControl.addEventListener(CoreControl.UPDATE_SEQUENCE,this.seqChangeHandler);
		// CoreControl.addEventListener(CoreControl.CLIP_DBCLICK,this.clipDBclickHandler);
		// CoreControl.addEventListener(CoreControl.PLAYHEADTIME_CHANGE,this.playHeadTimerHandler);
		// CoreControl.addEventListener(CoreControl.HIDDEN_DEFTOOL,this.hiddenDefToolHandler);
		// LayoutControl.addEventListener(LayoutControl.PREVIEW_CHANGED,this.updateWHHandler);
		// this.root.click(this.clickHandler);
		// this.root.dblclick(this.rootdblclick);
		this.root.addEventListener('click',this.clickHandler);

		CORECONTROL.$on(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$on(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$on(CORECONTROL.PRESET_CHANGE,this.presetHandler);
		CORECONTROL.$on(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);

	}

	/**点击事件 */
	clickHandler = (event) =>{
		this.selectClip();
	}

	selectClip(){
		App.activeSequence.selectClip(this.clip);
	}

	render(){
		this.root.style.left = this.clip.pos.x*100+'%';
		this.root.style.top = this.clip.pos.y*100+'%';
		this.root.style.width = this.clip.pos.width*100+'%';
		this.root.style.height = this.clip.pos.height*100+'%';
	}

	/** 片断改变 */
    clipChangeHandler = (clip) =>{
        if(clip.modelId === this.clip.modelId){
			// this.clipChange(clip);
			this.clip = clip;
			this.render();
		}
    }

    /** 删除片断 */
    clipDeleteHandler = (clip) =>{
        if(clip.modelId === this.clip.modelId){
			this.destroy();
		}
	}

	/** 
     * 每帧更新事件
     */
    currentTimeHandler = (v) =>{
		let headTime = App.activeSequence.currentTime;
		if(headTime >= this.clip.trackIn && headTime < this.clip.trackOut){
			// this.root.show();
			this.root.style.visibility = 'visible';
			this.render();
			// this.deftool.show();
			// if(this.data.isFadeIn && headTime-this.data.startTime <= this.data.fadeInSec){
			// 	this.root.css({opacity:(headTime-this.data.startTime) / this.data.fadeInSec});
			// }else if(this.data.isFadeOut && headTime >= this.data.endTime-this.data.fadeOutSec){
			// 	this.root.css({opacity:1-(headTime-(this.data.endTime-this.data.fadeOutSec)) / this.data.fadeOutSec});
			// }else{
			// 	this.root.css({opacity:1});
			// }
		}else{
			// this.root.hide();
			this.root.style.visibility = 'hidden';
		}
        
	}
	
	presetHandler = () =>{
		this.render();
	}
	
	
	
	/**
     * 清空时间线
     */
    clearTimelineHandler=()=>{
        this.destroy();
    }
    /**
     * 销毁
     */
    destroy(){
		this.root.removeEventListener('click',this.clickHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$off(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$off(CORECONTROL.PRESET_CHANGE,this.presetHandler);
		CORECONTROL.$off(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);
        this.root.remove();
    }
	
}
export default BaseRender;