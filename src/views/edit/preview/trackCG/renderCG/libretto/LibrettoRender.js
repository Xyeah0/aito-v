import BaseRender from "../BaseRender.js";
import $ from "jquery";
import App from "../../../../../../model/App.js";
import HistoryModel from '../../../../../../model/HistoryModel.js';
import FormatText from "../../../../../../lib/FormatText.js";
const CoreControl = App.CONTROL.CORE;
const LayoutControl = App.CONTROL.LAYOUT;

class LibrettoRender extends BaseRender{
	constructor(obj){
		super(obj);

		this.root.empty();
		
		this.preText = $("<pre></pre>");
		this.ele_order = $("<div></div>");
		this.text = $('<div class="my-input"></div>');
		// this.bg = $("<div></div>");
		// this.scaleText = 1;//缩放比值
		this.beforeClipId = '';
		this.childrenData = null;
		this.beforeData = {
            modelId: this.data.modelId,
            clipLength: this.data.clips
		}
		
		this.formatText = new FormatText();

		this.root.css({
			'pointer-events': 'none'
		})
		this.preText.css({
			display:'inline',
			position: "absolute",
			'word-break': 'break-all',
			'white-space': 'pre',
			visibility: 'hidden',
			// top:'-120px',
			border: 'none',
			// overflow: 'hidden',
			'height':'auto'
			// top:-80
		});
		this.ele_order.css({
			position: "absolute",
			left:0,
			top:-18,
			width: 60,
			height: 18,
		});
		this.text.css({
			display:'inline',
			position: "absolute",
			left: '50%',
			top: '50%',
			transform: 'translate(-50%,-50%)',
			'word-break': 'break-all',
			'white-space': 'pre',
			// overflow: 'hidden',
			'height':'auto',
			'pointer-events': 'all'
		});

		// this.bg.css({
		// 	width:"100%",
		// 	height:"100%",
		// 	position:"absolute",
		// 	top:0
		// });
		// this.root.append(this.bg);
		this.root.append(this.preText);
		this.root.append(this.ele_order);
		this.root.append(this.text);
		// console.log(this.text[0])
		this.text[0].contentEditable = true;//'plaintext-only';
		this.deftool.setTextInput('libretto');

		this.text.on('input propertychange',this.textInputHandler);
		// this.text.on('change',this.textInputChanged); 
		this.text.on('blur',this.textInputChanged)


		CoreControl.addEventListener(CoreControl.LIBRETTO_CHILDREN_CHANGE,this.librettoCildrenChange);
		CoreControl.addEventListener(CoreControl.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
	}


	/** 唱词子级改变事件 */
    librettoCildrenChange = (obj) => {
		if(obj.from === 'LibrettoRender') return;
	
        switch(obj.type){
			case 'init-position':
				this.childrenInitPosition(obj);
				break;
            case 'content-change':
				if(obj.data.modelId === this.data.modelId){
					const datas = obj.datas;
					if(this.childrenData){
						for(let i=0;i<datas.length;i++){
							if(datas[i].modelId === this.childrenData.modelId){
								this.renderText(this.childrenData);
								break;
							}
						}
					}

					for(let i=0;i<datas.length;i++){
						this.computerWH(datas[i]);
					}
				}
				break;
			case 'style-change': //唱词属性的改变
				if(obj.data.modelId === this.data.modelId){
					let children = obj.datas[0];
					if(this.childrenData && this.childrenData.modelId === children.modelId){
						this.renderText(this.childrenData);
						//某些样式的改变也会影响高宽
						this.computerWH(this.childrenData);
					}
				}
				break;
			case 'add':
			case 'delete':
			case 'reset':
				this.beforeClipId = '';
				this.updatePosition();
				break;
        }
	}
	
	/** 撤销或重做完成后的事件 */
    undoOrRedoFinishHandler = () =>{
		if(!this.data) return;
		this.beforeClipId = '';
		this.updatePosition();
    }

	/** 初始化每个文字的 x y width height */
	childrenInitPosition = (obj) =>{
		if(!this.data) return;
		if(this.data.modelId !== obj.data.modelId) return;

		console.time('computerWH');
		let maxWidth=0,maxHeight = 0,willChanges = [];
		for(let i=0;i<this.data.clips.length;i++){
			let curTxt = this.data.clips[i];

			let transformScale = this.computerWH(curTxt);
			if(curTxt.width*transformScale>1){
				let willCutNum = Math.ceil(curTxt.width*transformScale);
				let rawContent = curTxt.content;
				let singleLen = Math.floor(rawContent.length/willCutNum)-2; 
				willCutNum = Math.ceil(rawContent.length/singleLen);
				// console.log(this.preText.width(),App.editWidth,transformScale,fontsize,curTxt.width,willCutNum,singleLen,rawContent.length)
				let newContent = '';
				for(let j=0;j<willCutNum;j++){
					newContent += rawContent.substring(j*singleLen,(j+1)*singleLen);
					if(j < willCutNum -1){
						newContent += '\n';
					}
				}
				curTxt.content = newContent;
				curTxt.height = curTxt.height*willCutNum;
				willChanges.push(curTxt);
			}
			// curTxt.x = (this.data.width - curTxt.width)*0.5;
			// curTxt.y = (this.data.height - curTxt.height)*0.5;
			maxWidth = Math.max(maxWidth,curTxt.width);
			maxHeight = Math.max(maxHeight,curTxt.height);
			// console.log('this.preText.width()',this.preText.width(),'this.preText.height()',this.preText.height())
		}
		console.timeEnd('computerWH');

		this.data.height = Math.max(maxHeight,this.data.height);
		this.setPosition();
		if(willChanges.length > 0){
			this.renderText(this.childrenData);
			App.activeSequence.Librettos.contentChange(this.data,willChanges,'LibrettoRender');
		}
	}
	/** 传入子级数据，计算该文字的高宽 */
	computerWH = (cData) => {
		this.preText.text(cData.content);
		let fontsize = cData.styles.fontSize/100*App.editHeight;
		let transformScale = 1;
		if(fontsize < 12){
			transformScale = fontsize/12;
			fontsize = 12;
		}
		let styleObj = {
			color:cData.styles.color,
			opacity:cData.styles.alpha,
			"font-size":fontsize+'px',
			"font-family":cData.styles.fontFamily,
			"text-align": cData.styles.textAlign,
			"font-weight": cData.styles.isBold?"bold":"normal",
			"text-decoration": cData.styles.isUnderline?"underline":"none",
			"font-style": cData.styles.isItalic?"italic":"normal",
			"text-shadow":cData.styles.isShadow?(cData.styles.shadowColor+' 2px 2px 2px'):'initial',
			'transform':"scale("+transformScale+")",
			'transform-origin':"0 0 0",
			'margin':0,
			'line-height':fontsize+'px',
		}
		this.preText.css(styleObj);
		// const pos = this.getPosition(this.data.styles.position,this.preText.width(),this.preText.height());
		// curTxt.x = pos.x;
		// curTxt.y = pos.y;
		// curTxt.width = pos.width;
		// curTxt.height = pos.height;
		cData.width = this.preText.width()/App.editWidth;
		cData.height = this.preText.height()/App.editHeight;
		return transformScale;
	}
	updateData(){
		if(!this.data.isSelect){
			this.ele_order.text('');
		}
	}

	updateWHH(){
		this.renderText(this.childrenData);
	}

	updatePosition(){
		let headTime = App.currentTime-this.data.startTime+0.001;
		let curTxt = null;
		for(let i=0;i<this.data.clips.length;i++){
			let clip = this.data.clips[i];
			if(headTime >= clip.inPoint && headTime < clip.outPoint){
				curTxt = clip;
				break;
			}
		}
		// const rootW = this.root.width() || App.editWidth;
		// const rootH = this.root.height() || App.editHeight*this.data.height;
		// console.log(headTime,this.data.startTime,App.currentTime)
		if(curTxt && (this.beforeClipId !== curTxt.modelId)){
			// console.log(this.deftool.root.parentNode)
			this.beforeClipId = curTxt.modelId;
			this.renderText(curTxt);
			
		}else{
			if(!curTxt){
				this.ele_order.text('');
				this.text.text('');
				this.childrenData = null;
				this.beforeClipId = '';
			}
		}
	}



	/** 设置坐标位置 */
	setPosition(type='BOTTOM_CENTER'){
		let y=0;
		const sideW = 10;
		if(type === 'BOTTOM_CENTER'){
			y = 1-this.data.height-sideW/App.editHeight;
		}else if(type === 'CENTER_CENTER'){
			y = (1-this.data.height)/2;
		}else if(type === 'TOP_CENTER'){
			y = sideW/App.editHeight;
		}
		this.data.y = y;
		this.calcRenderPos();
		this.root.css({
			left:this.nowPos.x,
			top:this.nowPos.y,
			width:this.nowPos.width,
			height:this.nowPos.height
		});
		this.deftool.flushPosition({x:this.nowPos.x,y:this.nowPos.y,w:this.nowPos.width,h:this.nowPos.height});
	}

	textInputHandler = () =>{
		// console.log(this.textInput.val());
		if(this.childrenData){
			this.childrenData.content = this.text[0].innerText;
		}
		//this.text.html();
		//div输入需要在完成时更新，否则会失去焦点
        // App.activeSequence.updateClip(this.data);
    }

    //输入改变完成
    textInputChanged = () =>{
		if(!this.childrenData) return;
		// console.log("渲染框内输入改变完成",HistoryModel)
		// window.testTextarea = this.text;
		let str = this.text[0].innerText;//this.text.html();
		//如果最后一个字符是回车，干之
		if(str.length && str.lastIndexOf('\n') === str.length-1){
			this.childrenData.content = str.substring(0,str.length-1);
		}
		this.text.scrollTop(0);
		this.renderText(this.childrenData);
		this.computerWH(this.childrenData);
		App.activeSequence.Librettos.contentChange(this.data,[this.childrenData],'LibrettoRender');
        HistoryModel.record("文字内容改变");
	}

	/** 渲染 */
	renderText = (curTxt) =>{
		this.childrenData = curTxt;
		if(!this.childrenData){
			this.ele_order.text('');
			this.text.text('');
			return;
		}
		if(this.deftool && (this.deftool.root.parentNode || this.data.isSelect)){
			this.ele_order.text('NO. '+curTxt.order);
		}else{
			this.ele_order.text('');
		}
		this.text.text(curTxt.content);
		let fontsize = curTxt.styles.fontSize/100*App.editHeight;
		let transformScale = 1;
		if(fontsize < 12){
			transformScale = fontsize/12;
			fontsize = 12;
		}
		// console.log('App.editHeight*curTxt.styles.fontSize/100',App.editHeight*curTxt.styles.fontSize/100)
		let styleObj = {
			color:curTxt.styles.color,
			opacity:curTxt.styles.alpha,
			"font-size":fontsize+'px',
			"font-family":curTxt.styles.fontFamily,
			"text-align": curTxt.styles.textAlign,
			"font-weight": curTxt.styles.isBold?"bold":"normal",
			"text-decoration": curTxt.styles.isUnderline?"underline":"none",
			"font-style": curTxt.styles.isItalic?"italic":"normal",
			"text-shadow":curTxt.styles.isShadow?(curTxt.styles.shadowColor+' 2px 2px 2px'):'initial',
			'transform':"translate(-50%,-50%) scale("+transformScale+")",
			// 'transform-origin':"0 0 0",
			'margin':0,
			'line-height':fontsize+'px',
			'pointer-events': 'all'
		}
		this.text.css(styleObj);

		// console.log('this.text.width',this.text.width())
		// this.childrenData.width = this.text.width()/App.editWidth;
		// this.childrenData.height = this.text.height()/App.editHeight;
	}

}
export default LibrettoRender;