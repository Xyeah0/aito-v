import BaseRender from "./BaseRender.js";

class ImageRender extends BaseRender{
	constructor(obj){
		super(obj);
		
		this.img = document.createElement('img');
		this.img.setAttribute('src',this.clip.previewUrl);
		this.img.style.width = '100%';
		this.img.style.height = '100%';
		this.img.crossOrigin = '*';

		this.root.appendChild(this.img);

		this.render();
	}


}
export default ImageRender;