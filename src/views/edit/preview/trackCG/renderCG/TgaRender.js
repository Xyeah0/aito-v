import BaseRender from "./BaseRender.js";
import App from 'AppCore';
import DataType from '@/model/DataType';
import TgaCache from "./tga/TgaCache.js";
const CORECONTROL = App.CONTROL.CORE;

class TgaRender extends BaseRender{
	constructor(obj,isTL){
		super(obj);
		// this.root.empty();

		// //this.tga = new Tga('http://172.28.9.46:3188/tga/testTga.seq');
		// this.tga = new Tga(this.data.prevUrl);
		// this.root.append(this.tga.root);
		// this.tga.onTgaComplete = this.handleTgaComplete;
		// this.tga.onSeqComplete = this.handleSeqComplete;
		// this.tgaReady = false;
		// this.isTL = isTL || false;

		// this.playTime = -1;
		// this.loadTgaSeq(this.clip.previewUrl).then((data)=>{
		// 	console.log('加载大要要枯！',data)
		// }).catch(error=>{
		// 	console.log('加载出错！')
		// });

		this.tga = TgaCache.getOneCache(this.clip.previewUrl);
		this.curImg = null;
		// console.log(this.tga)
		this.tga.$on('first',()=>{
			this.render();
		})
	}

	/** 渲染 */
	render(){
		super.render();
		if(this.tga.imgArr.length == 0) return;
		let headTime = App.activeSequence.currentTime;
		if(this.curImg){
			this.curImg.remove();
		}
		let curFrame = Math.floor((headTime - this.clip.trackIn)/(1000/this.clip.framerate));
		curFrame = curFrame%this.tga.imgArr.length;
		curFrame = Math.max(0,Math.min(curFrame,this.tga.imgArr.length-1));
		this.curImg = this.tga.imgArr[curFrame];
		// console.log('this.curImg',this.curImg,curFrame,headTime - this.clip.trackIn,this.clip.framerate)
		this.root.appendChild(this.curImg);
	}

}
export default TgaRender;