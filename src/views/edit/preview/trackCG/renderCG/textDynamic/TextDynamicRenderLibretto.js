
import TextDynamicRender from "./TextDynamicRender";
// import EasyDefTool from "../../../../../../lib/EasyDefTool";
// import Utils from "../../../../../../lib/Utils";
import App from "../../../../../../model/App";

const CoreControl = App.CONTROL.CORE;
// const LayoutControl = App.CONTROL.LAYOUT;
class TextDynamicRenderLibretto extends TextDynamicRender{
    constructor(obj,_pixiRender){
		super(obj,_pixiRender);
		
		CoreControl.addEventListener(CoreControl.LIBRETTO_CHILDREN_CHANGE,this.librettoCildrenChange);
	}
	
	initEditConfig(){
		super.initEditConfig();
		if(this.data.clips.length != this.data.contents.length){
			const srtGroup = [];
			for(let i=0;i<this.data.contents.length;i++){
				let c_item = this.data.contents[i];
				srtGroup.push({
					order: i,
					inPoint:c_item.inPoint/1000,
					outPoint:c_item.outPoint/1000,
					content: c_item.content,
					isItalic: false,
					isBlod: false,
					isUnderline: false,
					color: '#ffffff'
				})
			}
			App.activeSequence.Librettos.createChildrenOfSRT(this.data,srtGroup,this.data.title);
		}
	}

	/**刷新渲染之前 */
	selfFlushRender(){
		// console.log('this.data.clips',this.data.clips)
		
	}

	librettoCildrenChange = (obj) =>{
		if(!this.data) return;
        switch(obj.type){
            case 'add':
                // this.resetList();
                break;
            case 'delete':
                // this.resetList();
                break;
            case 'reset':
                if(obj.data.modelId === this.data.modelId){
                    this.resetRender();
                }
                break;
            case 'content-change': //唱词内容的改变
                if(obj.data.modelId === this.data.modelId){
                    this.resetRender();
                }
                break;
            case 'selected': //唱词选中
                break;
            case 'order-change':
                if(obj.data.modelId === this.data.modelId){
                    this.resetRender();
                }
                break;
            case 'inser-add':
                if(obj.data.modelId === this.data.modelId){
                    this.resetRender();
                }
                break;
            case 'auto-cut':
                break;
        }
	}

	/** 重新渲染 */
	resetRender(){
		//更新this.data.contents数据
		const contents = [];
		for(let i=0;i<this.data.clips.length;i++){
			let item = this.data.clips[i];
			contents.push({
				inPoint:item.inPoint*1000,
				outPoint:item.outPoint*1000,
				content:item.content
			})
		}
		this.data.contents = contents;
		this.MainAnimation.setContent(contents);
		console.log('this.MainAnimation',this.MainAnimation)
	}

	/*
	 * 删除自己
	 */
	selfDestroy(){
		CoreControl.removeEventListener(CoreControl.LIBRETTO_CHILDREN_CHANGE,this.librettoCildrenChange);
	}
}
export default TextDynamicRenderLibretto;