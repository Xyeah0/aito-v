
import EasyDefTool from "../../../../../../libs/EasyDefTool";
import Utils from "../../../../../../libs/Utils";
import App from "../../../../../../model/App";

const CORECONTROL = App.CONTROL.CORE;
class TextDynamicRender{
    constructor(obj,_pixiRender){

		this.clip = obj;
		this.pixiRender = _pixiRender;

		this.root = document.createElement('div');
		this.root.classList.add(this.clip.dataType);
		this.root.setAttribute('dataType',this.clip.dataType);
		this.root.style.position = 'absolute';
		this.root.style.left = '0%';
		this.root.style.top = '0%';
		this.root.style.width = '100%';
		this.root.style.height = '100%';
		this.root.style.zIndex = '0';
		this.root.style.pointerEvents = 'none';
		
		this.initSeekTime = 0;
        this.MainAnimation = null;

        this.easyDefTool = new EasyDefTool();
		this.easyDefTool.changeBack = this.easyDefToolChangeBack.bind(this);
		this.easyDefTool.changedBack = this.easyDefToolChangedBack.bind(this);
		this.easyDefTool.clickBack = this.rootclick.bind(this);
		this.easyDefTool.dblClickBack = this.rootdblclick.bind(this);

       
		this.root.appendChild(this.easyDefTool.root);
        


        // CoreControl.addEventListener(CoreControl.DESTROY_SEQUENCE,this.destroySeqHandler);
		// CoreControl.addEventListener(CoreControl.UPDATE_SEQUENCE,this.seqChangeHandler);
		// CoreControl.addEventListener(CoreControl.CLIP_DBCLICK,this.clipDBclickHandler);
		// CoreControl.addEventListener(CoreControl.PLAYHEADTIME_CHANGE,this.playHeadTimerHandler);
		// CoreControl.addEventListener(CoreControl.HIDDEN_DEFTOOL,this.hiddenDefToolHandler);
		// LayoutControl.addEventListener(LayoutControl.PREVIEW_CHANGED,this.updateWHHandler);
		// CoreControl.addEventListener(CoreControl.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
		// CoreControl.addEventListener(CoreControl.DYNAMIC_TEXT_CHANGE,this.dynamicChangeHandler.bind(this));
        

		CORECONTROL.$on(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$on(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$on(CORECONTROL.PRESET_PREVIEW_CHANGE,this.presetPreviewHandler);
		CORECONTROL.$on(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);
		CORECONTROL.$on(CORECONTROL.DYNAMIC_TEXT_CHANGE,this.dynamicChangeHandler);
		CORECONTROL.$on(CORECONTROL.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
		CORECONTROL.$on(CORECONTROL.SEQ_DURATION,this.seqDurationHandler);

        let strCfg = '';
		let cfgObj = {};
		if(this.clip.config){
			cfgObj.config = this.clip.config
		}
		if(this.clip.duration){
			cfgObj.duration = this.clip.duration;
		}
		if(this.clip.inAnimDur){
			cfgObj.inAnimDur = this.clip.inAnimDur;
		}
		if(this.clip.outAnimDur){
			cfgObj.outAnimDur = this.clip.outAnimDur;
		}
		if(this.clip.width){
			cfgObj.width = this.clip.width;
		}
		if(this.clip.height){
			cfgObj.height = this.clip.height;
		}
		if(this.clip.contents.length){
			cfgObj.contents = this.clip.contents;
		}
		// if(this.clip.carryData.isCustom && this.clip.carryData.baseData){
		// 	this.clip.carryData.isCustom = false;
		// 	strCfg = this.clip.carryData.baseData;
		// 	// console.log('this.clip.carryData',this.clip.carryData)
		// }else{
			let cfgObjStr = JSON.stringify(cfgObj);
			if(cfgObjStr != '{}'){
				strCfg = window.btoa(window.encodeURIComponent(cfgObjStr));
			}
		// }
		
		let sourceUrl = this.clip.sourceUrl;
		let jsUrl = sourceUrl.replace('index.html','index.js');
		let className = this.clip.className;
		if(!className){
			let end = sourceUrl.lastIndexOf('/');
			let start = sourceUrl.lastIndexOf('/',end-1);
			className = sourceUrl.substring(start+1,end);
		}

		if(jsUrl.charAt(0) == '/'){
			jsUrl = window.AppConfig.modulePath+jsUrl
		}

		this.pixiRender.addTemplate({
			id:this.clip.modelId,
			jsUrl:jsUrl,
			className:className,
			trackIn: this.clip.trackIn,
			trackOut:this.clip.trackOut,
			animationConfig:strCfg
		});

		this.duration = this.pixiRender.getDuration();
		this.MainAnimation = null;

		// this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.ON_START,this.onStartHandler.bind(this));
		// this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.ON_UPDATE,this.onUpdateHandler.bind(this));
		// this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.ON_COMPLETE,this.onCompleteHandler.bind(this));
		// this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.PAUSE_STATUS,this.pauseStatusHandler.bind(this));
		this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.TEXT_AREA_CHANGE,this.textAreaChangeHandler.bind(this));
		this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.CONFIG_ACTIVE_CHANGE,this.configActiveChangeHandler.bind(this));

		this.pixiRender.CORE.addEventListener(this.pixiRender.CORE.INIT_READY,(obj)=>{
			// console.log('字幕模板初始化完成',obj)
			if(obj.id == this.clip.modelId){
				this.MainAnimation = obj.app;

				this.pixiRender.seek(this.pixiRender.getCurrentTime());
				this.clip.templateType = this.MainAnimation.templateType;
				this.clip.duration = this.MainAnimation.getDuration();
				this.clip.inAnimDur = this.MainAnimation.inAnimDur;
				this.clip.outAnimDur = this.MainAnimation.outAnimDur;
				this.clip.enabledInOut = this.MainAnimation.enabledInOut;
				this.clip.width = this.MainAnimation.screen.width;
				this.clip.height = this.MainAnimation.screen.height;
				this.clip.config = this.MainAnimation.config;
				this.clip.contents = JSON.parse(JSON.stringify(this.MainAnimation.contents));
				this.clip.configCloumns = JSON.parse(JSON.stringify(this.MainAnimation.getConfigTemplate()));

				//在模板加载过程中，有可能时长被改变了,以入出点为准
				let curDur = this.MainAnimation.getDuration();
				let willDur = (this.clip.trackOut - this.clip.trackIn);
				if(willDur != curDur){
					this.clip.duration = willDur;
					this.MainAnimation.setDuration(willDur);
					this.clip.inAnimDur = this.MainAnimation.inAnimDur;
					this.clip.outAnimDur = this.MainAnimation.outAnimDur;
				}

				this.initEditConfig();
				if(this.clip.canRecord){
					this.clip.canRecord = false;
					//数据完整时，再记录
					App.activeSequence.HistoryModel.record('新建动态字幕');
				}
			}
		})
		
		window.testDynamic = this;
    }

    /** 片断改变 */
    clipChangeHandler = (clip) =>{
        if(clip.modelId === this.clip.modelId){
			// this.clipChange(clip);
			this.clip = clip;
			if(this.MainAnimation){
				let willDur = (this.clip.trackOut - this.clip.trackIn);
				// console.log('willDur != this.clip.duration',willDur , this.clip.duration)
				if(willDur != this.clip.duration){
					this.clip.duration = (this.clip.trackOut - this.clip.trackIn);
					this.MainAnimation.setDuration(this.clip.duration);
					this.clip.inAnimDur = this.MainAnimation.inAnimDur;
					this.clip.outAnimDur = this.MainAnimation.outAnimDur;
					App.activeSequence.dynamicTextChange({cmd:'render-inout-change',clip:this.clip});
				}
				for(let i=0;i<this.clip.needLoadFontFamilys.length;i++){
					let fontName = this.clip.needLoadFontFamilys[i];
					this.checkFontAndLoad(fontName);
				}
				this.clip.needLoadFontFamilys = [];
				this.pixiRender.updateTemplatePos(this.clip.modelId,this.clip.trackIn,this.clip.trackOut);
				
				if(this.clip.needFlushRender){
					this.flushRender();
					this.clip.needFlushRender = false;
				}
			}
		}
    }

    /** 删除片断 */
    clipDeleteHandler = (clip) =>{
        if(clip.modelId === this.clip.modelId){
			this.destroy();
		}
	}

	/** 
     * 每帧更新事件
     */
    currentTimeHandler = (v) =>{
		let headTime = App.activeSequence.currentTime;
		if(headTime >= this.clip.trackIn && headTime < this.clip.trackOut){
			this.root.style.visibility = 'visible';
		}else{
			this.root.style.visibility = 'hidden';
			this.easyDefTool.showTool = false;
		}
        
	}
	
	presetPreviewHandler = () =>{
		// this.render();
	}
	
	/** 撤销或重做完成后的事件 */
    undoOrRedoFinishHandler = () =>{
		// console.log('undoOrRedoFinishHandler',this.clip.contents)
		if(this.MainAnimation){
			
			this.flushRender();
			this.clip.config = this.MainAnimation.config;
			// console.log('undoOrRedoFinishHandler22',this.MainAnimation.contents);
		}
	}
	
	/** 时间线长度变化事件 */
	seqDurationHandler = () =>{
		this.pixiRender.updateTemplatePos(this.clip.modelId,this.clip.trackIn,this.clip.trackOut);
		// console.log('seqDurationHandler')
	}
	
	/**
     * 清空时间线
     */
    clearTimelineHandler=()=>{
        this.destroy();
    }
    

    // //时间线变化事件
    // seqChangeHandler = (obj) => {
    //     switch(obj.cmd){
    //         case CoreControl.SeqEmumType.CLIP_CHANGE:
    //             if(this.clip.modelId === obj.data.modelId){
    //                 this.clipChange(obj.data);
    //             }
    //             this.clipSelect(App.activeSequence.getSelectClip());
    //         break;
    //         case CoreControl.SeqEmumType.CLIP_REMOVE:
    //             if(this.clip.modelId === obj.data.modelId){
    //                 this.destroy();
    //             }
    //         break;
    //         case CoreControl.SeqEmumType.CLIP_REMOVE_ALL:
    //             this.destroy();
	// 		break;
	// 		// case CoreControl.SeqEmumType.CLIP_SELECT:
	// 		// if(this.clip.modelId === obj.data.modelId){
	// 		// 	this.updateSelect();
	// 		// }
	// 		// break;
    //     }
    // }
    // /**
	//  * 切段双击事件
	//  */
	// clipDBclickHandler = (data) => {
	// 	if(data == null) return;
	// 	if(data.modelId != this.clip.modelId) return;
	// }
	
	// /*
	//  * 双击事件
	//  */
	// rootdblclick(event){
	// 	App.activeSequence.selectClip(this.clip);
	// 	// this.deftool.show();
	// 	// CoreControl.tellClipdbclick(this.clip);
	// }
    
    // //播放头更新事件
	// playHeadTimerHandler = (obj) =>{
	// 	let headTime = obj.currentTime;
		
	// 	if(headTime >= this.clip.trackIn && headTime < this.clip.trackOut){
	// 		this.root.css('visibility','visible');
	// 	}else{
	// 		this.root.css('visibility','hidden');
	// 	}
    // }

    // /** 隐藏变形框 */
	// hiddenDefToolHandler = () =>{
	// 	// this.deftool.hide();
	// 	// this.childrenStatusDeftool(false);
    // }
    
    // /*
	//  * 更新编辑区高宽事件
	//  */
	// updateWHHandler = (obj) =>{
	// 	let WHObj = Utils.getWHof1080(App.activeSequence.preset.formatScale);
	// 	if(this.clip.width != WHObj.width || this.clip.height != WHObj.height){
	// 		this.clip.width = WHObj.width;
	// 		this.clip.height = WHObj.height;
	// 		this.MainAnimation.resizePreset(this.clip.width,this.clip.height);
	// 	}
	// 	// this.flushWHShow();
	// }

	// /** 撤销或重做完成后的事件 */
    // undoOrRedoFinishHandler = () =>{
	// 	// console.log('undoOrRedoFinishHandler',this.clip.contents)
	// 	// this.clip.needFlushRender = true;
	// 	// this.clipChange(this.clip);
	// 	if(this.MainAnimation){
			
	// 		this.flushRender();
	// 		// console.log('undoOrRedoFinishHandler22',this.MainAnimation.contents);
	// 	}
    // }
    
	// /*
	//  * 点击事件
	//  */
	// // clickHandler = (event) =>{
	// // 	App.activeSequence.selectClip(this.clip);
	// // 	this.deftool.show();
	// // 	this.childrenStatusDeftool(true);
    // // }
    // /*
	//  * 更新数据事件
	//  */
	// clipChange = (data) =>{
	// 	// if(data == null) return;
	// 	// if(data.modelId != this.clip.modelId) return;
	// 	this.clip = data;

	// 	if(this.MainAnimation){
	// 		let willDur = (this.clip.trackOut - this.clip.trackIn)*1000;
	// 		if(willDur != this.clip.duration){
	// 			this.clip.duration = (this.clip.trackOut - this.clip.trackIn)*1000;
	// 			this.MainAnimation.setDuration(this.clip.duration);
	// 			this.clip.inAnimDur = this.MainAnimation.inAnimDur;
	// 			this.clip.outAnimDur = this.MainAnimation.outAnimDur;
	// 			CORECONTROL.dynamicTextChange({cmd:'render-inout-change',clip:this.clip});
	// 		}
	// 		for(let i=0;i<this.clip.needLoadFontFamilys.length;i++){
	// 			let fontName = this.clip.needLoadFontFamilys[i];
	// 			this.checkFontAndLoad(fontName);
	// 		}
	// 		this.clip.needLoadFontFamilys = [];
	// 		this.pixiRender.updateTemplatePos(this.clip.modelId,this.clip.trackIn*1000,this.clip.trackOut*1000);
			
	// 		if(this.clip.needFlushRender){
	// 			this.flushRender();
	// 			this.clip.needFlushRender = false;
	// 		}
	// 	}
	// }
	
	/** 刷新渲染 */
	flushRender(){
		this.selfFlushRender();
		if(this.clip.templateType == 'normal'){
			for(let i=0;i<this.clip.contents.length;i++){
				let txt = this.clip.contents[i];
				this.MainAnimation.setContent(txt,i);
			}
		}else{
			this.MainAnimation.setContent(this.clip.contents);
		}
		
		this.MainAnimation.setConfig(this.clip.config);
		
	}

	/** 子对象继承 */
	selfFlushRender(){

	}
    
    // /*
	//  * 更新特技选择状态
	//  */
	// clipSelect = (clips) =>{
	// 	// if(clips.length == 0){
	// 	// 	this.deftool.hide();
	// 	// 	this.childrenStatusDeftool(false);
	// 	// }
	// 	// this.updateSelect(clips);
    // }
    
	/** 工具改变返回 */
    easyDefToolChangeBack(transform){
		this.MainAnimation.setTransfrom(transform);
	}
	/** 工具改变结束返回 */
	easyDefToolChangedBack(transform,isSame){
		if(!isSame){
			App.activeSequence.HistoryModel.record('动态字幕位置改变');
		}
	}
	
	/*
	 * 单击事件
	 */
	rootclick(event){
		App.activeSequence.selectClip(this.clip);
	}
	/*
	 * 双击事件
	 */
	rootdblclick(event){
		// CoreControl.tellClipdbclick(this.data);
		CORECONTROL.changeTab({type:'set'});
	}


    initEditConfig(){
		
		//通过当前的配置，初始化编目的默认值
		const configCloumns = this.clip.configCloumns;
		for(let info in configCloumns.styles){
			let style_items = configCloumns.styles[info];
			let config = this.clip.config.styles[info];
			for(let i=0;i<style_items.length;i++){
				let key_item = style_items[i];
				if(config[key_item.keyCode]){
					key_item.value = config[key_item.keyCode];
				}
			}
		}
		for(let i=0;i<configCloumns.systems.length;i++){
			let key_item = configCloumns.systems[i];
			if(this.clip.config.systems[key_item.keyCode]){
				key_item.value = this.clip.config.systems[key_item.keyCode];
			}
		}

		for(let i=0;i<configCloumns.animations.length;i++){
			let key_item = configCloumns.animations[i];
			if(this.clip.config.animations[key_item.keyCode]){
				key_item.value = this.clip.config.animations[key_item.keyCode];
			}
		}

        const styles = this.MainAnimation.config.styles;
        for(let info in styles){
            if(styles[info]['fontFamily']){
                this.checkFontAndLoad(styles[info]['fontFamily']);
            }
		}
		
		App.activeSequence.dynamicTextChange({cmd:'cloumns-init',clip:this.clip});
	}
	
	checkFontAndLoad(fontName){
		//查找当前字体
		const iframeDoc = document;
		if(!Utils.checkFont(fontName,iframeDoc)){
			Utils.loadFont(fontName).then(fontface=>{
				iframeDoc.fonts.add(fontface);
				console.log('字体加载完成',this.MainAnimation.ready)
				this.MainAnimation.ForcedUpdate();
				
			}).catch(err=>{
				console.log('字体加载错误',err)
			})
		}

	}

	
	configActiveChangeHandler(obj){
		if(obj.id == this.clip.modelId){
			App.activeSequence.dynamicTextChange({cmd:'render-config-active',clip:this.clip,config:obj.cfg});
		}
	}

    textAreaChangeHandler(obj){
        // console.log('textAreaChangeHandler',obj,JSON.stringify(obj.app.config.transform))
        // const transform = this.MainAnimation.config.transform;
		// this.easyDefTool.flushTransform(transform);
		if(obj.id == this.clip.modelId){
			const transform = obj.app.config.transform;
			this.easyDefTool.flushTransform(transform);
		}
	}
	
	dynamicChangeHandler =(obj)=>{
		if(obj.cmd.indexOf('render') == 0) return;
		if(this.clip.modelId == obj.clip.modelId){
			if(obj.cmd == 'property_in_dur_change'){
				this.MainAnimation.setInAnimDuration(obj.duration);
			}else if(obj.cmd == 'property_out_dur_change'){
				this.MainAnimation.setOutAnimDuration(obj.duration);
			}
		}
	}

    // /*
	//  * 删除自己
	//  */
	// destroy = () =>{
	// 	CoreControl.removeEventListener(CoreControl.DESTROY_SEQUENCE,this.destroySeqHandler);
	// 	CoreControl.removeEventListener(CoreControl.UPDATE_SEQUENCE,this.seqChangeHandler);
	// 	CoreControl.removeEventListener(CoreControl.CLIP_DBCLICK,this.clipDBclickHandler);
	// 	CoreControl.removeEventListener(CoreControl.PLAYHEADTIME_CHANGE,this.playHeadTimerHandler);
	// 	CoreControl.removeEventListener(CoreControl.HIDDEN_DEFTOOL,this.hiddenDefToolHandler);
	// 	LayoutControl.removeEventListener(LayoutControl.PREVIEW_CHANGED,this.updateWHHandler);
	// 	CoreControl.removeEventListener(CoreControl.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
	// 	CoreControl.removeEventListener(CoreControl.DYNAMIC_TEXT_CHANGE,this.dynamicChangeHandler.bind(this));
	// 	this.root.remove();
	// 	this.pixiRender.removeTemplate({id:this.clip.modelId});
	// 	this.selfDestroy();
	// }
	/**
     * 销毁
     */
    destroy(){
		// this.root.removeEventListener('click',this.clickHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_CHANGE,this.clipChangeHandler);
		CORECONTROL.$off(CORECONTROL.CLIP_DELETE,this.clipDeleteHandler);
		CORECONTROL.$off(CORECONTROL.CURRENTTIME_CHANGE,this.currentTimeHandler);
		CORECONTROL.$off(CORECONTROL.PRESET_PREVIEW_CHANGE,this.presetPreviewHandler);
		CORECONTROL.$off(CORECONTROL.CLEAR_TIMELINE,this.clearTimelineHandler);
		CORECONTROL.$off(CORECONTROL.DYNAMIC_TEXT_CHANGE,this.dynamicChangeHandler);
		CORECONTROL.$off(CORECONTROL.UNDOORREDO_FINISH,this.undoOrRedoFinishHandler);
		CORECONTROL.$off(CORECONTROL.SEQ_DURATION,this.seqDurationHandler);
		
		this.root.remove();
		this.pixiRender.removeTemplate({id:this.clip.modelId});
		this.selfDestroy();
    }

	selfDestroy(){

	}

}
export default TextDynamicRender;