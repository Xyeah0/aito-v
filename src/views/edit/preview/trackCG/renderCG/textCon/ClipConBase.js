import $ from 'jquery'

class TextConBaseClip{
	constructor(clip,conClip){
		this.clip = clip;//表示时间线顶层片断
		this.conClip = conClip;//容器内部片断

		this.root = $(`<div class="image-con-base"></div>`);
		let location = conClip.location;
		this.root.css({
			position:'absolute',
			left: location.x*100+'%',
			top: location.y*100+'%',
			width: location.width*100+'%',
			height: location.height*100+'%',
		})
	}

	/** 刷新数据 */
	updateData(clip){
		this.clip = clip;

		let finddata = null;
		for(let i=0;i<clip.conData.templateData.tracks.length;i++){
			let trackCon = clip.conData.templateData.tracks[i];
			for(let j=0;j<trackCon.clips.length;j++){
				let conClip = trackCon.clips[j];
				if(conClip.id === this.conClip.id){
					finddata = conClip;
					break;
				}
			}
			if(finddata) break;
		}
		if(finddata){
			this.conClip = finddata;
			this.updateDataConClip();
			// console.log(this.conClip.id,'updateDataupdateDataConClip')
		}
	}
	
	/** 子对象继承 */
	updateDataConClip(){
		this.root.css({
			position:'absolute',
			left: this.conClip.location.x*100+'%',
			top: this.conClip.location.y*100+'%',
			width: this.conClip.location.width*100+'%',
			height: this.conClip.location.height*100+'%',
		})
	}

	/** 刷新播放头 */
	flushCurrentTime(time){
		let sideTime = time - this.clip.startTime;
		if(sideTime >= this.conClip.trackIn && sideTime < this.conClip.trackOut){
			this.root.show();
		}else{
			this.root.hide();
			this.childrenStatusDeftool(false);
		}
	}

	childrenStatusDeftool(b){

	}
}

export default TextConBaseClip;