import $ from 'jquery'
class Loading{
	constructor(){
		this.root = $(`<div class="loading-box">
			<div class="close-btn">关闭</div>
			<div class="progress-box">
				<div class="progress"></div>
			</div>
		</div>`)

		this.progressBox = this.root.find('.progress-box');
		this.progress = this.root.find('.progress');
		this.closeBtn = this.root.find('.close-btn');

		this.root.css({
			'z-index': 500,
			'background-color': 'rgba(38, 38, 38, 0.5)',
			'width': '100%',
			'height': '100%',
			'position': 'absolute',
			'display': 'flex',
			'align-items': 'center',
			'padding': 10
		})

		this.progressBox.css({
			'width': '100%',
			'height': '20px',
			'background-color': '#cccccc',
			'border-radius': '50px',
			'overflow': 'hidden',
		});

		this.progress.css({
			'width': '0%',
			'height': '100%',
			'background-color': '#0096ff'
		})

		this.closeBtn.css({
			'position': 'absolute',
			'top': '5px',
			'right': '5px',
			'padding': '6px',
			'cursor':'pointer'
		});

		this.closeBtn.on('click',()=>{
			this.destroy();
		})
	}

	/** 传入0到1 */
	setProgress(v){
		this.progress.css('width',v*100+'%');
	}

	destroy(){
		this.root.remove();
	}
}

export default Loading;