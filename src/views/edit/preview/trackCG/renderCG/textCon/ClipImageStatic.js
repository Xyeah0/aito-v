import $ from 'jquery';
import Utils from '../../../../../../lib/Utils';
import ClipConBase from './ClipConBase';

class ClipImageStatic extends ClipConBase{
    constructor(clip,conClip){
		super(clip,conClip);
		this.root.addClass('image-static');
        
		this.eleImg = $('<img width="100%" height="100%" crossOrigin="*"/>');
		let rawData = this.clip.conData;
		let imgurl = Utils.getURLSplicing([rawData.httpHost,rawData.httpRelativePath,conClip.previewUrl]);
		this.eleImg.attr('src',imgurl);
		this.root.append(this.eleImg);
    }
}

export default ClipImageStatic;