import BaseRender from "../BaseRender";
import $ from "jquery";
import TextConTrack from "./TextConTrack";
import App from "../../../../../../model/App";
import Loading from "./Loading";
class TextConRender extends BaseRender{
	constructor(data){
		super(data);

		this.root.empty();
		this.root.addClass('clip_cg_text_con');
		// let location = data.location;
		// this.root.css({
		//     left: location.x*100+'%',
		//     top: location.y*100+'%',
		//     width: location.width*100+'%',
		//     height: location.height*100+'%',
		// })
		this.trackRenders = [];

		this.loadProgressObj = {};
		this.createElement();
		this.ele_load = new Loading();
		this.root.append(this.ele_load.root);
	}

	/** 生成元素 */
	createElement(){
		let conTrackData = this.data.conData.templateData.tracks;
		for(let i=0;i<conTrackData.length;i++){
			let track_data = conTrackData[i];
			let ele_track = new TextConTrack(this.data,track_data,this.loadProgress);
			this.trackRenders.push(ele_track);
			this.root.append(ele_track.root);
		}
	}

	/**loadProgress */
	loadProgress = (obj) =>{
		this.loadProgressObj[obj.id] = obj.progress;
		let per = 0;
		let count = 0;
		for(let info in this.loadProgressObj){
			per += this.loadProgressObj[info];
			count++;
		}
		if(count>0) per/=count;
		this.ele_load.setProgress(per);
		if(per> 0.999){
			this.ele_load.destroy();
		}
	}
	/** 加入到dom树中后的初始化，主要针对文字 */
	initPosition(){
		for(let i = 0;i<this.trackRenders.length;i++){
			this.trackRenders[i].initPosition();
		}
	}

	updateData(clip){
		for(let i = 0;i<this.trackRenders.length;i++){
			this.trackRenders[i].updateData(clip);
		}
	}

	updatePosition(){
		let currentTime = App.currentTime;
		for(let i=0;i<this.trackRenders.length;i++){
			this.trackRenders[i].flushCurrentTime(currentTime);
		}
	}

	childrenStatusDeftool(b){
		for(let i=0;i<this.trackRenders.length;i++){
			this.trackRenders[i].childrenStatusDeftool(b);
		}
	}


}
export default TextConRender;