import $ from 'jquery';
import ClipTextStatic from './ClipTextStatic';
import ClipImageSeq from './ClipImageSeq';
import ClipImageStatic from './ClipImageStatic';

class TextConTrack{
	constructor(clip,data,loadProgress){
		this.clip = clip;
		this.conTrack = data;
		this.loadProgress = loadProgress;

		this.root = $(`
			<div class="text-con-track"></div>
		`);

		this.root.css({
			'position': 'absolute',
			'left': 0,
			'top': 0,
			'width': '100%',
			'height': '100%',
			'pointer-event':'none',
			'z-index': 400-this.conTrack.zIndex
		});

		this.eleClips = [];
		this.loadProgressObj = {};
		this.imageSeqCount = 0;
		this.createClips();
	}

	/** 创建clip */
	createClips (){
		for(let i=0;i<this.conTrack.clips.length;i++){
			let conClip = this.conTrack.clips[i];
			let eleClip;
			if(conClip.dataType === 'cg_text'){
				eleClip = new ClipTextStatic(this.clip,conClip);
				this.root.append(eleClip.root);
			}else if(conClip.dataType === 'cg_image_seq'){
				eleClip = new ClipImageSeq(this.clip,conClip,this.loadProgressSeq);
				this.root.append(eleClip.root);
				this.imageSeqCount++;
				// this.loadProgressObj[conClip.id] = [];
			}else if(conClip.dataType === 'cg_image_static'){
				eleClip = new ClipImageStatic(this.clip,conClip);
				this.root.append(eleClip.root);
			}
			if(eleClip){
				this.eleClips.push(eleClip);
			}
		}
	}

	loadProgressSeq = (obj) =>{
		this.loadProgressObj[obj.id] = obj.progress;
		let per = 0;
		let count = 0;
		for(let info in this.loadProgressObj){
			per += this.loadProgressObj[info];
			count++;
		}
		if(this.imageSeqCount>0) per/=this.imageSeqCount;

		this.loadProgress({
			id: this.conTrack.id,
			progress: per
		})
	}
	/** 加入到dom树中后的初始化，主要针对文字 */
	initPosition(){
		for(let i = 0;i<this.eleClips.length;i++){
			let eleClip = this.eleClips[i];
			if(eleClip.conClip.dataType === 'cg_text'){
				eleClip.renderText();
			}
		}
	}

	/** 刷新播放头 */
	flushCurrentTime(time){
		for(let i=0;i<this.eleClips.length;i++){
			this.eleClips[i].flushCurrentTime(time);
		}
	}

	/** 刷新数据 */
	updateData(clip){
		this.clip = clip;
		for(let i = 0;i<this.eleClips.length;i++){
			this.eleClips[i].updateData(this.clip);
		}
	}
	
	childrenStatusDeftool(b){
		for(let i=0;i<this.eleClips.length;i++){
			this.eleClips[i].childrenStatusDeftool(b);
		}
	}
}

export default TextConTrack;