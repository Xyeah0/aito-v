import $ from 'jquery';

import ClipConBase from './ClipConBase';
import DefTool from '../../../../../../lib/DefTool';

class ClipTextStatic extends ClipConBase{
    constructor(clip,conClip){
		super(clip,conClip);
		this.root.addClass('text-static');

		this.text = $('<div></div>');
		this.root.append(this.text);

        this.text.css({
			display:'inline',
			position: "absolute",
			'word-break': 'break-all',
			'white-space': 'pre',
			// overflow: 'hidden',
			'height':'auto'
        });
        
		this.renderText();
		
		this.beforePosOfDEF = {x:0,y:0};

		this.deftool = new DefTool(this.root[0],this.deftoolChangeing,this.deftoolChanged,true);//最后一个值代表是否限制到父级范围
		this.deftool.setMinWH(18,18);
		// this.deftool.flushPosition({x:this.nowPos.x,y:this.nowPos.y,w:this.nowPos.width,h:this.nowPos.height});

		this.root.dblclick(this.rootdblclick);

		this.text[0].contentEditable = true;//'plaintext-only';
		this.deftool.setTextInput('title');

		this.text.on('blur',this.textInputChanged)
    }

	updateDataConClip(){
		super.updateDataConClip();
		this.renderText();
	}

	/** 重写刷新播放头 */
    flushCurrentTime(time){
		super.flushCurrentTime(time);
        let sideTime = time - this.clip.startTime;
        if(this.conClip.styles.isFadeIn && sideTime >= this.conClip.trackIn && sideTime <= this.conClip.trackIn+this.conClip.styles.fadeInSec){
			this.root.css({opacity:(sideTime-this.conClip.trackIn) / this.conClip.styles.fadeInSec});
		}else if(this.conClip.styles.isFadeOut && sideTime >= this.conClip.trackOut-this.conClip.styles.fadeOutSec){
			this.root.css({opacity:1-(sideTime-(this.conClip.trackOut-this.conClip.styles.fadeOutSec)) / this.conClip.styles.fadeOutSec});
		}else{
			this.root.css({opacity:1});
		}
    }

	/*
	 * 双击事件
	 */
	rootdblclick = (event) =>{
		event.stopPropagation();

		this.renderText();
		// let parentNode = this.root.parent();
		// if(parentNode.length){
		// 	let nowPos = {
		// 		x: this.conClip.location.x * parentNode.width(),
		// 		y: this.conClip.location.y * parentNode.height(),
		// 		w: this.conClip.location.width * parentNode.width(),
		// 		h: this.conClip.location.height * parentNode.height()
		// 	}
		// 	this.deftool.flushPosition(nowPos);
		// 	console.log(nowPos)
		// }
		this.deftool.show();
		
	}

	childrenStatusDeftool(b){
        this.deftool.hide();
    }
	/*
	 * 变形工具返回事件
	 */
	deftoolChangeing = (pos) =>{
		// this.data.x = parseInt(this.root.css("left"));
		// this.data.y = parseInt(this.root.css("top"));
		// this.data.width = this.root.width();
		// this.data.height = this.root.height();
		// App.activeSequence.updateClip(this.data);
		// console.log(this.root.width(),this.root.height());
		//这里暂时不处理
	}
	/*
	 * 变形工具返回事件2
	 */
	deftoolChanged = (pos) =>{
		// console.log('变形工具返回事件2',pos)
		//只要有一个不相等，就证明变化了位置
		// if(pos.x !== this.beforePosOfDEF.x || pos.y !== this.beforePosOfDEF.y){
		// 	this.beforePosOfDEF.x = pos.x;
		// 	this.beforePosOfDEF.y = pos.y;
			
		// }

		let parentNode = this.root.parent();
		this.conClip.location.x = pos.x / parentNode.width();
		this.conClip.location.y = pos.y / parentNode.height();
		this.renderText();
	}

	//输入改变完成
    textInputChanged = () =>{
		// console.log("渲染框内输入改变完成",HistoryModel)
		// window.testTextarea = this.text;
		let str = this.text[0].innerText;//this.text.html();
		this.conClip.content = str;
		//如果最后一个字符是回车，干之
		if(str.length && str.lastIndexOf('\n') === str.length-1){
			this.conClip.content = str.substring(0,str.length-1);
			// App.activeSequence.updateClip(this.data);
		}
		// console.log(this.conClip.content)
		this.text.scrollTop(0);
		this.renderText();
        // HistoryModel.record("文字内容改变");
	}

    renderText(){
		let data = this.conClip;
        
        this.text.html(data.content);
        
        let styles = data.styles;
		// this.bg.css({
		// 	"background-color":styles.bgColor,
		// 	"opacity":styles.bgAlpha
		// });
		
		let fontsize = styles.fontSize/100*App.editHeight;
		let transformScale = 1;
		if(fontsize < 12){
			transformScale = fontsize/12;
			fontsize = 12;
		}
		let styleObj = {
			color:styles.color,
			opacity:styles.alpha,
			"font-size":fontsize+'px',
			"font-family":styles.fontFamily,
			"text-align": styles.textAlign,
			"font-weight": styles.isBold?"bold":"normal",
			"text-decoration": styles.isUnderline?"underline":"none",
			"font-style": styles.isItalic?"italic":"normal",
			"text-shadow":styles.isShadow?(styles.shadowColor+' 2px 2px 2px'):'initial',
			'transform':"scale("+transformScale+")",
			'transform-origin':"0 0 0",
			'margin':0,
			'line-height':fontsize+'px',
		}
		if(styles.fontFamily === 'Microsoft YaHei'){
			styleObj["line-height"] = fontsize*1.234+'px';
		}
		this.text.css(styleObj);
		// let textW=this.text.width(),textH=this.text.height();
		// let w = renderW*transformScale || this.data.rawWidth;
		// let h = renderH*transformScale || this.data.rawHeight;
		// // console.log('height::::',w,h,this.data.rawWidth,this.data.rawHeight)
		
		// this.text.css({
		// 	width:renderW,
		// 	height:renderH
		// });
		// console.log('transformScale',transformScale)

		this.resizeParentWH(transformScale);
	}

	/** 根据文字高宽，重置父级高宽 */
	resizeParentWH(transformScale = 1){
		let rootW=this.root.width(),rootH=this.root.height(),textW=this.text.width(),textH=this.text.height();
		let parentNode = this.root.parent();
		if(parentNode.length && textW > 0){
			let parentW = parentNode.width(),parentH = parentNode.height();
			// console.log(transformScale,parentW,textW,rootW)
			if(rootW !== textW || rootH !== textH){
				this.conClip.location.width = textW*transformScale/parentW;
				this.conClip.location.height = textH*transformScale/parentH;
				// this.root.css({
				// 	width: textW,
				// 	height: textH
				// })
				// console.log('this.conClip.location.width',this.conClip)
				let nowPos = {
					x: this.conClip.location.x * parentNode.width(),
					y: this.conClip.location.y * parentNode.height(),
					w: textW*transformScale,
					h: textH*transformScale
				}
				this.deftool.flushPosition(nowPos);
			}
		}
	}
}

export default ClipTextStatic;