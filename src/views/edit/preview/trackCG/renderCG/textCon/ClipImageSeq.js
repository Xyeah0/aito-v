import $ from 'jquery';
import Utils from '../../../../../../lib/Utils';
import ClipConBase from './ClipConBase';

class ClipImageSeq extends ClipConBase{
    constructor(clip,conClip,loadProgress){
		super(clip,conClip);
		this.root.addClass('image-seq');
		
		this.loadProgress = loadProgress;
        this.imageEleList = [];
        
        this.createEleList();
    }

	/** 创建图片例表 */
    createEleList(){
		this.loadCount = 0;
		let rawData = this.clip.conData;
		let frameRate = this.conClip.frameRate || 25;
		let stepSec = 1/frameRate;
		let st = 0;
		for(let i=0;i<this.conClip.prevSeqList.length;i++){
			let _url = this.conClip.prevSeqList[i];
			let imageItem = {
				inPoint: st,
				outPoint: st+stepSec,
				prevUrl: Utils.getURLSplicing([rawData.httpHost,rawData.httpRelativePath,_url]),
				eleImg: $('<img width="100%" height="100%" crossOrigin="*"/>')
			}
			st += stepSec;
			imageItem.eleImg.attr('src',imageItem.prevUrl);
			this.root.append(imageItem.eleImg);
			this.imageEleList.push(imageItem);

			imageItem.eleImg.on('load',this.loadCountHandler);
		}
	}

	/**loadCountHandler */
	loadCountHandler = () =>{
		this.loadCount++;
		this.loadProgress({
			id: this.conClip.id,
			loadedCount: this.loadCount,
			sunCount: this.conClip.prevSeqList.length,
			progress: this.loadCount/this.conClip.prevSeqList.length
		})
		// console.log('this.loadCount',this.loadCount,this.loadCount/this.conClip.prevSeqList.length)
	}

	/** 刷新播放头 */
    flushCurrentTime(time){
        let sideTime = time - this.clip.startTime;
        if(sideTime >= this.conClip.trackIn && sideTime < this.conClip.trackOut){
			this.root.show();
			let sideTime2 = sideTime-this.conClip.trackIn;
			// let stepSec = 1/(this.data.frameRate || 25);
			let sunLen = this.conClip.prevSeqList.length/(this.conClip.frameRate || 25);
			// console.log('刷新播放头',sunLen)
			sideTime2 = sideTime2%sunLen;
			for(let i=0;i<this.imageEleList.length;i++){
				let imageEle = this.imageEleList[i];
				if(sideTime2 >= imageEle.inPoint && sideTime2 < imageEle.outPoint){
					imageEle.eleImg.show();
				}else{
					imageEle.eleImg.hide();
				}
			}
        }else{
            this.root.hide();
		}
    }
}

export default ClipImageSeq;