import Event from '../../../../../../libs/Event';
class Tga extends Event {
	constructor(_url) {
		super();
		this.url = _url;
		this.host = this.url.substring(0, this.url.lastIndexOf('/') + 1);
		this.tgaW = 0;
		this.tgaH = 0;
		this.tgaArr = [];
		this.imgArr = [];

		this.loadTgaSeq(this.url).then((data) => {
			// console.log('加载大要要枯！',data)
			// this.imageHandler(data);
			this.tgaW = data.tgaW;
			this.tgaH = data.tgaH;
			this.tgaArr = data.tgaArr;

			if (this.tgaArr.length == 0) {
				console.log('tga文件错误，不包含图片地址', this.url, this.host);
				return;
			}
			// let index = 0,len = this.tgaArr.length;
			this.$emit('startload', this);

			let firstImg = null, firstUrl = null;
			for (let i = 0; i < this.tgaArr.length; i++) {
				let img = new Image();
				img.style.width = '100%';
				img.style.height = '100%';
				this.imgArr.push(img);
				if (i == 0) {
					firstImg = img;
					firstUrl = this.tgaArr[i];
				}
			}
			firstImg.onload = () => {
				this.$emit('progress', this);
				this.$emit('first');
				this.imageHandlerOther();
			}
			firstImg.onerror = () => {
				this.$emit('progress', this);
				this.$emit('first');
				this.imageHandlerOther();
			}
			firstImg.src = this.host + firstUrl;

		}).catch(error => {
			console.log('加载出错！')
		});
	}

	/** 处理后续图片 */
	imageHandlerOther() {
		for (let i = 1; i < this.tgaArr.length; i++) {
			let item = this.tgaArr[i];
			let img = this.imgArr[i];
			img.onload = () => {
				this.$emit('progress', this);
			}
			img.onerror = () => {
				this.$emit('progress', this);
			}
			img.src = this.host + item;
		}
		// //先加载第一张
		// let firstItem = this.tgaArr[0];
		// let firstImg = this.loadImage(this.host + firstItem,()=>{
		//     index++;
		//     this.$emit('progress',{
		//         tga:this,
		//         index: index,
		//         len:len
		//     });
		//     this.$emit('first');
		// },()=>{
		//     index++;
		//     this.$emit('progress',{
		//         tga:this,
		//         index: index,
		//         len:len
		//     });
		// })
		// this.tgaArr.forEach((item,_index) =>{
		//     let img = new Image();
		//     img.onload = ()=>{
		//         index++;
		//         this.$emit('progress',{
		//             tga:this,
		//             index: index,
		//             len:len
		//         });
		//         if(_index == 0){
		//             this.$emit('first');
		//         }
		//     }
		//     img.onerror=()=>{
		//         index++;
		//         this.$emit('progress',{
		//             tga:this,
		//             index: index,
		//             len:len
		//         });
		//     }
		//     img.src = this.host + item;
		//     img.style.width = '100%';
		//     img.style.height = '100%';
		//     this.imgArr.push(img);
		// })
	}

	/** 加载图片 */
	loadImage(url, onload, onerror) {
		let img = new Image();
		img.onload = () => {
			onload();
		}
		img.onerror = () => {
			onerror();
		}
		img.src = url;
		img.style.width = '100%';
		img.style.height = '100%';
		return img;
	}

	/** 加载tgaSeq */
	loadTgaSeq(url) {

		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();

			xhr.responseType = 'text';
			// 一旦获取完成，对音频进行进一步操作，比如解码
			xhr.onreadystatechange = () => {
				// let audioContext = new AudioContext() || new WebkitAudioContext() || new mozAudioContext() || new msAudioContext();
				if (xhr.readyState === 4) {
					if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
						resolve(this.analysisTgaFile(xhr.response));
					} else {
						reject(null);
					}
				}
			}

			xhr.open('GET', url, true);
			xhr.send();
		})

	}

	analysisTgaFile = (info) => {
		if (info.indexOf('#EXTM3U') == 0) {

			let w = this.getTagIndex('#EXT-X-MEDIA-WIDTH', info).split(':');
			let tgaW = parseInt(w[1]);

			let h = this.getTagIndex('#EXT-X-MEDIA-HEIGHT', info).split(':');
			let tgaH = parseInt(h[1]);

			let s = info.indexOf('#EXTINF');
			let e = info.indexOf('#EXT-X-ENDLIST');

			//console.log('s=',s,'/e=',e)
			let a = info.substring(s, e).split('\n');  //arr.slice(s,e);
			//console.log('a=',a)
			const tgaArr = a.filter((ele, index) => {
				// return (index+1)%2==0;
				return ele.indexOf('.png') > -1 || ele.indexOf('.jpg') > -1
			});
			//console.log('tgaArr=',this.tgaArr)

			return {
				tgaW: tgaW,
				tgaH: tgaH,
				tgaArr: tgaArr
			};
		}
		else {
			return null;
		}
	}

	getTagIndex = (tag, info) => {
		if (info.indexOf(tag) != -1) {
			let s = info.indexOf(tag);
			let e = info.indexOf('\n', s);
			return info.substring(s, e);
		}
		else {
			return '';
		}
	}

}

export default Tga;