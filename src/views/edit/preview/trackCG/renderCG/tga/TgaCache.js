import Tga from "./Tga";
import App from 'AppCore';

class TgaCache{
	constructor(){
		this.caches = new Array();

		this.tgaImgSum = 0;
		this.tgaCurNum = 0;
	}


	/**
	 * 获得一个缓存
	 */
	getOneCache(tgaUrl){
		// if(!url) return null;
		for(let i=0;i<this.caches.length;i++){
			if(this.caches[i].url === tgaUrl){
				return this.caches[i];
			}
		}
		// console.log('未获得一个缓存',url)
		let cache = new Tga(tgaUrl);
		this.caches.push(cache);
		cache.$on('startload',this.startLoadImgHandler);
		cache.$on('progress',this.progressHandler);
		return cache;
	}

	/** 开始加载Img */
	startLoadImgHandler =(tga)=>{
		// console.log('开始加载tga',tga)
		let isFisrt = this.tgaImgSum == 0;
		this.tgaImgSum += tga.tgaArr.length;
		if(isFisrt && this.tgaImgSum >0){
			// console.log('发送开始进度事件')
			App.CONTROL.CORE.$emit(App.CONTROL.CORE.TGA_LOAD_START);
		}
	}

	/** 加载进度 */
	progressHandler =(obj)=>{
		this.tgaCurNum += 1;//obj.index;
		let progress = this.tgaImgSum>0?this.tgaCurNum/this.tgaImgSum:0;
		App.CONTROL.CORE.$emit(App.CONTROL.CORE.TGA_LOAD_PROGRESS,progress);
		if(progress >= 1){
			App.CONTROL.CORE.$emit(App.CONTROL.CORE.TGA_LOAD_COMPLETE);
		}
		// console.log('发送进度事件',progress,this.tgaCurNum,this.tgaImgSum)
		// console.log('tga加载进度',obj.tga.url,obj.index/obj.len)
	}

}

export default new TgaCache;