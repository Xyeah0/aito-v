import BaseRender from "./BaseRender.js";
import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
class TextRender extends BaseRender{
	constructor(obj){
		super(obj);
		
		this.text = document.createElement('div');
		this.text.style.display = 'inline';
		this.text.style.position = 'absolute';
		this.text.style.wordBreak = 'break-all';
		this.text.style.whiteSpace = 'pre';
		this.text.style.height = 'auto';
		this.text.style.pointerEvents = 'all';
		this.text.style.zIndex = '1';

		this.drag = document.createElement('div');
		this.drag.style.position = 'absolute';
		this.drag.style.width = 'calc(100% + 10px)';
		this.drag.style.height = 'calc(100% + 10px)';
		this.drag.style.left = '-5px';
		this.drag.style.top = '-5px';
		this.drag.style.background = 'rgba(255,255,255,0.3)';
		this.drag.style.border = '1px dashed #ffffff';
		this.drag.style.cursor = 'move';
		this.drag.style.pointerEvents = 'all';
		this.drag.style.zIndex = '2';
		// this.drag.classList.add('drag-text');
		this.root.style.outline = 'none';
		

		
		
		this.root.appendChild(this.text);
		this.root.appendChild(this.drag);
		
		this.root.tabIndex = '2';
		

		this.text.addEventListener('input',this.textInputHandler);
		// this.text.on('change',this.textInputChanged); 
		this.text.addEventListener('blur',this.textInputChanged)
		

		this.root.addEventListener('dblclick',this.dblclickHandler);
		this.drag.addEventListener('mousedown',this.mousedownHandler);
		this.root.addEventListener('keydown',this.keydownHandler)
		this.render();

		this.isEdit = false;
	}

	


	/** 鼠标按下事件 */
	mousedownHandler = (event) =>{
		let e = event ? event : window.event;
		e.preventDefault();
		e.stopPropagation();

		document.addEventListener('mousemove',mousemoveHandler);
		document.addEventListener('mouseup',mouseupHandler);

		let self = this;
		let recordX =  this.clip.pos.x;
		let recordY =  this.clip.pos.y;
		let recordMX = e.clientX;
		let recordMY = e.clientY;

		function mousemoveHandler(event){
			event.preventDefault();
			event.stopPropagation();

			let addW = event.clientX-recordMX;
			let addH = event.clientY-recordMY;
			let nowX = recordX+addW/App.activeSequence.presetPreview.width;
			let nowY = recordY+addH/App.activeSequence.presetPreview.height;
			nowX = Math.max(0,Math.min(nowX,1-self.clip.pos.width));
			nowY = Math.max(0,Math.min(nowY,1-self.clip.pos.height));
			self.clip.pos.x = nowX;
			self.clip.pos.y = nowY;
			self.root.style.left = nowX*100+'%';
			self.root.style.top = nowY*100+'%';

			App.activeSequence.changeClip(self.clip,false);
		}

		function mouseupHandler(event){
			event.preventDefault();
			event.stopPropagation();

			document.removeEventListener('mousemove',mousemoveHandler);
			document.removeEventListener('mouseup',mouseupHandler);
		}
	}

	/** 键盘按下事件 */
	keydownHandler = (event) =>{
		if(!this.clip.isSelected) return;
		if(this.isEdit) return;
		if(document.activeElement.className != 'clip_cg_text') return;
		event.preventDefault();
		event.stopPropagation();
		let add = event.shiftKey?0.01:0.001;
		if(event.keyCode === 38){
			this.clip.pos.y = Math.max(0,this.clip.pos.y-add);
		}else if(event.keyCode === 40){
			this.clip.pos.y = Math.min(1-this.clip.pos.height,this.clip.pos.y+add);
		}else if(event.keyCode === 37){
			this.clip.pos.x = Math.max(0,this.clip.pos.x-add);
		}else if(event.keyCode === 39){
			this.clip.pos.x = Math.min(1-this.clip.pos.width,this.clip.pos.x+add);
		}
		super.render();
		// if(document.activeElement.className == 'preview'){
		// 	if(event.keyCode === 32){
		// 		if(App.activeSequence.isPreview){
		// 			this.pauseHandler();
		// 		}else{
		// 			this.playHandler();
		// 		}
		// 	}else if(event.keyCode === 37){
		// 		this.preFrameHandler();
		// 	}else if(event.keyCode === 39){
		// 		this.nextFrameHandler();
		// 	}
		// }
	}

	/**重写选中文本事件 */
	selectClip(){
		// console.log(document.activeElement,this.isEdit)
		if(this.isEdit){
			return;
		}
		super.selectClip();
	}

	/** dblclickHandler */
	dblclickHandler = (event) =>{
		// event.stopPropagation();
		this.drag.style.display = 'none';
		// App.activeSequence.selectClip(null);
		this.isEdit = true;
		this.text.contentEditable = true;
		this.text.focus();
	}

	textInputHandler = () =>{
        // console.log(this.textInput.val());
		this.clip.content = this.text.innerText;//this.text.html();
		// console.log("textInputHandler",this.text.innerText)
		//div输入需要在完成时更新，否则会失去焦点
		// App.activeSequence.updateClip(this.data);
		App.activeSequence.changeClipTextContent(this.clip);
		
    }

    //输入改变完成
    textInputChanged = () =>{
		console.log("渲染框内输入改变完成")
		// window.testTextarea = this.text;
		let str = this.text.innerText;//this.text.html();
		//如果最后一个字符是回车，干之
		if(str.length && str.lastIndexOf('\n') === str.length-1){
			this.clip.content = str.substring(0,str.length-1);
			// App.activeSequence.changeClip(this.clip,false);
		}
		// this.text.scrollTop(0);
		this.render();
		this.isEdit = false;
		this.text.contentEditable = false;
		App.activeSequence.changeClipTextContent(this.clip);
	}

	/**渲染 */
	render(){
		super.render();
		// this.root.style.width = 'auto';
		// this.root.style.height = 'auto';
		if(this.clip.isSelected && this.clip.content.length){
			this.drag.style.display = 'block';
		}else{
			this.drag.style.display = 'none';
			this.text.blur();
		}
		// if(this.clip.isSelected){
		// 	this.root.focus();
		// }

		this.text.innerHTML = this.clip.content;
		let styles = this.clip.styles;
		let fontsize = styles.fontSize/100*App.activeSequence.presetPreview.height;
		// console.log('文本渲染低级',styles)
		let transformScale = 1;
		if(fontsize < 12){
			transformScale = fontsize/12;
			fontsize = 12;
		}
		this.text.style.color = styles.color;
		this.text.style.opacity = styles.alpha;
		this.text.style.fontSize = fontsize+'px';
		this.text.style.fontFamily = styles.fontFamily;
		this.text.style.textAlign = styles.textAlign;
		this.text.style.fontWeight = styles.isBold?"bold":"normal";
		this.text.style.textDecoration = styles.isUnderline?"underline":"none";
		this.text.style.fontStyle = styles.isItalic?"italic":"normal";
		this.text.style.textShadow = styles.isShadow?(styles.shadowColor+' 2px 2px 2px'):'initial';
		this.text.style.transform = "scale("+transformScale+")";
		this.text.style.transformOrigin = "0 0 0";
		this.text.style.lineHeight = fontsize+'px';
		if(styles.fontFamily === 'Microsoft YaHei'){
			this.text.style.lineHeight = fontsize*1.234+'px';
		}
		if(this.text.offsetWidth > 0 && this.text.offsetHeight>0){
			this.clip.pos.width = this.text.offsetWidth*transformScale/App.activeSequence.presetPreview.width;
			this.clip.pos.height = this.text.offsetHeight*transformScale/App.activeSequence.presetPreview.height;

			this.root.style.width = this.clip.pos.width*100+'%';
			this.root.style.height = this.clip.pos.height*100+'%';
		}
		
		// console.log(this.text.offsetWidth,this.text.offsetHeight)
	}

	/**
     * 销毁
     */
    destroy(){
		super.destroy();
		this.drag.removeEventListener('mousedown',this.mousedownHandler);
		this.text.removeEventListener('input',this.textInputHandler);
		this.text.removeEventListener('blur',this.textInputChanged)
		this.root.removeEventListener('dblclick',this.dblclickHandler);
    }
}
export default TextRender;