
import ImageRender from "./ImageRender.js";
import DecodeGIF from '../../../../../libs/DecodeGIF.js';
import App from "../../../../../model/App.js";

class GIFRender extends ImageRender{
	constructor(obj){
		super(obj);
		this.root.empty();
		
		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d');
		this.canvas.style.width = '100%';
		this.canvas.style.height = '100%';
		this.root.append(this.canvas);

		this.frames = [];
		this.currentOrder = -1;
		this.totalTime = 0;
		
		this.loadGif();
	}

	/** 加载gif */
	loadGif(){
		let xhr = new XMLHttpRequest(); 
        xhr.open('GET', this.clip.previewUrl, true); 
        xhr.responseType = 'arraybuffer'; 
        // 一旦获取完成，对音频进行进一步操作，比如解码
        xhr.onreadystatechange = () => {
            // let audioContext = new AudioContext() || new WebkitAudioContext() || new mozAudioContext() || new msAudioContext();
            if(xhr.readyState === 4){
                if((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304){
                    let arrayBuffer = xhr.response;
					this.decodeGif = new DecodeGIF(arrayBuffer);
					// this.decodeGif.parser(arrayBuffer);
					// console.log(this.decodeGif.raw)
					let frames = this.decodeGif.decompressFrames(true);
					this.createGIFFrame(frames);
                }else{
                    
                }
            }
        }

        
        xhr.send();
	}

	/** 生成GIF关键帧 */
	createGIFFrame(frames){
		// console.log(frames)
		let st = 0;
		this.frames = [];
		for(let i=0;i<frames.length;i++){
			let frame = frames[i];
			let delay = frame.delay/1000;
			let imgData = this.ctx.createImageData(frame.dims.width,frame.dims.height);
			imgData.data.set(frame.patch);
			let newFrame = {
				order: i,
				inPoint: st,
				outPoint: st + delay,
				dims: frame.dims,
				imgData: imgData
			}
			this.frames.push(newFrame);
			st += delay;
		}
		this.totalTime = st;
		this.updatePosition();
		
		if(this.totalTime > 0 && this.clip.gifTotalTime != this.totalTime){
			this.clip.endTime = this.clip.trackIn + this.totalTime;
			this.clip.totalTime = this.totalTime;
			this.clip.playCount = 1;//播放次数
			this.clip.totalFrames = this.frames.length;//总帧数
			this.clip.gifTotalTime = this.totalTime;//gif总时长

			if(this.clip.keyFrameUrl && this.clip.keyFrameUrl.indexOf('.gif') > -1){
				//生成一张缩略图
				let frame = this.frames[0];
				this.canvas.width = frame.dims.width;
				this.canvas.height = frame.dims.height;
				this.ctx.putImageData(frame.imgData,0,0);
				this.clip.keyFrameUrl = this.canvas.toDataURL();
			}
			App.activeSequence.changeClip(this.clip);
		}
	}


	updatePosition(){
		if(!this.frames) return;
		let time = App.activeSequence.currentTime - this.clip.trackIn;
		time = time%this.totalTime;
		let findFrame=null;
		for(let i=0;i<this.frames.length;i++){
			let frame = this.frames[i];
			if(frame.inPoint >= time && time < frame.outPoint){
				findFrame = frame;
				break;
			}
		}
		if(findFrame && this.currentOrder !== findFrame.order){
			this.currentOrder = findFrame.order;
			this.canvas.width = findFrame.dims.width;
			this.canvas.height = findFrame.dims.height;
			this.ctx.putImageData(findFrame.imgData,0,0);
		}
	}

	render(){
		super.render();
		this.updatePosition();
	}

}
export default GIFRender;