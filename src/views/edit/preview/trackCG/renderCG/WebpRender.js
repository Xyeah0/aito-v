
import ImageRender from "./ImageRender.js";
import App from "../../../../../model/App.js";
import WebpCache from "../../../../../model/WebpCache.js";

class WebpRender extends ImageRender{
	constructor(obj){
		super(obj);
		this.root.empty();
		
		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d');
		this.canvas.style.width = '100%';
		this.canvas.style.height = '100%';
		this.root.append(this.canvas);

		this.currentOrder = -1;
		this.wepbData = null;
		console.log('this.clip.previewUrl11111',this.clip.previewUrl)
		WebpCache.getOneCache(this.clip.previewUrl)
		.then(webdata=>{
			this.wepbData = webdata.webpFrames;
			this.initWebp();
		}).catch(err=>{
			console.log(err)
		})
	}

	/** 初始化 */
	initWebp(){
		console.log('this.wepbData',this.wepbData)
		this.clip.endTime = this.clip.trackIn + this.wepbData.duration;
		this.clip.totalTime = this.wepbData.duration;
		this.clip.playCount = 1;
		this.clip.totalFrames = this.wepbData.frameCount;
		this.clip.webpTotalTime = this.wepbData.duration;//webp总时长
		App.activeSequence.changeClip(this.clip,true,false);
		this.canvas.width = this.wepbData.width;
		this.canvas.height = this.wepbData.height;

		this.currentOrder = -1;
		this.render();
	}


	updatePosition(){
		if(!this.wepbData) return;
		let time = App.activeSequence.currentTime - this.clip.trackIn;
		time = time%this.clip.webpTotalTime;
		let findFrame=null;
		for(let i=0;i<this.wepbData.frames.length;i++){
			let frame = this.wepbData.frames[i];
			if(frame.inPoint >= time && time < frame.outPoint){
				findFrame = frame;
				break;
			}
		}
		if(findFrame && this.currentOrder !== findFrame.order){
			this.currentOrder = findFrame.order;
			// this.canvas.width = findFrame.dims.width;
			// this.canvas.height = findFrame.dims.height;
			this.ctx.putImageData(findFrame.rgba,0,0);
		}
	}


	render(){
		super.render();
		this.updatePosition();
	}

}
export default WebpRender;