class Utils{
	/**判断有无替换 */
	noReplace(str,key){
		return (str.substring(0,2) == '{{' && str.substring(str.length-2) == '}}' && str.substring(2,str.length-2) == key)
	}

	/** 获得主题系列颜色  */
	getCategoryColors(){
		//当前主题色
		return [{"label":"主题色0","value":"0","categoryColors":["#5AAEF3","#62D9AD","#5B6E96","#a8dffa","#ffdc4c","#FF974C","#E65A56","#6D61E4","#4A6FE2","#6D9AE7","#23C2DB","#D4EC59","#FFE88E","#FEB64D","#FB6E6C"]},{"label":"主题色1","value":"1","categoryColors":["#4150d8","#28bf7e","#ed7c2f","#f2a93b","#f9cf36","#4a5bdc","#4cd698","#f4914e","#fcb75b","#ffe180","#b6c2ff","#96edc1","#ffbe92","#ffd6ae","#ffecb6"]},{"label":"主题色2","value":"2","categoryColors":["#00aeef","#c759a1","#5552a3","#00bef3","#d07cb3","#6a65ad","#6ecff6","#dda6cc","#908ac2","#afe2fa","#e7c4dd","#b5b1d8","#d2eefc","#f0ddec","#d5d2ea"]},{"label":"主题色3","value":"3","categoryColors":["#1F358B","#0064AA","#00A472","#A9C8E9","#D2EAE2","#B5BFD9","#543C96","#6F5BA5","#719F3B","#9D8FC3","#85C73D","#4A90E2","#6C95A9","#61799F","#05753F"]},{"label":"主题色4","value":"4","categoryColors":["#710C19","#B12E48","#ECBD2B","#CD5532","#B1D14A","#ECBBCB","#E47894","#EC8C72","#F0BE79","#D2E390","#F5B7C2","#F3AAB5","#FCEA8C","#65BE92","#8BCBA7"]},{"label":"主题色5","value":"5","categoryColors":["#D75C29","#F3A502","#6AA006","#2A6DB9","#87409C","#DD4622","#D05881","#9B4172","#696969","#14539A","#DF2F2F","#BB8700","#7F3912","#4A90E2","#9B4172"]},{"label":"主题色6","value":"6","categoryColors":["#ED0F64","#F48897","#008ED2","#C3DCF2","#0064AA","#6E6CC4","#ACAAE3","#A865CF","#C398DD","#BD4B98","#DD6A29","#DFA676","#EEE124","#F9F39D","#89D924"]},{"label":"主题色7","value":"7","categoryColors":["#88071A","#1E6392","#003257","#4092C8","#B81F37","#74C9E2","#F7B952","#EC6200","#6A451D","#006C30","#A28F2B","#A34193","#7C48BC","#0E0575","#4461A2"]},{"label":"主题色8","value":"8","categoryColors":["#2E3E96","#9A4B99","#2F3452","#75250D","#7C98AC","#B5BFD9","#077388","#5B8EA0","#88071A","#885907","#A6A167","#5D7249","#456890","#635E7D","#812267"]},{"label":"主题色9","value":"9","categoryColors":["#6674C4","#FF95D2","#FACF5B","#3DC0E4","#4C6471","#1B95E6","#0DCB74","#E57B88","#F4B246","#A77C56","#AF65BE","#8368D9","#1C436F","#6BBEAC","#9DCA6C"]},{"label":"主题色10","value":"10","categoryColors":["#EE793D","#D66329","#C08D0B","#927E63","#765F3D","#504533","#231815","#844632","#A9441F","#E69732","#EECB49","#B6AA5C","#A9C441","#71AE5D","#3D9837"]},{"label":"主题色11","value":"11","categoryColors":["#B7DFCB","#5ABAD1","#3984B6","#264992","#161F63","#E0EFCF","#95D3D4","#469CC5","#3D81B7","#3369A8","#461364","#692293","#7F2CA0","#A13B9B","#C04B97"]},{"label":"主题色12","value":"12","categoryColors":["#056FFD","#C58DFF","#FF87AC","#FFE05D","#9EFCC8","#76ACFF","#C17BE5","#03E9FC","#FFA587","#F26D7D","#E57EF0","#868BE7","#6FD87F","#CEB64B","#F7A863"]},{"label":"主题色13","value":"13","categoryColors":["#218CE3","#FEE63D","#FE4B3E","#6CCEFE","#0439FF","#87F5FB","#CEC3C1","#335C67","#FFF3B0","#DF9F3D","#9E2A2B","#883677","#CA62C3","#EE85B5","#FF958C"]},{"label":"主题色14","value":"14","categoryColors":["#FBBA72","#CA5310","#BA4D02","#8F250C","#691D08","#2076AE","#58B8FF","#FBB13C","#FBB13C","#FE6847","#F3E309","#EDBA0B","#A31621","#BFDBF7","#083C5F"]},{"label":"主题色15","value":"15","categoryColors":["#7C8CDE","#C0E6DE","#D7E0E9","#B3C5D7","#C5D5EA","#272727","#D4AA7D","#EFD09E","#D2D8B3","#3F88C5","#1C3144","#CF0302","#FFBA09","#726953","#5B3001"]},{"label":"主题色16","value":"16","categoryColors":["#153243","#274B63","#9BC4BB","#537A5A","#66666E","#537A5A","#8DB580","#C2CFB2","#DDD0C6","#593D3B","#8A7968","#A18973","#C3A995","#9B1C1F","#C9CBA3"]},{"label":"主题色17","value":"17","categoryColors":["#191616","#E6AF2E","#3D348B","#BEB7A4","#5DD39E","#BCE784","#338AA7","#513B56","#577399","#BDD5EA","#FE5F55","#420139","#8F754F","#C5C5C5","#D8483F"]}]
	}

	/** 获取一个颜色的浅色值 */
	getUndertint(col,amt){
		let usePound = false;
		if (col[0] == "#") {
			col = col.slice(1);
			usePound = true;
		}
		amt = Math.min(255,Math.max(-255,amt));
		let num = parseInt(col,16);
		let r = (num >> 16) + amt;
		if (r > 255) r = 255;
		else if (r < 0) r = 0;
		
		let g = ((num >> 8) & 0x00FF) + amt;
		if (g > 255) g = 255;
		else if (g < 0) g = 0;
		
		let b = (num & 0x0000FF) + amt;
		if (b > 255) b = 255;
		else if (b < 0) b = 0;
		let rStr = ("0" + parseInt(r).toString(16)).slice(-2);
		let gStr = ("0" + parseInt(g).toString(16)).slice(-2);
		let bStr = ("0" + parseInt(b).toString(16)).slice(-2);

		return (usePound?"#":"") + rStr+gStr+bStr;
	}
	/** 将hex颜色转为 rgb 0-1 数组*/
	HEXToRGB01(hex){
		if (hex[0] == "#") {
			hex = hex.slice(1);
		}
		let num = parseInt('0x'+hex);
		let r = num >> 16;
		let g = (num >> 8) & 0x00FF;
		let b = num  & 0x0000FF;
		return [r/255,g/255,b/255];
	}
	/**处理传入参数 */
	analysis(){
		
	}

	getQueryString(name) {
		let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		let r = window.location.search.substr(1).match(reg);
		if (r != null) return unescape(r[2]);
		return null;
	}

	/** 当前字体样式结构转pixi字体样式结构 */
	stylesToPixiStyles(rawStyles){
		const nowStyles = {
			fontFamily: 'Microsoft YaHei',
			fontSize: 100,
			fontStyle: '',
			fontWeight: '',
			fill: '#000000', // gradient
			align:'center',
			stroke: '#ff0000',
			lineHeight:100,
			strokeThickness: 0,
			dropShadow: false,
			dropShadowColor: '#000000',
			dropShadowBlur: 4,
			dropShadowAlpha:1,
			dropShadowAngle: Math.PI / 6,
			dropShadowDistance: 6,
			wordWrap: false,
			wordWrapWidth: 500,
			padding:0,
			breakWords:true,
			letterSpacing:0,
			// lineJoin: 'round',
		};

		const NumberKeys = ['fontSize','strokeThickness','dropShadowBlur','dropShadowAlpha','dropShadowAngle','dropShadowDistance','lineHeight','wordWrapWidth','letterSpacing'];
		for(let info in rawStyles){
			
			if(info == 'isItalic'){
				nowStyles.fontStyle = rawStyles[info]?'italic':'';
			}else if(info == 'isBold'){
				nowStyles.fontWeight = rawStyles[info]?'bold':'';
			}else if(info == 'color'){
				nowStyles.fill = rawStyles[info];
			}else{
				if(NumberKeys.indexOf(info)>-1){
					nowStyles[info] = Number(rawStyles[info]);
				}else{
					nowStyles[info] = rawStyles[info];
				}
			}
		}
		//填充为10-100之间
		nowStyles.padding = (nowStyles.fontSize-12)/500 * 90 + 10;
		// console.log("pixi文本样式",nowStyles)
		
		return nowStyles;
	}

	throttle (func, delay) {     
		let timer = null;     
		let startTime = Date.now();     
		return function() {             
			let curTime = Date.now();             
			let remaining = delay - (curTime - startTime);             
			let context = this;             
			let args = arguments;             
			clearTimeout(timer);              
			if (remaining <= 0) {                    
				func.apply(context, args);                    
				startTime = Date.now();              
			} else {                    
				timer = setTimeout(func, remaining);              
			}      
		}
	}
}
export default Utils;