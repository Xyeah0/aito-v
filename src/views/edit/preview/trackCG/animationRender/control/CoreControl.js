/**
 * 开发者：tangshanghai
 * 日期：2017/10/10
 * 说明：主控制器
 */
import Event from '../libs/Event.js';
class CoreControl extends Event{
	constructor(){
		super();

		this.SOURCE_COMPLETE = 'source-complete';
		this.INIT_READY = 'init_ready';

		this.ON_START = 'on_start';
		this.ON_UPDATE = 'on_update';
		this.ON_COMPLETE = 'on_complete';
		this.PAUSE_STATUS = 'pause_status';

		this.CONFIG_ACTIVE_CHANGE = 'config_active_change';
		this.TEXT_AREA_CHANGE = 'text_area_change';
	}

	/**
	 * 发出界面渲染完成事件
	 */
	sourceComplete(obj){
		this.dispatchEvent(this.SOURCE_COMPLETE,obj);
	}

	/**
	 * 发出界面渲染完成事件
	 */
	initReady(obj){
		this.dispatchEvent(this.INIT_READY,obj);
	}

	/**
	 * 发出开始播放事件
	 */
	onStart(obj){
		this.dispatchEvent(this.ON_START,obj);
	}

	/**
	 * 发出开始播放事件
	 */
	onUpdate(obj){
		this.dispatchEvent(this.ON_UPDATE,obj);
	}

	/**
	 * 发出完成播放事件
	 */
	onComplete(obj){
		this.dispatchEvent(this.ON_COMPLETE,obj);
	}

	/**
	 * 发出暂停状态事件
	 */
	onPauseStatus(obj){
		this.dispatchEvent(this.PAUSE_STATUS,obj);
	}

	/**
	 * 发出文字区域大小改变事件
	 */
	onTextAreaChange(obj){
		this.dispatchEvent(this.TEXT_AREA_CHANGE,obj);
	}

	/**
	 * 发出配置主动改变事件
	 */
	onConfigActiveChange(obj){
		this.dispatchEvent(this.CONFIG_ACTIVE_CHANGE,obj);
	}
 }
 export default CoreControl;