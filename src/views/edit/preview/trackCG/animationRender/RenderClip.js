/**
 * Created by tangshanghai on 2022/05/18.
 */
// import "./less/index.less";
import Event from './libs/Event.js'
import * as PIXI from 'pixi.js';

class RenderClip extends Event{
	constructor(_AnimationRender,data){
		super();
		this.AnimationRender = _AnimationRender;
		this.root = new PIXI.Container();

		this.trackIn = 0;

		this.id = data.id;
		this.jsUrl = data.jsUrl;
		this.className = data.className;
		this.trackIn = data.trackIn;
		this.trackOut = data.trackOut;
		this.animationConfig = data.animationConfig;

		if(window.AnimationTemplate && typeof window.AnimationTemplate[this.className] == 'function'){
			this.animationApp = new window.AnimationTemplate[this.className];
			this.initSource();
		}else{
			const LoadTemplateCache = this.AnimationRender.LoadTemplateCache;
			const loadPromise = LoadTemplateCache.getPromise(this.jsUrl);
			loadPromise.then(res=>{
				this.animationApp = new window.AnimationTemplate[this.className];
				this.initSource();
			}).catch(err=>{

			})
		}
	}

	initSource(){
		this.root.addChild(this.animationApp.root);

		this.animationApp.initExternal(this.animationConfig,this.jsUrl);
		const screen = this.AnimationRender.screen;
		if(screen.width != this.animationApp.screen.width || screen.height != this.animationApp.screen.height){
			this.animationApp.screen.width = screen.width;
			this.animationApp.screen.height = screen.height;
		}
		if(this.animationApp.screen.needComputerScale){
			this.animationApp.screen.scale = screen.width/screen.baseWidth;
		}

		//转发
		this.animationApp.CORE.addEventListener(this.animationApp.CORE.TEXT_AREA_CHANGE,(obj)=>{
			this.AnimationRender.CORE.onTextAreaChange({
				id:this.id,
				app: this.animationApp,
				textarea:obj
			});
		})
		//转发
		this.animationApp.CORE.addEventListener(this.animationApp.CORE.CONFIG_ACTIVE_CHANGE,(obj)=>{
			this.AnimationRender.CORE.onConfigActiveChange({
				id:this.id,
				app: this.animationApp,
				cfg:obj
			});
		})
		//转发
		this.animationApp.CORE.addEventListener(this.animationApp.CORE.INIT_READY,(obj)=>{
			// console.log('字幕模板初始化完成',this.AnimationRender.CORE)
			this.AnimationRender.CORE.initReady({
				id:this.id,
				app: this.animationApp
			});

			if(this.initSeekTime){
				this.animationApp.seek(this.initSeekTime);
				this.initSeekTime = 0;
			}

			// const screen = this.AnimationRender.screen;
			// if(screen.width != this.animationApp.screen.width || screen.height != this.animationApp.screen.height){
			// 	this.animationApp.resizePreset(screen.width,screen.height);
			// }
		})

		this.ready = true;
	}

	resizePreset(w,h){
		if(this.animationApp){
			this.animationApp.resizePreset(w,h);
		}
	}

	seek(v=0){
		if(this.animationApp){
			this.animationApp.seek(v);
		}else{
			this.initSeekTime = v;
		}
	}

	/** 释放 */
	destroy(){
		if(this.root.parent){
			this.root.parent.removeChild(this.root);
		}
	}
}
export default RenderClip;