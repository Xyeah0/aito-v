/**
 * Created by tangshanghai on 2022/05/18.
 */
// import "./less/index.less";
import Event from './libs/Event.js'
import * as PIXI from 'pixi.js';
import RenderClip from './RenderClip.js';

class TrackPlayer extends Event{
	constructor(_AnimationRender){
		super();
		this.callBacks = [];
		this.AnimationRender = _AnimationRender;
		this.root = new PIXI.Container();


		this.currentTime = 0;
		this.duration = 0;
		this.isPause = true;
		this.timer = null;

		this.clips = [];
	}

	/** 添加一个字幕模板
	 * 一个字幕模板的实例
	 */
	addTemplate(obj){
		let clip = new RenderClip(this.AnimationRender,obj);
		this.root.addChild(clip.root);
		this.clips.push(clip);

		this.updateDuration();
	}

	/** 更新某一个时长的位置 */
	updateTemplatePos(id,trackIn,trackOut){
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			if(clip.id == id){
				clip.trackIn = trackIn;
				clip.trackOut = trackOut;
				break;
			}
		}

		this.updateDuration();
	}

	/** 更新某一个模板层级 */
	updateTemplateZindex(id,zIndex=0){
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			if(clip.id == id){
				this.root.addChildAt(clip.root,zIndex);
				break;
			}
		}
		this.seek(this.getCurrentTime());
	}

	/** 删除模板对象 */
	removeTemplate(obj){
		let id = obj.id || '';
		let findClip = null;
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			if(clip.id == id){
				findClip = clip;
				this.clips.splice(i,1);
				break;
			}
		}
		findClip.destroy();

		this.updateDuration();
	}

	/** 重置所有模板高宽 */
	resizePreset(width,height){
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			clip.resizePreset(width,height);
		}
	}

	clearAll(){
		this.clips = [];
		this.root.removeChildren();
	}


	/** 跳转时间 */
	seek(time){
		// v = v/1000;
		// if(v == this.timeLine.duration()){
		// 	v -= 0.01*Math.random();
		// }
		this.currentTime = time;
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			if(time >= clip.trackIn && time < clip.trackOut){
				clip.root.visible = true;
				let v = (time-clip.trackIn);
				clip.seek(v);
			}else{
				clip.root.visible = false;
			}
		}
		// console.log('MainSprite seek',time)
		this.dispatchEvent('on_update',{currentTime:time});
		// if(!this.isPause){
		// 	this.pause();
		// }
	}

	play(){
		if(this.currentTime >= this.duration){
			this.seek(0);
		}
		this.timer = setInterval(this.enterframeHandler.bind(this),40);
		this.isPause = false;
		this.dispatchEvent('pause_status',{isPause:this.isPause});
	}

	enterframeHandler(){
		this.seek(this.currentTime + 40);
		// this.updateChild(this.currentTime + 40);
		if(this.currentTime >= this.duration){
			this.pause();
			this.dispatchEvent('on_complete',{currentTime:this.currentTime});
		}
	}

	pause(){
		clearInterval(this.timer);
		this.isPause = true;
		this.dispatchEvent('pause_status',{isPause:this.isPause});
	}


	updateDuration(){
		let dur = 0;
		for(let i=0;i<this.clips.length;i++){
			let clip = this.clips[i];
			dur = Math.max(clip.trackOut,dur);
		}
		this.duration = dur;
	}
}
export default TrackPlayer;