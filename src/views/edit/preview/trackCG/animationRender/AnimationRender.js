/**
 * Created by tangshanghai on 2022/05/18.
 */
import * as PIXI from 'pixi.js';
import TrackPlayer from './TrackPlayer.js';
import CoreControl from './control/CoreControl.js';
import LoadTemplateCache from './LoadTemplateCache.js';
class AnimationRender{
	constructor(){
		const version = '2.0.0';
		console.log("\n %c AnimationTemplatesTemplate %c v" + version + " \n", "color: #fadfa3; background: #18A689; font-width: 600; font-size: 20px; padding:5px 0;", "font-size: 16px;background: #fadfa3; padding:5px 0;color:red;");

		this.PIXI = PIXI;
		PIXI.utils.skipHello();
		PIXI.TextMetrics.BASELINE_SYMBOL = "字";

		this.screen = {
		    width: 1920,
			height: 1080,
			baseWidth: 1920,//基于 1920*1080 制作的模板
			baseHeight: 1080,
			scale:1,
			needComputerScale:false,
		}
		
		this.LoadTemplateCache = LoadTemplateCache;
		
		//事件控制器
		this.CORE = new CoreControl();
		//主轨道播放器
		this.trackPlayer = new TrackPlayer(this);

		//主显示对象
		this.app = new PIXI.Application({
			width: this.screen.width, 
			height: this.screen.height,
			antialias: true,
			backgroundAlpha:0,
			// preserveDrawingBuffer:true,
		});
		// console.log(this.app)
		// document.body.appendChild(this.app.view);
		this.app.view.style.width = '100%';
		this.app.view.style.height = '100%';
		this.app.stage.addChild(this.trackPlayer.root);

		this.view = this.app.view;
		
		//共享对象
		this.shotUin8Array = new Uint8Array(this.screen.width*this.screen.height*4);;

		this.trackPlayer.addEventListener('on_start',(obj)=>{
			this.CORE.onStart(obj);
		})

		this.trackPlayer.addEventListener('on_update',(obj)=>{
			this.CORE.onUpdate(obj);
		})

		this.trackPlayer.addEventListener('on_complete',(obj)=>{
			this.CORE.onComplete(obj);
		})

		this.trackPlayer.addEventListener('pause_status',(obj)=>{
			this.CORE.onPauseStatus(obj);
		})
	}

	/** 获得模板预设 */
	getPreset () {
		return {
			width: this.screen.width ,
			height: this.screen.height
		}
	}
	/** 获得当前播放头 */
	getCurrentTime () {
		return this.trackPlayer.currentTime;
	} 
	/** 获得模板时长 */
	getDuration () {
		return this.trackPlayer.duration;
	}
	
	/** seek */
	seek (v=0) {
		// 0.01*Math.random()
		
		this.trackPlayer.seek(v);
	}
	/**播放 */
	play(){
		this.trackPlayer.play();
	}

	/**暂停 */
	pause(){
		this.trackPlayer.pause();
	}

	/**获得暂停状态 */
	get paused(){
		return this.trackPlayer.isPause;
	}

	/**
	 * 重置画面高宽
	 */
	resizePreset(w,h){
		this.screen.width = w || this.screen.width;
		this.screen.height = h || this.screen.height;
		this.app.renderer.resize(this.screen.width,this.screen.height);

		this.shotUin8Array = new Uint8Array(this.screen.width*this.screen.height*4);

		if(this.screen.needComputerScale){
			this.screen.scale = this.screen.width/this.screen.baseWidth;
		}

		this.trackPlayer.resizePreset(w,h);
		
		this.seek(this.getCurrentTime());
	}


	/** 添加模板对象 */
	addTemplate(obj){
		this.trackPlayer.addTemplate(obj);
	}

	/** 更新某一个时长的位置 */
	updateTemplatePos(id,trackIn,trackOut){
		this.trackPlayer.updateTemplatePos(id,trackIn,trackOut);
		this.seek(this.getCurrentTime());
	}

	/** 更新某一个模板层级 */
	updateTemplateZindex(id,zIndex=0){
		this.trackPlayer.updateTemplateZindex(id,zIndex);
		this.seek(this.getCurrentTime());
	}

	/** 删除模板对象 */
	removeTemplate(obj){
		this.trackPlayer.removeTemplate(obj);
		this.seek(this.getCurrentTime());
	}

	/** 清空所有 */
	clearAll(){
		this.trackPlayer.clearAll();
	}

	/** 获取某一帧画面 供CEF调用 */
	getFrameArrayBuffer(v,isFlipY = false){
		this.seek(v);
		this.app.render();
		const gl = this.app.renderer.gl;
		const width = this.screen.width;
		const height = this.screen.height;
		if(isFlipY){
			const length = width * height * 4;
			const pixels = new Uint8Array(length);
			gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);

			for (var i = 0; i < pixels.length; i += 4) {
				var alpha = pixels[i + 3];
				if (alpha !== 0) {
					pixels[i] = Math.round(Math.min(pixels[i] * 255.0 / alpha, 255.0));
					pixels[i + 1] = Math.round(Math.min(pixels[i + 1] * 255.0 / alpha, 255.0));
					pixels[i + 2] = Math.round(Math.min(pixels[i + 2] * 255.0 / alpha, 255.0));
				}
			}

			const row = width * 4;
			const end = (height - 1) * row;
			for (let i = 0; i < length; i += row) {
				this.shotUin8Array.set(pixels.subarray(i, i + row), end - i);
			}
		}else{
			gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, this.shotUin8Array);
		}
		return this.shotUin8Array;
	}
	
}
export default AnimationRender;