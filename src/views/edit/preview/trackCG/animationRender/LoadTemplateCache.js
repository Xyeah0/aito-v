class LoadTemplateCache{
	constructor(){
		this.cache = {};
	}

	getPromise(url){
		if(!this.cache[url]){
			this.cache[url] = this.loadTemplate(url);
		}
		return this.cache[url];
	}

	loadTemplate(url){
		return new Promise((resolve,reject)=>{
			let head = document.head;
			let script = document.createElement('script');
			script.type = 'text/javascript';
			// script.charset = 'GBK';
			script.onload = script.onreadystatechange = function() {
				if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete" ) {
					script.onload = script.onreadystatechange = null;
					resolve();
				}
			};
			script.onerror = function(){
				reject();
			}
			script.src= url;
			head.appendChild(script);
		})
	}
}
export default new LoadTemplateCache();