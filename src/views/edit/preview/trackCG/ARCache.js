/**
 * 默认一个轨道一个渲染器，以后可以整个工具一个渲染器
 */
import AnimationRender from './animationRender/AnimationRender.js' 
class ARCache{
	constructor(){
		this.cacheAR = [];
	}

	/** 根据层级获取一个渲染器*/
	getRenderOfIndex(v=0){
		if(!this.cacheAR[v]){
			this.cacheAR[v] = new AnimationRender();
		}
		return this.cacheAR[v];
	}
}
export default new ARCache;