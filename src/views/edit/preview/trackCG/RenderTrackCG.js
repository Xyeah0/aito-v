import App from 'AppCore';
import DataType from '@/model/DataType';
const CORECONTROL = App.CONTROL.CORE;
import BaseRenderTrack from '../BaseRenderTrack.js';
import TextRender from './renderCG/TextRender';
import ImageRender from './renderCG/ImageRender';
import TgaRender from './renderCG/TgaRender.js';
import GIFRender from './renderCG/GIFRender.js';
import WebpRender from './renderCG/WebpRender.js';
import TextDynamicRender from "./renderCG/textDynamic/TextDynamicRender";
import TextDynamicRenderLibretto from "./renderCG/textDynamic/TextDynamicRenderLibretto";
import ARCache from "./ARCache";
import Utils from '../../../../libs/Utils.js';
/**
 * CG预览轨道
 */

class RenderTrackCG extends BaseRenderTrack{
	constructor (data) {
		super(data);

		this.pixiRender = ARCache.getRenderOfIndex(data.trackIndex);
        this.root.appendChild(this.pixiRender.view);
        this.pixiRender.view.style.pointerEvents = 'none';
        this.pixiRender.view.classList.add('pixi-render');
        this.pixiRender.view.setAttribute('trackIndex',data.trackIndex);
        const preset = App.activeSequence.preset;
        let WHObj = Utils.getWHof1080(preset.width,preset.height);
        this.pixiRender.resizePreset(WHObj.width,WHObj.height);
        // console.log('this.pixiRender',this.pixiRender)

        CORECONTROL.$on(CORECONTROL.CURRENTTIME_CHANGE,this.playHeadChangeHandler);
		CORECONTROL.$on(CORECONTROL.CLIP_ADD,this.clipAddHandler);
	}

	/** 添加片断 */
	clipAddHandler = (clip) =>{
		if(clip.trackId === this.data.modelId){
			if(clip.dataType === DataType.CLIP_CG_TEXT){
                // this.judgeWH(data);
                let r = new TextRender(clip);
                this.root.append(r.root);
            }else if(clip.dataType === DataType.CLIP_CG_TEXT_DYNAMIC){
                let r = new TextDynamicRender(clip,this.pixiRender);
                this.root.append(r.root);
            }
            else if(clip.dataType === DataType.CLIP_CG_IMAGE){
                let r = new ImageRender(clip);
                this.root.append(r.root);
            }else if(clip.dataType === DataType.CLIP_CG_TGA){
                let r = new TgaRender(clip);
                this.root.append(r.root);
            }else if(clip.dataType === DataType.CLIP_CG_IMAGE_GIF){
                let r = new GIFRender(clip);
                this.root.append(r.root);
            }else if(clip.dataType === DataType.CLIP_CG_IMAGE_WEBP){
                let r = new WebpRender(clip);
                this.root.append(r.root);
            }
		}
	}

	destroy(){
        super.destroy();

        CORECONTROL.$off(CORECONTROL.CURRENTTIME_CHANGE,this.playHeadChangeHandler);
        CORECONTROL.$off(CORECONTROL.CLIP_ADD,this.clipAddHandler);
    }

    //播放头外部改变
    playHeadChangeHandler = (obj)=> {
        let time = App.activeSequence.currentTime;
        this.pixiRender.seek(time);
    }
}

export default RenderTrackCG;