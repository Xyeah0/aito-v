const demoProxy = {
    '/appsocket': {
        target: 'https://svip-mtool.demo.chinamcloud.cn/svip',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/appsocket': ''
        }
    },
    '/svip': {
        target: 'https://tool-test.demo.chinamcloud.cn/yumi-svip/svip',
        // target: 'https://svip-mtool.demo.chinamcloud.cn/svip',
        // target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        // ws:true,
        changeOrigin: true,
        pathRewrite: {
            '^/svip': ''
        }
    },
    '/mediaCut': {
        target: 'https://acut-mtool.demo.chinamcloud.cn/mediaCut',
        changeOrigin: true,
        pathRewrite: {
            '^/mediaCut': ''
        }
    },
    '/aigc': {
        target: 'https://agicapi-mtool.demo.chinamcloud.cn/aigc',
        changeOrigin: true,
        pathRewrite: {
            '^/aigc': ''
        }
    },
    '/tlSource': {
        target: 'https://yumi-nginx.demo.chinamcloud.cn/',
        changeOrigin: true,
    },
    '/spider-upload': {
        target: 'https://console.demo.chinamcloud.cn/',
        changeOrigin: true,
        pathRewrite: {
            '^/spider-upload': ''
        },
    },
    '/spider-material': {
        // target: 'https://cloudtoolsplatform-mtool.demo.chinamcloud.cn/spider-material/',
        // target: 'https://spidercrms-mtool.demo.chinamcloud.cn/spidercrms/',
        target: 'https://console.demo.chinamcloud.cn/spidercrms',
        // target: 'https://tool-test.demo.chinamcloud.cn/yumi-aitovideo/spider-material',
        changeOrigin: true,
        pathRewrite: {
            '^/spider-material': ''
        },
    },
}

const chengduProxy = {
    '/nodeconvert': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-nodeconvert/nodeconvert',
        changeOrigin: true,
        pathRewrite: {
            '^/nodeconvert': ''
        }
    },
    '/appsocket': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/appsocket': ''
        }
    },
    '/aigcsocket': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/aigcsocket': ''
        }
    },
    '/svip': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        // ws:true,
        changeOrigin: true,
        pathRewrite: {
            '^/svip': ''
        }
    },
    '/mediaCut': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        changeOrigin: true,
        pathRewrite: {
            '^/mediaCut': ''
        }
    },
    '/aigc-pic': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-aigc/aigc-pic',
        // target: 'http://172.29.3.57:9092/aigc',
        changeOrigin: true,
        pathRewrite: {
            '^/aigc-pic': ''
        }
    },

    '/aigc': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-aigc/aigc',
        // target: 'http://172.29.3.57:9092/aigc',
        changeOrigin: true,
        pathRewrite: {
            '^/aigc': ''
        }
    },
    '/tlSource': {
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-nginx',
        changeOrigin: true,
    },
    '/spider-upload': {
        target: 'https://console.mtooltest.chinamcloud.cn/',
        changeOrigin: true,
        pathRewrite: {
            '^/spider-upload': ''
        },
    },
    '/spider-material': {
        // target: 'https://cloudtoolsplatform-mtool.demo.chinamcloud.cn/spider-material/',
        target: 'https://console.mtooltest.chinamcloud.cn/spidercrms/',
        changeOrigin: true,
        pathRewrite: {
            '^/spider-material': ''
        },
    },
}

const localProxy = {
    '/nodeconvert': {
        target: 'https://mcore.mty.chinamcloud.com/yumi-nodeconvert/nodeconvert',
        changeOrigin: true,
        pathRewrite: {
            '^/nodeconvert': ''
        }
    },
    '/appsocket': {
        target: 'https://mcore.mty.chinamcloud.com/svip',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/appsocket': ''
        }
    },
    '/aigcsocket': {
        target: 'https://mcore.mty.chinamcloud.com/aigc',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/aigcsocket': ''
        }
    },
    '/svip': {
        target: 'https://mcore.mty.chinamcloud.com/svip',
        // ws:true,
        changeOrigin: true,
        pathRewrite: {
            '^/svip': ''
        }
    },
    '/mediaCut': {
        target: 'https://mcore.mty.chinamcloud.com/mediaCut',
        changeOrigin: true,
        pathRewrite: {
            '^/mediaCut': ''
        }
    },
    '/aigc-pic': {
        target: 'https://mcore.mty.chinamcloud.com/aigc-pic',
        // target: 'http://172.29.3.57:9092/aigc',
        changeOrigin: true,
        pathRewrite: {
            '^/aigc-pic': ''
        }
    },

    '/aigc': {
        target: 'https://mcore.mty.chinamcloud.com/aigc',
        // target: 'http://172.29.3.57:9092/aigc',
        changeOrigin: true,
        pathRewrite: {
            '^/aigc': ''
        }
    },
    '/tlSource': {
        target: 'https://mcore.mty.chinamcloud.com/',
        changeOrigin: true,
    },
    '/public_proxy': {
        target: 'http://localhost:8000',
        changeOrigin: true,
        onProxyReq: (proxyReq, req, res) => {
            proxyReq.removeHeader('referer')  //移除请求头
            proxyReq.removeHeader('origin') //移除请求头
            // proxyReq.setHeader('origin','www.abc.com') //添加请求头
            // proxyReq.setHeader('host','www.abc.com') //添加请求头
        },
        router: (req, res, proxyOptions) => {
            console.log('req', req.headers, req.headers.host);
            req.headers.host = 'baijiahao.baidu.com';
            // req.headers.setHeader('host','')
            return req.query.url;
        }
    }
}

// location /svip{
//     proxy_pass  http://svip.xcttest.chinamcloud.cn/svip;
// }

const path = require('path');
function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    publicPath: './',
    lintOnSave: false,
    productionSourceMap: false,
    devServer: {
        // host: 'default.proframe.com',
        // https: true,
        disableHostCheck: true,
        port: 8020,
        proxy: demoProxy,//demoProxy,//  //localProxy, //chengduProxy,
    },
    css: {
        loaderOptions: {
            less: {
                javascriptEnabled: true
            }
        }
    },
    chainWebpack: config => {
        config.module
            .rule('txt')
            .test(/\.txt$/)
            .use('text-loader')
            .loader('text-loader')
            .end();

        config.module
            .rule('c')
            .test(/\.c$/)
            .use('amcut-c-loader')
            .loader('amcut-c-loader')
            .end();
        // config.resolve.alias.set('@', resolve('src'))
        // console.log(config)
        config.resolve.alias.set('AppCore', resolve('src/model/App.js'));
        config.plugin('html')
            .tap(args => {
                args[0].title = "依案成片";
                return args;
            })
    },
}
