window.AppConfig = {
    modulePath: "",
    themeColor: '18A689',
    appType: 'aigc-creative', // aigc-creative 文案创意 aigc-document 文稿
    containerBg: '1',
    helpUrl: 'https://www.yuque.com/loveone/cztdiq/gqgfrb',//帮助文档地址
    canUpload: true,
    uploadLimit: 100,//M 文件大小
    uploadSplit: 10,//分片文件大小
    favicon: '',
    microEditUrl: "",//微编地址
    showProofreadBtn: true,//是否显示文本校对按钮
    showExamineBtn: true,//是否显示审核按钮
    showHighlightBtn: true,//是否显示高亮按钮
    showExpandBtn: true,//是否显示扩写按钮
    showAudioDubBtn: true,//是否显示语音合成按钮D
    autoSaveTime: 60000,//自动保存时间
    projectName: '',//项目名称
    AiToVideoSVIP: 'https://mcore.mty.chinamcloud.com/svip',//socket地址1
    AiToVideoAIGC: 'https://mcore.mty.chinamcloud.com',//socket地址2
    // AiToVideoSVIP:'https://svip-mtool.mtooltest.chinamcloud.cn/svip',//chengdu socket地址1
    // AiToVideoAIGC:'https://aigc-mtool.mtooltest.chinamcloud.cn',// chengdu socket地址2
}
